---
title: Donation received - Thank you!
hidden: true
---

Thank you very much for your donation to the Year End 2014 fundraiser!

In case your donation qualifies for a greeting card gift we will contact you mid-November to ask for the design you want and address you want to send them to.

Remember you can become a "KDE Supporting Member" by doing recurring donations. Learn more at https://kde.org/community/donations/.

You can see your donation on <a href="..">the Year End 2014 fundraiser page</a>.
