---
title: Support Good People
layout: yearend2024
scssFiles:
- /scss/yearend2024.scss
jsFiles:
- /js/yearend2024.js
draft: false
---

<a name="top"></a>Good people in the world of tech populate the non-profit communities run by altruistic volunteers with no commercial interests, and therefore no interest in exploiting you, your work, or your data.

Good people defend your autonomy and give you the means to control your devices and your digital life.

Good people actively oppose those who would strip you of your privacy for monetary or ideological gains.

Good people undermine the power of monopolistic multinational companies by transparently creating free and open products offering safe and ethical solutions beyond the closed and exploitative alternatives offered by corporations bent on market dominance.

Good people embrace and encourage diversity via communities that are open and accepting.

Our fundraiser this year encourages you to Support the Good People of Tech, like the people working at [GNOME](https://www.gnome.org/donate/), the [Open Document Foundation](https://www.libreoffice.org/donate/), [FSFE](https://my.fsfe.org/donate), and so many more. Maybe also us here in KDE!

Help us keep doing good work by…

* …making a one-time donation using the donation box on this page!
* …setting up regular periodic donations (also possible via the donation box) or even becoming a [Supporting Member](https://kde.org/fundraisers/plasma6member/)!
* …adopting an App!

## <a name="adopt"></a>…Adopt an App!

Adopt one of KDE's apps and we can share with the whole world how awesome you are and how much you're doing to support us. Adopting a KDE app is easy:

1. [Make a donation](#top) of €50 or more to our fundraiser.
2. Choose an app you'd like to adopt from the list below. Each app can be adopted by up to 5 adopters. We will add more apps to the list as the adoption slots fill up.
3. During the donation process, use the comment section to write which app you want to adopt and the name you'd like to be known as. This can be your name, nickname, or your social media handle<sup>[*](#anot01)</sup><sup>[†](#anot02)</sup>…
4. Your name will appear on the app's [apps.kde.org](https://apps.kde.org) as a _Supporter_ from the moment of the donation until KDE Gear 25.04 is released in April of next year. We will also give you a shout-out on social media.

### Adoptable Apps

|App|Adopters|
|---|---|
|[Kubrick](https://apps.kde.org/kubrick)|🟦⬜⬜⬜⬜|
|[KGoldRunner](https://apps.kde.org/kgoldrunner)|🟦⬜⬜⬜⬜|
|[Picmi](https://apps.kde.org/picmi)|⬜⬜⬜⬜⬜|
|[KiGo](https://apps.kde.org/kigo)|🟦⬜⬜⬜⬜|
|[Knights](https://apps.kde.org/knights)|⬜⬜⬜⬜⬜|
|[KNetWalk](https://apps.kde.org/knetwalk)|⬜⬜⬜⬜⬜|
|[KBreakOut](https://apps.kde.org/kbreakout)|⬜⬜⬜⬜⬜|
|[KTuberling](https://apps.kde.org/ktuberling)|🟦⬜⬜⬜⬜|
|[Alligator](https://apps.kde.org/alligator)|🟦🟦🟦🟦⬜|
|[ISOImageWriter](https://apps.kde.org/isoimagewriter)|🟦🟦⬜⬜⬜|
|[SkanPage](https://apps.kde.org/skanpage)|🟦🟦⬜⬜⬜|
|[Arianna](https://apps.kde.org/arianna/)|🟦🟦🟦⬜⬜|
|[AudioTube](https://apps.kde.org/audiotube/)|🟦⬜⬜⬜⬜|
|[KRuler](https://apps.kde.org/kruler/)|🟦🟦⬜⬜⬜|
|[Ruqola](https://apps.kde.org/ruqola/)|🟦⬜⬜⬜⬜|
|[KolourPaint](https://apps.kde.org/kolourpaint/)|🟦🟦🟦⬜⬜|
|[KRFB](https://apps.kde.org/krfb/)|🟦🟦⬜⬜⬜|

### Already Adopted Apps

|App|Adopters|
|---|---|
|~~[LabPlot](https://apps.kde.org/labplot/)~~|🟦🟦🟦🟦🟦|
|~~[Elisa](https://apps.kde.org/elisa/)~~|🟦🟦🟦🟦🟦|
|~~[Kasts](https://apps.kde.org/kasts/)~~|🟦🟦🟦🟦🟦|
|~~[System Monitor](https://apps.kde.org/plasma-systemmonitor/)~~|🟦🟦🟦🟦🟦|
|~~[PartitionManager](https://apps.kde.org/partitionmanager)~~|🟦🟦🟦🟦🟦|
|~~[Konqueror](https://apps.kde.org/konqueror/)~~|🟦🟦🟦🟦🟦|
|~~[GCompris](https://apps.kde.org/gcompris/)~~|🟦🟦🟦🟦🟦|
|~~[Discover](https://apps.kde.org/discover/)~~|🟦🟦🟦🟦🟦|
|~~[KRDC](https://apps.kde.org/krdc/)~~|🟦🟦🟦🟦🟦|
|~~[KWrite](https://apps.kde.org/kwrite/)~~|🟦🟦🟦🟦🟦|
|~~[Gwenview](https://apps.kde.org/gwenview/)~~|🟦🟦🟦🟦🟦|
|~~[Tokodon](https://apps.kde.org/tokodon/)~~|🟦🟦🟦🟦🟦|
|~~[Ark](https://apps.kde.org/ark/)~~|🟦🟦🟦🟦🟦|
|~~[NeoChat](https://apps.kde.org/neochat/)~~|🟦🟦🟦🟦🟦|
|~~[Merkuro](https://apps.kde.org/merkuro/)~~|🟦🟦🟦🟦🟦|
|~~[Itinerary](https://apps.kde.org/itinerary/)~~|🟦🟦🟦🟦🟦|
|~~[KDE Connect](https://apps.kde.org/kdeconnect/)~~|🟦🟦🟦🟦🟦|
|~~[Dolphin](https://apps.kde.org/dolphin/)~~|🟦🟦🟦🟦🟦|
|~~[Kate](https://apps.kde.org/kate/)~~|🟦🟦🟦🟦🟦|
|~~[Okular](https://apps.kde.org/okular/)~~|🟦🟦🟦🟦🟦|
|~~[Konsole](https://apps.kde.org/konsole/)~~|🟦🟦🟦🟦🟦|
|~~[Filelight](https://apps.kde.org/filelight/)~~|🟦🟦🟦🟦🟦|
|~~[Spectacle](https://apps.kde.org/spectacle/)~~|🟦🟦🟦🟦🟦|
|~~[Kontact](https://apps.kde.org/kontact/)~~|🟦🟦🟦🟦🟦|

---
<p class="annotation"><a name="anot01"></a><sup>*</sup> We reserve the right to refuse applicants with offensive names or nicknames, or whose social media accounts contain objectionable material. See KDE's <a href="https://kde.org/code-of-conduct/">Code of Conduct</a>.</p>

<p class="annotation"><a name="anot02"></a><sup>†</sup> This reward is open to individuals only. If you represent a company that would like to support us, we would be happy to <a href="https://ev.kde.org/getinvolved/supporting-members/">welcome you as a patron</a>.</p>

## <span class="white">Donated (updated daily)</span>
<div>
{{< yearend2024/progress >}}
</div>

<!-- <p class="annotation"><sup>*</sup>More stretch goals coming soon.</p>-->

## Why donate

KDE is funded mainly by you: our friends, users, and supporters. Thanks to your donations, we can deliver the best free and open software that respects your privacy and gives you control over your devices and digital life. Your donations also help us keep tech corporations in check.

By getting most of our support from people like you, we can preserve our independence and focus on developing what you need. We will never be pressured by any company to include the anti-features used in proprietary software that abuse your trust to maximize owners' profits.

And that's how we like it. We really appreciate your support. Today, we want to show the world how you help keep Free Software projects alive and ethical!


