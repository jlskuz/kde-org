<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdeaccessibility-4.2.0.tar.bz2">kdeaccessibility-4.2.0</a></td><td align="right">6,4MB</td><td><tt>f32f24c4f07906b7af39ca18d47b5e27</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdeadmin-4.2.0.tar.bz2">kdeadmin-4.2.0</a></td><td align="right">1,9MB</td><td><tt>2c5b33477b5679bcd9fdbc1f8017e6fb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdeartwork-4.2.0.tar.bz2">kdeartwork-4.2.0</a></td><td align="right">22MB</td><td><tt>d81623b60c7deb314bc2e28a52254ac2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdebase-4.2.0.tar.bz2">kdebase-4.2.0</a></td><td align="right">4,1MB</td><td><tt>da86a8ad624e86eda3a7509f39272060</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdebase-runtime-4.2.0.tar.bz2">kdebase-runtime-4.2.0</a></td><td align="right">67MB</td><td><tt>8ef48aae16a6dddb3055d81d7e5c375f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdebase-workspace-4.2.0.tar.bz2">kdebase-workspace-4.2.0</a></td><td align="right">49MB</td><td><tt>193e30b9ed0b55b0196289d9df43a904</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdebindings-4.2.0.tar.bz2">kdebindings-4.2.0</a></td><td align="right">4,5MB</td><td><tt>6eae8fd968da83fe65e592993e416adc</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdeedu-4.2.0.tar.bz2">kdeedu-4.2.0</a></td><td align="right">57MB</td><td><tt>aaddbdab29e1d284ad8ee67a78b4c597</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdegames-4.2.0.tar.bz2">kdegames-4.2.0</a></td><td align="right">37MB</td><td><tt>68cefd627025be99ba136e5a4e35e554</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdegraphics-4.2.0.tar.bz2">kdegraphics-4.2.0</a></td><td align="right">3,5MB</td><td><tt>8beb6fe5d475d0b0245ea6d4cc7e9732</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdelibs-4.2.0.tar.bz2">kdelibs-4.2.0</a></td><td align="right">9,6MB</td><td><tt>2d830a922195fefe6e073111850247ac</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdemultimedia-4.2.0.tar.bz2">kdemultimedia-4.2.0</a></td><td align="right">1,5MB</td><td><tt>3e944c87888ac1ac5b11d3722dd31f88</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdenetwork-4.2.0.tar.bz2">kdenetwork-4.2.0</a></td><td align="right">7,2MB</td><td><tt>0ea1628e11d398fdf45276a35edd3cae</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdepim-4.2.0.tar.bz2">kdepim-4.2.0</a></td><td align="right">13MB</td><td><tt>a80631de21930b2544c86722138aaa6c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdepimlibs-4.2.0.tar.bz2">kdepimlibs-4.2.0</a></td><td align="right">1,6MB</td><td><tt>8a91677e2dca7d4db26b33c78e239e5e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdeplasma-addons-4.2.0.tar.bz2">kdeplasma-addons-4.2.0</a></td><td align="right">4,1MB</td><td><tt>d98f805f4c9b7af7278067f9e544b2ec</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdesdk-4.2.0.tar.bz2">kdesdk-4.2.0</a></td><td align="right">5,2MB</td><td><tt>79d01b4f10f1ecc283f7860d2c7973e9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdetoys-4.2.0.tar.bz2">kdetoys-4.2.0</a></td><td align="right">1,3MB</td><td><tt>3adf538297e5dca51f15186b4acd02ce</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdeutils-4.2.0.tar.bz2">kdeutils-4.2.0</a></td><td align="right">2,2MB</td><td><tt>f0ca24c7d3e5bb0ab55bf6b26fc6224e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.0/src/kdewebdev-4.2.0.tar.bz2">kdewebdev-4.2.0</a></td><td align="right">3,3MB</td><td><tt>8b60c68f6cbbe9c5bb48caa576853f9e</tt></td></tr>
</table>
