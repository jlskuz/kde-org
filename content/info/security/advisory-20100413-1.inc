<li>KDM versions before KDE 4.4.3 have a race condition that
allows valid local users to overwrite arbitrary files. 
Read the <a href="/info/security/advisory-20100413-1.txt">detailed advisory</a>
for more information.
</li>
