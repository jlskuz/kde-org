-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory: Secure Cookie Vulnerability
Original Release Date: 2002-09-08
URL: http://www.kde.org/info/security/advisory-20020908-1.txt

0. References
	None.

1. Systems affected:
	Konqueror in KDE 3.0, KDE 3.0.1 and KDE 3.0.2. 
	KDE 2.2.2 and KDE 3.0.3 are NOT affected.

2. Overview:
	Konqueror fails to detect the "secure" flag in HTTP cookies and as 
	a result may send secure cookies back to the originating site over 
	an unencrypted network connection. 
      
3. Impact:
	A secure session that relies solely on secure cookies for 
	identifying the session can possibly be hijacked, or an account 
	which relies solely on secure cookies for logging on may be 
        compromised, by an attacker who manages to eavesdrop on the 
	unencrypted network connection.

4. Solution:
	Upgrade to KDE 3.0.3 in which this problem is fixed or apply the
	patch below.

5. Patch:
        A patch for KDE 3.0, KDE 3.0.1 and KDE 3.0.2 is available from 
	ftp://ftp.kde.org/pub/kde/security_patches :

	1abff4a02381b5ca11273d02c6a5c6ca  post-3.0-kdelibs-kcookiejar.diff
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.0.7 (GNU/Linux)

iD8DBQE9fldFvsXr+iuy1UoRAkfxAJ9tqM141Dx+7b8ZHlxUcU6uJIsJ0QCg5kXu
PFXLjBmWgER6vfvpYcOiLYM=
=UT1J
-----END PGP SIGNATURE-----
