-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory: kimgio input validation errors
Original Release Date: 2005-04-21
URL: http://www.kde.org/info/security/advisory-20050421-1.txt

0. References

        http://bugs.kde.org/102328
        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2005-1046


1. Systems affected:

        kdelibs as shipped with KDE 3.2 up to including KDE 3.4.


2. Overview:

        kimgio contains a PCX image file format reader that does
        not properly perform input validation. A source code audit
        performed by the KDE security team discovered several
        vulnerabilities in the PCX and other image file format
        readers, some of them exploitable to execute arbitrary
        code.


3. Impact:

        Remotly supplied, specially crafted image files can be used
        to execute arbitrary code.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        A patch for KDE 3.4.0 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        78473d4dad612e6617eb6652eec2ab80  post-3.4.0-kdelibs-kimgio.diff

        A patch for KDE 3.3.2 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        8366d0e5c8101c315a0bdafac54536d6  post-3.3.2-kdelibs-kimgio.diff


6. Time line and credits:

        24/03/2005 Notification of KDE by Bruno Rohee
        21/04/2005 Coordinated Public Disclosure


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.0 (GNU/Linux)

iD8DBQFCaDv7vsXr+iuy1UoRAhugAJ9W2ZuWaHbOpGR7KJXdD/61NvreRACfbtQ/
qzf3tKGkvLisWtHy5CVhwuo=
=Q/FW
-----END PGP SIGNATURE-----
