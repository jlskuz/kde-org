
KDE Security Advisory: konqueror address bar spoofing
Original Release Date: 2007-08-16
URL: http://www.kde.org/info/security/advisory-20070816-1.txt

Please refer to http://www.kde.org/info/security/advisory-20070914-1.txt
instead.
