KDE Project Security Advisory
=============================

Title:          NeoChat: user IP can be leaked
Risk Rating:    Low
CVE:            CVE-2024-52868
Versions:       neochat < 24.08.2
Date:           20/11/2024

Overview
========
NeoChat is a chat application for the Matrix network.

Some specially crafted messages sent by a remote user can leak the user IP.

Solution
========
Update to NeoChat >= 24.08.2

Credits
=======
Thanks to Claire (BlankEclair) for the report and to Carl Schwan, Tobias Fella, and James Graham for the fix.
