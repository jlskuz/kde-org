<ul>

<!-- ASP LINUX -->
<li><a href="http://www.asp-linux.com/">ASP Linux</a>
<!--  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/ASPLinux/README">README</a>)-->
  : 
  <ul type="disc">
    <li>
      7.3:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/ASPLinux/7.3/i386/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>

<!-- CONECTIVA LINUX -->
<li><a href="http://www.conectiva.com/">Conectiva Linux</a>:
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/Conectiva/README">README</a>
<!--
  /
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/Conectiva/LEIAME">LEIAME</a>
--> ) :
  <ul type="disc">
    <li>
      8.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/Conectiva/conectiva/RPMS.kde/">Intel
      i386</a>
    </li>
  </ul>
  <p />
</li>

<!--  DEBIAN -->
<li><a href="http://www.debian.org/">Debian</a>:
  (<a href="http://download.kde.org/stable/3.1.1/Debian/README">README</a>) :
    <ul type="disc">
      <li>apt-get: <tt>deb http://download.kde.org/stable/3.1.1/Debian stable main</tt>
      </li>
<!--
      <li>
         Debian unstable (sid):
         <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/Debian/">Intel i386</a>
      </li>
-->
      <li>
         Debian stable (woody):
         <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/Debian/dists/stable/main/binary-i386/">Intel i386</a>,
         <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/Debian/dists/stable/main/binary-powerpc/">IBM PowerPC</a>
      </li>
    </ul>
  <p />
</li>

<!--   FREEBSD -->
<!--
  <a href="http://www.freebsd.org/">FreeBSD</a>:  
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/FreeBSD/">4.5-STABLE</a>
  <p />
-->

<!--   MAC OS X (FINK PROJECT) -->
<!--
<li>
  <a href="http://www.apple.com/macosx/">Mac OS</a> /
  <a href="http://www.opendarwin.org/">OpenDarwin</a>
  (downloads via the <a href="http://fink.sourceforge.net/">Fink</a> project):
  <ul type="disc">
    <li>
      <a href="http://fink.sourceforge.net/news/kde.php">X</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   MANDRAKE LINUX -->
<!--
<li>
  <a href="http://www.linux-mandrake.com/">Mandrake Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/Mandrake/README">README</a>): 
  <ul type="disc">
    <li>
      8.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/Mandrake/8.2/">Intel
      i586</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   REDHAT LINUX -->
<li>
  <a href="http://www.redhat.com/">Red Hat</a>
 (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/RedHat/8.0/README">README</a>):
  <ul type="disc">
    <li>
      9: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1a/RedHat/9/i386/">Intel i386</a>
    </li>
     <li>
      8.0: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1a/RedHat/8.0/i386/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution):
   <ul type="disc">
     <li>
       9.0: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/contrib/Slackware/9.0/">Intel i386</a>
     </li>
     <li>
       8.1: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/contrib/Slackware/8.1/">Intel i386</a>
     </li>
   </ul>
  <p />
</li>

<!-- SOLARIS -->
<!--
<li>
  <a href="http://www.sun.com/solaris/">SUN Solaris</a> (Unofficial contribution):
  <ul type="disc">
    <li>
      8.0: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/contrib/Solaris/8.0/x86/">Intel x86</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   SUSE LINUX -->
<li>
  <a href="http://www.suse.com/">SuSE Linux</a>
<!--
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/SuSE/README">README</a>,
   <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/SuSE/SORRY">SORRY</a>)-->:
  <ul type="disc">
    <li>
      8.1:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/SuSE/ix86/8.1/">Intel
      i586</a>
    </li>
    <li>
      8.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/SuSE/ix86/8.0/">Intel
      i386</a>
    </li>
    <li>
      7.3:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/SuSE/ix86/7.3/">Intel
      i386</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/SuSE/ppc/7.3/">IBM
      PowerPC</a>
    </li>
    <li>
     7.2:
     <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/SuSE/ix86/7.2/">Intel
      i386</a>
    </li>
    <li>
     7.1:
     <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/SuSE/ix86/7.1/">Intel
      i386</a>
    </li>
  </ul>
  <p />
</li>

<!-- TURBO LINUX -->
<li>
    <a href="http://www.turbolinux.com/">Turbolinux</a>
    (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/Turbo/Readme.txt">README</a>):
    <ul type="disc">
      <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/Turbo/noarch/">Language
        packages</a> (all versions and architectures)
      </li>
      <li>
       8.0: 
       <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/Turbo/8/i586/">Intel i586</a>
      </li>
      <li>
       7.0:
       <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/Turbo/7/i586/">Intel i586</a>
      </li>
    </ul>
  <p />
</li>

<!--   TRU64 -->
<!--
<li>
  <a href="http://www.tru64unix.compaq.com/">Tru64</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/contrib/Tru64/README.Tru64">README</a>):
  <ul type="disc">
    <li>
      4.0d, e, f and g and 5.x:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/contrib/Tru64/">Compaq Alpha</a>
    </li>
  </ul>
  <p />
</li>
-->
<!--   YELLOWDOG LINUX -->
<!--
  <a href="http://www.yellowdoglinux.com/">YellowDog</a>:
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/YellowDog">2.2</a>
-->

<!-- YOPER LINUX -->
<!--
<li>
  <a href="http://www.yoper.com/">Yoper</a>:
  <ul type="disc">
    <li>
      1.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/Yoper/">Intel i686 tgz</a>
    </li>
  </ul>
  <p />
</li>
-->

<!-- VECTOR LINUX -->
<li>
  <a href="http://www.vectorlinux.com/">VECTORLINUX</a>:
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/contrib/VectorLinux/3.0/README">README</a>):
  <ul type="disc">
    <li>
      3.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/contrib/VectorLinux/3.2/">Intel i386</a>
    </li>
    <li>
      3.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/contrib/VectorLinux/3.0/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>

</ul>
