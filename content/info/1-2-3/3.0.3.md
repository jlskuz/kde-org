---
title : "KDE 3.0.3 Info Page"
publishDate: 2002-08-18 00:01:00
unmaintained: true
---

<p>
KDE 3.0.3 was released on August 19th, 2002.
Read the <a href="/announcements/announce-3.0.3">official announcement</a>.
</p>

<p>This page will be updated to reflect changes in the status of
3.0.3 release so check back for new information.</p>

<h2>FAQ</h2>

See the <a href="https://userbase.kde.org/Asking_Questions">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed
<a href="http://konqueror.kde.org/faq/">to the Konqueror FAQ</a> and sound related
questions are answered in the 
<a href="http://www.arts-project.org/doc/handbook/faq.html">FAQ of the aRts Project</a>

<h2>Download and Installation</h2>

<u>Source code</u>

{{< readfile "/content/info/1-2-3/source-3.0.3.inc" >}}

<p>
  The translation package has been split into individual language
  packages so you can
  <a href="http://download.kde.org/stable/3.0.3/src/kde-i18n/">download</a> only the
  translations you need.
</p>

<u>Binary packages</u>

<p>
Binary packages can be found under
<a href="http://download.kde.org/stable/3.0.3/">http://download.kde.org/stable/3.0.3/</a>
or in the equivalent directory at one of the KDE
<a href="/mirrors/ftp.php">FTP mirrors</a>.
</p>
<p>
The current list of available binary packages:
</p>

{{< readfile "/content/info/1-2-3/binary-3.0.3.inc" >}}

<p>
  Several users have contributed packages for this release. You can find them
  in the <a href="http://download.kde.org/stable/3.0.3/contrib/">contrib subdir</a>
  of the KDE 3.0.3 download area.
</p>

<p>
Additional binary packages might become available in the coming weeks,
as well as updates to the current packages.
</p>

<h2>Updates</h2>

<h2>Security Issues</h2>

<p>Please report possible problems to <a href="mai&#108;t&#111;:&#x73;&#101;&#x63;u&#114;i&#00116;&#121;&#00064;k&#x64;&#101;&#046;&#00111;r&#x67;">&#x73;&#101;&#00099;ur&#105;&#116;y&#x40;k&#100;&#00101;.o&#x72;g</a>.</p>
<ul>
<li>
<p> Konqueror fails to correctly initialize the site domains for sub-(i)frames
    and may as a result allow access to forein cookies. 
</p>
<p>
It is strongly recommended to upgrade at least kdelibs to KDE 3.0.3a in which
this bug is fixed. 
</p>
<p><a href="ftp://ftp.kde.org/pub/kde/security_patches/post-3.0.3-kdelibs-khtml.diff">A patch</a>
  is also available for download to address this particular problem.
</p>
</li>
<li>
<p>Several buffer overflows have been found in code KGhostview shared from
    other postscript viewers. Read the <a href="/info/security/advisory-20021008-1.txt">detailed 
    advisory</a>. Update to KDE 3.0.4 is recommended. 
</p>
<p><a href="ftp://ftp.kde.org/pub/kde/security_patches/post-3.0.3-kdegraphics-kghostview.diff">A patch</a>
   is also available for download to address this particular problem.
</p>
</li>
<li>
<p>A path traversal exploit has been found in kpf. 
    Read the <a href="/info/security/advisory-20021008-2.txt">detailed 
    advisory</a>. Update to KDE 3.0.4 is recommended. 
</p>
<p><a href="ftp://ftp.kde.org/pub/kde/security_patches/post-3.0.3-kdenetwork-kpf.diff">A patch</a>
   is also available for download to address this particular problem.
</p>
</li>
<li>Several vulnerabilites have been found in LISa/resLISa and the rlan:// protocol, 
 including the possibility to escalate the privileges to root via a remote attack. See the 
<a href="/info/security/advisory-20021111-1.txt">detailed advisory</a> for
 an explanation and instructions for immediate workaround. 
<a href="ftp://ftp.kde.org/pub/kde/security_patches/post-3.0.4-kdenetwork-lanbrowsing.diff">
A patch</a> is available for download. <b> The use of LISa/resLISa is strongly discouraged
in any security relevant area. Never make it available outside your local, trusted network.</b> </li>
<li>the rlogin protocol implementation in KIO allows remote command execution. See the
<a href="/info/security/advisory-20021111-2.txt">detailed advisory</a> for
an explanation and instructions for immediate workaround.
<a href="ftp://ftp.kde.org/pub/kde/security_patches/post-3.0.4-kdelibs-kio-misc.diff">A patch</a>
is available for download. </li>

<li>
<p>Several shell escaping vulnerabilities have been found throughout KDE which allow a remote attacker to execute commands as the local user.
   Read the <a href="/info/security/advisory-20021220-1.txt">detailed
   advisory</a>. It is strongly recommended to update to KDE 3.0.5a.
</p>
</li>
<li>
Several problems with KDE's use of Ghostscript where discovered that allow the execution of
arbitrary commands contained in PostScript (PS) or PDF files with the privileges of the victim.
Read the <a href="/info/security/advisory-20030409-1.txt">detailed advisory</a>. 
It is strongly recommended to update to <a href="../3.0.5b">KDE 3.0.5b</a>
</li>
<li>
A HTTP authentication credentials leak via the a "Referrer" was discovered by George Staikos
in Konqueror. If the HTTP authentication credentials were part of the URL they would be possibly sent
in the referer header to a 3rd party web site.
Read the <a href="/info/security/advisory-20030729-1.txt">detailed advisory</a>. KDE 3.1.3 and newer
are not vulnerable.
</li>
</ul>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
surfacing after the release date:</p>

<ul>
<li>currently none known.</li>
</ul>

<h2>Developer Info</h2>

If you need help porting your application to KDE 3.x see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/3.5/kdelibs/KDE3PORTING.html">
porting guide</a> or subscribe to the 
<a href="http://mail.kde.org/mailman/listinfo/kde-devel">KDE Devel Mailinglist</a> 
to ask specific questions about porting your applications.

<p>There is also info on the <a
href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/index.html">architecture</a>
and the <a href="http://developer.kde.org/documentation/library/3.0-api/classref/index.html">
programming interface of KDE 3.0</a>.
</p>
