<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/3.96/src/kdelibs-3.96.0.tar.bz2">kdelibs-3.96.0</a></td><td align="right">8.6MB</td><td><tt>d73ec2c5473c99a2a0e2c685335e0180</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/3.96/src/kdepimlibs-3.96.0.tar.bz2">kdepimlibs-3.96.0</a></td><td align="right">1.7MB</td><td><tt>c7ca2bb95cf318a97b9504cdd5988621</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/3.96/src/kdebase-runtime-3.96.0.tar.bz2">kdebase-runtime-3.96.0</a></td><td align="right">53MB</td><td><tt>51f9ca43f8a6f6632f774f588208268c</tt></td></tr>
</table>
