---
title: "KDE Applications 18.11.90 Info Page"
date: 2018-11-01
announcement: /announcements/announce-applications-18.12-rc
layout: applications
signer: Christoph Feck
signing_fingerprint: F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87
binary_package: false
---
