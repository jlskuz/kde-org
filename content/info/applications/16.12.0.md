---
title: "KDE Applications 16.12.0 Info Page"
announcement: /announcements/announce-applications-16.12.0
layout: applications
signer: Albert Astals Cid
signing_fingerprint: 8692A42FB1A8B666C51053919D17D97FD8224750
---
