---
version: "5.20.0"
title: "KDE Plasma 5.20.0, Feature Release"
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: EC94D18F7F05997E
---

This is a Feature release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer. Details in the
[Plasma 5.20.0 announcement](/announcements/plasma-5.20.0).
