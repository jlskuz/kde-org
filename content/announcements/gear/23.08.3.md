---
date: 2023-11-09
appCount: 120
image: true
layout: gear
---
+ ark: Compatibility with shared-mime-info >= 2.3 ([Commit](http://commits.kde.org/ark/347c6f79e71529d89587d691bc6eca096e22bc2a), [Commit](http://commits.kde.org/ark/dac14f552fb147bf4c9f759611cdb3b544a500b9), [Commit](http://commits.kde.org/ark/785640b090b51e0067dbfc6c7944a478429204a2), [Commit](http://commits.kde.org/ark/9bcbcb056c43abef88540c54f25bc6c1a78c7c0e), [Commit](http://commits.kde.org/ark/62d94c62f2fd2052be91dfe565e35a6d43c7d381))
+ kate: No longer crash when dropping a file into the project panel ([Commit](http://commits.kde.org/kate/6b2165f2dda73fd7fed9e5586956fc9d403663cd), fixes bug [#476016](https://bugs.kde.org/476016))
+ akonadi: Don't keep huge MySQL logs from past sessions ([Commit](http://commits.kde.org/akonadi/897d583942e7acb2bce5b65b56a6c0c19260d6f9), fixes bug [#456983](https://bugs.kde.org/456983))
