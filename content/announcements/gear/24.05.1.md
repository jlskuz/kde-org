---
date: 2024-06-13
appCount: 180
layout: gear
---
+ elisa: Fix DBus service activation ([Commit](http://commits.kde.org/elisa/ec8ae492467320e6cb09b88167d220493dba9479), fixes bug [#487905](https://bugs.kde.org/487905))
+ kcalc: Handle certain expressions correctly ([Commit](http://commits.kde.org/kcalc/1e2716452a5a29a9a5bbca92b19afcda3a4f16a3), fixes bug [#487614](https://bugs.kde.org/487614))
+ kcolorpicker: Allow picking the screen color with Wayland ([Commit](http://commits.kde.org/kcolorchooser/7a03f1ccac935cb9c1a8e030a198d9267b0479f1), fixes bug [#479406](https://bugs.kde.org/479406))
