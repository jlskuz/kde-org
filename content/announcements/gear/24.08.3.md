---
date: 2024-11-07
appCount: 180
layout: gear
---
+ neochat: Adjustments to make it work with the newly released libquotient 0.9 ([Commit](http://commits.kde.org/neochat/eb802ff91f98204784d87db61a698b4038b7ef84))
+ kdevelop: MesonManager: remove test suites when a project is closing ([Commit](http://commits.kde.org/kdevelop/d9a2e3f2d355e1f8299c849032610fc78c82d132), fixes bug [#427157](https://bugs.kde.org/427157))
+ kdenlive: Fix a qml crash building timeline with Qt 6.8 ([Commit](http://commits.kde.org/kdenlive/81453e228724c43a9f786eb6811f8b95d7caa710), fixes bug [#495335](https://bugs.kde.org/495335))
