---
aliases:
- ../../fulllog_releases-21.12.3
title: KDE Gear 21.12.3 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi" title="akonadi" link="https://commits.kde.org/akonadi" >}}
+ Use exec variable. [Commit.](http://commits.kde.org/akonadi/fefe36c3df9fd647cd44aca44d126c586a50271e) 
+ Check executables exist in PATH before passing them to QProcess. [Commit.](http://commits.kde.org/akonadi/191dfe9afac9a4495d0a65f2f9cad88f2f58671d) 
{{< /details >}}
{{< details id="akonadi-contacts" title="akonadi-contacts" link="https://commits.kde.org/akonadi-contacts" >}}
+ Fix build on Windows. [Commit.](http://commits.kde.org/akonadi-contacts/9b9f901daa66a111bd6f7340ff8a130694802b79) 
+ Make sure helper apps we start are in path. [Commit.](http://commits.kde.org/akonadi-contacts/6652f2ab5d84534263f3c8f728eee7ef1033aa32) 
{{< /details >}}
{{< details id="akregator" title="akregator" link="https://commits.kde.org/akregator" >}}
+ Fix bug 450650:  URL encoded chars in feed-entry-link-href become invalid - replaced by question marks. [Commit.](http://commits.kde.org/akregator/4c061653e32af8944a78fdd1c3f3edda0b106b44) Fixes bug [#450650](https://bugs.kde.org/450650)
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Fix multivolume archive creation. [Commit.](http://commits.kde.org/ark/ac29e1853f971f91d752a9b38b4c15a6aa3df4ef) Fixes bug [#448065](https://bugs.kde.org/448065)
+ Zip: Fix setting un-initialized access time. [Commit.](http://commits.kde.org/ark/229b9ce8372014469443de93a193c584e3936509) Fixes bug [#450125](https://bugs.kde.org/450125)
+ Fix build when libzip is missing. [Commit.](http://commits.kde.org/ark/bd43fd793b944a16060f078c0a701c96b563acf2) 
+ Libzip: Implement proper cancelation, using libzip 1.6. [Commit.](http://commits.kde.org/ark/6ffd4344f7913eb3d9b3978f4b91a38f9d2db1df) See bug [#389292](https://bugs.kde.org/389292)
+ CreateJob: Clean up temp file after cancellation. [Commit.](http://commits.kde.org/ark/e1c19e2222d1ce364d7ec5b4b275ffb3aab0d234) See bug [#389292](https://bugs.kde.org/389292)
+ Libzipplugin: Prevent crash when canceling archive creation. [Commit.](http://commits.kde.org/ark/82c865cb0c0fe68688d5f100143b9f3f8e8007f0) Fixes bug [#446926](https://bugs.kde.org/446926)
{{< /details >}}
{{< details id="cantor" title="cantor" link="https://commits.kde.org/cantor" >}}
+ Build: Properly pass the parameters to cmake. [Commit.](http://commits.kde.org/cantor/82a9ae456bac4bf8727b0a1044a9859cea531380) 
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Fix rating pixmap alignment on high-dpi screens. [Commit.](http://commits.kde.org/dolphin/d031696a922e03cb43adf013b8d9b1b052ddf735) 
+ Use the CDN based endpoint rather than the legacy endpoint which hits our download redirector. [Commit.](http://commits.kde.org/dolphin/d0898592a17d14fc8d48fc5cba71f3d57f0a1856) 
+ Fix opening FTP files in their preferred app. [Commit.](http://commits.kde.org/dolphin/3b4676b3871cad5cd0e4695b211581f8db8accd0) Fixes bug [#443253](https://bugs.kde.org/443253)
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ SettingsForm: Fill width with long UI controls. [Commit.](http://commits.kde.org/elisa/7bed984da15a2fdb12b5a8dc3a4cb337b9ffb980) See bug [#449992](https://bugs.kde.org/449992)
+ ListBrowserDelegate: Remove unnecessary properties. [Commit.](http://commits.kde.org/elisa/446ddde4c484f1db33727b7686eb6039a7773f3b) Fixes bug [#449936](https://bugs.kde.org/449936)
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Do not confuse portable seperator with native ones. [Commit.](http://commits.kde.org/filelight/28c4a2852a7d9f07474ca16e3f6a769b674f106e) Fixes bug [#450863](https://bugs.kde.org/450863)
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Prevent users from "losing" the thumbnail bar. [Commit.](http://commits.kde.org/gwenview/18cf2fafab365fc6a27136f7e72320940c7d8bf7) 
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Format user visible numbers using the current locale. [Commit.](http://commits.kde.org/itinerary/752beec06b12cefe33679993cfa47590b75ae534) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Update copyright. [Commit.](http://commits.kde.org/kalarm/07a585cdca7c2ef90476d6433808d33763458199) 
+ Fix failure to create a missing calendar file after enabling a resource. [Commit.](http://commits.kde.org/kalarm/af512c99a7b801e336b7e89c24b5d96b4d67e034) 
{{< /details >}}
{{< details id="kapptemplate" title="kapptemplate" link="https://commits.kde.org/kapptemplate" >}}
+ Use the CDN based endpoint rather than the legacy endpoint. [Commit.](http://commits.kde.org/kapptemplate/14c554ba403fded7a22b1c231e02f552da344670) 
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Fix stashing not working when Kate is quit using Ctrl+Q. [Commit.](http://commits.kde.org/kate/9f87e0106a912237e586db592f4d6eae396f673d) Fixes bug [#449229](https://bugs.kde.org/449229)
{{< /details >}}
{{< details id="kbruch" title="kbruch" link="https://commits.kde.org/kbruch" >}}
+ Link explicitly to KCoreAddons. [Commit.](http://commits.kde.org/kbruch/22434d9180dad097c22aa7f99384f6a10a9395aa) 
{{< /details >}}
{{< details id="kcron" title="kcron" link="https://commits.kde.org/kcron" >}}
+ Improve temporary file handling. [Commit.](http://commits.kde.org/kcron/ef4266e3d5ea741c4d4f442a2cb12a317d7502a1) 
+ KCronHelper: Return error when things don't work out. [Commit.](http://commits.kde.org/kcron/2c04c9f665283e8480a65f4ac0accfe6a8e0539a) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix tests and resize issue. [Commit.](http://commits.kde.org/kdenlive/b5eee433173c9c9646a0a17ef48d48e797870442) 
+ [Effect Keyframes] Fix "apply current value" uses wrong position. [Commit.](http://commits.kde.org/kdenlive/dbf9e4e6807d1afc9e690b30f9a33b0c461a2df3) 
+ [Effect Keyframes] Fix "copy current value to selected" crash. [Commit.](http://commits.kde.org/kdenlive/db588a07dc22f7de3386d5ee81c5e5c1fa0d77bd) 
+ [Effect Keyframes] Fix wrong keyframes shown as selected. [Commit.](http://commits.kde.org/kdenlive/b041e67f41ea81975455fe1bfe8a5a6fa9fb445a) 
+ Fix tags corrupting audio/video icons in bin. [Commit.](http://commits.kde.org/kdenlive/44a0501814ffab8dbd70c5e20df63b8e47a81003) 
+ Fix audio thumb speed not correctly initialized (broke monitor thumbs). [Commit.](http://commits.kde.org/kdenlive/aec144dedf885ba4d8a9b70dc248851223898c10) 
+ Fix audio thumbs for clips with speed effect. [Commit.](http://commits.kde.org/kdenlive/ef8d2637f0f2c90b59d2c59e51a455fc86a1fbf9) 
+ Fix crash cutting a grouped subtitle. [Commit.](http://commits.kde.org/kdenlive/67eaa5816c42eb5dc9e5e84576e0ae92192de76d) 
+ Fix 1 pixel offset at some zoom factors. [Commit.](http://commits.kde.org/kdenlive/cef3b29a2974ffa912f96582027895bd8b7eda12) 
+ Correctly update add/remove keyframe button on undo/redo and active keyframe on speed change. [Commit.](http://commits.kde.org/kdenlive/664c4361365ccd669fdea0c59f7b3c85789ed5b3) 
+ Fix possible crash in extract zone. [Commit.](http://commits.kde.org/kdenlive/79f8e687449ca9518f2bd345afd766c28eee164a) 
+ Fix thumbnails for playlist clips having a different resolution than project profile. [Commit.](http://commits.kde.org/kdenlive/2a4ab13057f094017e32ca8bbec075ac4df451a4) 
+ Fix crash updating speed before/after in time remap. [Commit.](http://commits.kde.org/kdenlive/406a48b16cdb226473a90b973fa2b60d30e290d5) 
+ Fix proxy clips not correctly disabled on rendering. [Commit.](http://commits.kde.org/kdenlive/45c25b882a678e284996a7251c26eb867de40635) 
+ Fix sometimes cannot resize clip when there is a 1 frame gap. [Commit.](http://commits.kde.org/kdenlive/4032344f54c923afc7fdcea61aa44f5c3509c47c) 
+ Various fixes for remove space in subtitle track. [Commit.](http://commits.kde.org/kdenlive/a75b30c06421b7c4f31428794f9dc1cb609cb6e5) 
+ Fix same track transitions sometimes broken by clip resize. [Commit.](http://commits.kde.org/kdenlive/9b5dc7e8f83ece65e9db8a72483e9c39cc3e21a1) 
+ Fix 1 frame offset in subtitles when removing space. [Commit.](http://commits.kde.org/kdenlive/36809e3bd57c18d3f2e3478bf153b2a219bb90f8) 
+ Show clip labels as soon as there is one letter width. [Commit.](http://commits.kde.org/kdenlive/1093294c953e6a9cf46e31f37550c3dc7d7bc47d) 
+ Fix marker thumbnail size. [Commit.](http://commits.kde.org/kdenlive/6aa79cbc84a80b1f86bd85801bdc981a9f26d092) 
+ Don't show clip thumbs on when clip is too small (<16 pixels). [Commit.](http://commits.kde.org/kdenlive/c2262e4f0b9da6773bb8676b294d520344532032) 
+ Missing change from last commit (fix remove space). [Commit.](http://commits.kde.org/kdenlive/11507b589243aaf3799f2a00450cd97c95cc87f3) 
+ Fix "remove space" not working on 1 frame space. [Commit.](http://commits.kde.org/kdenlive/6c34acb20be6481cbefad8f91c0401ca677f1426) 
+ Only create proxy clips automatically if requested. [Commit.](http://commits.kde.org/kdenlive/add919c7c33ceb6f1f2aed02d70389fe864c7fc6) 
+ Fix audio wave for non stereo clips. [Commit.](http://commits.kde.org/kdenlive/d8170f0031446a872b934a962b6e5dc16cd5ce09) 
+ Fix qml binding loop warning. [Commit.](http://commits.kde.org/kdenlive/817e35be1b4f560a3e6867dcefa86281fb50d86f) 
+ Fix clip thumbnails extending past clip length. [Commit.](http://commits.kde.org/kdenlive/6ca9ae64b6715f811598f064c3eda76e331d5d5c) 
+ Fix adjust to original size using proxy resolution. [Commit.](http://commits.kde.org/kdenlive/9d19863e2fd7cc672f178b801125f2b7e26ac841) 
+ Try to fix ghost icons on Windows. [Commit.](http://commits.kde.org/kdenlive/68e0cb402d1c2802f1edb3e203d7d9e2fa338005) 
+ Major speedup in audio thumbs drawing on high zoom levels. [Commit.](http://commits.kde.org/kdenlive/f43d851218ca6e9d5eb0617d8162d81590326c89) 
+ Fix clip name not scrolling anymore. [Commit.](http://commits.kde.org/kdenlive/03e2ca4590e443e3d441e0dba81c8fc63e9eaab6) 
+ Fix unusable bin icon for audio/video drag. [Commit.](http://commits.kde.org/kdenlive/b7dd4819012f37ce9e3f1728bb8439107a6bbeb0) 
+ Fix Wayland crash on layout switch. [Commit.](http://commits.kde.org/kdenlive/1d5847a4ffd6f330a65187723d0d826dacb71dce) 
+ Minor optimization for audio thumbs drawing. [Commit.](http://commits.kde.org/kdenlive/b4911b0d4f99789d97cd711c9623b14f3d23fa09) 
+ Fix .ass subtitle files not correctly read. [Commit.](http://commits.kde.org/kdenlive/24e12eba52cfe7050078dea60426f64001bc3cf5) 
+ Ensure processes are in the path before starting an executable. [Commit.](http://commits.kde.org/kdenlive/de580556454818846a064a8de8f2016d726cd636) 
+ Fix timeline keyframes sometimes disappearing from view. [Commit.](http://commits.kde.org/kdenlive/a582fd66d71e2a55351fab5eab26f8e9bae69d6e) 
+ Fix wrong comparison of current settings and settings stored in the project settings dialog. [Commit.](http://commits.kde.org/kdenlive/2c5a5e5812de566f3b39a0e935a7d3fad17b8b9e) 
+ Fix again VPx quality: use constrained quality (bitrate>0). [Commit.](http://commits.kde.org/kdenlive/086ab6b24d10cbb16a7f268892c922cd1b4de886) 
+ [Spot Remover effect] Add xml ui to fix initalization (and other minor. [Commit.](http://commits.kde.org/kdenlive/40e3de128bd6a3104cf93b996a91dd2af6578cca) 
+ [Extract Frame] Fix wrong frame exported when using source resolution. [Commit.](http://commits.kde.org/kdenlive/080da9380775178cc57efa6fd170992fb4fa1fa0) 
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Fix Bug 434335 Zoom in/out is missing in the context menu. [Commit.](http://commits.kde.org/kdepim-addons/4fbaeb8e501914bd63e716cdac0e8ad810de8160) Fixes bug [#434335](https://bugs.kde.org/434335)
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Apply patch for disable sync contact as it don't ported yet. [Commit.](http://commits.kde.org/kdepim-runtime/624ef213e50ae4712bfe2a254c5f444caaacfa22) Fixes bug [#449024](https://bugs.kde.org/449024)
{{< /details >}}
{{< details id="kgpg" title="kgpg" link="https://commits.kde.org/kgpg" >}}
+ Update list of keyservers. [Commit.](http://commits.kde.org/kgpg/c791dd61c4b9b48970e31551c0a855b5c8e776e6) 
{{< /details >}}
{{< details id="khelpcenter" title="khelpcenter" link="https://commits.kde.org/khelpcenter" >}}
+ Mark as SingleMainWindow in desktop file. [Commit.](http://commits.kde.org/khelpcenter/4a3ca9271ff14658c37d744338089e3a601cf363) 
{{< /details >}}
{{< details id="kig" title="kig" link="https://commits.kde.org/kig" >}}
+ Empty Coordinates are Kind of Valid. [Commit.](http://commits.kde.org/kig/ba78d8aed88d56e337c409faafde023f0f18e8f3) Fixes bug [#448700](https://bugs.kde.org/448700)
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Add missing "truncating" parameter. [Commit.](http://commits.kde.org/kio-extras/3259705bb974979c8c3ee819f12882d942951b80) 
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Don't pass PDFDoc arguments that Poppler already has the same defaults for. [Commit.](http://commits.kde.org/kitinerary/e21d1ffc5fa81a636245f49c97fe7cda63abbb1d) 
+ Ignore more files for cppcheck that hang the latest version on the CI. [Commit.](http://commits.kde.org/kitinerary/9867f3ef64ada35677d4ae6af73eb91dc54da55c) 
+ Add basic Air France PDF ticket extractor. [Commit.](http://commits.kde.org/kitinerary/4608ca3df162470bf9fa5312f8abd498b63a0e2d) 
+ Reduce the lower size threshold for 2D barcodes. [Commit.](http://commits.kde.org/kitinerary/61af3d32efc9d3180f63200cd066640362f171dd) 
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Fix order. [Commit.](http://commits.kde.org/kmail/f249a80be263f76af00949d2dfa937000f7f0a0f) 
+ Avoid to duplicate entries. [Commit.](http://commits.kde.org/kmail/f35dcc8979a05ae0e1d1df51efe52cecf5a2bfca) 
+ Make sure helper apps we start are in path. [Commit.](http://commits.kde.org/kmail/582c5910bfc8791fce8321cde92237b87252f116) 
{{< /details >}}
{{< details id="kontact" title="kontact" link="https://commits.kde.org/kontact" >}}
+ Fix Manager Crash when clicking New. [Commit.](http://commits.kde.org/kontact/060fb5f4bfdeb4ce50e6b5ad47aebddba2a228c6) Fixes bug [#424252](https://bugs.kde.org/424252)
+ Use KIO/ApplicationLauncherJob. [Commit.](http://commits.kde.org/kontact/7bd08c0549199dbb883f21854b332ec7dfc076cf) 
+ Make sure helper apps we start are in path. [Commit.](http://commits.kde.org/kontact/749f97fdbfdebe8ef65700eda6f7524416e9a252) 
{{< /details >}}
{{< details id="konversation" title="konversation" link="https://commits.kde.org/konversation" >}}
+ KStatusNotifierItem new API need to be guarded with KNotifications version. [Commit.](http://commits.kde.org/konversation/10551650a9ccc17d370424e07d814b0c8c17088f) 
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Format user visible numbers using the current locale. [Commit.](http://commits.kde.org/kosmindoormap/31bc464c26bdb76000ccb47108a9e6d016e3ac67) 
{{< /details >}}
{{< details id="kpmcore" title="kpmcore" link="https://commits.kde.org/kpmcore" >}}
+ Do not repeatedly open and close file when reading from it. [Commit.](http://commits.kde.org/kpmcore/062acec7e41ef0cb852d1efddd94d280d04b2840) 
+ Do not repeatedly open and close file when writing to it. [Commit.](http://commits.kde.org/kpmcore/dbf3bf64d6cc7ad5cf20e487c689d8b4d5a8e09a) 
+ Make sure that path passed to WriteData is block device. [Commit.](http://commits.kde.org/kpmcore/36f1bf62dcb8436d302e7bbc8cc9c026db18fb7c) 
+ Restrict CopyFileData to writing to already existing files. [Commit.](http://commits.kde.org/kpmcore/facc621e9beae166587c16e08f782eddacfa06e9) 
+ Rename CopyBlocks to CopyFileData. [Commit.](http://commits.kde.org/kpmcore/8edb1233264e38236497ca37b63c276d5306405d) 
+ Check for relative paths in ExternalCommandHelper::CopyBlocks. [Commit.](http://commits.kde.org/kpmcore/78418c3c2ec77cd1b2ffe0bd27c77a8bcdba4dca) 
+ Restrict QProcess::ProcessChannelMode to two used values. [Commit.](http://commits.kde.org/kpmcore/1cdc62187bfa95692f26354fd4c4cd01f1ba46cd) 
+ Be a bit more strict in root helper when checking path to /etc/fstab. [Commit.](http://commits.kde.org/kpmcore/8d802039bc5edbc9ba1965cb429314361fd900eb) 
{{< /details >}}
{{< details id="kwalletmanager" title="kwalletmanager" link="https://commits.kde.org/kwalletmanager" >}}
+ Desktop file: fix to announce taking local files only, not URLs. [Commit.](http://commits.kde.org/kwalletmanager/d8f2b6a7fac01861386e14e363879887e1dc36c0) 
+ Fix skipping the first wallet arg name on the commandline. [Commit.](http://commits.kde.org/kwalletmanager/7db9d50d636872c116231e448ea8f62a3beabc0d) 
+ Fix QCommandLineParser setup, wallet names are taken as positional args. [Commit.](http://commits.kde.org/kwalletmanager/c0a6d323adddcf3b18b28fb91563bcdcf7f35765) 
{{< /details >}}
{{< details id="libkdegames" title="libkdegames" link="https://commits.kde.org/libkdegames" >}}
+ Make the installed files reproducible. [Commit.](http://commits.kde.org/libkdegames/26720af9df6d10e0b26ec3c50b3762a4581a826b) 
{{< /details >}}
{{< details id="libkgapi" title="libkgapi" link="https://commits.kde.org/libkgapi" >}}
+ Make sure utf8 text is displayed as utf8. [Commit.](http://commits.kde.org/libkgapi/a1b8c71fc98169dc1862456983f72c5cfded7cdc) 
{{< /details >}}
{{< details id="libkmahjongg" title="libkmahjongg" link="https://commits.kde.org/libkmahjongg" >}}
+ Make the installed files reproducible. [Commit.](http://commits.kde.org/libkmahjongg/5ebf50c1d7d38e0a666fbf5cf013edbb9c71f628) 
{{< /details >}}
{{< details id="libksane" title="libksane" link="https://commits.kde.org/libksane" >}}
+ Fix setting list values. [Commit.](http://commits.kde.org/libksane/31fc0db2cc307fc0022f290cac55de879b7df6ec) 
{{< /details >}}
{{< details id="lokalize" title="lokalize" link="https://commits.kde.org/lokalize" >}}
+ Make sure helper apps we start are in path. [Commit.](http://commits.kde.org/lokalize/4915871de253a28eecba6655c669963b0a58a637) 
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ [messagecomposer] Do not sign long headers. [Commit.](http://commits.kde.org/messagelib/b23d11d27d8619715a2fb3fa5a290e11cb5a027b) Fixes bug [#439958](https://bugs.kde.org/439958)
+ Fix Bug 449809 KMail2 does not resize images. [Commit.](http://commits.kde.org/messagelib/56e8511dd2ddd92f5be948ffedfa6b8ac61c5cba) Fixes bug [#449809](https://bugs.kde.org/449809)
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Fix wrong default font string for annotation tools. [Commit.](http://commits.kde.org/okular/818b714252faa2451040551c972dade865221f6c) 
{{< /details >}}
{{< details id="pim-sieve-editor" title="pim-sieve-editor" link="https://commits.kde.org/pim-sieve-editor" >}}
+ Fix build with GCC 12 (standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/pim-sieve-editor/5f487602fc284a63536c93a560ae5cd1e773e13a) 
{{< /details >}}
