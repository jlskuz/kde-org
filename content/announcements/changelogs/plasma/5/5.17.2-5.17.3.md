---
aliases:
- /announcements/plasma-5.17.2-5.17.3-changelog
hidden: true
plasma: true
title: Plasma 5.17.3 Complete Changelog
type: fulllog
version: 5.17.3
---

### <a name='breeze-gtk' href='https://commits.kde.org/breeze-gtk'>Breeze GTK</a>

- [GTK3] Modify scrollbar states to better resemble Breeze Qt. <a href='https://commits.kde.org/breeze-gtk/62ca13b84a2dc1e2660ee72bc76b2a46f26822c7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413498'>#413498</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25246'>D25246</a>
- [GTK3/Firefox] Fix scrollbar click region. <a href='https://commits.kde.org/breeze-gtk/be9775281fa9018e69588174856da34c96fab82e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413118'>#413118</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25180'>D25180</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Flatpak: --verbosity. <a href='https://commits.kde.org/discover/17c3bced46491a76b1b85db8ecd2b9321e4a2225'>Commit.</a>
- Ui: fix updates page. <a href='https://commits.kde.org/discover/93e9f4bda9e9577afe1dd8e7022fae83b91d04ff'>Commit.</a>
- Flatpak: show an error message when remotes are faulty. <a href='https://commits.kde.org/discover/a6dac5a6045b84901853b19033067a76d13a1445'>Commit.</a>
- Pk+suse: include a hack to allow apps to be executed. <a href='https://commits.kde.org/discover/6e752399db71b8942a3b5c184206b33b115b1410'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412986'>#412986</a>
- Pk: fix fallback invocation plan. <a href='https://commits.kde.org/discover/949b2aa718cf48e445e1057dec0c5bfbb0eb588f'>Commit.</a>
- Ui: improve busy indicator handling. <a href='https://commits.kde.org/discover/836b5595516c323ad75b27ecbec11f7fdbd857ca'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412908'>#412908</a>
- Kns: don't read globals when loading KNS files. <a href='https://commits.kde.org/discover/219e5a2559205182c76407a0ecf62178d06fc192'>Commit.</a>
- Flatpak: save quite some unnecessary QString allocations at startup. <a href='https://commits.kde.org/discover/cbf1241d57ebeb0f4960066593ed8366db4aef96'>Commit.</a>
- App page: fix display of faulty screenshots. <a href='https://commits.kde.org/discover/d7579e8dedbeca31ef46c7c816ab5dfbc28c1621'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413812'>#413812</a>
- --qml warnings. <a href='https://commits.kde.org/discover/a539db7d4d5045d97e4a0e196967c55ece8849de'>Commit.</a>
- --warnings. <a href='https://commits.kde.org/discover/9e081fb2d1751097baa4550df2179d106b29765a'>Commit.</a>
- Kns: fetching details might increase the description we have. <a href='https://commits.kde.org/discover/909dac1e69b74c45bcb0779cbbec30feebdb402c'>Commit.</a>
- --qclp runtime warnings. <a href='https://commits.kde.org/discover/2609ab269b651042da9358f6198b0a15270f5c27'>Commit.</a>
- Ui: remove LinkLabel. <a href='https://commits.kde.org/discover/db7364d451da0d65c68bd420a33d848e2c1df972'>Commit.</a>
- Fix qml warnings. <a href='https://commits.kde.org/discover/77d64f93e1a3b19c089ada925a286e7f998ac889'>Commit.</a>
- Address qml tests. <a href='https://commits.kde.org/discover/c71b5a2525681a4c78e9068657936249f7a189b6'>Commit.</a>
- Dummy: fix test. <a href='https://commits.kde.org/discover/c7742b10a4c96859f8af64adf6a54a6eccfd4198'>Commit.</a>
- Help cmake find the tests. <a href='https://commits.kde.org/discover/50afc5352f35ce8b9aaa7b88b13f2c0d5a543b31'>Commit.</a>
- Flatpak: make test more resilient. <a href='https://commits.kde.org/discover/29b1519d4b0bed55fdd810fa7fe4a019b8a158fc'>Commit.</a>
- Flatpak: downloading flathub metadata takes a while. <a href='https://commits.kde.org/discover/d53ac695407f6e7f3b3cc3892fb2c3cbdc31438f'>Commit.</a>
- Flatpak: remove duplicated code. <a href='https://commits.kde.org/discover/fb0e1b6cfe58b5d3884ded8fccb6690145ce9de3'>Commit.</a>
- Kns: fix test. <a href='https://commits.kde.org/discover/ac2dc9152bc93fdfd438e09ad2d266944d885933'>Commit.</a>

### <a name='drkonqi' href='https://commits.kde.org/drkonqi'>Dr Konqi</a>

- Move plus character in query test to connection. <a href='https://commits.kde.org/drkonqi/8a03e199c2049e609aa8bf1c50473f82982e0dce'>Commit.</a>
- Force encoding on all queries. <a href='https://commits.kde.org/drkonqi/885a58205ba05af1e04b3a720b0eaf26f18d07ce'>Commit.</a> See bug <a href='https://bugs.kde.org/413920'>#413920</a>
- Force-encode passwords on login. <a href='https://commits.kde.org/drkonqi/4e29b8a75635a2084d1cda0f1c9e8721cb8137c8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413920'>#413920</a>

### <a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a>

- Remove gtkrc-2.0 legacy settings. <a href='https://commits.kde.org/kde-gtk-config/308342b16f0a1af0437e1a7d7049b59d30a81952'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413678'>#413678</a>. Fixes bug <a href='https://bugs.kde.org/413107'>#413107</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25147'>D25147</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- [applets/weather] Reduce label minimum width to accommodate narrow vertical panels. <a href='https://commits.kde.org/kdeplasma-addons/8aba24da397cc9ae37d9d74f46661cbbd6b3c082'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413592'>#413592</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25082'>D25082</a>
- [applets/weather] Don't show temp label in panel when it's blank. <a href='https://commits.kde.org/kdeplasma-addons/ddac86d815a05a334f3bfab6f3ab7b7c77db5ae8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413702'>#413702</a>

### <a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a>

- Fix(kded): read rotated output size. <a href='https://commits.kde.org/kscreen/0cf25b853ae3bff1bb1c1c05d6317994feedf061'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413627'>#413627</a>. See bug <a href='https://bugs.kde.org/396354'>#396354</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25062'>D25062</a>
- Fix: use output hashMd5 for control files. <a href='https://commits.kde.org/kscreen/0f685b76633ed42ba0b2408dcaff9fa2523565d9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25021'>D25021</a>
- Fix: create control files only when needed. <a href='https://commits.kde.org/kscreen/7de6c0c36d55a032b3881ad729a6ffdee4673102'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25020'>D25020</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- [wayland] Fix sha check of filtered applications. <a href='https://commits.kde.org/kwin/18a4ded30771240916454bfcfa4fb037b539f9ff'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25169'>D25169</a>
- [effects/startupfeedback] Fallback to small icon size when no cursor size is configured. <a href='https://commits.kde.org/kwin/87f36f53b3e64012bce2edc59f553341fc04cfc2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413605'>#413605</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25065'>D25065</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- [Cursor Theme KCM] Elide size ComboBox text. <a href='https://commits.kde.org/plasma-desktop/582bd8e04af07f7bf1ecad1acc2c5b9d9276f460'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25264'>D25264</a>
- [Night Color KCM] Reset seconds and milliseconds in provided timings. <a href='https://commits.kde.org/plasma-desktop/59e3003943da2586f4f596255b9a70e0bd918c1d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25047'>D25047</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Fix binding loop in lockscreen media controls. <a href='https://commits.kde.org/plasma-workspace/419b9708ee9e36c4d5825ff6f58ca71f61d32d83'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413087'>#413087</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25252'>D25252</a>
- Update panel shadows when background hints are changed. <a href='https://commits.kde.org/plasma-workspace/4c84ede8f8d0e0d7887965f959352deb84da1b73'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25121'>D25121</a>
- [Slideshow] Reenable other modes. <a href='https://commits.kde.org/plasma-workspace/d0df800b46f91f06cb6fbc6b365b80eb5b5ed58f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25157'>D25157</a>
- Make env in plasma-sourceenv.sh call portable. <a href='https://commits.kde.org/plasma-workspace/0ebdf83a2f9efd96b0c0a50dcd84aec5fb1ff3b2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25124'>D25124</a>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

- [Icon View] Don't use dialog-warning emblem on icon when KCM requires authentication. <a href='https://commits.kde.org/systemsettings/efb35162be7f21f28d0c293fb0c024d9ad9c0f90'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413183'>#413183</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25150'>D25150</a>

### <a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a>

- Un-break remote input portal. <a href='https://commits.kde.org/xdg-desktop-portal-kde/db12a964b139bcb41689efc5283fd28a099b543b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24452'>D24452</a>