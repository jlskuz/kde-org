---
title: Plasma 5.27.4 complete changelog
version: 5.27.4
hidden: true
plasma: true
type: fulllog
---
{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ CheckBox,RadioButton: Fix RTL icon alignment. [Commit.](http://commits.kde.org/breeze/580183a01d0355ddf4995e58b0064d9e358e823e) 
+ CheckBox,RadioButton: Fix RTL focus frame positioning. [Commit.](http://commits.kde.org/breeze/ed0aee8b3f7f24e2a7365f11f86b9823aa37e0aa) 
+ CheckBox,RadioButton: Extend focus line to cover an icon too. [Commit.](http://commits.kde.org/breeze/cd75ecd72a9d058f9fb2b2b10f74c0d98ef71aba) Fixes bug [#467824](https://bugs.kde.org/467824)
+ Fix RTL for SP_ToolBarHorizontalExtensionButton. [Commit.](http://commits.kde.org/breeze/2a511ba8805f515a275191ecd01e5a7257ce2183) 
+ KStyle: make painted arrows more scalable, fix RTL delay menu arrows. [Commit.](http://commits.kde.org/breeze/a15812125c61ae531e029b5afc646c332758a074) 
{{< /details >}}

{{< details title="breeze-gtk" href="https://commits.kde.org/breeze-gtk" >}}
+ Gtk4/windowcontrols: reduce right margin of close button. [Commit.](http://commits.kde.org/breeze-gtk/08100477221990bea383f669156b9e750f3d637e) 
+ Gtk3/titlebutton: extend control area to window borders. [Commit.](http://commits.kde.org/breeze-gtk/59448164f7df462ae4a2c1119f6be59146d60399) See bug [#414777](https://bugs.kde.org/414777)
+ Gtk4/windowcontrols: extend control area to window borders. [Commit.](http://commits.kde.org/breeze-gtk/83f4f7c184954eb89aaea52684f1ce076873e8c3) Fixes bug [#414777](https://bugs.kde.org/414777)
+ Iconhelper: Query size via CSS. [Commit.](http://commits.kde.org/breeze-gtk/0479940e79b045f54d2bad2c10c6b327a6ed957e) 
+ Gtk4/aboutdialog: set icon size for large icons. [Commit.](http://commits.kde.org/breeze-gtk/d4c9ccf7b4b9b8d84a1ced7893b70292f3901d12) 
+ Gtk4/theme: Use 0.5 opacity for disabled pictures. [Commit.](http://commits.kde.org/breeze-gtk/1964c8d55e2cf7f387c7411bca58f58c8329cb80) 
+ Menus: make size react to fractional scaling. [Commit.](http://commits.kde.org/breeze-gtk/10c7dee6838906e96d78bee901ce39bb707aab2e) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Rpm-ostree: Improve handling of externally started transactions. [Commit.](http://commits.kde.org/discover/1aa5ac965e41500eca19c31f414eeb5d61384fe5) 
+ Rpm-ostree: Correctly set fetching and transaction state. [Commit.](http://commits.kde.org/discover/6e2d3b195a7350c93f2add9a13417bcd8057e84d) 
+ Rpm-ostree: Unify transaction setup in a single function. [Commit.](http://commits.kde.org/discover/c59bc08cc946bcab71ad01c8d1c74754596f1094) 
+ Pk: Clear the offline state before starting a new one. [Commit.](http://commits.kde.org/discover/18a85033c4f79157693b45247c7a19a6a648ec32) Fixes bug [#467638](https://bugs.kde.org/467638)
+ Notifier: Don't show updates notification if Discover is running. [Commit.](http://commits.kde.org/discover/0f98292f1e1245312734dcb13107671764378dcc) 
+ Pk: Do not create a QSet unnecessarily for isPackageNameUpgradeable. [Commit.](http://commits.kde.org/discover/1faa81a636143702679f0109128f53ee15da4dac) 
+ Pk: Group getUpdateDetail calls into one. [Commit.](http://commits.kde.org/discover/8f914d825ee5382901606cbbb7d78af0871cb3dc) 
+ Pk: Group offline updates resource size notifications. [Commit.](http://commits.kde.org/discover/4050d76a42776ec829bf2bd1a7b5e2268786c6a2) 
+ PackageKitNotifier: Do not refresh database if an offline update is pending. [Commit.](http://commits.kde.org/discover/23eead2841c291f11f399fbd605e0a53b62d1a89) 
{{< /details >}}

{{< details title="Flatpak Permissions" href="https://commits.kde.org/flatpak-kcm" >}}
+ FlatpakPermissionModel: Refactor loops, use const auto more. [Commit.](http://commits.kde.org/flatpak-kcm/291deaeb187b4ada90f6ed33d1c4d3945ea01581) 
+ FlatpakPermissionModel: Mark FlatpakPermission argument as const where possible. [Commit.](http://commits.kde.org/flatpak-kcm/37464c6e4bd711039de67c316b318991b00de2ea) 
+ FlatpakPermissionModel: Refactor FlatpakPermission pointers to references. [Commit.](http://commits.kde.org/flatpak-kcm/475b20fccb93067576b33b5802caf9d2a2d5b2aa) 
+ QML: Remove `var` return type from QML method. [Commit.](http://commits.kde.org/flatpak-kcm/9b3ebdeb33cdd6095367e1610c90adc5ed2f844d) 
+ FlatpakPermissionModel: Don't translate strings that are part of config format. [Commit.](http://commits.kde.org/flatpak-kcm/9a2272fe2c48ab897f3960f8cbce33d059670d57) 
+ FlatpakPermission: Register FlatpakPolicy to fix std::variant to QVariant conversion. [Commit.](http://commits.kde.org/flatpak-kcm/a00fd14be04de6c522af5e9e3ba8681d09ce9b71) 
+ FlatpakPermissionModel: Use std::variant and FlatpakPolicy to get rid of i18n strings. [Commit.](http://commits.kde.org/flatpak-kcm/ccb339da8cdaf7c3c2a0417da068ff3106fbefd5) 
+ FlatpakPermissionModel: Turn list of possible values into a model. [Commit.](http://commits.kde.org/flatpak-kcm/44782a66435851d4270b1bd5ddeef19937cc2d80) 
+ Tests: Add test for addUserEnteredPermission. [Commit.](http://commits.kde.org/flatpak-kcm/d6847b170fe97e191c9bc991bedff920558cf470) 
+ FlatpakPermissionModel: Fix adding/removing custom D-Bus permission entries. [Commit.](http://commits.kde.org/flatpak-kcm/547e6943d9f3dcb4a5cf860d5bd8535faec13b35) 
+ Tests: Switch QStringList to explicit constructor call. [Commit.](http://commits.kde.org/flatpak-kcm/04526d0b4d62f7e6aadf78e8e94768e45b94e81d) 
+ Tests: Improve override config comparison. [Commit.](http://commits.kde.org/flatpak-kcm/634fc30fca0fc61ffaf749ed5d618f4d462aeb9d) 
+ FlatpakPermissionModel: Set default value of custom added permission entries. [Commit.](http://commits.kde.org/flatpak-kcm/d1f2a1f5a1d4626fdb393a069c5e79b3d62dcd4b) 
+ Guard flatpakcommon.h with #pragma once. [Commit.](http://commits.kde.org/flatpak-kcm/b5b79d0f1d8af3fab53f3435f87a12602c6d3803) 
+ Nuke possibleValues data member and getter from FlatpakPermission class. [Commit.](http://commits.kde.org/flatpak-kcm/f2ab610746c624ccb537fb338c2b65567d6cce42) 
+ Add tests for filesystems entry (de-)serializer. [Commit.](http://commits.kde.org/flatpak-kcm/faf21b7e76b7ed6060bccf90cca415808ce55aa8) 
+ Implement filesystems entry (de-)serializer. [Commit.](http://commits.kde.org/flatpak-kcm/936d21af6ce2c93be946faf04d7b3ded460ec325) 
+ FlatpakPermission: Remove valueType from stored members, infer it from section type. [Commit.](http://commits.kde.org/flatpak-kcm/80b41ff2b6a6ad93a638e8e8126b39fe30cafe5e) 
+ FlatpakPermissionModel: Move "Add New" button's tooltip text to model. [Commit.](http://commits.kde.org/flatpak-kcm/890c53aca801dd7ffefdf98d88e3ccb9060de221) 
+ Delegate possibleValues generation using valueListForSectionType function. [Commit.](http://commits.kde.org/flatpak-kcm/ac2e8d68459a940eea62a7b0f7f2ade41aa12b7b) 
+ FlatpakPermissionModel: Rewrite permission editing if-else chain using new section types. [Commit.](http://commits.kde.org/flatpak-kcm/5b297770f7266729eb30dca2c9681aa5781ede1b) 
+ Revert ellipsis strings changes due to string freeze on stable branch. [Commit.](http://commits.kde.org/flatpak-kcm/1d9eaabf218a7a2939abe8a80c1a10328c3469af) 
+ FlatpakPermissionModel: Use enum for list section headers, move i18n to QML. [Commit.](http://commits.kde.org/flatpak-kcm/2bfe054c4108b3315718dc276fa0108cbb23a946) 
+ FlatpakPermissionModel: Split read and write access to a counter variable. [Commit.](http://commits.kde.org/flatpak-kcm/c8703f19a2bc42febcab0f3072ab78b34244c14e) 
+ Don't allow unchecking D-Bus entries which are provided by default. [Commit.](http://commits.kde.org/flatpak-kcm/05bbb9659b2b7222072a157984f052ac71f1f5f9) 
+ FlatpakPermissionModel: Expose default enabled and value as model roles. [Commit.](http://commits.kde.org/flatpak-kcm/f4717e4dba1f2ae86c4d73df82d6462584d25298) 
+ FlatpakPermissionModel: Sort out enabled state for D-Bus entries. [Commit.](http://commits.kde.org/flatpak-kcm/b9e4a7363ecdc41485fad12896d3b9dcadb729a8) 
+ FlatpakPermissionModel: Add D-Bus "none" policy to the possible values list. [Commit.](http://commits.kde.org/flatpak-kcm/f72b2ad20c358c389a5b03e8167a664942d7f2c8) 
+ FlatpakPermissionModel: Fix D-Bus "none" policy's i18n, capitalization and loading/saving. [Commit.](http://commits.kde.org/flatpak-kcm/e269bf6797e86ae74fdb278c10491dacfd03ec9c) 
+ Cherry-pick FlatpakReference constructor from master branch for use in tests. [Commit.](http://commits.kde.org/flatpak-kcm/fd22d68bc893adc3cc010d04921ff5f1d0fa844b) 
+ FlatpakPermissionModel: Replace KDesktopFile with simpler KConfig. [Commit.](http://commits.kde.org/flatpak-kcm/e5e8e6219c8157108bf78c701435d3930991724b) 
+ Require passing tests on Linux. [Commit.](http://commits.kde.org/flatpak-kcm/a74ff3dd6d487cd1a4b9354e6abcbbbecbdabe66) 
+ CI: Add required kdeclarative dependency. [Commit.](http://commits.kde.org/flatpak-kcm/9865c08e58a0043a95b3717907373a66eeb8b3c8) 
+ CI: Sort dependencies. [Commit.](http://commits.kde.org/flatpak-kcm/b9a31ea25337aaa7871b36a6f3102a95e05dad97) 
+ FlatpakPermissionModel: Fix off-by-one insertion index of custom filesystems. [Commit.](http://commits.kde.org/flatpak-kcm/30357e5caedd905e6edf203a9a634755a5a623a2) 
+ FlatpakPermission: Add back no-args default constructor. [Commit.](http://commits.kde.org/flatpak-kcm/331105fa44a0b03c7f7f5a543b92aab3ec659385) Fixes bug [#467399](https://bugs.kde.org/467399)
+ FlatpakPermissionModel: Stop shuffling list of possible values for entries. [Commit.](http://commits.kde.org/flatpak-kcm/85f14e9636ba40005f276c0d0b639d567afe0cb0) 
+ FlatpakPermissionModel: Rename IsGranted role into IsEffectiveEnabled. [Commit.](http://commits.kde.org/flatpak-kcm/7eaba5fd464f8499cbbc5979c7a7b75f39629716) 
+ FlatpakPermissionModel: Rename CurrentValue role into EffectiveValue. [Commit.](http://commits.kde.org/flatpak-kcm/73e5c42d6cf8d332834913ea245db28768f8bf79) 
+ FlatpakPermissionModel: Fix failing overrides test. [Commit.](http://commits.kde.org/flatpak-kcm/ef3e9ff9bb1e3896cfe8982068ee7095c53e79e1) 
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Kded: provide `org.gtk.Settings` when `GTK_USE_PORTAL` is not set on Wayland. [Commit.](http://commits.kde.org/kde-gtk-config/5f75f4c94d5fc07cc3dd9b36c78a297f3135feae) Fixes bug [#421745](https://bugs.kde.org/421745)
+ Gtkconfig: set `color-scheme` when current color scheme changes. [Commit.](http://commits.kde.org/kde-gtk-config/ad698ba78518e3793bbd745337b6e96b01e7c714) 
+ Gsettings: check param exists before setting value. [Commit.](http://commits.kde.org/kde-gtk-config/68f772c376b5a9b8a8a8004340b8079129ff6dd5) 
+ Beside monitor scaling factors, a user may specify a preferred. [Commit.](http://commits.kde.org/kde-gtk-config/d2a84a92ca563b94fdfbea9d8edb8eb5955bccf0) Fixes bug [#466463](https://bugs.kde.org/466463). Fixes bug [#461106](https://bugs.kde.org/461106)
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Wallpapers/potd: replace `anchor.{left,right}` with `Layout.fillWidth`. [Commit.](http://commits.kde.org/kdeplasma-addons/2d9b1c131f3f58a474760ff7245f712c9db17b77) 
+ ProfilesModel: Add placeholder for option "Start Kate (no arguments)". [Commit.](http://commits.kde.org/kdeplasma-addons/9d0658a36736ab99c920a04e53e3f2e5f404ef46) Fixes bug [#464724](https://bugs.kde.org/464724)
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ Energy: Use text colour for the grid lines. [Commit.](http://commits.kde.org/kinfocenter/f3635488bb0e15cdca3c379987a58a3e04f5c028) 
{{< /details >}}

{{< details title="kpipewire" href="https://commits.kde.org/kpipewire" >}}
+ Source: Handle BGRA buffers gracefully. [Commit.](http://commits.kde.org/kpipewire/351035b816dfce22f1de2d2adf53d7a8e8ddcb66) 
+ Record: Only create the sws_context when necessary. [Commit.](http://commits.kde.org/kpipewire/f92fa87703f9f393ceaa3520c3a1dd0917dd634d) 
+ Record: Use a good amount of threads as recommended by QThread. [Commit.](http://commits.kde.org/kpipewire/9070ddd796f1275b8890beeed760392f38ff6a00) 
+ Record: Make sure we process all the frames before leaving. [Commit.](http://commits.kde.org/kpipewire/ddda58592ae47494eb0e18016d1956859bbae853) 
+ Record: Improve packet fetching. [Commit.](http://commits.kde.org/kpipewire/0e869df695cdd2b5290250dcb3ca5e3e7de0e5f9) 
+ Use a different API call to make importing DmaBufs work on Nvidia. [Commit.](http://commits.kde.org/kpipewire/6a6438e681ac60d8ec4670b075ce77de0fdd2e11) Fixes bug [#448839](https://bugs.kde.org/448839)
+ Options to disable motion estimation and in-loop filtering. [Commit.](http://commits.kde.org/kpipewire/c1d5bc03578047bf085ff0c1f64dcc3a7848a034) 
+ Record: Refactor thread distribution. [Commit.](http://commits.kde.org/kpipewire/a6331c75b70bd775cb4e4d279732a4a943f2d929) 
+ Record: Allocate SwsContext only when necessary. [Commit.](http://commits.kde.org/kpipewire/526e792a8b409142573abfa31fef850081265da7) 
+ Recording: Allocate frames when we render. [Commit.](http://commits.kde.org/kpipewire/4a0aae13d6fcb377167dfb08fc7640754f42486d) 
+ Recording: Extend the encoders API. [Commit.](http://commits.kde.org/kpipewire/ff8d89c321781a437a636544db21912076a187a1) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ OSD: Do not connect to member QObject's destroyed signal. [Commit.](http://commits.kde.org/kscreen/7a5e79edf6d12e1da6377a277a905d47424d688c) Fixes bug [#466914](https://bugs.kde.org/466914)
+ Display connector name instead of type name when serial number is identical. [Commit.](http://commits.kde.org/kscreen/7f2c1f8b104efb748cae5814ed2aa87fb2046796) Fixes bug [#466046](https://bugs.kde.org/466046)
{{< /details >}}

{{< details title="kwallet-pam" href="https://commits.kde.org/kwallet-pam" >}}
+ Exit early if the target user is root. [Commit.](http://commits.kde.org/kwallet-pam/9885133c9a1ddcb42f0ec4475e8b3bb1f34f655b) 
+ Don't do anything if the password is empty. [Commit.](http://commits.kde.org/kwallet-pam/d1910a2280d0394525bed903af16db4bd238e3e7) 
+ Verify that XDG_RUNTIME_DIR is usable. [Commit.](http://commits.kde.org/kwallet-pam/bcd18f0992439cbd89af796f3b96e676eea23977) 
{{< /details >}}

{{< details title="kwayland-integration" href="https://commits.kde.org/kwayland-integration" >}}
+ Implement SkipSwitcher state for plasma surfaces. [Commit.](http://commits.kde.org/kwayland-integration/2051f8de9508d6a510a79e912106e464522478a6) See bug [#465303](https://bugs.kde.org/465303)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Revert "backends/libinput: don't multiply v120 value by scroll speed". [Commit.](http://commits.kde.org/kwin/659cfcb4937f36e3084478afcd0ac2518d53b3cd) Fixes bug [#464592](https://bugs.kde.org/464592)
+ Improve Workspace::outputAt(). [Commit.](http://commits.kde.org/kwin/b6cd89a859448d15e257a8eed433511e3112c8d9) 
+ Workspace: prevent dangling pointers in output order list. [Commit.](http://commits.kde.org/kwin/7ba8015a1e5b205b8c3ab39aebf64d059b237445) 
+ Dpms: Make sure we are not calling the interface after the output is gone. [Commit.](http://commits.kde.org/kwin/737af922c0b15a3038521836e68cd3245bb38b80) Fixes bug [#466346](https://bugs.kde.org/466346)
+ Backends/drm: restrict common mode generation to drivers that support scaling. [Commit.](http://commits.kde.org/kwin/dc95392b982ac9ae95a72e5f38e5ecc499f0bc19) 
+ Kcms/rules: Make Comboboxes bordered again. [Commit.](http://commits.kde.org/kwin/02a618a0a8447647ef83b7d2b0f1984db90dcce3) 
+ Backends/drm: consider color property changes as failed when the output is off. [Commit.](http://commits.kde.org/kwin/7121c1e3d7f22eabdd741cb6f7c9467c4d0a2628) 
+ Wayland: Handle xdg_wm_base being destroyed before surface role. [Commit.](http://commits.kde.org/kwin/f6e7d2ab5a57d363dba7666d716b51a2df2b87f3) 
+ Avoid accidental creation of backing stores for offscreen surfaces. [Commit.](http://commits.kde.org/kwin/715f4147fec2734a0ed56f7ae799e678e18f451f) Fixes bug [#465790](https://bugs.kde.org/465790)
+ Inputmethod: Properly report that it's not visible. [Commit.](http://commits.kde.org/kwin/cf086a191efcfb907fa0d9814197c037829eb4e6) 
+ Wayland: Truncate strings sent via plasmawindowmanager interface. [Commit.](http://commits.kde.org/kwin/ec6a5e7945806afcc80f26950820a5307659fad0) Fixes bug [#465775](https://bugs.kde.org/465775)
+ Simplify tile dismissal. [Commit.](http://commits.kde.org/kwin/0be551c6bf565a939e179e7b0533dfb165167120) Fixes bug [#465740](https://bugs.kde.org/465740)
+ Fix picking drag target. [Commit.](http://commits.kde.org/kwin/c1a6f02ebc6824d679f96480908a5ff6f6aa1d17) 
+ Screencast: avoid using DMABufs exclusively to allow renegotiation. [Commit.](http://commits.kde.org/kwin/90c5a5850c21b78910b7c4ad16318b43ee95ba2d) 
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Add translation domain before diving in subdirectories. [Commit.](http://commits.kde.org/libksysguard/bfe9b5be8b7011d741aec8885d5c5d9ee27584aa) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Applets/taskmanager: don't refresh virtual desktop info when menu item is not enabled. [Commit.](http://commits.kde.org/plasma-desktop/c88a6520a41cbb7dc75b1ffca7544950c265cb2e) 
+ Applets/taskmanager: skip updating tooltip when it's disabled. [Commit.](http://commits.kde.org/plasma-desktop/47edbf51e4e68279944670df3fce86a9d11c4273) Fixes bug [#467709](https://bugs.kde.org/467709). See bug [#452187](https://bugs.kde.org/452187)
+ Divide minimum panel size by two when not floating. [Commit.](http://commits.kde.org/plasma-desktop/2960f6c54cdb529d20a5eec326bf6f70a8599fdb) Fixes bug [#466098](https://bugs.kde.org/466098)
+ Applets/taskmanager: press menu key to open task menu. [Commit.](http://commits.kde.org/plasma-desktop/81fc0ac277d51990add527c3d2b26b99e744ec94) 
+ Applets/kicker: Hide separators when sorted alphabetically. [Commit.](http://commits.kde.org/plasma-desktop/d492a691d44a2878eb3c189219dc04260294bb77) Fixes bug [#465865](https://bugs.kde.org/465865)
+ Activate Emoji Selector using emoji key. [Commit.](http://commits.kde.org/plasma-desktop/c305c46e3d1f8a24b0a5c1df9aa8155f5e7ccb28) 
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Actiondrawer: Smoothen the brightness slider so that it doesn't jump when sending events. [Commit.](http://commits.kde.org/plasma-mobile/87482ff712b8f05c82c635c65efc17d736f9d051) 
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Kcm: stop setting sourceSize in avatar. [Commit.](http://commits.kde.org/plasma-pa/8d0633859ad432037bc5cba3510ccf7f120a3b51) 
+ Kcm/DeviceListItem: Set width for comboboxes, with correctly sized popup. [Commit.](http://commits.kde.org/plasma-pa/b6715dc950ab5019542f2f7a77e9f2aa4afab9dc) 
+ Applet: add missing function for "Show virtual devices" menu item. [Commit.](http://commits.kde.org/plasma-pa/1e4d0d567a72d444dff1f90bb9c1712212d6e07e) Fixes bug [#465996](https://bugs.kde.org/465996)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Applets/systemtray: press and hold to open context menu for plasmoids. [Commit.](http://commits.kde.org/plasma-workspace/1f8fa162e8f58a73208a5a990852552083d6de12) 
+ Applets/digital-clock: reload timezone after saving in Datetime KCM. [Commit.](http://commits.kde.org/plasma-workspace/1b65c6b362b081c5ca252356d240befcba72288c) Fixes bug [#467494](https://bugs.kde.org/467494)
+ Components/keyboardlayout: Fix forced activation of vkbd. [Commit.](http://commits.kde.org/plasma-workspace/9d4b9cf547037cbb3b4aaf7c21ad08f75ea98f48) See bug [#466948](https://bugs.kde.org/466948)
+ Create directory before installing session-local. [Commit.](http://commits.kde.org/plasma-workspace/1c25f1bb963610f92bcd125607b32b88873c1415) 
+ Applets/systemtray: fix menu key not working in SNI. [Commit.](http://commits.kde.org/plasma-workspace/a7e343c023357c2860476eb9c14288cd25658a61) 
+ Find PkgConfig before first call to pkg_check_modules. [Commit.](http://commits.kde.org/plasma-workspace/800f57bd39335c241a9bcd7d257e0a9eb47c725c) 
+ Sddm-theme: Populate keyboard layouts menu only on first show. [Commit.](http://commits.kde.org/plasma-workspace/45d0968384c78f2f5b768ecfdedd00bf2cf99aa8) 
+ [dashboard] Skip task switcher. [Commit.](http://commits.kde.org/plasma-workspace/29eb56d4d16c9a5544ba2b1da2369a9ed3190bb7) See bug [#465303](https://bugs.kde.org/465303)
+ Klipper: test bug 465225. [Commit.](http://commits.kde.org/plasma-workspace/5b0563362494ab5717655f054ccd2627ad3d51e2) See bug [#465225](https://bugs.kde.org/465225)
+ Systemtraytest: take screenshot only when test fails. [Commit.](http://commits.kde.org/plasma-workspace/2d8681990e473b0e09585ce1f76a1854ea7f17d3) 
+ Kcms/nightcolor: fix timing strings with narrow window widths. [Commit.](http://commits.kde.org/plasma-workspace/32982eff1abe7653f96f746cc15c0d6d1bbbb249) 
+ Libtaskmanager: simplify `test_openCloseWindow`. [Commit.](http://commits.kde.org/plasma-workspace/0b1d9706bb79eaffc2e8d5cb1552186c8fe716aa) 
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ Fix font rendering on android. [Commit.](http://commits.kde.org/qqc2-breeze-style/752f8636203df7c358a14d5312f304d79437d869) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Cancel pending resolveChanges dialog when a new one is started. [Commit.](http://commits.kde.org/systemsettings/988ee832615a396595108159f24a8e342bd22612) Fixes bug [#465510](https://bugs.kde.org/465510)
+ Include version number in bug report URL. [Commit.](http://commits.kde.org/systemsettings/7212ef0222f6c29de97cfbb872d463a8ac8964f5) Fixes bug [#466881](https://bugs.kde.org/466881)
{{< /details >}}

