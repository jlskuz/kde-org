---
title: Plasma 6.2.2 complete changelog
version: 6.2.2
hidden: true
plasma: true
type: fulllog
---

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Kwallet: set folder explicitly. [Commit.](http://commits.kde.org/drkonqi/aac8b2924b99c87192900c94f92469f80b10347e) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Backends/drm: leave all outputs disabled by default, including VR headsets. [Commit.](http://commits.kde.org/kwin/b7dbb2845bfe454efe75be8f7f1b1631f50be174) Fixes bug [#493148](https://bugs.kde.org/493148)
+ Cmake: fix build with KWIN_BUILD_X11_BACKEND. [Commit.](http://commits.kde.org/kwin/8bf522e0fe13e5748ba180ca51f24180df858956) 
+ Cmake: don't try to build kwin_x11 when KWIN_BUILD_X11_BACKEND is turned off. [Commit.](http://commits.kde.org/kwin/710604aa9280a071dc655c46cc8c1743b01cecd1) 
+ Ci: Switch to Qt 6.8. [Commit.](http://commits.kde.org/kwin/67a6aef8bcbd86199765c477b4fd50b98648425f) 
+ Initialize KCrash for kwin_x11 too. [Commit.](http://commits.kde.org/kwin/97bec11340942ea421cb34aea2575e2c572e3d08) 
+ Backends/drm: don't allow tearing until the cursor plane is disabled. [Commit.](http://commits.kde.org/kwin/8090e0264fff32dd73197a5e39d3b2de6309fde0) Fixes bug [#493166](https://bugs.kde.org/493166)
+ Core: make sure we don't call a null buffer. [Commit.](http://commits.kde.org/kwin/e64350ecd2bd59b84b47777bb8340c2b1cae7b68) 
+ Revert "temporarily revert version number for respin". [Commit.](http://commits.kde.org/kwin/29c64202514fd94fa2e0bcdf832d8cf4b3322efb) 
+ Effect: Make opengl context current in OffscreenEffect::unredirect() and CrossFadeEffect::unredirect(). [Commit.](http://commits.kde.org/kwin/4814d9602ba5a242608f55642323baaaee95427b) 
+ Backends/drm: check for the output being active before setting legacy gamma. [Commit.](http://commits.kde.org/kwin/4f574dd3047342ba2370c56aa6848873c6ce1fb4) 
+ Avoid creating Unmanaged for reparented window. [Commit.](http://commits.kde.org/kwin/2823f142593164e309690d6af75e93bbd0754183) 
+ Backends/drm: don't skip colorops when matching the pipeline. [Commit.](http://commits.kde.org/kwin/d1eb9f0534cb454b173ce758d84956aae52b507c) Fixes bug [#494611](https://bugs.kde.org/494611)
+ Backends/drm: transform damage to match the framebuffer. [Commit.](http://commits.kde.org/kwin/f46a1c2fdb269c8403533e34504c9a583ffb9de7) Fixes bug [#494837](https://bugs.kde.org/494837)
+ Set WAYLAND_DISPLAY before starting wayland server. [Commit.](http://commits.kde.org/kwin/5275c31819c795f2095c211f87af9008c94d099f) 
+ Scene/itemrenderer_opengl: use the color pipeline to check if color transformations are needed. [Commit.](http://commits.kde.org/kwin/9370f7d19e4a047c72981d5b8f46a24eefd7cc83) Fixes bug [#493295](https://bugs.kde.org/493295)
+ Temporarily revert version number for respin. [Commit.](http://commits.kde.org/kwin/331a800a13c93e71be80843731dd08f74c6fdb9f) 
+ Backends/drm: don't scale infiniteRegion(). [Commit.](http://commits.kde.org/kwin/d352a368e21b9c95eb738a0cbde749425906f7fe) See bug [#494829](https://bugs.kde.org/494829)
+ Backends/drm: fix crash with multi gpu. [Commit.](http://commits.kde.org/kwin/8ff57a865f0702ba164b58d77d45c9168618f042) 
+ Ci: Require tests to pass only on Linux. [Commit.](http://commits.kde.org/kwin/ce12806c1573dcfbd8f7f3d07f5e9e459825c591) 
+ Report correct input timestamps for popup keyboard events. [Commit.](http://commits.kde.org/kwin/709fa395ff8a5b6140e0430842acda03611a35e3) 
+ Backends/drm: disable hdr if we removed the capabilities for it. [Commit.](http://commits.kde.org/kwin/9680e4f1f3d2a6ec9b7495322cbec41e02d56812) Fixes bug [#494706](https://bugs.kde.org/494706)
+ Wayland/color management: ignore obviously wrong HDR metadata. [Commit.](http://commits.kde.org/kwin/50d48fd7ba9fb701c9d2b838e63ad36b78f1cfa0) Fixes bug [#494502](https://bugs.kde.org/494502)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Foldermodel: Save item positions when any item gets renamed. [Commit.](http://commits.kde.org/plasma-desktop/97323985eb97f9e5be0a0daecd926f76331f5bfc) 
+ FolderView: Ignore file-related clicks if rename editor is open and is clicked. [Commit.](http://commits.kde.org/plasma-desktop/487fe1d36d805b54b31caf69e8fe1510b2e00940) Fixes bug [#494558](https://bugs.kde.org/494558)
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Reintroduce maximizing task on activating it in switcher. [Commit.](http://commits.kde.org/plasma-mobile/ada43b08cdf9ab50118cd769ffc8d5647507854e) Fixes bug [#492077](https://bugs.kde.org/492077)
+ Lockscreen: Forward keyboard key immediately to password bar. [Commit.](http://commits.kde.org/plasma-mobile/cf0f142dcacceff411007348c5ee6b3abbb1010b) 
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Fix text display for auto_null device. [Commit.](http://commits.kde.org/plasma-pa/ce9b240dc6e5efdb9f6b1a24ad4b38b2de87d15c) Fixes bug [#494324](https://bugs.kde.org/494324)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Shell: disable incremental gc until QTBUG-129241 is resolved. [Commit.](http://commits.kde.org/plasma-workspace/13bf351966be782af4bb025b7c68e8a3a6d13395) 
+ Applets/devicenotifier: ensure udi is not dangling to fix crash. [Commit.](http://commits.kde.org/plasma-workspace/2d6a8ccff178ee4d9adf740c3843e5c52bea4872) 
+ Applets/devicemonitor: document when formating a device is removed and added back immediately. [Commit.](http://commits.kde.org/plasma-workspace/b340476903ec432869c2d847c4fed6509c0dedc1) 
+ Revert "temporarily revert version number for respin". [Commit.](http://commits.kde.org/plasma-workspace/3949d34c646cb193b41bb38d6349c0dad8b7bc78) 
+ Klipper: Avoid creating lots of native windows in settings dialog. [Commit.](http://commits.kde.org/plasma-workspace/dcb1b27d38a84f14c794504655161e23e3d43192) Fixes bug [#494169](https://bugs.kde.org/494169)
+ Temporarily revert version number for respin. [Commit.](http://commits.kde.org/plasma-workspace/52bb8daeccfddde002f6d778433ec282472d3296) 
+ Startkde: Specify --type=method_call to correctly notify ksplash. [Commit.](http://commits.kde.org/plasma-workspace/271801a547a94f8979d6293dc653643aa70dafe7) See bug [#494840](https://bugs.kde.org/494840)
+ Applets/devicenotifier: reduce string allocation. [Commit.](http://commits.kde.org/plasma-workspace/8d78d84b6d672f19a7d13bbd0838c10335ffad7b) 
+ Klipper: correct boolean in desktop file. [Commit.](http://commits.kde.org/plasma-workspace/ec3ae6ab71481a5ffd4202ae360806dd0072da22) 
+ PanelView: Fix popups position when NoBackground hint is set. [Commit.](http://commits.kde.org/plasma-workspace/b5e8d8d3395ccfdc2d7af99cc3adb1827d2228e8) Fixes bug [#494193](https://bugs.kde.org/494193)
{{< /details >}}

{{< details title="SDDM KCM" href="https://commits.kde.org/sddm-kcm" >}}
+ Fix autologin session value. [Commit.](http://commits.kde.org/sddm-kcm/980813b76e37c50eb38afd05de839168047921f5) See bug [#495025](https://bugs.kde.org/495025)
+ Resolve and de-duplicate theme paths. [Commit.](http://commits.kde.org/sddm-kcm/6ef96b5b2811bc2490ed0b365a23096569c7b8dc) Fixes bug [#482484](https://bugs.kde.org/482484)
{{< /details >}}

{{< details title="Spacebar" href="https://commits.kde.org/spacebar" >}}
+ Remove all SQL queries from client, move to DBus only. [Commit.](http://commits.kde.org/spacebar/1aedd57b9b65e05f01e20effa0e26889db740982) 
+ Only handle incoming messages in incoming message handler. [Commit.](http://commits.kde.org/spacebar/037786ff955c5b26de053b7bd275a86699000fb3) 
+ Improve behavior with daemon handling, and UI if daemon is off. [Commit.](http://commits.kde.org/spacebar/8da1bb6ce3df5f23bab713d137cf84bce8a6c485) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ SettingsBase: Make sure we save window state on quit or geometryChange. [Commit.](http://commits.kde.org/systemsettings/4bd4bce4b834840772d91e643c4ddd3d633c636c) Fixes bug [#494377](https://bugs.kde.org/494377)
{{< /details >}}

