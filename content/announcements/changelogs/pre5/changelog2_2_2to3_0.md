---
aliases:
- ../changelog2_2_2to3_0
hidden: true
title: KDE 2.2.2 to 3.0 Changelog
---

<p>This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 2.2.2 and 3.0 releases.
The primary goal of the KDE 3.0 release is to port the existing codebase
of the KDE 2 series to be based on the Qt 3 library. </p>

<p>The use of Qt 3 provides a set of new features and improvements
as well as allows a long period of binary compatible releases. </p>

<h3>General</h3><ul>
<li>A lot of fixes for reported bugs in all applications</li>
<li>Porting to make full use of the Qt 3 GUI toolkit</li>
<li>Performance improvements in some areas</li>
<li>Arts has been splitted in a KDE-independent part and KDE-bindings</li>
</ul>

<h3>Arts</h3><ul>
<li>More PlayObjects (more fileformats)</li>
<li>Improvements of the MIDI capabilities (alsa support)</li>
<li>Integration of new GSL scheduling code</li>
<li>More support for using samples as instruments (.PAT loader)</li>
<li>Environments/Mixers</li>
<li>Recording support in the APIs (&#x6b;re&#x74;z&#x40;&#x6b;d&#x65;&#x2e;&#x6f;rg)</li>
<li>Threaded OSS support (should run more reliable on more kernel drivers)</li>
<li>Moved code to a separate CVS module</li>
</ul>

<h3>kdelibs</h3><ul>
<li>KSSL: Completion of certificate and CA management tools</li>
<li>KSSL: X.509 and PKCS12 certificate viewer and import tool part (KPart) - embeddable in Konqueror</li>
<li>KFileDialog: URL Speedbar</li>
<li>Support for Icons on Buttons in various dialogs</li>
<li>A GUI Item class that encapsulates KAction attributes</li>
<li>Added plugin interface for the Renaming Dialog</li>
<li>Improved service activation (dcopstart)</li>
<li>Support for Multi-key shortcuts (emacs-style) added.</li>
<li>WebDAV support</li>
<li>Plugin interface for retrieving / modifying meta information of files</li>
<li>KDirLister is now cached (i.e. directory listings of ftp servers in konqueror)</li>
<li>Optional emulation of traditional Mac keyboard</li>
<li>KDEPrint: Improved CUPS support.</li>    
</ul>

<h3>kdeaddons</h3><ul>
<li>Improved stability of some of the plugins</li>
</ul>

<h3>kdeadmin</h3><ul>
<li>Reinclusion of KDat</li>
</ul>

<h3>kdeartwork</h3><ul>
<li>Inclusion of several themes (icon, window decoration etc)</li>
</ul>

<h3>kdebase</h3><ul>
<li>KWin: smart mechanism that avoids focus stealing from windows the user is active on by windows that pop-up (M. Ettrich)</li>
<li>KWin: don't crash when popup-menu of a window is still visible when that window gets closed</li>
<li>KWin: don't shade/unshade (gross ugly flicker) windows that are moved fast in hover-unshaded state</li>
<li>KWin: deny to the masochist the resizing of a shaded window</li>
<li>KWin: automatically unshade on maximize, on restore-from-maximized and on restore-from-minimized</li>
<li>KWin: <b>work around ugly jre-1.3.1 bug</b> with popup dialogs vanishing forever after first use</li>
<li>KWin: improve moving by keyboard and bring back Ctrl-key ordered fine/coarse-grained keyboard moving</li>
<li>KWin: abort keyboard moving of windows with Escape too</li>
<li>KWin: no active desktop edges on resizing</li>
<li>KWin: don't warp mouse pointer when touching desktop edge (with active edges enabled) if desktop isn't actually changed</li>
<li>KWin: contain desktop navigation inside a box (don't wrap around from last to first desktop of a line or column)</li>
<li>KWin: don't stack windows under desktops</li>
<li>KWin: gracefully handle more than one desktop client application</li>
<li>KWin: fix bogus gravitating for non-NW-gravitated windows on session restore (i.e., no more drifting of Xclock when started with -geometry -0-0 or such)</li>
<li>don't allow &lt;any-modifier&gt;+Alt+mouse to do things as if it was Alt+mouse (L.Lunak)</li>
<li>any mouse button moves window when dragging titlebar, unless mouse click was popping an operations menu (this greatly improves consistency for configurable mouse bindings)</li>
<li>don't show operation menus for desktop (no more move desktop to desktop 1 %-)</li>
<li>KTip: center on screen</li>
<li>KTip: readable on dark color schemes</li>
<li>Kate: added plugin and new KTextEditor interface</li>
<li>Kate: XML Plugin</li>
<li>Konqueror/khtml: GUI for animated gifs: Always / Play Once / Never</li>
<li>Konqueror/khtml: Major rework of the ECMAScript ("Javascript") implementation</li>
<li>Konqueror/khtml: Major improviements in the DHTML compatibility</li>
<li>Konqueror/khtml: Added "smart" window.open Javascript policy that skips popup banners</li>
<li>Konqueror/khtml: Support for Actions in the new sidebar</li>
<li>Konqueror/Sidebar: Added "New directory" option</li>
<li>Konqueror/Sidebar: Added mediaplayer</li>
<li>Konqueror/fileview: Extended tooltips for information about files</li>
<li>Konqueror/popup plugins: Added "kuick", the quick copy and move plugin</li>
<li>Konsole: New parameters: --nomenubar, --noframe, --noscrollbar and -tn &lt;foo&gt; (set $TERM=&lt;foo&gt;)</li>
<li>Konsole: Keyboard shortcuts to activate menubar and rename session (Defaults: Ctrl-Alt-m &amp; Ctrl-Alt-s).</li>
<li>Konsole: New options: Blinking cursor, configurable line spacing, no/system/visible bell</li>
<li>Konsole: Monitoring for activity and/or silence, sending of input to all sessions (cluster management)</li>
<li>Konsole: History of a session can be cleared, searched and saved to a file.</li>
<li>Konsole: Session types can specify a working directory.</li>
<li>Konsole: Changed behaviour of &quot;New&quot; in toolbar, now starts session of type last selected.</li>
<li>Konsole: Session buttons display state (e.g. bell) and session type icons. Double click renames them.</li>
<li>Konsole: Sessions can be reordered via menu entries or keyboard shortcuts (Default: Ctrl-Shift-Left/Right).</li>
<li>Konsole: Extend selection until end of line if no more characters are printed on that line.</li>
<li>Konsole: Stop scrolling of output when selecting.</li>
<li>Konsole: Drag &amp; drop of selected text (like CDE's dtterm)</li>
<li>Konsole: Pressing Ctrl while pasting with middle mouse button will send selection buffer.</li>
<li>Konsole: Hollow out cursor when losing focus.</li>
<li>Konsole: Support for ScrollLock with LED display.</li>
<li>Konsole: Write utmp entries (requires installed utempter library).</li>
<li>Konsole: Proper implementation of secondary device attributes, MODE_Mouse1000 and wrapped lines.</li>
<li>Konsole: Session management remembers and activates last active session.</li>
<li>Konsole: DCOP interface, sets environment variables KONSOLE_DCOP &amp; KONSOLE_DCOP_SESSION</li>
<li>Konsole: Made embeddable Konsole part configurable.</li>
<li>Konsole: KDE Control Center: Added "Terminal Size Hint" option and session type editor.</li>
<li>Kicker: Implemented support for centerring the panel on screen</li>
<li>Kicker: new applet: kpf - a web server applet, designed for sharing files</li>
<li>KControl: Unified behaviour of root-only modules</li>
<li>KControl: Rearranged dialogs</li>
<li>KControl: Font Installation Assistant added</li>
</ul>

<h3>kdebindings</h3><ul>
<li>added Objective C bindings</li>
<li>added C bindings</li>
<li>updated and improved the existing Java bindings</li>
</ul>

<h3>kdegames</h3><ul>
<li>Various improvements to the games</li>
<li>Generalized more functionality into a libkdegames</li>
</ul>

<h3>kdegraphics</h3><ul>
<li>KDvi: Copy and paste text from a DVI file</li>
<li>KDvi: Full text search</li>
<li>KDvi: Export DVI files to plain text</li>
<li>KDvi: Forward search with Emacs and XEmacs</li>
<li>KDvi: Inverse search with a variety of editory</li>
<li>KDvi: DCOP interface</li>
<li>KDvi: Improved commandline options</li>
</ul>

<h3>kdemultimedia</h3><ul>
<li>Noatun: Global XML import/export for the playlist</li>
<li>Noatun: Winamp skin loader</li>
<li>Noatun: Icecast / shoutcast streaming</li>
<li>Noatun: Hide close status und tag displaying</li>
</ul>

<h3>kdenetwork</h3><ul>
<li>KMail: Maildir support</li>
<li>KMail: Distribution lists and aliases</li>
<li>KMail: SMTP authentication</li>
<li>KMail: SMTP over SSL/TLS</li>
<li>KMail: Pipelining for POP3 (faster mail download on slow responding networks)</li>
<li>KMail: On demand downloading or deleting without downloading of big mails on a POP3 server</li>
<li>KMail: Various improvements for IMAP</li>
<li>KMail: Permanent header caching</li>
<li>KMail: Header fetching is <i>much</i> faster</li>
<li>KMail: Creating/removing of folders</li>
<li>KMail: Drats/sent-mail/trash folders on the server</li>
<li>KMail: Mail checking in all folders</li>
<li>KMail: Automatic configuration of the POP3/IMAP/SMTP security features</li>
<li>KMail: Automatic encoding selection for outgoing mails</li>
<li>KMail: DIGEST-MD5 authentication</li>
<li>KMail: Identity based sent-mail and drafts folders</li>
<li>KMail: Expiry of old messages</li>
<li>KMail: Hotkey to temporary switch to fixed width fonts</li>
<li>KMail: UTF-7 support</li>
<li>KMail: Enhanced status reports for encrypted/signed messages</li>
</ul>

<h3>KDEPIM</h3><ul>
<li>New Addressbook API (libkabc). Ported applications to use the new API</li>
<li>KPilot: Rework conduits as plugins</li>
<li>KPilot: Support for USB Visors</li>
<li>KPilot: Extensive addition of tooltips</li>
<li>KPilot: Move to .ui files as much as possible</li>
<li>KOrganizer: Plugin interface</li>
<li>KOrganizer: Group scheduling</li>
<li>KOrganizer: Split alarm daemon in a lowlevel and a GUI frontend</li>
<li>KOrganizer: pinning contacts to appointments and TODO's</li>
</ul>

<h3>KDESDK</h3><ul>
<li>KBabel: Catalog Manager is now a standalone application</li>
<li>KBabel: Find/Replace in <em>all</em> files</li>
</ul>

<h3>KDEToys</h3><ul>
<li>New Applet: KWeather</li>
<li>KWeather: Better reportview, support for european weather data</li>
<li>KWeather: Improved report view, uses http to get the data more quickly</li>
<li>KWeather: Improved METAR parser support</li>
<li>KWeather: added DCOP interface</li>
<li>KWeather: improved support for iconscaling</li>
</ul>

<h3>KDEUtils</h3><ul>
<li>KRegExpEditor: new</li>
<li>Kpm got replaced by ksysguard</li>
</ul>

<h3>KDEEdu</h3><ul>
<li>New in KDE 3.0, a collection of edu(cation/tainmnent) applications for KDE</li>
</ul>

<!-- Created: Fri Mar 22 22:00:22 EST 2002 -->
<!-- hhmts start -->

Last modified: Sat Apr 6 21:32:57 EST 2002

<!-- hhmts end -->