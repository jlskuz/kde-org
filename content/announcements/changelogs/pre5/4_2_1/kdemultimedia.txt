------------------------------------------------------------------------
r915640 | scripty | 2009-01-23 13:14:49 +0000 (Fri, 23 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r915880 | mpyne | 2009-01-24 01:55:16 +0000 (Sat, 24 Jan 2009) | 5 lines

Only stop playback if the stopped event came while we were playing (instead of the normal Loading -> Stop)
sequence if JuK didn't commence playback quickly enough.  Backported from trunk, will be in KDE 4.2.1.

BUG:176329

------------------------------------------------------------------------
r918041 | mpyne | 2009-01-29 01:03:38 +0000 (Thu, 29 Jan 2009) | 4 lines

Backport fix for bug 175035 (crash on JuK shutdown) to KDE 4.2.1.

BUG:175035

------------------------------------------------------------------------
r918629 | hasso | 2009-01-30 11:47:31 +0000 (Fri, 30 Jan 2009) | 2 lines

Backport rev 918612 - make it build on DragonFly.

------------------------------------------------------------------------
r918630 | hasso | 2009-01-30 11:49:11 +0000 (Fri, 30 Jan 2009) | 2 lines

Backport rev 918616 - fix SOUND_MIXER_INFO ioctl usage.

------------------------------------------------------------------------
r918649 | hasso | 2009-01-30 12:33:17 +0000 (Fri, 30 Jan 2009) | 2 lines

Backport rev 918641 - make it find cdparanoia headers from ${incs}/cdparanoia.

------------------------------------------------------------------------
r918650 | hasso | 2009-01-30 12:34:45 +0000 (Fri, 30 Jan 2009) | 2 lines

Backport rev 918643 - attempt to make it work on DragonFly.

------------------------------------------------------------------------
r918690 | winterz | 2009-01-30 14:20:57 +0000 (Fri, 30 Jan 2009) | 5 lines

backport SVN commit 918684 by winterz

have the buildsystem check for unistd.h
and include it in videoWindow.cpp (for usleep)

------------------------------------------------------------------------
r921398 | davidedmundson | 2009-02-04 23:35:25 +0000 (Wed, 04 Feb 2009) | 33 lines

Backport all the fixes made to Dragonplayer after 4.2.0
Same as trunk revision 918684 except "reset video settings" feature not backported as includes new translation

add a check for unistd.h to the buildsystem.
and include unistd.h in dragonplayer/src/app/videoWindow.cpp, for usleep.
Fixed crash when loading Video after using VideoSettings Dialog, and VideoSettings dialog now only accessible if 
playing a Video file
BUG:177623
BUG:165249
Check channel validity before setting audio channel.
BUG: 171301
Fixes bug 171301 by adding a "Restore defaults" button to video settings dialog and looking for the "valueChanged" 
signal instead of "sliderMoved" for the Vdeo settings slider, ensuring that it will recognise all movements, not 
just those caused by dragging with mouse
When track finished pushing "play" now restarts track
Support for additional MediaSource state 'empty'
BUG:163099 Video window sometimes doesn't resize when opening a file with a restore position.
Supress powermanager from sleeping when playing a file
Replace all references to CODEINE with DRAGONPLAYER
Remove unused code. Fixed compile warning
Implements TheStream::prettyTitle() and removes two unused signals statusMessage(const QString &) and
titleChanged(const QString &) (Now obsolete due to MediaObject::metaDataChanged())
Code tidy up by Paul B
Fix pause button in DragonPart
Enable position slider based on seekableChanged phonon signal
Fix compile
extra ';'
Revert dbus FileChanged signal broken in last commit
Fix compiler warning on inline consts
Remove all of custom Engine::State enum and replace with Phonon::State
General refactor of state changing 


------------------------------------------------------------------------
r921564 | scripty | 2009-02-05 08:39:31 +0000 (Thu, 05 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r926737 | mpyne | 2009-02-16 02:25:11 +0000 (Mon, 16 Feb 2009) | 6 lines

Backport a slightly more correct revision 909740 to 4.2 branch to fix a JuK bug where a PlaylistItem could
be deleted while cached in some data structures of its parent Playlist.  I believe this fixes bug 169125 (at
least the initial crash, I still need to test removable media).

CCBUG:169125

------------------------------------------------------------------------
r928206 | scripty | 2009-02-19 06:56:49 +0000 (Thu, 19 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r928795 | scripty | 2009-02-20 07:22:34 +0000 (Fri, 20 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r931261 | mpyne | 2009-02-25 02:24:43 +0000 (Wed, 25 Feb 2009) | 1 line

SVN_SILENT Bump JuK version in preparation for 4.2.1 (yes, 4.2 had 3.2.1 because I messed up)
------------------------------------------------------------------------
