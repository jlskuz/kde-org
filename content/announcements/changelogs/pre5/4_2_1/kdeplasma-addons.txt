------------------------------------------------------------------------
r915135 | scripty | 2009-01-22 13:52:57 +0000 (Thu, 22 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r916989 | mfuchs | 2009-01-26 16:41:41 +0000 (Mon, 26 Jan 2009) | 3 lines

Backport r915780
Not autohiding the tooltip works now.

------------------------------------------------------------------------
r917004 | mfuchs | 2009-01-26 17:03:36 +0000 (Mon, 26 Jan 2009) | 4 lines

Backport r915755
Adds the option to set the text codec as it is not detected in some
cases.

------------------------------------------------------------------------
r917030 | dfaure | 2009-01-26 18:10:30 +0000 (Mon, 26 Jan 2009) | 2 lines

fix runtime warning, patch by zlin on irc

------------------------------------------------------------------------
r917599 | murrant | 2009-01-28 05:04:50 +0000 (Wed, 28 Jan 2009) | 2 lines

Add basic tooltip when the nowplaying applet is in the panel. Backported from r906904.

------------------------------------------------------------------------
r917643 | annma | 2009-01-28 09:57:36 +0000 (Wed, 28 Jan 2009) | 3 lines

keep color change when changed through context menu
BUG=182018

------------------------------------------------------------------------
r918225 | scripty | 2009-01-29 17:19:46 +0000 (Thu, 29 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r920673 | mfuchs | 2009-02-03 15:29:19 +0000 (Tue, 03 Feb 2009) | 3 lines

Backport r920249 and r920251
Fixes that comics with identifier=0 were not possible.

------------------------------------------------------------------------
r922008 | scripty | 2009-02-06 08:23:57 +0000 (Fri, 06 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r922561 | scripty | 2009-02-07 08:50:51 +0000 (Sat, 07 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r922685 | mfuchs | 2009-02-07 14:09:29 +0000 (Sat, 07 Feb 2009) | 2 lines

Backport r922684

------------------------------------------------------------------------
r923078 | scripty | 2009-02-08 07:42:12 +0000 (Sun, 08 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r924304 | mfuchs | 2009-02-10 14:11:25 +0000 (Tue, 10 Feb 2009) | 3 lines

Backport r924279
CCBUG:181120

------------------------------------------------------------------------
r924606 | scripty | 2009-02-11 08:24:33 +0000 (Wed, 11 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r926348 | scripty | 2009-02-15 07:26:22 +0000 (Sun, 15 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r929135 | ogoffart | 2009-02-20 18:43:04 +0000 (Fri, 20 Feb 2009) | 6 lines

Backport r929134
Fix the pupil position when the mouse is exactly on the same vertical as the pupil




------------------------------------------------------------------------
r929343 | scripty | 2009-02-21 07:32:06 +0000 (Sat, 21 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r929577 | alexmerry | 2009-02-21 16:44:27 +0000 (Sat, 21 Feb 2009) | 5 lines

Backport r929567: Respond properly to slider value changes.  Makes mouse-wheel scrolling work as expected.

CCBUG: 182693


------------------------------------------------------------------------
r929790 | scripty | 2009-02-22 07:12:11 +0000 (Sun, 22 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r930775 | scripty | 2009-02-24 08:20:47 +0000 (Tue, 24 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
