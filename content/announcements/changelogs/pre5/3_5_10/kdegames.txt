2008-05-12 11:12 +0000 [r806767]  aacid

	* branches/KDE/3.5/kdegames/kmines/solver/Makefile.am: Make having
	  -Wl,--as-needed in LDFLAGS not make fail in 'make check' BUGS:
	  149178

2008-06-24 15:37 +0000 [r824000]  anagl

	* branches/KDE/3.5/kdegames/katomic/gamewidget.cpp: kapplication.h
	  needs to be included here to get this building in alpha (Debian).

2008-06-24 15:41 +0000 [r824004]  anagl

	* branches/KDE/3.5/kdegames/kgoldrunner/src/KGoldrunner.desktop,
	  branches/KDE/3.5/kdegames/ktuberling/ktuberling.desktop,
	  branches/KDE/3.5/kdegames/kasteroids/kasteroids.desktop,
	  branches/KDE/3.5/kdegames/kbackgammon/kbackgammon.desktop,
	  branches/KDE/3.5/kdegames/kpoker/kpoker.desktop,
	  branches/KDE/3.5/kdegames/kolf/kolf.desktop,
	  branches/KDE/3.5/kdegames/ksokoban/data/ksokoban.desktop,
	  branches/KDE/3.5/kdegames/knetwalk/src/knetwalk.desktop,
	  branches/KDE/3.5/kdegames/ksnake/ksnake.desktop,
	  branches/KDE/3.5/kdegames/kenolaba/kenolaba.desktop,
	  branches/KDE/3.5/kdegames/lskat/lskat.desktop,
	  branches/KDE/3.5/kdegames/kpat/kpat.desktop,
	  branches/KDE/3.5/kdegames/kspaceduel/kspaceduel.desktop,
	  branches/KDE/3.5/kdegames/kolf/x-kourse.desktop,
	  branches/KDE/3.5/kdegames/kfouleggs/kfouleggs.desktop,
	  branches/KDE/3.5/kdegames/kbounce/kbounce.desktop,
	  branches/KDE/3.5/kdegames/ksmiletris/ksmiletris.desktop,
	  branches/KDE/3.5/kdegames/atlantik/atlantik.desktop,
	  branches/KDE/3.5/kdegames/katomic/katomic.desktop,
	  branches/KDE/3.5/kdegames/konquest/konquest.desktop,
	  branches/KDE/3.5/kdegames/kreversi/kreversi.desktop,
	  branches/KDE/3.5/kdegames/kmines/data/kmines.desktop,
	  branches/KDE/3.5/kdegames/kwin4/kwin4.desktop,
	  branches/KDE/3.5/kdegames/ktron/ktron.desktop,
	  branches/KDE/3.5/kdegames/kolf/x-kolf.desktop,
	  branches/KDE/3.5/kdegames/kbattleship/kbattleship/kbattleship.desktop,
	  branches/KDE/3.5/kdegames/klickety/klickety.desktop,
	  branches/KDE/3.5/kdegames/ksirtet/ksirtet/ksirtet.desktop,
	  branches/KDE/3.5/kdegames/kblackbox/kblackbox.desktop,
	  branches/KDE/3.5/kdegames/ksame/ksame.desktop,
	  branches/KDE/3.5/kdegames/ktuberling/x-tuberling.desktop,
	  branches/KDE/3.5/kdegames/kjumpingcube/kjumpingcube.desktop,
	  branches/KDE/3.5/kdegames/kshisen/kshisen.desktop,
	  branches/KDE/3.5/kdegames/klines/klines.desktop,
	  branches/KDE/3.5/kdegames/kmahjongg/kmahjongg.desktop: Desktop
	  validation fixes: -remove deprecated entries SwallowExec and
	  SwallowTitle. -remove deprecated entries for Encoding.

2008-08-19 19:47 +0000 [r849598]  coolo

	* branches/KDE/3.5/kdegames/kdegames.lsm: update for 3.5.10

