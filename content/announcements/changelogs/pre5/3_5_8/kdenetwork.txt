2007-05-16 20:25 +0000 [r665405]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/ui/kopetelistview.h,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/appearance/appearanceconfig.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/appearance/appearanceconfig_contactlist.ui,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteprefs.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/contactlist/kopetecontactlistview.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/ui/kopetelistview.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteprefs.h:
	  Remove smooth scrolling and auto-hiding scrollbar functions, as
	  discussed on kopete-devel this week, to solve CPU waking problems
	  caused by rampant timer overuse.

2007-05-17 06:53 +0000 [r665563]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetepassword.cpp:
	  When operating in insecure password storage mode, replace buggy
	  kmail password code with KStringHandler::obscure()

2007-05-22 16:49 +0000 [r667377]  helio

	* branches/KDE/3.5/kdenetwork/wifi/kcmwifi/kcmwifi.desktop,
	  branches/KDE/3.5/kdenetwork/krfb/kcm_krfb/kcmkrfb.desktop,
	  branches/KDE/3.5/kdenetwork/wifi/kwifimanager.desktop,
	  branches/KDE/3.5/kdenetwork/kget/kget.desktop,
	  branches/KDE/3.5/kdenetwork/kppp/Kppp.desktop,
	  branches/KDE/3.5/kdenetwork/ktalkd/kcmktalkd/kcmktalkd.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/kopete.desktop,
	  branches/KDE/3.5/kdenetwork/filesharing/simple/fileshare.desktop,
	  branches/KDE/3.5/kdenetwork/krdc/krdc.desktop,
	  branches/KDE/3.5/kdenetwork/ksirc/ksirc.desktop,
	  branches/KDE/3.5/kdenetwork/filesharing/advanced/kcm_sambaconf/kcmsambaconf.desktop,
	  branches/KDE/3.5/kdenetwork/knewsticker/knewsticker-standalone.desktop,
	  branches/KDE/3.5/kdenetwork/kdict/kdict.desktop,
	  branches/KDE/3.5/kdenetwork/krfb/krfb/krfb.desktop,
	  branches/KDE/3.5/kdenetwork/kppp/logview/kppplogview.desktop:
	  Fixed freedesktop categories in desktop files. Provided by
	  Nicolas Lécureuil and reviewed at core-devel

2007-05-22 17:29 +0000 [r667396]  boiko

	* branches/KDE/3.5/kdenetwork/wifi/kcmwifi/kcmwifi.desktop,
	  branches/KDE/3.5/kdenetwork/krfb/kcm_krfb/kcmkrfb.desktop,
	  branches/KDE/3.5/kdenetwork/ktalkd/kcmktalkd/kcmktalkd.desktop,
	  branches/KDE/3.5/kdenetwork/filesharing/simple/fileshare.desktop,
	  branches/KDE/3.5/kdenetwork/filesharing/advanced/kcm_sambaconf/kcmsambaconf.desktop:
	  Configuration modules shouldn't receive the Network category

2007-05-22 17:48 +0000 [r667399]  boiko

	* branches/KDE/3.5/kdenetwork/kget/kget.desktop: KGet is not a P2P
	  application

2007-05-23 21:01 +0000 [r667758]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/jabbergroupcontact.cpp:
	  commit patch from bug 135211 to fix that bug. Show all contacts
	  history for jabber groupchats. Thanks for the patch! BUG: 135211

2007-05-24 23:19 +0000 [r668060]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwcontact.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwaccount.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwcontact.h:
	  Enable Kopete to respond to chats started by others who are
	  blocking you (Novell bug #267832)

2007-05-26 16:15 +0000 [r668496]  mlaurent

	* branches/KDE/3.5/kdenetwork/filesharing/advanced/kcm_sambaconf/kcmsambaconf.cpp:
	  Fix mem leak

2007-06-08 18:56 +0000 [r673005-673004]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/kopete.kdevelop,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h:
	  we're on 0.12.6 now, mkay?

	* branches/KDE/3.5/kdenetwork/kopete/kopete.kdevelop (removed): I
	  thought we removed this file...

2007-06-08 19:44 +0000 [r673027]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/aim/aimcontact.cpp:
	  fix bug 146380 by removing the last <br> tag from the message
	  before sending Based on a patch by Guillermo A. Amaral. Thanks
	  for the patch! The fix should appear in KDE 3.5.8 when/if that is
	  released. BUG: 146380 CCMAIL: me@guillermoamaral.com

2007-06-09 01:27 +0000 [r673109]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp:
	  fix bugs 140528 and 143376. BUGS: 140528, 143376

2007-06-09 01:37 +0000 [r673114]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/Makefile.am: install
	  kopetepluginmanager.h

2007-06-09 01:51 +0000 [r673118]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/kopetechatwindow.h,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/kopetechatwindow.cpp:
	  Commit patch by Josh Berry to fix bug 59080. Thanks for the
	  patch! BUG: 59080

2007-06-09 02:58 +0000 [r673127]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/kopete/systemtray.cpp: fix bug
	  133186 by escaping the nickname text when passing it to the
	  balloon. For the kOne chat style, that would be a bug in the
	  style since it works here with the default Kopete style. BUG:
	  133186

2007-06-13 13:20 +0000 [r674906]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/yahooaccount.cpp:
	  fix adding of yahoo contacts

2007-06-13 14:24 +0000 [r674921]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/client.cpp:
	  Fix crash after duplicate login.

2007-06-21 15:28 +0000 [r678509]  binner

	* branches/KDE/3.5/kdenetwork/kopete/kopete/kopete.desktop: fix
	  invalid .desktop file

2007-06-21 17:14 +0000 [r678555]  jritzerfeld

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/behavior/behaviorconfig.cpp:
	  Check mInstantMessageOpeningChk radio button if neither
	  mUseQueueChk nor mUseStackChk is checked. BUG: 146882

2007-07-06 17:16 +0000 [r684469]  mlaurent

	* branches/KDE/3.5/kdenetwork/filesharing/advanced/propsdlgplugin/propertiespage.cpp:
	  Backport "fix mem leak"

2007-07-16 12:44 +0000 [r688617]  binner

	* branches/KDE/3.5/kdenetwork/filesharing/advanced/propsdlgplugin/propsdlgshareplugin.cpp,
	  branches/KDE/3.5/kdenetwork/wifi/kwifimanager.cpp: ensure that
	  the correct kcmshell gets called

2007-07-26 09:57 +0000 [r692802]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/chatroommanager.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/gwerror.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/searchchattask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwaccount.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/conferencetask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/task.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/getchatsearchresultstask.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/ui/gwchatsearchdialog.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/gwerror.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/chatroommanager.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/task.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/modifycontactlisttask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/Makefile.am:
	  *) Better error reporting *) Fix error in chatroom search so that
	  all chatrooms are listed *) Fetch user info for invitation
	  declined event user if we joined after the invitation was made *)
	  Deactivate the Join Chatrooms action when offline, fix crash

2007-08-24 18:29 +0000 [r704366]  taupter

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.h,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videoinput.h,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videocontrol.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicepool.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videocontrol.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/Makefile.am:
	  Backporting changes made to the Comunip branch, as discussed with
	  Matt Rogers.

2007-08-24 19:20 +0000 [r704382]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnswitchboardsocket.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/dispatcher.cpp:
	  fix msn live emoticons. Patch from Flavio. Thanks! CCMAIL:
	  flavio@flavioweb.it

2007-08-29 09:16 +0000 [r706035]  bvirlet

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/behavior/behaviorconfig.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteprefs.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/behavior/behaviorconfig_general.ui,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteprefs.h:
	  Remove the option because this was removed by the commits against
	  smooth scrolling and this is not standard behavior.
	  https://bugzilla.novell.com/show_bug.cgi?id=304786 CCBUG: 146541

2007-08-29 14:15 +0000 [r706131]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/client.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwaccount.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/client.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwaccount.h:
	  Fix message loss bug (bnc:299155) that manifests when a user goes
	  offline, and reconnects, while keeping a chatwindow open all the
	  time. The old GUID was not invalidated, and subsequent messages
	  to this GUID were silently dropped. Warn the user if this
	  happens, and invalidate all GUIDs on any disconnect.

2007-08-31 16:50 +0000 [r706961]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/irc/ircnetworks.xml:
	  Prune obsolete Freenode servers and correct description.

2007-09-08 11:34 +0000 [r709856]  savernik

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp:
	  Restore support for V4L < 2

2007-09-20 21:28 +0000 [r714909]  mueller

	* branches/KDE/3.5/kdenetwork/ksirc/KSTicker/speeddialog.cpp,
	  branches/KDE/3.5/kdenetwork/ksirc/KSTicker/speeddialogData.cpp:
	  the usual "unbreak compilation"

2007-09-25 09:26 +0000 [r716800]  tpatzig

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicepool.cpp:
	  -removed redundant code block without initializing
	  m_currentDevice, which causes crash when scanning for video
	  devices

2007-09-26 03:24 +0000 [r717103]  jkekkonen

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicepool.cpp:
	  reverting r716800 because it causes crash with and without
	  devices, tpatzig come to kopete-devel@kde.org to find out the
	  solution to this with us, thanks

2007-09-26 03:26 +0000 [r717104]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/webcam/libmimic/mimic-private.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/webcam/libmimic/vlc_common.c:
	  Port patch from aMSN to fix issue with MSN webcam images being
	  garbled. Should be in KDE 3.5.8 BUG: 126996

2007-09-29 12:26 +0000 [r718647]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/ymsgprotocol.cpp:
	  Rewrite the code that parses the key-value pairs. That probably
	  fixes #145514. BUG:145514

2007-10-04 18:31 +0000 [r721162]  taupter

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicepool.cpp:
	  Fixes VideoDevicePool::scanDevices()'s return code when devices
	  are found in the 2nd step of scan.

2007-10-05 02:02 +0000 [r721341]  rm

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/oscartypes.h:
	  fix version...ok?

2007-10-08 11:05 +0000 [r722969]  coolo

	* branches/KDE/3.5/kdenetwork/kdenetwork.lsm: updating lsm

2007-10-08 11:14 +0000 [r722984]  coolo

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h:
	  assume it's a newer version

