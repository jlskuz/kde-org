------------------------------------------------------------------------
r1047729 | cguthrie | 2009-11-11 22:27:03 +0000 (Wed, 11 Nov 2009) | 7 lines

kmix(pulse): Fix double unref of context

This is a simple fix to prevent an assert. The general method of
reallocating a single static context is in itself invalid and will
be addressed in future commits, but it wont crop up with the current
code structure.
CCBUG: 194178
------------------------------------------------------------------------
r1053069 | scripty | 2009-11-23 04:17:03 +0000 (Mon, 23 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1054460 | mpyne | 2009-11-26 02:35:59 +0000 (Thu, 26 Nov 2009) | 5 lines

Clear the now playing bar's filtering if the now playing bar is hidden. This is a backport
of the trunk bugfix to KDE 4.3.4.

BUG:112238

------------------------------------------------------------------------
r1054467 | mpyne | 2009-11-26 03:16:40 +0000 (Thu, 26 Nov 2009) | 3 lines

Fix unknown icon when dragging a track. Backported from trunk, will be in
KDE Software Compilation 4.3.4.

------------------------------------------------------------------------
