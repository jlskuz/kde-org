------------------------------------------------------------------------
r1044117 | scripty | 2009-11-03 04:07:46 +0000 (Tue, 03 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1045405 | shaforo | 2009-11-06 00:07:07 +0000 (Fri, 06 Nov 2009) | 2 lines

fix a bad bug

------------------------------------------------------------------------
r1046495 | dhaumann | 2009-11-08 21:35:58 +0000 (Sun, 08 Nov 2009) | 7 lines

backport SVN commit 1046420 by jowenn:

Only show the warning dialog if the state of the sidebars has really changed, so the warning dialog is not
 shown anymore at startup if a session has disabled sidebars.

CCBUG:212218

------------------------------------------------------------------------
r1048467 | shaforo | 2009-11-13 11:57:00 +0000 (Fri, 13 Nov 2009) | 10 lines

avoid destroying m_multiEditorAdaptor explicitly
hopefully this adds more reliability

BUG: 207201

Marta: i've added some precautions, please report if the crashes repeat with Lokalize from KDE 4.3.4 (yet to be released)




------------------------------------------------------------------------
r1048674 | shaforo | 2009-11-13 18:12:37 +0000 (Fri, 13 Nov 2009) | 3 lines

generate PO more closely to gettext-tools


------------------------------------------------------------------------
r1050760 | scripty | 2009-11-18 04:10:36 +0000 (Wed, 18 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1051240 | scripty | 2009-11-19 04:15:36 +0000 (Thu, 19 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1054481 | scripty | 2009-11-26 04:11:11 +0000 (Thu, 26 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
