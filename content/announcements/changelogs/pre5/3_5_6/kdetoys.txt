2006-10-21 08:25 +0000 [r597654]  fabo

	* branches/KDE/3.5/kdetoys/kweather/weather_stations.desktop: BUG:
	  104988 Add Lithuania to the country list of kweather. Add weather
	  stations that are available in Lithuania: - Vilnius (EYVI) as
	  "Vilnius" - Kaunas (EYKA) as "Kaunas" - Siauliai Intl./Mil.
	  (EYSA) as "Siauliai" - Palanga Intl. (EYPA) as "Palanga" Thanks
	  to Modestas Vainius

2006-11-14 17:27 +0000 [r604932]  lueck

	* branches/KDE/3.5/kdetoys/doc/kteatime/index.docbook: backport
	  from trunk CCMAIL:schwarzerf@gmail.com

2006-11-25 01:28 +0000 [r607543]  schwarzer

	* branches/KDE/3.5/kdetoys/doc/kteatime/config.png: screenshot
	  updated

2006-12-23 18:40 +0000 [r616122]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/stations.dat: correct typo

2006-12-23 23:11 +0000 [r616157]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/sun.cpp: BUG: 137448 correct
	  calculation of sunset/sunrise

2006-12-23 23:56 +0000 [r616167-616165]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/weather_stations.desktop: BUG:
	  120146 Add missing Karlsruhe Correct typo for Voslau

	* branches/KDE/3.5/kdetoys/kweather/stationdatabase.cpp: read
	  station names in UTF-8 to allow for non-ascii chars

2006-12-24 00:44 +0000 [r616177]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/stations.dat: BUG: 135244
	  correct CYXD name

2006-12-24 21:05 +0000 [r616273]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/kcmweather.cpp,
	  branches/KDE/3.5/kdetoys/kweather/kweather.cpp,
	  branches/KDE/3.5/kdetoys/kweather/kcmweatherservice.cpp,
	  branches/KDE/3.5/kdetoys/kweather/kcmweather.h,
	  branches/KDE/3.5/kdetoys/kweather/kweather.h,
	  branches/KDE/3.5/kdetoys/kweather/prefdialogdata.ui,
	  branches/KDE/3.5/kdetoys/kweather/dockwidget.cpp: BUG: 124754
	  BUG: 138047 BUG: 136683 BUG: 126296 BUG: 113828 BUG: 122850 BUG:
	  118458 BUG: 116375 BUG: 103887 BUG: 76300 BUG: 73093 Long
	  standing bug: make selection of a station work

2006-12-25 15:10 +0000 [r616456]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/metar_parser.cpp,
	  branches/KDE/3.5/kdetoys/kweather/sun.cpp: BUG: 136312 BUG:
	  129679 BUG: 115920 BUG: 113339 BUG: 87642 Fix the calculation of
	  sunrise/sunset and the calculation of isNight. Patch provided by
	  J.O. Aho

2006-12-25 15:14 +0000 [r616459]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/reportview.cpp,
	  branches/KDE/3.5/kdetoys/kweather/dockwidget.cpp: Also show
	  country name in report/tooltip (Yes, there is a La Paz in Mexico
	  ...)

2006-12-25 15:28 +0000 [r616461]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/stations.dat: update to current
	  version from http://weather.noaa.gov/data/nsd_cccc.gz

2006-12-25 22:26 +0000 [r616506]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/reportview.cpp,
	  branches/KDE/3.5/kdetoys/kweather/weatherlib.cpp,
	  branches/KDE/3.5/kdetoys/kweather/dockwidget.cpp,
	  branches/KDE/3.5/kdetoys/kweather/metar_parser.cpp: BUG: 97107
	  BUG: 134426 BUG: 108427 Display weather data even if station
	  needs maintenance Patch included from Sok Ann Yap
	  sokann@gmail.com Make the tooltip more informative by showing
	  station name and more data

2006-12-28 00:01 +0000 [r617100]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/weather_stations.desktop: Added
	  one missing station

2006-12-28 00:06 +0000 [r617101]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/weatherbutton.h,
	  branches/KDE/3.5/kdetoys/kweather/dockwidget.cpp: BUG: 81098 BUG:
	  73202 BUG: 134387 Fix layouting in all different display modes
	  and kicker orientation modes Always use a sane font size and
	  limit overall size of applet

2007-01-15 09:15 +0000 [r623692]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/VERSION,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: increase version

