---
aliases:
- ../changelog4_8_3to4_8_4
hidden: true
title: KDE 4.8.4 Changelog
---

<h2>Changes in KDE 4.8.4</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_8_4/kdelibs.txt">all SVN changes</a> ]</span></h3>
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kdeui">kdeui</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Do not show email icon in the About dialog if the author has no email associated. See Git commit <a href="http://commits.kde.org/kdelibs/c9b6191945a2416156e418712dca1fda7f4a8820">c9b6191</a>. </li>
      </ul>
      </div>
      <h4><a name="khtml">khtml</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Various rendering fixes for select elements (combobox + listview). Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=299260">299260</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=299741">299741</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=299934">299934</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=300227">300227</a>.  See Git commits <a href="http://commits.kde.org/kdelibs/fcc12b02f685ed860f6187571b6e14fd3b61285a">fcc12b0</a>, <a href="http://commits.kde.org/kdelibs/9169e3286911f102dda285408ce003337967744b">9169e32</a>, <a href="http://commits.kde.org/kdelibs/a5c79cf442b4f8875d2f8eb2ba4c7e01959986e5">a5c79cf</a>, <a href="http://commits.kde.org/kdelibs/38ee1c13b5670458a7208be6822ec8fe5d68ed73">38ee1c1</a> and <a href="http://commits.kde.org/kdelibs/b83af04037d2288e2242dfae5f4c7f8877de214e">b83af04</a>. </li>
        <li class="bugfix crash">Fix a segmentation fault caused by duplicated mouse move events. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=243910">243910</a>.  See Git commit <a href="http://commits.kde.org/kdelibs/5feb2da93c4fcd18d3a38659abb9fb040704d123">5feb2da</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="allsvnchanges"> [ <a href="4_8_4/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash when saving new alarm. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=300376">300376</a>.  See Git commit <a href="http://commits.kde.org/kdepim/1f9a2a30176d8dfc4bcdd5aed97645ae02c1ac50">1f9a2a3</a>. </li>
        <li class="bugfix normal">Warn user and disable KAlarm if Akonadi fails to run. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=300083">300083</a>.  See Git commit <a href="http://commits.kde.org/kdepim/37458cc29f531186d7453ef955b7de330d02ecad">37458cc</a>. </li>
      </ul>
      </div>
      <h4><a name="kmail">kmail</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Save size of message/mime splitter on size change. See Git commit <a href="http://commits.kde.org/kdepim/dd562605699304f26f1bfe914dda6982894e6077">dd56260</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_8_4/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/kgpg" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Do not refresh keys that are unchanged after download from keyserver. See Git commit <a href="http://commits.kde.org/kgpg/be8ce4498e512a97f9c434c9d5587db1b4b4a838">be8ce44</a>. </li>
        <li class="bugfix normal">Prevent that a key shows up twice after reloading it from keyserver. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=299044">299044</a>.  See Git commit <a href="http://commits.kde.org/kgpg/2fe1b683cec044f96cf973ea2feace70aeb7e6f5">2fe1b68</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeworkspace"><a name="kdeworkspace">kdeworkspace</a><span class="allsvnchanges"> [ <a href="4_8_4/kdeworkspace.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
            <h4><a name="kwin">kwin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Crashing and forcing number of desktops to one Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=174118">174118</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/0910f10fbdc0e3a5165011055be35a91836b8d29">0910f10</a>. </li>
        <li class="bugfix crash">kwin crash switching desktop Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=285747">285747</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/19c0fa5abd90a46de2ef6949a15de31111f930f4">19c0fa5</a>. </li>
        <li class="bugfix crash">KWin crash, when trying to use the new qml-based switcher Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=290482">290482</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/19c0fa5abd90a46de2ef6949a15de31111f930f4">19c0fa5</a>. </li>
        <li class="bugfix normal">Shader Effects get loaded with OpenGL 1 backend Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=299426">299426</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/db42152def79381547965eaadb3771f0d2fe908c">db42152</a>. </li>
        <li class="bugfix normal">Screen Transformation not reset after using Cube Effect Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=299869">299869</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/6a950aa510e33ce77b15608f19f6032176acd1f0">6a950aa</a>. </li>
        <li class="bugfix normal">mplayer2 loses keyboard input after switching to fullscreen Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=300245">300245</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/b6081d623550aa2d1145d3b3b5ceafbc119e12eb">b6081d6</a>. </li>
        <li class="bugfix crash">The composition with OpenGL (the default) has sent in KWin crashes in the past. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=300768">300768</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/08c10bf6961dbb97e3ff0bc5ae14c3e746ca4980">08c10bf</a>. </li>
      </ul>
      </div>
        </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_8_4/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://dolphin.kde.org" name="dolphin">dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Improve arrow key navigation in details-view with expanded folders. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=300582">300582</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/93daadae28f7e3b5cffd7b713a2a287e4fa62ccd">93daada</a>. </li>
        <li class="bugfix normal">Show all items in the directory when the name filter is cleared. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=300504">300504</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/288473a96cdd8888f7fc91d0a551d6cbde5fd5dc">288473a</a>. </li>
        <li class="bugfix normal">Assure that a newly created item gets selected and visible. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=291064">291064</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/0139537a35d636356fa088c61557baf13bb2e71b">0139537</a>. </li>
      </ul>
      </div>
      <h4><a name="konsole">konsole</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Do not forward the button release event when Shift is also pressed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=299437">299437</a>.  See Git commit <a href="http://commits.kde.org/konsole/efae7d95366a240b953425a1ee6dea15de45332c">efae7d9</a>. </li>
        <li class="bugfix normal">Memory-based history should save and restore the 'isRealCharacter' field. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=294330">294330</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=299252">299252</a>.  See Git commit <a href="http://commits.kde.org/konsole/cc72338175bd8c08d4998de606e0a48ee9e6d54f">cc72338</a>. </li>
      </ul>
      </div>
    </div>