------------------------------------------------------------------------
r1169576 | cfeck | 2010-08-30 06:07:01 +1200 (Mon, 30 Aug 2010) | 5 lines

Fix resizing grip handles causing X11 size overflow (backport r1169574)

BUG: 237936
FIXED-IN: 4.5.2

------------------------------------------------------------------------
r1169581 | cfeck | 2010-08-30 06:17:44 +1200 (Mon, 30 Aug 2010) | 2 lines

Allocate proper QPixmap (backport r1169578)

------------------------------------------------------------------------
r1169588 | cfeck | 2010-08-30 06:44:41 +1200 (Mon, 30 Aug 2010) | 5 lines

Make sure we have a selection before deleting it (backport r1169587)

BUG: 219344
FIXED-IN: 4.5.2

------------------------------------------------------------------------
r1170081 | aacid | 2010-08-31 06:22:18 +1200 (Tue, 31 Aug 2010) | 4 lines

Do not crash if we are in dummy mode and the user tries to "select an area"
BUGS: 249436
FIXED-IN: 4.5.2

------------------------------------------------------------------------
r1170312 | cgilles | 2010-09-01 01:58:09 +1200 (Wed, 01 Sep 2010) | 2 lines

fix memory leak (sync with trunk)

------------------------------------------------------------------------
r1170638 | aacid | 2010-09-02 06:45:09 +1200 (Thu, 02 Sep 2010) | 2 lines

add some const makes the code a bit easier to understand

------------------------------------------------------------------------
r1170643 | aacid | 2010-09-02 06:53:17 +1200 (Thu, 02 Sep 2010) | 2 lines

make the forward and backward search code more similar

------------------------------------------------------------------------
r1170659 | lueck | 2010-09-02 07:27:40 +1200 (Thu, 02 Sep 2010) | 1 line

doc backport for 4.5.2
------------------------------------------------------------------------
r1170802 | yurchor | 2010-09-02 18:18:01 +1200 (Thu, 02 Sep 2010) | 1 line

backport typo fixes and screenshots
------------------------------------------------------------------------
r1171461 | gmartres | 2010-09-04 09:53:54 +1200 (Sat, 04 Sep 2010) | 2 lines

kamera/solid_camera.desktop: Fix parsing again (backport r1171460)

------------------------------------------------------------------------
r1171472 | gateau | 2010-09-04 10:36:17 +1200 (Sat, 04 Sep 2010) | 3 lines

Apply black-list extension filter to documents only.

BUG:249878
------------------------------------------------------------------------
r1171473 | gateau | 2010-09-04 10:36:20 +1200 (Sat, 04 Sep 2010) | 1 line

Updated
------------------------------------------------------------------------
r1171493 | aacid | 2010-09-04 12:13:49 +1200 (Sat, 04 Sep 2010) | 4 lines

restore sidebar visibility correctly
BUGS: 249345
FIXED-IN: 4.5.2

------------------------------------------------------------------------
r1171817 | cgilles | 2010-09-05 21:56:52 +1200 (Sun, 05 Sep 2010) | 2 lines

update libraw to 0.10.0 final

------------------------------------------------------------------------
r1172869 | scripty | 2010-09-08 15:21:03 +1200 (Wed, 08 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1173927 | aacid | 2010-09-11 09:53:04 +1200 (Sat, 11 Sep 2010) | 2 lines

Protect us against "impossible" things (Qt not knowing a font we just added), probably fixes 250728

------------------------------------------------------------------------
r1177654 | lueck | 2010-09-21 07:46:23 +1200 (Tue, 21 Sep 2010) | 1 line

backport: change gui strings from ktts to Jovie
------------------------------------------------------------------------
r1178842 | scripty | 2010-09-24 14:43:57 +1200 (Fri, 24 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1180450 | scripty | 2010-09-28 16:24:18 +1300 (Tue, 28 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1180502 | lueck | 2010-09-28 21:40:30 +1300 (Tue, 28 Sep 2010) | 1 line

backport from trunk r1180501: load gwenview translation catalog to get 'Zoom to Fit' and 'Properties' in context menu of gvpart translated
------------------------------------------------------------------------
r1181387 | aacid | 2010-10-01 10:17:41 +1300 (Fri, 01 Oct 2010) | 2 lines

bump versions

------------------------------------------------------------------------
