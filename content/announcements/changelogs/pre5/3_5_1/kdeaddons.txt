2005-12-09 15:49 +0000 [r487129-487128]  dfaure

	* branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/metabar/src/Makefile.am,
	  branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/metabar/src/metabar.cpp:
	  Make it translatable BUG: 115758

	* branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/metabar/src/Makefile.am:
	  Fixes

2005-12-19 15:22 +0000 [r489711]  mhunter

	* branches/KDE/3.5/kdeaddons/kate/filetemplates/plugin/filetemplates.cpp:
	  Typographical corrections and changes CCMAIL:kde-i18n-doc@kde.org

2005-12-21 00:47 +0000 [r490180]  mueller

	* branches/KDE/3.5/kdeaddons/noatun-plugins/tyler/display.c,
	  branches/KDE/3.5/kdeaddons/noatun-plugins/synaescope/core.cpp,
	  branches/KDE/3.5/kdeaddons/noatun-plugins/tippercanoe/core.cpp:
	  fix undefined operation

2005-12-22 16:07 +0000 [r490649]  fedemar

	* branches/KDE/3.5/kdeaddons/konq-plugins/arkplugin/arkplugin.cpp:
	  Extract application/x-tbz2 files.

2005-12-27 17:15 +0000 [r491813]  mueller

	* branches/KDE/3.5/kdeaddons/konq-plugins/domtreeviewer/domtreeview.cpp:
	  fix if() statements

2005-12-29 15:52 +0000 [r492319]  adridg

	* branches/KDE/3.5/kdeaddons/kicker-applets/ktimemon/sample.cc:
	  Avoid totally bogus values for swap on unsupported systems

2005-12-30 10:28 +0000 [r492551]  adawit

	* branches/KDE/3.5/kdeaddons/konq-plugins/dirfilter/dirfilterplugin.cpp,
	  branches/KDE/3.5/kdeaddons/konq-plugins/dirfilter/dirfilterplugin.h:
	  - Save and restore K(Icon|List)ViewSearchLine filters just like
	  we do with the Mime filters: Currently if we filter a directory
	  using the filter line edit, change the directory and remove the
	  filter, the previously filtered items will be incorrectly
	  displayed. CC: Shahar Weiss <sweiss4@gmx.net>

2006-01-01 19:48 +0000 [r493190]  alund

	* branches/KDE/3.5/kdeaddons/kate/textfilter/plugin_katetextfilter.cpp:
	  try to handle encoding. BUG: 104178

2006-01-02 14:18 +0000 [r493444]  scripty

	* branches/KDE/3.5/kdeaddons/konq-plugins/validators/cr22-action-validators.png,
	  branches/KDE/3.5/kdeaddons/konq-plugins/validators/cr22-action-htmlvalidator.png,
	  branches/KDE/3.5/kdeaddons/konq-plugins/validators/cr16-action-validators.png,
	  branches/KDE/3.5/kdeaddons/konq-plugins/validators/cr16-action-htmlvalidator.png,
	  branches/KDE/3.5/kdeaddons/konq-plugins/validators/cr22-action-cssvalidator.png,
	  branches/KDE/3.5/kdeaddons/konq-plugins/validators/cr16-action-cssvalidator.png:
	  Remove svn:executable from some typical non-executable files
	  (goutte)

2006-01-11 11:18 +0000 [r496805]  henrique

	* branches/KDE/3.5/kdeaddons/konq-plugins/arkplugin/arkplugin.cpp:
	  * Fix the right-click menu for RAR archives. Patch by Fikret
	  Skrgic. BUG: 116552

2006-01-14 11:49 +0000 [r497952]  alund

	* branches/KDE/3.5/kdeaddons/kate/make/katemake.desktop,
	  branches/KDE/3.5/kdeaddons/kate/make/plugin_katemake.cpp: Make it
	  compile and appear in the plugins list.

2006-01-16 15:25 +0000 [r498894-498893]  ogoffart

	* branches/KDE/3.5/kdeaddons/konq-plugins/rellinks/plugin_rellinks.cpp:
	  Fix a crash in the rellinks plugin that may occur when pressing
	  the 'back' button BUG: 115259

	* branches/KDE/3.5/kdeaddons/konq-plugins/rellinks/plugin_rellinks.desktop:
	  Add me as author

2006-01-18 20:35 +0000 [r499812-499811]  alund

	* branches/KDE/3.5/kdeaddons/kate/filetemplates/plugin/filetemplates.cpp:
	  better get rid of this spelling error even in the comments ;)

	* branches/KDE/3.5/kdeaddons/kate/filetemplates/plugin/filetemplates.h:
	  declare updateTemplateDirs a slot is better, since I connect to
	  it.

