------------------------------------------------------------------------
r1184024 | rdale | 2010-10-09 19:57:13 +1300 (Sat, 09 Oct 2010) | 3 lines

* Promote some fixes to the release branch for handling long long
types and parsing the QtOpenGL module

------------------------------------------------------------------------
r1185271 | sedwards | 2010-10-13 08:30:11 +1300 (Wed, 13 Oct 2010) | 2 lines

4.4 appears in a couple of places, it should read 4.5.

------------------------------------------------------------------------
r1189611 | rdieter | 2010-10-26 01:07:25 +1300 (Tue, 26 Oct 2010) | 4 lines

backport r1184234, fixes build with newer mono(2.8)

BUG: 254629

------------------------------------------------------------------------
