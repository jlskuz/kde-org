---
aliases:
- ../changelog3_5_4to3_5_5
hidden: true
title: KDE 3.5.5 Changelog
---

<h2>Changes in KDE 3.5.5</h2>
    <h3 id="kdebase">kdebase<span class="allsvnchanges"> [ <a href="3_5_5/kdebase.txt">all SVN
    changes</a> ]</span>
</h3><ul>
      <h4>KIOslave</h4>
<ul>
        <li class="bugfix crash">Check the return value from dbus_connection_open_private(). Fixes KDED crashing on startup when D-BUS is unavailable.   See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kioslave/?rev=566466&amp;view=rev">566466</a>. </li>
        <li class="bugfix ">Properly disconnect DCOP signals in the medianotifier when unloaded.
          Avoids to have two notification dialogs when it's reloaded. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kioslave/?rev=570281&amp;view=rev">570281</a>. </li>
        <li class="bugfix ">If remote says it's a directory, go with that instead of guessing the mimetype based on filename. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=106648">106648</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kioslave/?rev=570332&amp;view=rev">570332</a>. </li>
      </ul>
      <h4><a href="http://konqueror.kde.org">konqueror</a></h4>
<ul>
        <li class="bugfix ">Cancel and reuse single-shot timer for emitActivePartChanged(), instead of queueing them up. This makes tab switching noticeably faster. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/konqueror/?rev=568040&amp;view=rev">568040</a>. </li>
        <li class="bugfix ">Fix kfmclient sometimes returning bogus exit code. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/konqueror/?rev=569345&amp;view=rev">569345</a>. </li>
      </ul>
      <h4><a href="http://konsole.kde.org">Konsole</a></h4>
<ul>
        <li class="bugfix ">Fix wrong char at end of block for unicode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131938">131938</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/?rev=570191&amp;view=rev">570191</a>. </li>
        <li class="bugfix ">Fix Copyright/Licences missing from source files. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=99329">99329</a>. </li>
      </ul>
      <h4>KDesktop</h4>
<ul>
        <li class="bugfix ">If icons are dragged outside the desktop area, jerk them back in.
          Fixes the long-hated issue with the desktop becoming scrollable. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=40418">40418</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kdesktop/?rev=567551&amp;view=rev">567551</a>. </li>
        <li class="optimize">NEW: Improve loading speed of background images, especially SVG ones. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kdesktop/?rev=586073&amp;view=rev">586073</a>. </li>
      </ul>
      <h4>kdesu</h4>
<ul>
        <li class="bugfix ">Add sudo support. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=20914">20914</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kdesu/?rev=570637&amp;view=rev">570637</a>. </li>
      </ul>
      <h4>KWin</h4>
<ul>
        <li class="feature">Support for input shape from XShape1.1. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=122425">122425</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kwin/?rev=571711&amp;view=rev">571711</a>. </li>
        <li class="feature">Draw outlines around the selected window while doing Alt+Tab. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=108142">108142</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kwin/?rev=572782&amp;view=rev">572782</a>. </li>
        <li class="bugfix ">Fix incorrect reseting of the electric border setting. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=123891">123891</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kwin/?rev=571672&amp;view=rev">571672</a>. </li>
      </ul>
      <h4>nsplugins</h4>
<ul>
        <li class="bugfix ">If we get redirected when grabbing a file, tell the plugin about the final
            URL and not the original one. Fixes playback of youtube videos embedded in other web sites. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=121964">121964</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/nsplugins/?rev=565998&amp;view=rev">565998</a>. </li>
        <li class="bugfix ">Fix some bugs in sizing of plugins. Fixes Google Video,
            and makes the acroread plugin behave better. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=117603">117603</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=125076">125076</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=118038">118038</a>.  See SVN commits <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/nsplugins/?rev=589919&amp;view=rev">589919</a> and <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/nsplugins/?rev=590832&amp;view=rev">590832</a>. </li>
      </ul>
      <h4>ksysguard</h4>
<ul>
        <li class="bugfix ">Fixed bug #128306 like proposed in the patch, just with a small cleanup ;)
		Thank you very much! Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=128306">128306</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/ksysguard/?rev=568458&amp;view=rev">568458</a>. </li>
      </ul>
      <h4>ksystraycmd</h4>
<ul>
        <li class="bugfix ">Fix argument getting de-quoted. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=124116">124116</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/ksystraycmd/?rev=572103&amp;view=rev">572103</a>. </li>
      </ul>
      <h4>Kicker</h4>
<ul>
        <li class="bugfix ">Fix stupid typo. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=128552">128552</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kicker/?rev=570840&amp;view=rev">570840</a>. </li>
        <li class="bugfix ">Fixed a hairy problem with nested event loops deleting objects at inappropriate times. May solve other bugs that I'm not currently aware of. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=123869">123869</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kicker/?rev=571260&amp;view=rev">571260</a>. </li>
        <li class="bugfix ">Delete all signal connections to a TaskContainer when marking it for deletion. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=132208">132208</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kicker/?rev=571782&amp;view=rev">571782</a>. </li>
        <li class="bugfix ">attention blinking configurability. how fun.
          optimize things in some fairly significant ways: don't repaint taskbar
          buttons on window movements, don't set the taskmanager to track geom
          changes unless you turn on desktop previews in the pager and don't
          redraw the pager on window geometry changes unless we are actually
          showing desktop previews. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kicker/?rev=572257&amp;view=rev">572257</a>. </li>
        <li class="bugfix ">optimization: don't redraw for every time windowsChanged is called. in
          the case of moving a window when desktop previews are on, moving a
          window across the screen at a reasonable pace used to result in over 100
          repaints. now it only does ~15. given that the repaints aren't exactly
          trivial for these things, this is probably a good thing. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kicker/?rev=572260&amp;view=rev">572260</a>. </li>
      </ul>
      <h4>KSMServer</h4>
<ul>
        <li class="feature">Set KDE_FULL_SESSION also as a root window X property. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/ksmserver/?rev=579568&amp;view=rev">579568</a>. </li>
        <li class="bugfix ">Increase timeout during session saving and killing. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133044">133044</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/ksmserver/?rev=578861&amp;view=rev">578861</a>. </li>
      </ul>
      <h4>KHotKeys</h4>
<ul>
        <li class="feature">Make it possible to use even keys that have no Qt keycode in input actions. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/khotkeys/?rev=580188&amp;view=rev">580188</a>. </li>
      </ul>
      <h4>KControl</h4>
<ul>
        <li class="feature">NEW: Make it possible to explicitly force 96DPI or 120DPI. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=111754">111754</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kcontrol/?rev=585725&amp;view=rev">585725</a>. </li>
      </ul>
    </ul>
    <h3 id="kdeaddons">kdeaddons<span class="allsvnchanges"> [ <a href="3_5_5/kdeaddons.txt">all SVN
    changes</a> ]</span>
</h3><ul>
      <h4>Kate</h4>
<ul>
        <li class="bugfix ">Add support for finding the right make command during configure time. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=126052">126052</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdeaddons/kate/?rev=572366&amp;view=rev">572366</a>. </li>
      </ul>
    </ul>
    <h3 id="kdelibs">kdelibs<span class="allsvnchanges"> [ <a href="3_5_5/kdelibs.txt">all SVN
    changes</a> ]</span>
</h3><ul>
      <h4>kdeinit</h4>
<ul>
        <li class="feature">Add protection against poor Linux OOM-killer heuristic. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kdeinit/?rev=579164&amp;view=rev">579164</a>. </li>
      </ul>
      <h4><a href="http://kate.kde.org">Kate</a></h4>
<ul>
        <li class="feature">Support Q_DECLARE_INTERFACE-macro when coding Qt-code See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kate/?rev=567242&amp;view=rev">567242</a>. </li>
        <li class="bugfix ">Don't enter "word selection mode" without an actual selection. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131369">131369</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kate/?rev=566355&amp;view=rev">566355</a>. </li>
        <li class="bugfix ">Fixed another stringAtPos(-1) call. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131933">131933</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kate/?rev=570246&amp;view=rev">570246</a>. </li>
      </ul>
      <h4><a href="http://khtml.org">KHTML</a></h4>
<ul>
          <li class="bugfix ">Heavy DHTML optimizations for the case where the changes in the style merely moves a layer, which is very common. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=118658">118658</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=588455&amp;view=rev">588455</a>. </li>
         <li class="feature">Significant improvements in painting of inline elements,
            to be more compliant with CSS 2.1 - section 10.8.1 and Appendix E-2;
            also includes support for CSS 3 outline-offset. See SVN commits <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=576101&amp;view=rev">576101</a> and <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=576627&amp;view=rev">576627</a>. </li>
        <li class="feature">Support CSS3's hsv/hsva color values. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=576708&amp;view=rev">576708</a>. </li>
        <li class="bugfix ">Fixes to make the new yahoo photos site basically work:
                fix a bug in XML parser and emission of scroll event. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=104236">104236</a>.  See SVN commits <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=587710&amp;view=rev">587710</a> and <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=588069&amp;view=rev">588069</a>. </li>
        <li class="optimize">Fix a regression in performance of background painting. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133529">133529</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=581150&amp;view=rev">581150</a>. </li>
        <li class="bugfix ">Properly compute length of utf-8 data in XMLHttpRequest. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131242">131242</a>.  See SVN commits <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=576656&amp;view=rev">576656</a> and <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=576640&amp;view=rev">576640</a>. </li>
        <li class="bugfix ">Support parsing of attribute/&gt;. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=87221">87221</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=576632&amp;view=rev">576632</a>. </li>
        <li class="bugfix ">Evaluate scripts in
            &lt;iframe src="javascript:..."&gt; properly. Fixes some of the product pages on amazon.com and
            the reader on freemail.hu. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=134791">134791</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=106748">106748</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=590600&amp;view=rev">590600</a>. </li>
        <li class="bugfix ">Permit faster auto-scrolling with the middle mouse button. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=125668">125668</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=581817&amp;view=rev">581817</a>. </li>
        <li class="bugfix ">Permit manually scrolling frames with keyboard et al. even if they have scrolling=no. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=91113">91113</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=581838&amp;view=rev">581838</a>. </li>
        <li class="bugfix ">Don't miss some of the attribute changes relevant for restyling. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133570">133570</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=581101&amp;view=rev">581101</a>. </li>
        <li class="bugfix ">Properly restyle when when an anchor stop being a link of becomes one. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=576638&amp;view=rev">576638</a>. </li>
        <li class="bugfix ">Properly honor bottom padding on scrolled overflow. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133310">133310</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=586756&amp;view=rev">586756</a>. </li>
        <li class="bugfix ">Honor no-repeat in background when background offset is negative. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133626">133626</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=586793&amp;view=rev">586793</a>. </li>
        <li class="bugfix ">Fixes in application of padding to inline elements. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131366">131366</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=576084&amp;view=rev">576084</a>. </li>
        <li class="bugfix ">Various memory leak fixes, in particular in the
                element from ID or name lookup caches, and some table and attribute node operations. See SVN commits <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=582675&amp;view=rev">582675</a>, <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=583314&amp;view=rev">583314</a> and <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=584998&amp;view=rev">584998</a>. </li>
        <li class="bugfix ">Fix handling of &lt;colgroup&gt; elements without span attribute. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=576356&amp;view=rev">576356</a>. </li>
        <li class="bugfix ">Fixes in stacking order of elements with non-visible overflow. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=577053&amp;view=rev">577053</a>. </li>
        <li class="bugfix crash">Fixing crash upon deleting cells from rows. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=90462">90462</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=570648&amp;view=rev">570648</a>. </li>
        <li class="bugfix ">Do not improperly reset text-align in tables in strict mode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=106812">106812</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=576076&amp;view=rev">576076</a>. </li>
        <li class="bugfix ">Properly re-layout table caption if needed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133001">133001</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=577316&amp;view=rev">577316</a>. </li>
        <li class="bugfix crash">Fixing two crashes in the capitalization code (regression). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=132050">132050</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=571252&amp;view=rev">571252</a>. </li>
        <li class="bugfix crash">Fix crash on hover due to dangling placeholder box. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=134291">134291</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=586170&amp;view=rev">586170</a>. </li>
        <li class="bugfix ">Fix sometimes incorrect positioning of positioned elements having root as containing block. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=134518">134518</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=587726&amp;view=rev">587726</a>. </li>
        <li class="bugfix crash">Fixed crash when characterSet is accessed on newly-created document. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133071">133071</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=590814&amp;view=rev">590814</a>. </li>
        <li class="bugfix ">Fix regression in &lt;label&gt; support. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=59489">59489</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=567892&amp;view=rev">567892</a>. </li>
        <li class="bugfix ">Support the add method on the options collection. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=134914">134914</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=590758&amp;view=rev">590758</a>. </li>
        <li class="bugfix ">Properly discard malformed content properties. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=586037&amp;view=rev">586037</a>. </li>
        <li class="bugfix ">Parse 'align' attribute of iframes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=130736">130736</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=568025&amp;view=rev">568025</a>. </li>
        <li class="bugfix ">Make sure to properly escape ampersands in right-click google search. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=132678">132678</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=579136&amp;view=rev">579136</a>. </li>
        <li class="bugfix ">Resetting a SELECT with no default selection should not select the first item if it's a multiple selection list. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133326">133326</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=579594&amp;view=rev">579594</a>. </li>
        <li class="bugfix ">Do not let NodeIterators escape out of the root node. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=576812&amp;view=rev">576812</a>. </li>
        <li class="bugfix ">For percentage height calculation purposes, make body appear to have the same height as the viewport (quirk mode). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=95489">95489</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=580037&amp;view=rev">580037</a>. </li>
        <li class="bugfix ">Properly account for overflows induced by inline-blocks and text-shadows. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=584408&amp;view=rev">584408</a>. </li>
        <li class="bugfix ">Remove incorrect addition of borders to layers which made some links unclickable (regression). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133132">133132</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=587994&amp;view=rev">587994</a>. </li>
        <li class="bugfix ">Fix mistake which made the viewport width be used instead of the root block width when calculating the document width (regression). Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=134304">134304</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=134051">134051</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=588278&amp;view=rev">588278</a>. </li>
        <li class="bugfix ">Make sure that when display:compact blocks are merged into a host block, the height
                of the resulting block is at least that of the compact block. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=123915">123915</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=588483&amp;view=rev">588483</a>. </li>
        <li class="bugfix ">Do not limit single-line input elements to entry of 1024 characters. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=116132">116132</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=589303&amp;view=rev">589303</a>. </li>
        <li class="bugfix ">Do not try to run external VBScript; add the newly
                approved ECMAScript mimetypes. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=98216">98216</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=134763">134763</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=589296&amp;view=rev">589296</a>. </li>
        <li class="bugfix ">Fix bug involving first-letter property where text fragments would
                'disappear' (being offset far to the right or left) instead of breaking to next line. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=119167">119167</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=590440&amp;view=rev">590440</a>. </li>
        <li class="bugfix ">Allow instanceof to work on most DOM objects (partial fix). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=134771">134771</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/khtml/?rev=591021&amp;view=rev">591021</a>. </li>
      </ul>
      <h4>kjs</h4>
<ul>
          <li class="bugfix ">Parse dates with ',', '-' and ':' where whitespace was expected.
          Example: ",-:September,03,-:,2006, 13:53:02,-:" Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133517">133517</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kjs/?rev=588460&amp;view=rev">588460</a>. </li>
          <li class="bugfix crash">Do not crash
                when modifying an invalid date object. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=121528">121528</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kjs/?rev=580498&amp;view=rev">580498</a>. </li>
          <li class="bugfix ">Fix locale-dependence in the parser, which resulted
              in floating point numbers getting misparsed in some locales. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=126482">126482</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kjs/?rev=572947&amp;view=rev">572947</a>. </li>
      </ul>
      <h4>kio</h4>
<ul>
        <li class="bugfix ">Set m_bDeep in all KServiceGroup constructors, or it may be used uninitialized in load() and entries(). kbuildsycoca did this, and valgrind caught it. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kio/?rev=567480&amp;view=rev">567480</a>. </li>
      </ul>
      <h4>kdecore</h4>
<ul>
        <li class="bugfix ">Pick correctly the best icon size. Also make -1,-1 return the largest
          icon and not the first one (makes more sense and saying the first one
          is as good as saying a random one). See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kdecore/?rev=568222&amp;view=rev">568222</a>. </li>
        <li class="bugfix ">Be explicit about windowInfo() args. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=130934">130934</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kdecore/?rev=568454&amp;view=rev">568454</a>. </li>
        <li class="bugfix ">NET::Override is old and hated. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kdecore/?rev=568554&amp;view=rev">568554</a>. </li>
        <li class="feature">Add support for more icon contexts in the icon loader. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=120562">120562</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kdecore/?rev=576245&amp;view=rev">576245</a>. </li>
        <li class="feature">Search for icons also in $XDG_DATA_DIRS. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=97776">97776</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kdecore/?rev=576641&amp;view=rev">576641</a>. </li>
        <li class="bugfix ">Fix KSharedConfig to actually share readonly configs. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kdecore/?rev=579777&amp;view=rev">579777</a>. </li>
      </ul>
      <h4>kdefx</h4>
<ul>
        <li class="bugfix ">- "features" is clobbered by the longjmp, so mark it as volatile
          to avoid that a clever compiler (gcc 4.2) optimizes reads away. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kdefx/?rev=567223&amp;view=rev">567223</a>. </li>
      </ul>
      <h4>kdesu</h4>
<ul>
        <li class="bugfix ">Add sudo support. See that bug for some remaining issues. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=20914">20914</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kdesu/?rev=570635&amp;view=rev">570635</a>. </li>
      </ul>
      <h4>kdeui</h4>
<ul>
        <li class="bugfix ">Don't draw a highlighting line above KPopupMenu title sections.
          It looks very out-of-place, especially when using styles with rounded PE_HeaderSections. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=71452">71452</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kdeui/?rev=566543&amp;view=rev">566543</a>. </li>
      </ul>
      <h4>kdoctools</h4>
<ul>
        <li class="bugfix ">Some extra entities required for 100% successful docs compilation. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kdoctools/?rev=567588&amp;view=rev">567588</a>. </li>
      </ul>
      <h4><a href="http://printing.kde.org/">kdeprint</a></h4>
<ul>
        <li class="feature">CUPS 1.2 support</li>
        <li class="feature">Show prettier urls.</li>
      </ul>
      <h4>knewstuff</h4>
<ul>
        <li class="bugfix ">Unbreak "latest" and "most downloads" views. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=131979">131979</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=133852">133852</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/knewstuff/?rev=585244&amp;view=rev">585244</a>. </li>
        <li class="bugfix ">Fix memory leaks. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/knewstuff/?rev=585299&amp;view=rev">585299</a>. </li>
      </ul>
      <h4>KWallet</h4>
<ul>
        <li class="bugfix ">Fix deadlock with autocompletion popups. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=126593">126593</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kwallet/?rev=568553&amp;view=rev">568553</a>. </li>
        <li class="bugfix ">Associate wallet dialogs properly with main windows. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=113057">113057</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kwallet/?rev=578532&amp;view=rev">578532</a>. </li>
      </ul>
      <h4>l10n</h4>
<ul>
        <li class="bugfix ">Fix short date format for Turkish.</li>
      </ul>
    </ul>
    <h3 id="kdebindings">kdebindings<span class="allsvnchanges"> [ <a href="3_5_5/kdebindings.txt">all SVN
    changes</a> ]</span>
</h3><ul>
      <h4>qtruby</h4>
<ul>
        <li class="bugfix ">* The Ruby VALUE to 'uchar *' marshaller wasn't working correctly if the Ruby string contained nulls. Also applied for 'char *' types. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdebindings/qtruby/?rev=571840&amp;view=rev">571840</a>. </li>
      </ul>
    </ul>
    <h3 id="kdegraphics">kdegraphics<span class="allsvnchanges"> [ <a href="3_5_5/kdegraphics.txt">all SVN
    changes</a> ]</span>
</h3><ul>
      <h4><a href="http://kpdf.kde.org">KPDF</a></h4>
<ul>
        <li class="bugfix ">Fix possible crash when the document has an incorrectly defined blend mode. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdegraphics/kpdf/?rev=567994&amp;view=rev">567994</a>. </li>
        <li class="bugfix ">Several memoryleaks fixed. See SVN commits <a href="http://websvn.kde.org/branches/KDE/3.5/kdegraphics/kpdf/?rev=571992&amp;view=rev">571992</a> and <a href="http://websvn.kde.org/branches/KDE/3.5/kdegraphics/kpdf/?rev=572008&amp;view=rev">572008</a>. </li>
        <li class="bugfix ">Make "find as you type" work better. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdegraphics/kpdf/?rev=573005&amp;view=rev">573005</a>. </li>
        <li class="bugfix ">Fix middle-click zoom not working after dragging to the top of the screen. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=111255">111255</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdegraphics/kpdf/?rev=580022&amp;view=rev">580022</a>. </li>
        <li class="bugfix ">Fix crash on some malformed documents. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdegraphics/kpdf/?rev=581596&amp;view=rev">581596</a>. </li>
        <li class="bugfix ">DCOP currentPage() not updated while in Presentation mode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133549">133549</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdegraphics/kpdf/?rev=582529&amp;view=rev">582529</a>. </li>
        <li class="bugfix ">Fix bug that prevented the correct restoring of the document viewport on session restore. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133507">133507</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdegraphics/kpdf/?rev=582561&amp;view=rev">582561</a>. </li>
        <li class="bugfix ">More friendly for RightToLeft languages: fix the mini-progressbar, the message pane and the previous/next page buttons to look and work correctly for that writing style. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdegraphics/kpdf/?rev=586747&amp;view=rev">586747</a>. </li>
      </ul>
      <h4>kdvi</h4>
<ul>
        <li class="bugfix ">fixes bug #128358. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=128358">128358</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdegraphics/kdvi/?rev=571112&amp;view=rev">571112</a>. </li>
      </ul>
    </ul>
    <h3 id="kdepim">kdepim<span class="modulehomepage">[ <a href="http://pim.kde.org">homepage</a> ] </span><span class="allsvnchanges"> [ <a href="3_5_5/kdepim.txt">all SVN
    changes</a> ]</span>
</h3><ul>
      <h4><a href="http://www.astrojar.org.uk/linux/kalarm.html">KAlarm</a></h4>
<ul>
        <li class="feature">Use an alarm's previous deferral time interval as default for its next deferral. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdepim/kalarm/?rev=575676&amp;view=rev">575676</a>. </li>
      </ul>
      <h4><a href="http://kmail.kde.org">KMail</a></h4>
<ul>
        <li class="bugfix ">Add missing top and bottom buttons for the filter dialog. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=132077">132077</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdepim/kmail/?rev=571255&amp;view=rev">571255</a>. </li>
        <li class="bugfix ">Improve GUI-strings. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131915">131915</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdepim/kmail/?rev=571133&amp;view=rev">571133</a>. </li>
        <li class="bugfix crash">Fixing a crash on start when using IMAP. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=132008">132008</a>. </li>
      </ul>
    </ul>
    <h3 id="kdesdk">kdesdk<span class="allsvnchanges"> [ <a href="3_5_5/kdesdk.txt">all SVN
    changes</a> ]</span>
</h3><ul>
      <h4><a href="http://uml.sourceforge.net">Umbrello</a></h4>
<ul>
        <li class="feature">Documentation generator for DocBook and XHTML. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=54307">54307</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdesdk/umbrello/?rev=574994&amp;view=rev">574994</a>. </li>
        <li class="bugfix ">"Role A Properties" should give class name. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=69244">69244</a>. </li>
        <li class="bugfix ">External folders - improved implementation. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=87252">87252</a>. </li>
        <li class="bugfix ">"Open recent" list doesn't reorder when a file is opened. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=111757">111757</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdesdk/umbrello/?rev=582767&amp;view=rev">582767</a>. </li>
        <li class="feature">Properties menu: move to top/bottom buttons. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=126467">126467</a>. </li>
        <li class="bugfix ">Java import - importing interfaces - absent visibility treated as package instead of public. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131327">131327</a>. </li>
        <li class="bugfix ">Python code generation is not independent of diagram view. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131790">131790</a>. </li>
        <li class="bugfix ">Java import - method parameter types not resolved correctly. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131825">131825</a>. </li>
        <li class="bugfix ">Java import: unable to import AzareusCore. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131961">131961</a>. </li>
        <li class="bugfix ">Java import: error on multidimensional arrays. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=132017">132017</a>. </li>
        <li class="bugfix ">Java import - array types not resolved correctly. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=132035">132035</a>. </li>
        <li class="bugfix ">Java import - "final" and comments in method declaration not parsed correctly. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=132174">132174</a>. </li>
        <li class="bugfix ">All native importers: Spaces in strings cause next member var to be ignored. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=132472">132472</a>. </li>
        <li class="bugfix ">Java import - static member vars ignored in interfaces. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=132657">132657</a>. </li>
        <li class="bugfix ">Strange behavior with datatypes in French. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133597">133597</a>. </li>
        <li class="bugfix ">Header file names are lowercase in .cpp file but mixed case on disk. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=134279">134279</a>. </li>
        <li class="feature">PHP5 code generator adds stub-methods for all interfaces a class implements. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdesdk/umbrello/?rev=577139&amp;view=rev">577139</a>. </li>
        <li class="bugfix ">Fix for generation of include statements in PHP5 generator. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdesdk/umbrello/?rev=577335&amp;view=rev">577335</a>. </li>
        <li class="bugfix ">Fix loading of associations in collaboration diagram. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdesdk/umbrello/?rev=579792&amp;view=rev">579792</a>. </li>
        <li class="bugfix ">Small fix for printing in landscape mode. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdesdk/umbrello/?rev=580058&amp;view=rev">580058</a>. </li>
        <li class="bugfix ">Various fixes to mouse behaviour, e.g. multiple selection using shift button not working, mouse tracking disabled when creating associations and messages, inconsistent behaviour in some tools. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdesdk/umbrello/?rev=590624&amp;view=rev">590624</a>. </li>
        <li class="bugfix ">Fix for message widget selection behaviour. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdesdk/umbrello/?rev=590635&amp;view=rev">590635</a>. </li>
        <li class="bugfix ">Fix for class association not updating the association position when being moved. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdesdk/umbrello/?rev=590644&amp;view=rev">590644</a>. </li>
      </ul>
    </ul>
    <h3 id="kdeedu">kdeedu<span class="allsvnchanges"> [ <a href="3_5_5/kdeedu.txt">all SVN
    changes</a> ]</span>
</h3><ul>
      <h4><a href="http://edu.kde.org/kanagram">kanagram</a></h4>
<ul>
        <li class="bugfix ">capital of Turkey is Ankara, not Istanbul. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdeedu/kanagram/?rev=569085&amp;view=rev">569085</a>. </li>
      </ul>
      <h4><a href="http://kgeography.berlios.de">KGeography</a></h4>
<ul>
        <li class="bugfix ">Serbia and Montenegro are two separate countries since June 2006. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133499">133499</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdeedu/kgeography/?rev=580546&amp;view=rev">580546</a>. </li>
      </ul>
      <h4><a href="http://edu.kde.org/kig">kig</a></h4>
<ul>
        <li class="bugfix ">Correctly sets the document as modified when editing a script. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdeedu/kig/?rev=582569&amp;view=rev">582569</a>. </li>
      </ul>
      <h4><a href="http://edu.kde.org/kstars">KStars</a></h4>
<ul>
        <li class="bugfix ">Fix large deep sky object outlines not always match ingtheir actual orientation in the sky. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=134824">134824</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdeedu/kstars/?rev=589956&amp;view=rev">589956</a>. </li>
      </ul>
      <h4><a href="http://edu.kde.org/kalzium/">Kalzium</a></h4>
<ul>
        <li class="bugfix ">Fix name of C. W. Scheele. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133297">133297</a>. </li>
        <li class="bugfix ">Reset spectrum width on element change. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=134086">134086</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdeedu/kalzium/?rev=588062&amp;view=rev">588062</a>. </li>
      </ul>
    </ul>
    <h3 id="kdegames">kdegames<span class="allsvnchanges"> [ <a href="3_5_5/kdegames.txt">all SVN
    changes</a> ]</span>
</h3><ul>
      <h4>Kolf</h4>
<ul>
        <li class="bugfix ">Make the floater not have so big range on level 5 of hard course so it can not put the ball out of the walls. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=54097">54097</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdegames/kolf/?rev=582482&amp;view=rev">582482</a>. </li>
      </ul>
      <h4><a href="http://opensource.bureau-cornavin.com/ktuberling/">KTuberling</a></h4>
<ul>
        <li class="feature">New language Low Saxon added. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdegames/ktuberling/?rev=572499&amp;view=rev">572499</a>. </li>
      </ul>
      <h4>KPatience</h4>
<ul>
        <li class="bugfix ">Fix bug "cannot win this game" message just after start of mod3 game. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131587">131587</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdegames/kpatience/?rev=581887&amp;view=rev">581887</a>. </li>
      </ul>
    </ul>
    <h3 id="kdeutils">kdeutils<span class="allsvnchanges"> [ <a href="3_5_5/kdeutils.txt">all SVN
    changes</a> ]</span>
</h3><ul>
      <h4>kcalc</h4>
<ul>
	<li class="bugfix ">Wrong result with large number in hexa on amd64. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131482">131482</a>. </li>
	<li class="bugfix ">Alt-H selected both Hexadecimal and Help. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=132446">132446</a>. </li>
	<li class="bugfix ">CMP function only complemented lower 32-bits. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133420">133420</a>. </li>
	<li class="bugfix ">Wrong DEC to HEX conversion in 64bit math. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133720">133720</a>. </li>
	<li class="bugfix ">Avoid crash on large results, when using "exp" etc. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=134070">134070</a>. </li>
	<li class="bugfix ">Negative number raised to zero gives now one. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=134192">134192</a>. </li>
      </ul>
      <h4>ark</h4>
<ul>
        <li class="bugfix ">Fix race condition at exit. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=127341">127341</a>. </li>
        <li class="bugfix ">Fix drag'n'drop problems. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=91556">91556</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdeutils/ark/?rev=577395&amp;view=rev">577395</a>. </li>
      </ul>
      <h4>kmilo</h4>
<ul>
        <li class="bugfix ">Small fixes. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=581607">581607</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=583838">583838</a>. </li>
      </ul>
      <h4>kwalletmanager</h4>
<ul>
        <li class="bugfix ">Allow overwriting of files on export. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=122946">122946</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdeutils/kwalletmanager/?rev=566720&amp;view=rev">566720</a>. </li>
      </ul>
      <h4>khexedit</h4>
<ul>
       <li class="bugfix ">Reorder "find previous/next" actions in that order for consistency. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=122378">122378</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdeutils/khexedit/?rev=579021&amp;view=rev">579021</a>. </li>
      </ul>
      <h4><a href="http://netdragon.sourceforge.net">SuperKaramba</a></h4>
<ul>
        <li class="bugfix ">Improve unicode support. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=128925">128925</a>. </li>
      </ul>
      <h4>klaptopdaemon</h4>
<ul>
        <li class="bugfix ">Make the UI reflect default battery critical behavior. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdeutils/klaptopdaemon/?rev=578797&amp;view=rev">578797</a>. </li>
      </ul>
    </ul>
    <h3 id="kdewebdev">kdewebdev<span class="allsvnchanges"> [ <a href="3_5_5/kdewebdev.txt">all SVN
    changes</a> ]</span>
</h3><ul>
      <h4><a href="http://quanta.kdewebdev.org">Quanta Plus</a></h4>
<ul>
        <li class="bugfix ">Avoid growing quantarc and causing instability. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=111049">111049</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=579846&amp;view=rev">579846</a>. </li>
        <li class="bugfix ">Fixed a crash when navigating PHP documents via "Document Structure"/"Functions" Bug introduced in r415463 See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=567289&amp;view=rev">567289</a>. </li>
        <li class="bugfix ">Fix Save As behavior Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131728">131728</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=579876&amp;view=rev">579876</a>. </li>
        <li class="feature">dd some more
    special characters to the list Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=130513">130513</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=124628">124628</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=579881&amp;view=rev">579881</a>. </li>
        <li class="bugfix ">Fix crash in
    CSS editor. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131849">131849</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=579883&amp;view=rev">579883</a>. </li>
        <li class="bugfix ">Don't crash when
    closing a document in VPL mode Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=133082">133082</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=126585">126585</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=125153">125153</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=579903&amp;view=rev">579903</a>. </li>
        <li class="bugfix ">Fix crash when
    copying to clipboard inside VPL. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=130212">130212</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=579975&amp;view=rev">579975</a>. </li>
        <li class="bugfix ">Show the correct
    column number if tabs are used in the document. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133313">133313</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=579980&amp;view=rev">579980</a>. </li>
        <li class="bugfix ">Only one upload dialog can be visible at any time. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=132535">132535</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=579990&amp;view=rev">579990</a>. </li>
        <li class="bugfix ">Do not show Find in Files menu if KFileReplace is not installed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=132530">132530</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=579993&amp;view=rev">579993</a>. </li>
        <li class="bugfix ">Fix a crash when using Close All. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=580040&amp;view=rev">580040</a>. </li>
        <li class="bugfix ">Allow logging to files outside of project directory - don't send closing events for untitled, unmodified documents. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131782">131782</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=580055&amp;view=rev">580055</a>. </li>
        <li class="bugfix ">Respect the order of items in the .docrc. Patch from the bugreport. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133704">133704</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=581836&amp;view=rev">581836</a>. </li>
        <li class="bugfix ">Fix crash when creating project through slow links. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=581882&amp;view=rev">581882</a>. </li>
        <li class="bugfix ">Really abort if a remote directory cannot be created. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=117032">117032</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=581888&amp;view=rev">581888</a>. </li>
        <li class="bugfix ">Fix automatic updating of closing tags. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=132357">132357</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=582175&amp;view=rev">582175</a>. </li>
        <li class="bugfix ">Accept float numbers for length values in the CSS editor Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=130295">130295</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=582192&amp;view=rev">582192</a>. </li>
        <li class="bugfix ">Make CSS completion work inside style attributes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=80605">80605</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=582414&amp;view=rev">582414</a>. </li>
        <li class="bugfix ">Improve usability of the File Changed dialog. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=111049">111049</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=582429&amp;view=rev">582429</a>. </li>
        <li class="bugfix ">Handle correctly the escaped backslashes inside strings. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=111049">111049</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=582484&amp;view=rev">582484</a>. </li>
        <li class="bugfix ">Improve mimetype and extension based searching for a DTEP that can handle the currently opened file. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=129808">129808</a>.  See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/quanta%20plus/?rev=582528&amp;view=rev">582528</a>. </li>
      </ul>
      <h4><a href="http://kommander.kdewebdev.org">Kommander</a></h4>
<ul>
        <li class="feature">Add initialization/destroy for Wizard. Use setEnabled() to enable/disable Finish button. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/kommander/?rev=568027&amp;view=rev">568027</a>. </li>
        <li class="feature">Improve the integrated text editor a lot by using the KTextEditor interface (katepart) for editing Kommnader text. This means syntax highlighting, improved undo/redo, possibility to have line number bar, etc. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/kommander/?rev=581148&amp;view=rev">581148</a>. </li>
        <li class="bugfix ">Fix crash on exit for the editor. See SVN commit <a href="http://websvn.kde.org/branches/KDE/3.5/kdewebdev/kommander/?rev=581829&amp;view=rev">581829</a>. </li>

      </ul>
    </ul>