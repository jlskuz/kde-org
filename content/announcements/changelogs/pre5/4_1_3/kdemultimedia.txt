2008-09-26 19:41 +0000 [r865129]  lueck <lueck@localhost>:

	* branches/KDE/4.1/kdemultimedia/libkcddb/kcmcddb/libkcddb.desktop,
	  branches/KDE/4.1/kdemultimedia/libkcddb/kcmcddb/kcmcddb.cpp:
	  enable the Help button in systemsettings module CDDB Retrieval
	  and add a doc path CCMAIL:kde-multimedia@kde.org

2008-09-27 07:59 +0000 [r865240]  woebbe <woebbe@localhost>:

	* branches/KDE/4.1/kdemultimedia/kioslave/audiocd/plugins/wav/encodercda.cpp,
	  branches/KDE/4.1/kdemultimedia/kioslave/audiocd/audiocd.cpp:
	  Backport of rev. 864818 by ogoffart: Compile (cdparanoia 10.2 has
	  a member called 'private')

2008-09-27 15:58 +0000 [r865448]  lueck <lueck@localhost>:

	* branches/KDE/4.1/kdemultimedia/doc/kscd/index.docbook,
	  branches/KDE/4.1/kdemultimedia/doc/dragonplayer/index.docbook,
	  branches/KDE/4.1/kdemultimedia/doc/juk/index.docbook:
	  documentation backport for 4.1.3 from trunk

2008-10-14 06:17 +0000 [r871195]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdemultimedia/dragonplayer/misc/dragonplayer.desktop,
	  branches/KDE/4.1/kdemultimedia/dragonplayer/misc/dragonplayer_play_dvd.desktop,
	  branches/KDE/4.1/kdemultimedia/dragonplayer/misc/dragonplayer_part.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-21 17:51 +0000 [r874483]  larkang <larkang@localhost>:

	* branches/KDE/4.1/kdemultimedia/kioslave/audiocd/plugins/vorbis/encodervorbis.cpp,
	  branches/KDE/4.1/kdemultimedia/kioslave/audiocd/plugins/flac/encoderflac.cpp,
	  branches/KDE/4.1/kdemultimedia/kioslave/audiocd/plugins/lame/encoderlame.cpp:
	  Backport: Fix off by one in track info BUG: 171561

