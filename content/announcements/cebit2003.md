---
custom_about: true
custom_contact: true
title: KDE@CeBIT 2003
date: "2003-03-11"
description: The KDE Project will present itself at CeBIT, the world's largest computer trade show, taking place in Hannover from March 12th to March 19th.
---


<p>The KDE Project will present itself at <a href="http://www.cebit.de">CeBIT</a>,
the world's largest computer trade show, taking place in Hannover, Germany, from
March 12th to March 19th.</p>
<p><a href="/">KDE</a> will be presenting the latest release
KDE 3.1 and give a preview of current developments for KDE 3.2 on
Saturday 15th and Sunday 16th in the
<a href="http://www.linux-events.de">LinuxNewMedia Linux Park</a>
in Hall 6 booth A53/068.</p>

<p>On Saturday 15th 10:15, Rainer Endres will give a
talk about using KDE on the corporate desktop in the Linux Park.</p>

<h2>Other attractions</h2>

<p>Martin Konold will be giving a talk on the KDE Kolab Groupware
Solution at the Linux Park on Saturday 15th at 15:00.  In addition, <a
href="http://www.erfrakon.de/">erfrakon</a> will be presenting the
latest <a href="http://www.kroupware.org/">Kroupware and Kolab</a>
developments running on a multi-platform high-availability cluster in
Hall 2, booth A32.</p>

<p>For the newest developments in syncing KDE with PDAs
please visit the <a href="http://www.opie.info/">OPIE-Team</a>
at the Sharp booth in Hall 1, booth 7a/2.</p>

<h2>Key signing</h2>

<p>There will be a GPG key signing party on Sunday 16th 14:00 at the KDE booth. If you want to have your key signed by a KDE developer ensure that you bring a printout of "gpg --fingerprint ID" and proper identification.
Please read <a
href="http://www.mathematik.uni-bielefeld.de/~mmutz/sign-policy.html"> this
sign-policy</a>.</p>


<h2>Sponsors</h2>

<p>Many thanks go out to <a href="http://www.lsexperts.de">LSE Linux and Security
Experts GmbH</a> for sponsoring accommodation, <a
href="http://www.fujitsu-siemens.de/">Fujitsu-Siemens</a> for the
complete hardware and <a href="http://www.suse.de">SuSE Linux AG</a> for the
fair tickets and LinuxNewMedia for sponsoring the booth itself.</p>
<p>Many thanks go to <a href=" http://www.credativ.de/">credativ</a>
and <a href="http://www.danka.de/">Danka</a> for sponsoring and
manufacturing a nice set of KDE Live-CD's based on <a
href="http://www.knopper.net">Knoppix</a> featuring Debian GNU/Linux 3.0
(woody) with KDE 3.1, KDevelop and KOffice and many many other applications
available for the KDE Desktop as free software. They donated 1000 CDs to the
KDE Project for promoting KDE as the leading desktop technology - stop by and
get your copy at the KDE booth !</p>
<p>The CD is available for free (as in beer) but we would be greatful for a contribution to KDE e.V. to help fund the next KDE developer meeting later this year.</p>


