---
aliases:
- ../announce-3.5.5
custom_about: true
custom_contact: true
date: '2006-10-11'
description: Project Ships Fifth Translation and Service Release for Leading Free
  Software Desktop.
title: KDE 3.5.5 Release Announcement
---

<h3 align="center">
   KDE Project Ships Fifth Translation and Service Release for Leading Free
   Software Desktop
</h3>

<p align="justify">
  KDE 3.5.5 features translations in 65 languages, improvements in the
  instant messaging application and in the HTML rendering engine (KHTML).
</p>

<p align="justify">
  The <a href="/">KDE
  Project</a> today announced the immediate availability of KDE 3.5.5,
  a maintenance release for the latest generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes. KDE now
  supports 65 languages, making it available to more people than most non-free
  software and can be easily extended to support others by communities who wish
  to contribute to the open source project.
</p>

<p align="justify">
  Significant enhancements include:
</p>

<ul>
  <li>
    Version 0.12.3 of <a href="http://kopete.kde.org/">Kopete</a>
    replaces 0.11.3 in KDE 3.5.5, it includes support for
    <a href="http://www.adiumx.com/">Adium</a> themes, performance improvements
    and better support for the <a href="http://messenger.yahoo.com/">Yahoo!</a>
    and <a href="http://www.jabber.org/">Jabber</a> protocols.
  </li>
  <li>
    Support for sudo in kdesu.
  </li>
  <li>
    Support for input shape from XShape1.1 in KWin (KDE window manager).
  </li>
  <li>
    Lots of speed improvements and fixes in
    <a href="http://www.konqueror.org">Konqueror</a>'s HTML engine,
    KHTML.
  </li>
  <li>
    <a href="http://www.cups.org/">CUPS</a> 1.2 support in
    <a href="http://printing.kde.org/">KDEPrint</a>.
  </li>
  <li>
    Big improvements in the number of translated interface elements in
    <a href="http://l10n.kde.org/stats/gui/stable/zh_TW/index.php">Chinese Traditional</a>,
    <a href="http://l10n.kde.org/stats/gui/stable/fa/index.php">Farsi</a>,
    <a href="http://l10n.kde.org/stats/gui/stable/km/index.php">Khmer</a>,
    <a href="http://l10n.kde.org/stats/gui/stable/nds/index.php">Low Saxon</a>
    and <a href="http://l10n.kde.org/stats/gui/stable/sk/index.php">Slovak</a>
    translations.
  </li>
</ul>

<p align="justify">
  For a more detailed list of improvements since
  <a href="/announcements/announce-3.5.4">the KDE 3.5.4 release</a>
  on the 2nd August 2006, please refer to the
  <a href="/announcements/changelogs/changelog3_5_4to3_5_5">KDE 3.5.5 Changelog</a>.
</p>

<p align="justify">
  KDE 3.5.5 ships with a basic desktop and fifteen other packages (PIM,
  administration, network, edutainment, utilities, multimedia, games,
  artwork, web development and more). KDE's award-winning tools and
  applications are available in <strong>65 languages</strong>.
</p>

<h4>
  Distributions shipping KDE
</h4>
<p align="justify">
  Most of the Linux distributions and UNIX operating systems do not immediately
  incorporate new KDE releases, but they will integrate KDE 3.5.5 packages in
  their next releases. Check
  <a href="/distributions">this list</a> to see
  which distributions are shipping KDE.
</p>

<h4>
  Installing KDE 3.5.5 Binary Packages
</h4>
<p align="justify">
  <em>Package Creators</em>.
  Some operating system vendors have kindly provided binary packages of
  KDE 3.5.5 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  download server at
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.5/">download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE
  Project has been informed, please visit the
  <a href="/info/1-2-3/3.5.5">KDE 3.5.5 Info Page</a>.
</p>

<h4>
  Compiling KDE 3.5.5
</h4>
<p align="justify">
  <a id="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 3.5.5 may be
  <a href="http://download.kde.org/stable/3.5.5/src/">freely
  downloaded</a>.  Instructions on compiling and installing KDE 3.5.5
  are available from the <a href="/info/1-2-3/3.5.5">KDE
  3.5.5 Info Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> project that exists and grows only because of the
help of many volunteers that donate their time and effort. KDE
is always looking for new volunteers and contributions, whether its
help with coding, bug fixing or reporting, writing documentation,
translations, promotion, money, etc. All contributions are gratefully
appreciated and eagerly accepted. Please read through the <a href="/community/donations/">Supporting
KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an <a href="/community/awards/">award-winning</a>, independent <a href="/people/">project of hundreds</a>
  of developers, translators, artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.</p>

<p align="justify">
  KDE provides a stable, mature desktop including a state-of-the-art browser
  (<a href="http://konqueror.kde.org/">Konqueror</a>), a personal information
  management suite (<a href="http://kontact.org/">Kontact</a>), a full 
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking application and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE<sup>&#174;</sup> and the K Desktop Environment<sup>&#174;</sup> logo are 
  registered trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group in the United States and
  other countries.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>

<hr />

<h4>Press Contacts</h4>
<table cellpadding="10" align="center"><tr valign="top">
<td>

Africa<br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />
</td>

<td>
Asia and India<br />
  Pradeepto Bhattacharya<br/>
  A-4 Sonal Coop. Hsg. Society<br/>
  Plot-4, Sector-3,<br/>
  New Panvel,<br/>
  Maharashtra.<br/>
  India 410206<br/>
  Phone : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
Europe<br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
North America<br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
Oceania<br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
South America<br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>

</tr></table>
