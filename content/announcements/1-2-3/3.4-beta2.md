---
aliases:
- ../announce-3.4beta2
custom_about: true
custom_contact: true
date: 2005-02-09
title: Announcing KDE 3.4 Beta 2 (\"Keinstein\")
---

DATELINE February 9, 2005

### KDE Project Ships Second Beta of Next Major Release

February 9, 2005 (The Internet) - The <a href="/">KDE Project</a> is pleased to announce the immediate availability of KDE 3.4 Beta 2, dubbed "Keinstein".

#### Getting Keinstein

KDE 3.4 Beta 2 can be downloaded over the Internet by visiting <a href="http://download.kde.org/unstable/3.3.92/src">download.kde.org</a>. Source code and vendor supplied binary packages are available. For additional information on package availability and to read further release notes, please visit the <a href="/info/1-2-3/3.4beta2">KDE 3.4 Beta 2 information page</a>.

The KDE team asks everyone to try the version and give feedback through <a href="http://bugs.kde.org">the bug tracking system</a>.

#### Supporting KDE

KDE is supported through voluntary contributions of time, money and resources by individuals
and companies from around the world. To discover how you or your company can join in and help
support KDE please visit the <a href="/community/donations/">Supporting KDE</a> web page. There
may be more ways to support KDE than you imagine, and every bit of support helps make KDE
a better project and a better product for everyone. Communicate your support today with a monetary donation,
new hardware or a few hours of your time!

Especially for beta releases, we can need any helping hand that is offered us to categorize and fix problem reports</a>

#### KDE Sponsorship

Besides the superb and invaluable efforts by the
<a href="http://www.kde.org/people/">KDE developers</a>
themselves, significant support for KDE development has been provided by
<a href="http://www.mandrakesoft.com/">MandrakeSoft</a>,
<a href="http://www.trolltech.com/">Trolltech</a> and
<a href="http://www.suse.com/">SUSE/Novell</a>.
<a href="http://www.ibm.com/">IBM</a> has donated significant hardware
to the KDE Project, and the
<a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
and the <a href="http://www.uni-kl.de/">University of Kaiserslautern</a>
provide most of the Internet bandwidth for the KDE project. Thanks!

#### About KDE

KDE is an independent project of hundreds of developers, translators,
artists and other professionals worldwide collaborating over the Internet
to create and freely distribute a sophisticated, customizable and stable
desktop and office environment employing a flexible, component-based,
network-transparent architecture and offering an outstanding development
platform. KDE provides a stable, mature desktop, a full, component-based
office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
set of networking and administration tools and utilities, and an
efficient, intuitive development environment featuring the excellent IDE
<a href="http://www.kdevelop.org/">KDevelop</a>. KDE is working proof
that the Open Source "Bazaar-style" software development model can yield
first-rate technologies on par with and superior to even the most complex
commercial software.

<hr noshade="noshade" size="1" width="98%" align="center" />

  <font size="2">
  <em>Trademark Notices.</em>
  KDE and K Desktop Environment are trademarks of KDE e.V.

Linux is a registered trademark of Linus Torvalds.

UNIX is a registered trademark of The Open Group in the United States and
other countries.

All other trademarks and copyrights referred to in this announcement are
the property of their respective owners.
</font>

<hr noshade="noshade" size="1" width="98%" align="center" />

#### Press Contacts

<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#x6d;&#x61;&#00105;&#108;&#00116;o&#58;&#105;&#110;&#x66;o-&#097;&#x66;r&#105;ca&#064;k&#100;e&#46;org">&#x69;n&#x66;&#x6f;-&#00097;frica&#00064;kd&#x65;.&#x6f;&#0114;g</a><br />

</td>

<td>
<b>Asia</b><br />
Sirtaj S. Kang <br />
C-324 Defence Colony <br />
New Delhi <br />
India 110024 <br />
Phone: +91-981807-8372 <br />
<a href="&#x6d;&#0097;&#x69;&#x6c;&#x74;&#x6f;:&#105;&#0110;fo&#x2d;&#x61;&#x73;ia&#x40;kde&#0046;&#111;rg">&#105;nfo&#x2d;&#097;&#115;i&#0097;&#x40;&#107;&#00100;&#0101;&#x2e;&#111;rg</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;&#x69;&#0108;to&#58;&#x69;&#x6e;&#x66;o&#45;e&#117;&#x72;&#x6f;&#x70;&#x65;&#x40;k&#x64;&#101;.&#x6f;r&#103;">i&#00110;fo-europ&#101;&#64;&#107;d&#00101;&#0046;&#111;r&#103;</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="&#0109;ailto:in&#x66;&#111;-&#00110;&#111;&#114;&#x74;&#x68;a&#00109;eri&#99;&#97;&#x40;kde&#0046;&#0111;r&#103;">info&#0045;&#110;o&#0114;&#0116;&#0104;a&#109;er&#105;&#0099;a&#00064;&#107;&#0100;&#x65;.&#111;&#114;g</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="ma&#105;lto&#x3a;&#00105;nfo&#x2d;o&#0099;&#x65;&#0097;ni&#x61;&#x40;k&#x64;&#101;.o&#x72;g">in&#x66;o-oce&#x61;n&#105;a&#x40;&#00107;d&#x65;&#46;&#x6f;r&#103;</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="&#x6d;ai&#x6c;to:in&#102;o&#x2d;&#115;&#x6f;&#0117;&#x74;hamer&#0105;&#x63;a&#00064;kde&#00046;&#0111;r&#00103;">info-s&#x6f;&#x75;&#116;ha&#00109;er&#105;&#0099;&#97;&#0064;&#00107;de&#46;&#111;rg</a><br />
</td>

</tr></table>
