---
aliases:
- ../announce-2.0
custom_about: true
custom_contact: true
date: '2000-10-23'
description: The KDE Team today announced the release of KDE 2.0, KDE's powerful,
  modular, Internet-enabled desktop. This highly anticipated release constitutes the
  next generation of the award-winning KDE 1 series, which culminated in the release
  of KDE 1.1.2 just over a year ago. KDE 2.0 is the work product of hundreds of dedicated
  developers originating from over 30 countries.
title: KDE 2.0 Release Announcement
---

FOR IMMEDIATE RELEASE

<h3 ALIGN="center">New KDE Release Is a Major Advance for Linux<sup>&reg;</sup> Desktop</h3>

<strong>Next Generation of Leading Desktop for Linux<sup>&reg;</sup>
and Other UNIXes<sup>&reg;</sup> Ships</strong>

October 23, 2000 (The INTERNET). The <a href="/">KDE
Team</a> today announced the release of KDE 2.0
KDE's powerful, modular, Internet-enabled desktop. This highly anticipated
release constitutes the next generation of the
<a href="/community/awards">award-winning</a> KDE 1
series, which culminated in the
<a href="../1.1.2">release of
KDE 1.1.2</a> just over a year ago. KDE 2.0 is the work product
of hundreds of dedicated developers originating from over 30 countries.

"With the experience gained from developing KDE 1, we
almost completely re-engineered KDE 2 to make it even more intuitive,
powerful and user friendly," explained
<a href="&#109;&#0097;&#x69;lt&#0111;:et&#x74;r&#0105;c&#x68;&#0064;k&#x64;&#x65;&#46;&#x6f;&#x72;g">Matthias Ettrich</a>,
founder of the KDE project.
"We think that current KDE users will be pleasantly surprised with the
remarkable improvements we have achieved. KDE 2 offers
the desktop user the benefit of standards compliance and an array of
new technologies, from
<a href="#Konqueror">Konqueror</a>, a full featured web browser and
file manager, to <a href="#KOffice">KOffice</a>, an integrated
office suite, as well as a slew of usability enhancements, such as KDE's
expanded themeability and configurability and a new KDE Help Center.
It also offers developers an assortment of powerful new tools -- from
<a href="#KParts">KParts</a>, KDE's component object technology,
to <a href="#KIO">KIO</a>, KDE's network transparent I/O architecture
-- for rapid development and deployment of first-rate free or
proprietary software."

"KDE 2.0 is an important release," stated Ransom Love, president and
CEO of <a href="http://www.caldera.com/">Caldera Systems, Inc.</a>
"Our customers are anxious to migrate not only their servers but also
their desktops to the Linux technology. KDE 2.0 will be a key component
of OpenLinux eDesktop, our solution for a seamless and cost-effective
transition strategy."

"SuSE Linux views KDE 2 as one of
the key milestones to vault Linux to the same landslide success on the
desktop that it already has in the server space," added Dirk Hohndel,
CTO of <a href="http://www.suse.com/">Suse AG</a>. "We are excited to
be able to offer KDE 2.0 as the
default desktop with our next version of the SuSE Linux OS. I am
confident that third party developers will realize the enormous
potential KDE 2 offers and will migrate their applications
to Linux/KDE."

"As Linux-Mandrake focuses on making Linux easy to use, we are very
pleased to include KDE 2, a major evolution of the already superb KDE 1,
in our upcoming Linux-Mandrake 7.2 release", added
<a href="mailto:gduval@mandrakesoft.com">Ga�l Duval</a>,
co-founder of <a href="http://www.linux-mandrake.com/">Mandrakesoft</a>.
"With KDE 2 and KOffice, the KDE team demonstrates again their deep
commitment to make Linux a viable desktop alternative for all users."

"Corel has had a long, successful relationship with the KDE project, and
the release of KDE 2.0 is an important milestone for Linux," said Rene
Schmidt, Executive Vice President for Linux Products,
<a href="http://linux.corel.com/">Corel Corporation</a>.
"We believe that our customers will be ecstatic over the improvements
and new features of this landmark version. The enhancements to the
framework provide power for the desktop in the simple and elegant
fashion that our customers have grown to expect."

KDE 2.0 includes the core KDE libraries,
the core desktop environment, the initial release of the KOffice suite, as well
as the over 100 applications from the other standard base KDE packages:
Administration, Games,
Graphics, Multimedia, Network, Personal Information Management (PIM),
Toys and Utilities. KDE 2.0 is currently available in 15 languages
and translations into 20 additional languages will be available in the
coming weeks.

All of KDE 2.0 is available for free under an Open Source license.
Likewise,
<a href="http://www.trolltech.com/">Trolltech's</a><sup>tm</sup>
Qt<sup>&reg;</sup> 2.2.1, the GUI toolkit on which KDE is based,
is now available for free under two Open Source licenses: the
<a href="http://www.trolltech.com/products/download/freelicense/license.html">Q
Public License</a> and the <a href="http://www.gnu.org/copyleft/gpl.html">GNU
General Public License</a>.

More information about KDE 2 is available in a
<a href="http://devel-home.kde.org/~granroth/LWE2000/index.html">slideshow
presentation</a> and on
<a href="/">KDE's web site</a>, including an evolving
<a href="/info/1-2-3/faq-2.x">FAQ</a> to answer questions about
migrating to KDE 2.0 from KDE 1.x, a number of
<a href="/screenshots/kde2shots">screenshots</a>, <a href="http://developer.kde.org/documentation/kde2arch.html">developer information</a> and
a developer's
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html?rev=2.3">KDE 1 - KDE 2 porting guide</a>.

<strong><EM>KDE 2: The K Desktop Environment</EM></strong>.
<a id="Konqueror"></a><a href="http://konqueror.kde.org/">Konqueror</a>
is KDE 2's next-generation web browser,
file manager and document viewer. Widely heralded as a
technological break-through for the Linux desktop, the standards-compliant
Konqueror has a component-based architecture which combines the features and
functionality of Internet Explorer<sup>&reg;</sup>/Netscape
Communicator<sup>&reg;</sup> and Windows Explorer<sup>&reg;</sup>.
Konqueror will support the full gamut of current Internet technologies,
including JavaScript, Java<sup>&reg;</sup>, HTML 4.0, CSS-1 and -2
(Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
and Netscape Communicator<sup>&reg;</sup> plug-ins (for
playing Flash<sup>TM</sup>, RealAudio<sup>TM</sup>, RealVideo<sup>TM</sup>
and similar technologies). The great bulk of this technology is already
in place and functional for KDE 2.0.

KDE 2 also ships with the highly anticipated initial release of
<a id="KOffice"></a>the <a href="http://www.koffice.org/">KOffice
suite</a>. The integrated
suite consists of a spreadsheet application (KSpread), a vector drawing
application (KIllustrator), a frame-based word-processing application
(KWord), a presentation program
(KPresenter), and a chart and diagram application (KChart). Native file
formats are XML-based, and work on filters for proprietary binary file
formats is progressing. Combined with a powerful scripting language and the
ability to embed individuals components within each other using KDE's
component technology (KParts), the free KOffice suite will soon provide
all the necessary functionality to all but the most demanding power users.

In addition, <a id="KIO">KIO's</a> network transparency offers
seamless support for accessing
or browsing files on Linux, NFS shares, MS Windows<sup>&reg;</sup>
SMB shares, HTTP pages, FTP directories and LDAP directories. The modular,
plug-in nature of KDE's file architecture makes it simple to add additional
protocols (such as IPX or WebDAV) to KDE, which would then automatically be
available to all KDE applications.

KDE 2 introduces a new multimedia architecture based on <A
id="arts">aRts</a>, the Analog Realtime Synthesizer. ARts enables
playing multiple audio or video streams concurrently, whether on the
desktop or over a network. ARts is a full-featured sound system, and
includes filters, a modular analog synthesizer and a mixer. Its
architecture allows developers to create additional filter plugins and
users to apply sequences of filters using a graphical drag-n-drop
approach. Video support is <a
href="http://mpeglib.sourceforge.net/">available</a> for MPEG versions
1, 2 and 4 (experimental), as well as the AVI and DivX formats.

KDE's <a id="customizability">customizability</a> touches every
aspect of this next-generation
desktop. KDE's sophisticated theme support starts with Qt's
style engine, which permits developers and artists to create their
own widget designs. KDE 2.0 ships with over 14 of these styles,
some of which emulate the look of various operating systems, and additionally
does an excellent job of
<a href="ftp://ftp.kde.org/pub/kde/Incoming/gnome.png">importing themes</a>
from GTK and GNOME. Other configuration options permit users to: choose among
icon themes and system sounds (using a simple drop-and-replace approach);
configure key bindings; select from over 30 languages; customize toolbar
layouts and entries and menu composition; employ single-click or double-click
to activate desktop items; navigate the desktop using a keyboard
instead of a mouse; and much, much more. Moreover, KDE 2 fully
supports Unicode and KHTML is the only free HTML rendering engine on
Linux/X11 that features nascent support for BiDi scripts
such as Arabic and Hebrew.

Besides the exceptional compliance with Internet and file-sharing standards
<a href="#Konqueror">mentioned above</a>, KDE 2 achieves exceptional
compliance with the available Linux desktop standards. KWin, KDE's new
re-engineered window manager, complies to the new
<a href="http://www.freedesktop.org/standards/wm-spec.html">Window Manager
Specification</a>. Konqueror and KDE comply to the <A
href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/kio/DESKTOP_ENTRY_STANDARD?rev=1.9">Desktop
Entry Standard</a>. KDE 2 generally complies with the
<a href="http://www.newplanetsoftware.com/xdnd/">X Drag-and-Drop (XDND)
protocol</a> as well as with the
<a href="http://www.rzg.mpg.de/rzg/batch/NEC/sx4a_doc/g1ae04e/chap12.html">X11R6 session management protocol (XSMP)</a>.

<strong><EM>KDE 2: The K Development Environment</EM></strong>.
KDE 2.0 offers developers a rich set of major technological improvements over
the critically acclaimed KDE 1 series. Chief among these are
the <a href="#DCOP">Desktop COmmunication Protocol (DCOP)</a>, the
<a href="#KIO">I/O libraries (KIO)</a>, <a href="#KParts">the component
object model (KParts)</a>, an <a href="#XMLGUI">XML-based GUI class</a>, and
a <a href="#KHTML">standards-compliant HTML rendering engine (KHTML)</a>.

<a id="DCOP">DCOP</a> is a client-to-client communications
protocol intermediated by a
server over the standard X11 ICE library. The protocol supports both
message passing and remote procedure calls using an XML-RPC to DCOP "gateway".
Bindings for C, C++ and Python, as well as experimental Java bindings, are
available.

<a id="KIO-devel">KIO</a> implements application I/O in a separate
process to enable a
non-blocking GUI without the use of threads. The class is network transparent
and hence can be used seamlessly to access HTTP, FTP, POP, IMAP,
NFS, SMB, LDAP and local files. Moreover, its modular
and extensible design permits developers to "drop in" additional protocols,
such as WebDAV, which will then automatically be available to all KDE
applications. KIO also implements a trader which can locate handlers
for specified mimetypes; these handlers can then be embedded within
the requesting application using the KParts technology.

<a id="KParts">KParts</a>, KDE 2's component object model, allows
an application to embed another within itself. The technology handles
all aspects of the embedding, such as positioning toolbars and inserting
the proper menus when the embedded component is activated or deactivated.
KParts can also interface with the KIO trader to locate available handlers for
specific mimetypes or services/protocols.
This technology is used extensively by the KOffice suite and
Konqueror.

The <a id="XMLGUI">XML GUI</a> employs XML to create and position
menus, toolbars and possibly
other aspects of the GUI. This technology offers developers and users
the advantage of simplified configurability of these user interface elements
across applications and automatic compliance with the
<a href="http://developer.kde.org/documentation/standards/">KDE Standards
and Style Guide</a> irrespective of modifications to the standards.

<a id="KHTML">KHTML</a> is an HTML 4.0 compliant rendering
and drawing engine. The class
will support the full gamut of current Internet technologies, including
JavaScript<sup>TM</sup>, Java<sup>&reg;</sup>, HTML 4.0, CSS-2
(Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
and Netscape Communicator<sup>&reg;</sup> plugins (for
viewing Flash<sup>TM</sup>,
RealAudio<sup>TM</sup>, RealVideo<sup>TM</sup> and similar technologies).
The KHTML class can easily
be used by an application as either a widget (using normal X Window
parenting) or as a component (using the KParts technology).
KHTML, in turn, has the capacity to embed components within itself
using the KParts technology.

#### Downloading and Compiling KDE 2.0

The source packages for KDE 2.0 are available for free download at
<a href="http://ftp.kde.org/stable/2.0/distribution/tar/generic/src/">http://ftp.kde.org/stable/2.0/distribution/tar/generic/src/</a> or in the
equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>. KDE 2.0 requires
qt-2.2.1, which is available from the above locations under the name
<a href="http://ftp.kde.org/stable/2.0/distribution/tar/generic/src/qt-x11-2.2.1.tar.gz">qt-x11-2.2.1.tar.gz</a>. KDE 2.0 will not work with
older versions of Qt.

For further instructions on compiling and installing KDE 2.0, please consult
the <a href="http://developer.kde.org/build/index.html">installation
instructions</a> and, if you encounter problems, the
<a href="http://developer.kde.org/build/index.html">compilation FAQ</a>.

#### Installing Binary Packages

Some distributors choose to provide binary packages of KDE for certain
versions of their distribution. Some of these binary packages for KDE
2.0
will be available for free download under
<a href="http://ftp.kde.org/stable/2.0/distribution/">http://ftp.kde.org/stable/2.0/distribution/</a>
or under the equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution.

KDE 2.0 requires qt-2.2.1, the free version of which is available
from the above locations usually under the name qt-x11-2.2.1. KDE 2.0
will not work with older versions of Qt.

At the time of this release, pre-compiled packages are available for:

<ul>
<li><a href="http://ftp.kde.org/stable/2.0/distribution/rpm/COL-2.4/">Caldera OpenLinux 2.4</a></li>
<li><a href="http://ftp.kde.org/stable/2.0/distribution/deb/Debian/dists/potato/">Debian GNU/Linux 2.2 (potato)</a> and <a href="http://ftp.kde.org/stable/2.0/distribution/deb/Debian/dists/woody/">Debian GNU/Linux Devel (woody)</a></li>
<li><a href="http://ftp.kde.org/stable/2.0/distribution/rpm/Mandrake/">Linux-Mandrake 7.2</a></li>
<li><a href="http://ftp.kde.org/stable/2.0/distribution/rpm/RedHat/rh7/">RedHat Linux 7.0</a>
</li>
<li><a href="http://ftp.kde.org/stable/2.0/distribution/rpm/SuSE/6.4-i386/">SuSE Linux 6.4 (i386)</a>, <a href="http://ftp.kde.org/stable/2.0/distribution/rpm/SuSE/7.0-i386/">SuSE Linux 7.0 (i386)</a>, <a href="http://ftp.kde.org/stable/2.0/distribution/rpm/SuSE/7.0-ppc/">SuSE Linux 7.0 (ppc)</a> and <a href="http://ftp.kde.org/stable/2.0/distribution/rpm/SuSE/7.0-sparc/">SuSE Linux 7.0 (sparc)</a></li>
<li><a href="http://ftp.kde.org/stable/2.0/distribution/tar/true64/">Tru64 Systems</a></li>
</ul>

Please check the servers periodically for pre-compiled packages for other
distributions. More binary packages will become available over the
coming days and weeks.

#### About KDE

KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.

For more information about KDE, please visit KDE's <a href="http://www.kde.org/whatiskde/">web site</a>.

<table border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
    Kurt Granroth <br>
    
  [granroth@kde.org](mailto:granroth@kde.org)
    <br>
    (1) 480 732 1752<br>&nbsp;<br>
    Andreas Pour<br>
    [pour@kde.org](mailto:pour@kde.org)<br>
    (1) 718-456-1165
  </td>
</tr>
<tr valign="top"><TD>
Europe (French and English):
</TD><TD >
David Faure<BR>

[faure@kde.org](mailto:faure@kde.org)<BR>
(44) 1225 837409

</TD></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Martin Konold<br>
    
  [konold@kde.org](mailto:konold@kde.org)
    <br>
    (49) 179 2252249
  </td>
</tr>
</table>
