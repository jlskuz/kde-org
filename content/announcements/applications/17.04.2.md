---
aliases:
- ../announce-applications-17.04.2
changelog: true
date: 2017-06-08
description: KDE Ships KDE Applications 17.04.2
layout: application
title: KDE Ships KDE Applications 17.04.2
version: 17.04.2
---

June 8, 2017. Today KDE released the second stability update for <a href='../17.04.0'>KDE Applications 17.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 15 recorded bugfixes include improvements to kdepim, ark, dolphin, gwenview, kdenlive, among others.

This release also includes Long Term Support version of KDE Development Platform 4.14.33.