---
date: 2021-05-04
changelog: 5.21.4-5.21.5
layout: plasma
youtube: ahEWG4JCA1w
asBugfix: true
draft: false
---

+ Fix crash when stopping PipeWire streaming. [Commit.](http://commits.kde.org/kwin/3a51749f09a95e37cc225c4c3f920925fc1de64b) 
+ Fix crash on drag-and-drop over panel. [Commit.](http://commits.kde.org/plasma-workspace/9e986f8c85ad79e75ed43c1af751ded44afbbc5b) Fixes bug [#398440](https://bugs.kde.org/398440)
+ Lockscreen: also activate lock screen windows. [Commit.](http://commits.kde.org/kwin/a8a4c9a881ecd57327b0c35f723292772f4537f3) Fixes bug [#427882](https://bugs.kde.org/427882)
+ Platforms/drm: Fix crash in EglGbmBackend::presentOnOutput(). [Commit.](http://commits.kde.org/kwin/5a57eeafc840e97e14b4ed465a60f02dae57d6d1) 
