---
aliases:
- ../../plasma-5.9.5
changelog: 5.9.4-5.9.5
date: 2017-04-25
layout: plasma
youtube: lm0sqqVcotA
figure:
  src: /announcements/plasma/5/5.9.0/plasma-5.9.png
  class: text-center mt-4
asBugfix: true
---

- Plastik window decoration now supports global menu. <a href="https://commits.kde.org/kwin/3e0ddba683ca68cb50b403cb3893fa2fc9d2d737">Commit.</a> See bug <a href="https://bugs.kde.org/375862">#375862</a>. Phabricator Code review <a href="https://phabricator.kde.org/D5131">D5131</a>
- Media Controller can now properly handle and seek long tracks (&gt; 30 minutes). <a href="https://commits.kde.org/plasma-workspace/550860f6366cc99d3f0ff19f74fd3fc3d1bfc0ad">Commit.</a> Fixes bug <a href="https://bugs.kde.org/377623">#377623</a>
- Sort the themes in decoration KCM. <a href="https://commits.kde.org/kwin/f5a43877a9ea6ddad9eaa8d7498c8ea518c29c81">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D5407">D5407</a>
