---
aliases:
- ../../plasma-5.10.4
changelog: 5.10.3-5.10.4
date: 2017-07-18
layout: plasma
youtube: VtdTC2Mh070
figure:
  src: /announcements/plasma/5/5.10.0/plasma-5.10.png
  class: text-center mt-4
asBugfix: true
---

- [Windowed Widgets Runner] Fix launching widget. <a href="https://commits.kde.org/plasma-workspace/8c5a75341849a621462c41bb685bb46dfef129e1">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D6602">D6602</a>
- [Notifications] Check for corona to avoid crash. <a href="https://commits.kde.org/plasma-workspace/8a05294e5b3ef1df86f099edde837b8c8d28ccaf">Commit.</a> Fixes bug <a href="https://bugs.kde.org/378508">#378508</a>. Phabricator Code review <a href="https://phabricator.kde.org/D6653">D6653</a>
- System Setting: Honour the NoDisplay attribute of KServices. <a href="https://commits.kde.org/systemsettings/85ed16cd422804971345bc492757fa0050b4b61d">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D6612">D6612</a>
