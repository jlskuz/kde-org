---
aliases:
- ../../plasma-5.11.5
changelog: 5.11.4-5.11.5
date: 2018-01-02
layout: plasma
youtube: nMFDrBIA0PM
figure:
  src: /announcements/plasma/5/5.11.0/plasma-5.11.png
  class: text-center mt-4
asBugfix: true
---

- Fixed a missing icon in the appmenu applet
- Better error handling in Discover
- Improved GTK support for the Breeze theme
- Fixed a crash in the screen locker on wayland
