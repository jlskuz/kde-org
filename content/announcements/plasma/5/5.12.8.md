---
aliases:
- ../../plasma-5.12.8
changelog: 5.12.7-5.12.8
date: 2019-03-05
layout: plasma
youtube: xha6DJ_v1E4
figure:
  src: /announcements/plasma/5/5.12.0/plasma-5.12.png
  class: text-center mt-4
asBugfix: true
---

- Plasma-pa: fix connecting to PulseAudio with Qt 5.12. <a href="https://commits.kde.org/plasma-pa/3b3b8c3d60e48db47d7b86e61f351bac03fd57b7">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D16443">D16443</a>
- weather dataengine: bbc,envcan,noaa: various fixes to update parsers. <a href="https://commits.kde.org/plasma-workspace/03e13b10d877733528d75f20a3f1d706088f7b9b">Commit.</a>
- SDDM KCM: fix autologin session loading. <a href="https://commits.kde.org/sddm-kcm/634a2dd1bef5dd8434db95a391300f68f30fd14e">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D18765">D18765</a>
