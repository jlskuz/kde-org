---
aliases:
- ../../plasma-5.3.1
changelog: 5.3.0-5.3.1
date: 2015-05-26
layout: plasma
figure:
  src: /announcements/plasma/5/5.3.0/plasma-5.3.png
---

{{< i18n_date >}}

{{< i18n "annc-plasma-bugfix-intro" "5" "5.3.1" >}}

{{% i18n "annc-plasma-bugfix-minor-release-4" "5.3" "/announcements/plasma/5/5.3.0" "2015" %}}

{{< i18n "annc-plasma-bugfix-worth-5" >}}

{{< i18n "annc-plasma-bugfix-last" >}}

- <a href="https://sessellift.wordpress.com/2015/05/14/in-free-software-its-okay-to-be-imperfect-as-long-as-youre-open-and-honest-about-it/">show desktop</a> has now been fixed
