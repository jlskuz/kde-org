---
date: 2021-03-16
changelog: 5.21.2-5.21.3
layout: plasma
youtube: ahEWG4JCA1w
asBugfix: true
draft: false
---

+ BlueDevil: Enable Bluetooth checkbox should be always enabled. [Commit.](http://commits.kde.org/bluedevil/4b4b0dd119b7bff79ece33bb253d746660cdb936) Fixes bug [#433232](https://bugs.kde.org/433232)
+ Plasma Addons: Fix outdated API key for flickr provider. [Commit.](http://commits.kde.org/kdeplasma-addons/905c21875e866ab59cc338ccff9ecd1c5ca84f21) See bug [#427566](https://bugs.kde.org/427566)
+ Re-add Force Font DPI spinbox on Wayland. [Commit.](http://commits.kde.org/plasma-workspace/1278ebee2a6e72de2b3600d2baa61e57378f3d9f) Fixes bug [#433115](https://bugs.kde.org/433115)
