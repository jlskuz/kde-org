---
date: 2022-10-18
changelog: 5.26.0-5.26.1
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Fuzzy-clock: Fix colors in full representation (calendar view). [Commit.](http://commits.kde.org/kdeplasma-addons/20b1d79134354b404c9ca15c59892d97a3046175) 
+ Colorpicker: Make left-clicking a color copy in the default format. [Commit.](http://commits.kde.org/kdeplasma-addons/6b809a159d010cab2065484c47ba01599720d574) Fixes bug [#457311](https://bugs.kde.org/457311)
+ Plasma Audio Volume Control: Don't crash when the server doesn't respond. [Commit.](http://commits.kde.org/plasma-pa/89d33990bbd4e63e4624a01fb0e74c341ac50fa4) Fixes bug [#454647](https://bugs.kde.org/454647). Fixes bug [#437272](https://bugs.kde.org/437272)
