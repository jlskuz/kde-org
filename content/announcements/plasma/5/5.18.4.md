---
aliases:
- ../../plasma-5.18.4
changelog: 5.18.3-5.18.4
date: 2020-03-31
layout: plasma
peertube: cda402b5-2bcb-4c0c-b232-0fa5a4dacaf5
figure:
  src: /announcements/plasma/5/5.18.0/plasma-5.18.png
asBugfix: true
---

- [Image Wallpaper] Fix thumbnail generation when model is reloaded in-flight. <a href="https://commits.kde.org/plasma-workspace/492301406a4656fbc6c9a1be0e77e68c5535bf93">Commit.</a> Fixes bug <a href="https://bugs.kde.org/419234">#419234</a>. Phabricator Code review <a href="https://phabricator.kde.org/D28420">D28420</a>
- [applets/systemtray] Clear item from shown/hidden list when disabling entry. <a href="https://commits.kde.org/plasma-workspace/fede85a3d0ea5a30755582e947cabd6bc7d1e4b8">Commit.</a> Fixes bug <a href="https://bugs.kde.org/419197">#419197</a>. Phabricator Code review <a href="https://phabricator.kde.org/D28311">D28311</a>
- [GTK Config] Construct font style by hand instead of relying on Qt function. <a href="https://commits.kde.org/kde-gtk-config/a581035b3f4793d96e9b5d2cf6b55191cbb4be91">Commit.</a> Fixes bug <a href="https://bugs.kde.org/333146">#333146</a>. Phabricator Code review <a href="https://phabricator.kde.org/D27380">D27380</a>
