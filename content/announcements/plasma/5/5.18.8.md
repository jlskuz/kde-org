---
date: 2021-10-19
changelog: 5.18.7-5.18.8
layout: plasma
youtube: npJEpfXGGfI
---

{{< i18n_date >}}

{{< i18n "annc-plasma-bugfix-intro" "5" "5.18.8" >}}

{{% i18n "annc-plasma-bugfix-minor-release-2" "5.18" "/announcements/plasma/5/5.18.0" "2020" %}}

{{< i18n "annc-plasma-bugfix-worth-9" >}}

{{< i18n "annc-plasma-bugfix-last" >}}

+ KDE GTK Config: Make sure to actually commit GSettings changes. [Commit.](http://commits.kde.org/kde-gtk-config/57cc9bd8d92bc48b6a27af63da39fa33a0b13bf5)
+ Plasma Desktop: Fix renaming shortcut for files selected via selection button and popup. [Commit.](http://commits.kde.org/plasma-desktop/f36b79311f097f08508f78e626fdadf637da4197) Fixes bug [#425436](https://bugs.kde.org/425436)
+ Plasma Workspace: Sync night colour default values. [Commit.](http://commits.kde.org/plasma-workspace/cfd55830571738958a2f98999ed0f5a692b153c3) Fixes bug [#442253](https://bugs.kde.org/442253)
