---
title: KDE Ships First Beta of Next Generation Plasma Workspace
date: 2014-05-14
description: KDE Ships First Beta of Plasma Next.
aliases:
- ../announce-plasma-2014.6-beta1
- ../announce-plasma-next-beta1
- ../plasma/5/0-beta1
---

{{<figure src="/announcements/plasma/next-beta1/ss-wallies.png" class="text-center" alt="Plasma Next" width="600px">}}

KDE today releases the <a href='/announcements/announce-plasma-2014.6-beta1'>first Beta version of the next-generation Plasma workspace</a>. The Plasma team would like to ask the wider Free Software community to test this release and give any feedback . Plasma Next is built using QML and runs on top of a fully hardware-accelerated graphics stack using Qt 5, QtQuick 2 and an OpenGL(-ES) scenegraph. Plasma Next provides a core desktop experience that will be easy and familiar for current users of KDE workspaces or alternative Free Software or proprietary offerings. You can find more details on the upcoming Plasma Next release in the <a href='http://dot.kde.org/2014/04/02/kde-releases-alpha-version-next-gen-plasma-workspace'>alpha release announcement</a>. Plasma Next is <a href='http://techbase.kde.org/Schedules/Plasma/2014.6_Release_Schedule'>planned to be released</a> in early July.

## Major changes

{{<figure src="/announcements/plasma/next-beta1/kicker-menu.png" class="text-center" alt="Kicker Menu in Plasma Next" width="300px">}}

Since the alpha, a wide range of changes has been made. Many are of course small yet important stability, performance and bug fixes, but there have also been larger changes. For example, the introduction of a new Application Menu widget, which is a reimplementation of what originally debuted as 'Homerun Kicker' in the homerun package. See this recent <a href='http://blogs.kde.org/2014/01/29/homerun-120'>blog about Homerun</a>. Homerun has been proven to be very popular, with some distributions picking it up as their default.

For the first time KDE is shipping its own font. Oxygen Font is designed to be optimised for the FreeType font rendering system and works well in all graphical user interfaces, desktops and devices.

## Ready for testing, not production

{{<figure src="/announcements/plasma/next-beta1/calendar.png" class="text-center" alt="Clock and calendar in Plasma Next" width="600px">}}

The workspace demonstrated in this pre-release is Plasma Desktop. It represents an evolution of known desktop and laptop paradigms. Plasma Next keeps existing workflows intact, while providing incremental visual and interactive improvements. Many of those can be observed in this technology preview, others are still being worked on. Workspaces optimized for other devices will be made available in future releases.

As an Beta release, this pre-release is not suitable for production use. It is meant as a base for testing and gathering feedback, so that the initial stable release of Plasma Next in July will be a smooth ride for everybody involved and lay a stable foundation for future versions. Plasma Next is intended for end users, but will not provide feature parity with the latest 4.x release, which will come in follow-up releases. The team is concentrating on the core desktop features first, instead of trying to transplant every single feature into the new workspaces. The feature set presented in Plasma Next will suffice for most users, though some might miss a button here and there. This is not because the Plasma team wants to remove features, but simply that not everything has been done yet. Of course, everybody is encouraged to help bringing Plasma back to its original feature set and beyond.

## Known issues

<strong>Stability</strong> is not yet up to the level where the developers want Plasma Next. With a substantial new toolkit stack below come exciting new crashes and problems that need time to be shaken out.

<strong>Performance</strong> of Plasma Next is heavily dependent on specific hardware and software configurations and usage patterns. While it has great potential, it takes time to wrangle this out of it and the underlying stack is not entirely ready for this either. In some scenarios, Plasma Next will display the buttery smooth performance it is capable off - while at other times, it will be hampered by various shortcomings. These can and will be addressed, however, much is dependent on components like Qt, Mesa and hardware drivers lower in the stack. Again, this will need time, as fixes made in Qt now simply won't be released by the time the first Plasma Next version becomes available.

<strong>Polish</strong> is a major benefit of QML2, as it allows seamless usage of openGL, much more precise positioning and many other abilities. At the same time, the immaturity of Qt Quick Controls, the brand new successor to the 15+ year old Qt Widgets technology, brings some rough edges yet to be smoothed out.

<strong>Design</strong> is not yet finalized. Much of the work on theming has not made it in yet and the state of design in this beta is not representative for the first Plasma Next release. Below is a glimpse into the new design that the Visual Design Group is working on and which will be released once it's ready.

{{<figure src="/announcements/plasma/next-beta1/Announce-Plasma_Next_2014_beta_1.png" class="text-center" alt="Current draft of new design" width="600px">}}

## For developers

Plasma Next builds on top of Qt 5. With this transition, all QML-based UIs—which Plasma is built exclusively with—will make use of a new scenegraph and scripting engine, resulting in huge performance wins as well as architectural benefits, such as being able to render using available graphics hardware.

Plasma Next is the first complex codebase to transition to <a href='http://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>, which is a modular evolution of the KDE development platform into leaner, less interdependent libraries.

## For users

Users testing this Plasma pre-release are greeted with a more refined visual appearance. The new Breeze Plasma theme debuts in this pre-release with a flatter, cleaner look. Less visual clutter and improved contrast make Plasma Next a noticeable improvement over the current stable Plasma workspaces. There has been some polish to much of Plasma's default functionality, such as the system tray area, the notifications, the settings for the compositor and window manager, and many more. While it will feel familiar, users will notice a more modern workspace.

## Installing and providing feedback

The easiest way to try it out is the <a href='http://files.kde.org/snapshots/neon5-latest.iso'>Neon5 ISO</a>, a live OS image updated with the latest builds straight from source.

Some distributions have created, or are in the process of creating, packages; for an overview of Beta 1 packages, see <a href='http://community.kde.org/Plasma/Next/UnstablePackages'>our unstable packages wiki page</a>

<a href='http://download.kde.org/unstable/plasma/4.96.0/src/'>Source download</a>. You can install Plasma Next directly from source. KDE's community wiki has <a href='http://community.kde.org/Frameworks/Building'>instructions</a>. Note that Plasma Next does not co-install with Plasma 1, you will need to uninstall older versions or install into a separate prefix.

You can provide feedback either via the <a href='irc://#plasma@freenode.net'>#Plasma IRC channel</a>, <a href='https://mail.kde.org/mailman/listinfo/plasma-devel'>Plasma-devel mailing list</a> or report issues via <a href='https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided'>bugzilla</a>. Plasma Next is also <a href='http://forum.kde.org/viewforum.php?f=287'>discussed on the KDE Forums</a>. Your feedback is greatly appreciated. If you like what the team is doing, please let them know!

## {{< i18n "supporting-kde" >}}

{{% i18n "whatiskde" %}}
