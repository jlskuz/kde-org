---
title: KDE Ships First Alpha of Next Generation Plasma Workspace
date: 2014-04-02
description: KDE Ships First Alpha of Plasma 2014.6.
aliases:
- ../announce-plasma-2014.6-alpha1
- ../plasma/5/0-alpha1
---

{{<figure src="/announcements/plasma/2014.6-alpha1/kickoff2.jpg" class="text-center" alt="Plasma Next" width="600px">}}

KDE today releases the first Alpha version of the next-generation Plasma workspace. This kicks off the public testing phase for the next iteration of the popular Free software workspace, code-named 'Plasma Next' (referring to the 'next' Plasma release-more below). Plasma Next is built using QML and runs on top of a fully hardware-accelerated graphics stack using Qt 5, QtQuick 2 and an OpenGL(-ES) scenegraph. Plasma Next provides a core desktop experience that will be easy and familiar for current users of KDE workspaces or alternative Free Software or proprietary offerings. Plasma Next is planned to be released as 2014.06 on the 17th of June.

#### Installing Plasma Next Alpha 1 Binary Packages

<em>Packages</em>.
A variety of distributions offer frequently updated packages of Plasma. This includes Kubuntu, Fedora and openSUSE. See <a href='http://community.kde.org/Plasma/Next/UnstablePackages'>this wikipage</a> for an overview.

#### Compiling Plasma

The complete source code for Plasma Next Alpha 1 may be <a href='http://download.kde.org/unstable/plasma/4.95.0/'>freely downloaded</a>. Planned future releases are listed on the <a href='http://techbase.kde.org/Schedules/Plasma/2014.6_Release_Schedule'>the Plasma release schedule.</a>

It comes with three tars which do not co-install with
the equivalents using KDE libs 4. You will need to uninstall these
older versions or install into a separate prefix.

#### {{< i18n "supporting-kde" >}}

{{% i18n "whatiskde" %}}
