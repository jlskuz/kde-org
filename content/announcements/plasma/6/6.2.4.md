---
date: 2024-11-26
changelog: 6.2.3-6.2.4
layout: plasma
video: false
asBugfix: true
draft: false
---

+ libkscreen Doctor: clarify the meaning of max. brightness zero. [Commit.](http://commits.kde.org/libkscreen/7660ca1b6ccd2beeaaf40f91f8fd64ec3e6bb704) Fixes bug [#495557](https://bugs.kde.org/495557)
+ Plasma Workspace: Battery Icon, Add headphone icon. [Commit.](http://commits.kde.org/plasma-workspace/c9f787e96dc4c2b62ceb80403b706abd26539794)
+ Plasma Audio Volume Control: Fix speaker test layout for Pro-Audio profile. [Commit.](http://commits.kde.org/plasma-pa/0b4a26ada8258f4d25fa5069f239f1041eefecb5) Fixes bug [#495752](https://bugs.kde.org/495752)
