---
date: 2024-08-06
changelog: 6.1.3-6.1.4
layout: plasma
video: false
asBugfix: true
draft: false
---

+ DrKonqi: Use frameworks version number from kcrash. [Commit.](http://commits.kde.org/drkonqi/2fd5d8b31c2e5a6b085e7d75f9574d42aff54606)
+ KWin: Fix sticky keys for AltGr. [Commit.](http://commits.kde.org/kwin/0374acb1d82e827c254eeb1444cb0ce15aee635c) See bug [#444335](https://bugs.kde.org/444335)
+ [kcms/access] Set range for visual bell duration selector. [Commit.](http://commits.kde.org/plasma-desktop/46f671005130b69184912d432817f0ce2124c9c5)
