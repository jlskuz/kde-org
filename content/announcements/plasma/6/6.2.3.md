---
date: 2024-11-05
changelog: 6.2.2-6.2.3
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Bluedevil: Correct PIN entry behavior. [Commit.](http://commits.kde.org/bluedevil/3c65c991902a58f4ce224cdabd13baab39129819) 
+ KWin: Backends/drm: don't set backlight brightness to 1 in HDR mode. [Commit.](http://commits.kde.org/kwin/e2d0bcb01e3e51258c3485d5e45d63eb54b6a551) Fixes bug [#495242](https://bugs.kde.org/495242)
+ KDE GTK Config: Gracefully handle decoration plugin failing to load. [Commit.](http://commits.kde.org/kde-gtk-config/bd50b0e6ef1e362484d354cc341c32aa56e08a7b) 
