---
aliases:
- ../../kde-frameworks-5.27.0
date: 2016-10-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---

New mimetypes icons.

{{<figure src="/announcements/frameworks/5/5.27.0/kf-5.27-mimetypes-icons.png" >}}

### Baloo

- Use correct config entry in autostart condition
- Fix sorted insert (aka flat_map like insert) (bug 367991)
- Add missing close env, as pointed out by Loïc Yhuel (bug 353783)
- Transaction not created =&gt; don't try to abort them
- fix missing m_env = nullptr assignment
- Make e.g. Baloo::Query thread safe
- On 64-bit systems baloo allows now &gt; 5 GB index storage (bug 364475)
- Allow ctime/mtime == 0 (bug 355238)
- Handle corruption of index database for baloo_file, try to recreate the database or abort if that fails

### BluezQt

- Fix crash when trying to add device to unknown adapter (bug 364416)

### Breeze Icons

- New mimetypes icons
- Update some kstars icons (bug 364981)
- Wrong style actions/24/format-border-set (bug 368980)
- Add wayland app icon
- Add xorg app icon (bug 368813)
- Revert distribute-randomize, view-calendar + reapply the transform fix (bug 367082)
- Change folder-documents from one file to the multiple file cause in a folder more than one file is included (bug 368224)

### Extra CMake Modules

- Make sure we don't add the appstream test twice

### KActivities

- Sorting activities in the cache alphabetically by name (bug 362774)

### KDE Doxygen Tools

- Many changes to the overall layout of the generated API docs
- Correct tags path, depending of if the lib is part of a group or not
- Search: Fix href of libraries which are not part of a group

### KArchive

- Fix memory leak with KTar's KCompressionDevice
- KArchive: fix memory leak when an entry with the same name already exists
- Fix memory leak in KZip when handling empty directories
- K7Zip: Fix memory leaks on error
- Fix memory leak detected by ASAN when open() fails on the underlying device
- Remove bad cast to KFilterDev, detected by ASAN

### KCodecs

- Add missing export macros on classes Decoder and Encoder

### KConfig

- Fix memory leak in SignalsTestNoSingletonDpointer, found by ASAN

### KCoreAddons

- Register QPair&lt;QString,QString&gt; as metatype in KJobTrackerInterface
- Don't convert as url an url which has a double-quote character
- Windows compile fix
- Fix very old bug when we remove space in url as "foo &lt;&lt;url&gt; &lt;url&gt;&gt;"

### KCrash

- CMake option KCRASH_CORE_PATTERN_RAISE to forward to kernel
- Change default log level from Warning to Info

### KDELibs 4 Support

- Cleanup. Do not install includes that point to non-existing includes and also remove those files
- Use more correct and with c++11 available std::remove_pointer

### KDocTools

- Fix 'checkXML5 prints generated html to stdout for valid docbooks' (bug 369415)
- Fix bug not been able to run native tools in package using cross compiled kdoctools
- Setup targets for cross compiling running kdoctools from other packages
- Add cross compiling support for docbookl10nhelper
- Add cross compile support for meinproc5
- Convert checkxml5 into a qt executable for cross platform support

### KFileMetaData

- Improve epub extractor, less segfaults (bug 361727)
- Make odf indexer more error prove, check if the files are there (and are files at all) (meta.xml + content.xml)

### KIO

- Fix KIO slaves using only tls1.0
- Fix ABI break in kio
- KFileItemActions: add addPluginActionsTo(QMenu *)
- Show copy buttons only after checksum has been calculated
- Add missing feedback when computing a checksum (bug 368520)
- Fix KFileItem::overlays returning empty string values
- Fix launching terminal .desktop files with konsole
- Classify nfs4 mounts as probablySlow, like nfs/cifs/..
- KNewFileMenu: show New Folder action shortcut (bug 366075)

### KItemViews

- In listview mode use the default implementation of moveCursor

### KNewStuff

- Add KAuthorized checks to allow disabling of ghns in kdeglobals (bug 368240)

### Package Framework

- Don't generate appstream files for components that are not in rdn
- Make kpackage_install_package work with KDE_INSTALL_DIRS_NO_DEPRECATED
- Remove unused var KPACKAGE_DATA_INSTALL_DIR

### KParts

- Fix URLs with a trailing slash being always assumed to be directories

### KPeople

- Fix ASAN build (duplicates.cpp uses KPeople::AbstractContact which is in KF5PeopleBackend)

### KPty

- Use ECM path to find utempter binary, more reliable than simple cmake prefix
- Call the utempter helper executable manually (bug 364779)

### KTextEditor

- XML files: remove hard-coded color for values
- XML: Remove hard-coded color for values
- XML Schema Definition: Turn 'version' into an xs:integer
- Highlighting definition files: round version up to next integer
- support multi char captures only in {xxx} to avoid regressions
- Support regular expressions replaces with captures &gt; 9, e.g. 111 (bug 365124)
- Fix rendering of characters spanning into next line, e.g. underlines are no longer cut off with some fonts/font-sizes (bug 335079)
- Fix crash: Make sure the display cursor is valid after text folding (bug 367466)
- KateNormalInputMode needs to rerun SearchBar enter methods
- try to "fixup" rendering of underlines and stuff like that (bug 335079)
- Show "View Difference" button only, if 'diff' is installed
- Use non-modal message widget for externally modified file notifications (bug 353712)
- fix regression: testNormal did only work because of test execution at once
- split the indent test into separate runs
- Support "Unfold Toplevel Nodes" action again (bug 335590)
- Fix crash when showing top or bottom messages multiple times
- fix eol setting in mode lines (bug 365705)
- highlight .nix files as bash, guess can't hurt (bug 365006)

### KWallet Framework

- Check whether kwallet is enabled in Wallet::isOpen(name) (bug 358260)
- Add missing boost header
- Remove duplicate search for KF5DocTools

### KWayland

- [server] Don't send key release for not pressed keys and no double key press (bug 366625)
- [server] When replacing the clipboard selection previous DataSource needs to be cancelled (bug 368391)
- Add support for Surface enter/leave events
- [client] Track all created Outputs and add static get method

### KXmlRpcClient

- Convert categories to org.kde.pim.*

### NetworkManagerQt

- We need to set the state during initialization
- Replace all blocking calls for initialization with just one blocking call
- Use standard o.f.DBus.Properties interface for PropertiesChanged signal for NM 1.4.0+ (bug 367938)

### Oxygen Icons

- Remove invalid directory from index.theme
- Introduce dupe test from breeze-icons
- Convert all duplicated icons into symlinks

### Plasma Framework

- Improve timetracker output
- [ToolButtonStyle] Fix menu arrow
- i18n: handle strings in kdevtemplate files
- i18n: review strings in kdevtemplate files
- Add removeMenuItem to PlasmaComponents.ContextMenu
- update ktorrent icon (bug 369302)
- [WindowThumbnail] Discard pixmap on map events
- Don't include kdeglobals when dealing with a cache config
- Fix Plasma::knownLanguages
- resize the view just after setting the containment
- Avoid creating a KPluginInfo from a KPluginMetaData instance
- running tasks must have some indicator
- task bar lines according to RR 128802 marco give the ship it
- [AppletQuickItem] Break from loop when we found a layout

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
