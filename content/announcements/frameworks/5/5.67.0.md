---
aliases:
- ../../kde-frameworks-5.67.0
date: 2020-02-02
layout: framework
libCount: 70
---

### General

- Port away from many Qt 5.15 deprecated methods, this reduces the number of warnings during the build.

### Baloo

- Migrate config from KConfig to KConfigXt in order to allow KCM to use it

### Breeze Icons

- create Breeze style Kate icon based on new design by Tyson Tan
- Change VLC icon to be more like official VLC icons
- add ktrip icon from ktrip repo
- Add icon for application/sql
- Cleanup and add 22px media repeat icons
- Add icon for text/vnd.kde.kcrash-report
- Turn application/x-ms-shortcut into an actual shortcut icon

### Extra CMake Modules

- Add missing Import Env Variable
- ECMAddAppIcon: Add sc in regex to extract extension from valid names
- ECMAddQch: support &amp; document K_DOXYGEN macro usage

### Framework Integration

- Drop unused dependency QtDBus

### KActivitiesStats

- Fix broken SQL query in allResourcesQuery

### KActivities

- Remove files that Windows cannot handle
- Ensure to store resource uri without a trailing slash

### KDE Doxygen Tools

- Unbreak module imports for Python2
- Hardcode utf-8 as filesystem encoding with Python2 to help api.kde.org

### KCMUtils

- prefer the new kcm plugins to the old
- KCModuleQml: Ensure defaulted is emitted with the current configModule-&gt;representsDefaults on load
- Show button respecting what is declared by KCModule
- Update KPluginSelector to allow KCM to show good state for reset, apply and default button

### KConfig

- Refactor KConfigXT
- Fix python bindings build after ebd14f29f8052ff5119bf97b42e61f404f223615
- KCONFIG_ADD_KCFG_FILES: regenerate also on new version of kconfig_compiler
- Allow to also pass a target instead of list of sources to KCONFIG_ADD_KCFG_FILES
- Add KSharedConfig::openStateConfig for storing state information
- Fix Python bindings compilation after 7ab8275bdb56882692846d046a5bbeca5795b009

### KConfigWidgets

- KStandardAction: add method for SwitchApplicationLanguage action creation
- [KColorSchemeManager] Don't list duplicates
- [KColorschemeManager] Add option to reenable following global theme

### KCoreAddons

- demote plugin load errors from warning to debug level + reword
- Document how to filter by servicetype the right way
- Add perlSplit() overload taking a QRegularExpression and deprecate the QRegExp one
- Add mime type for backtraces saved from DrKonqi
- Add utility text function KShell::tildeCollapse
- KPluginMetaData: add initialPreference() getter
- desktoptojson: also convert InitialPreference key

### KDeclarative

- Correctly compute bottom margin for grid delegates with subtitles
- [ConfigModule] Say which package is invalid

### KHolidays

- Update holidays and add flagdays and namedays for Sweden

### KI18n

- ki18n_wrap_ui: error when file doesn't exist
- [Kuit] Revert changes in parseUiMarker()

### KIO

- Add missing renamed event when a destination file already existed
- KFilePlacesModel: On new profile in recent show only recentlyused:/ based entries by default
- Add KFileCustomDialog constructor with a startDir parameter
- Fix QRegularExpression::wildcardToRegularExpression() usage
- Allow to handle apps with Terminal=True in their desktop file, handle their associated mimetype properly (bug 410506)
- KOpenWithDialog: Allow to return a newly created KService created associated to a mimetype
- Add KIO::DropJobFlag to allow manually showing the menu (bug 415917)
- [KOpenWithDialog] Hide collapsible group box when all options inside are hidden (bug 415510)
- Revert effective removal of KUrlPixmapProvider from API
- SlaveBase::dispatchLoop: Fix timeout calculation (bug 392768)
- [KDirOperator] Allow renaming files from the context menu (bug 189482)
- Upstream Dolphin's file rename dialog (bug 189482)
- KFilePlaceEditDialog: move logic into isIconEditable()

### Kirigami

- Clip the flickable parent item (bug 416877)
- Remove header top margin from private ScrollView
- proper size hint for the gridlayout (bug 416860)
- use attached property for isCurrentPage
- Get rid of a couple of warnings
- try to keep the cursor in window when typing in an OverlaySheet
- properly expand fillWidth items in mobile mode
- Add active, link, visited, negative, neutral and positive background colors
- Expose ActionToolBar's overflow button icon name
- Use QQC2 Page as base for Kirigami Page
- Specify where the code is coming from as the URL
- Don't anchor AbstractApplicationHeader blindly
- emit pooled after the properties have been reassigned
- add reused and pooled signals like TableView

### KJS

- Abort machine run once a timeout signal has been seen
- Support ** exponentiation operator from ECMAScript 2016
- Added shouldExcept() function that works based on a function

### KNewStuff

- Unbreak the KNSQuick::Engine::changedEntries functionality

### KNotification

- Add new signal for default action activation
- Drop dependency to KF5Codecs by using the new stripRichText function
- Strip richtext on Windows
- Adapt to Qt 5.14 Android changes
- Deprecate raiseWidget
- Port KNotification from KWindowSystem

### KPeople

- Adjust metainfo.yaml to new tier
- Remove legacy plugin loading code

### KQuickCharts

- Fix Qt version check
- Register QAbstractItemModel as anonymous type for property assignments
- Hide the line of a line chart if its width is set to 0

### Kross

- addHelpOption already adds by kaboutdata

### KService

- Support multiple values in XDG_CURRENT_DESKTOP
- Deprecate allowAsDefault
- Make "Default Applications" in mimeapps.list the preferred applications (bug 403499)

### KTextEditor

- Revert "improve word completion to use highlighting to detect word boundaries" (bug 412502)
- import final breeze icon
- Message-related methods: Use more member-function-pointer-based connect
- DocumentPrivate::postMessage: avoid multiple hash lookups
- fix Drag&amp;copy function (by using Ctrl Key) (bug 413848)
- ensure we have a quadratic icon
- set proper Kate icon in about dialog for KatePart
- inline notes: correctly set underMouse() for inline notes
- avoid use of old mascot ATM
- Variable expansion: Add variable PercentEncoded (bug 416509)
- Fix crash in variable expansion (used by external tools)
- KateMessageWidget: remove unused event filter installation

### KTextWidgets

- Drop KWindowSystem dependency

### KWallet Framework

- Revert readEntryList() to use QRegExp::Wildcard
- Fix QRegularExpression::wildcardToRegularExpression() usage

### KWidgetsAddons

- [KMessageWidget] Subtract the correct margin
- [KMessageBox] Only allow selecting text in the dialog box using the mouse (bug 416204)
- [KMessageWidget] Use devicePixelRatioF for animation pixmap (bug 415528)

### KWindowSystem

- [KWindowShadows] Check for X connection
- Introduce shadows API
- Deprecate KWindowEffects::markAsDashboard()

### KXMLGUI

- Use KStandardAction convenience method for switchApplicationLanguage
- Allow programLogo property to be a QIcon, too
- Remove ability to report bugs against arbitrary stuff from a static list
- Remove compiler information from bug report dialog
- KMainWindow: fix autoSaveSettings to catch QDockWidgets being shown again
- i18n: Add more semantic context strings
- i18n: Split translations for strings "Translation"

### Plasma Framework

- Fixed tooltip corners and removed useless color attributes
- Removed hardcoded colors in background SVGs
- Fix the size and pixel alignment of checkboxes and radiobuttons
- Update breeze theme shadows
- [Plasma Quick] Add WaylandIntegration class
- Same behavior for scrollbar as the desktop style
- Make use of KPluginMetaData where we can
- Add edit mode menu item to desktop widget context menu
- Consistency: colored selected buttons
- Port endl to \n Not necessary to flush as QTextStream uses QFile which flush when it's deleted

### Purpose

- Fix QRegularExpression::wildcardToRegularExpression() usage

### QQC2StyleBridge

- Remove scrollbar related workarounds from list delegates
- [TabBar] Remove frame
- Add active, link, visited, negative, neutral and positive background colors
- use hasTransientTouchInput
- always round x and y
- support mobile mode scrollbar
- ScrollView: Do not overlay scrollbars over contents

### Solid

- Add signals for udev events with actions bind and unbind
- Clarify referencing of DeviceInterface (bug 414200)

### Syntax Highlighting

- Updates nasm.xml with the latest instructions
- Perl: Add 'say' to keyword list
- cmake: Fix <code>CMAKE*POLICY**_CMP&amp;lt;N&amp;gt;</code> regex and add special args to <code>get_cmake_property</code>
- Add GraphQL highlighting definition

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
