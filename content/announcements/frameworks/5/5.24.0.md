---
aliases:
- ../../kde-frameworks-5.24.0
date: 2016-07-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---

### General changes

- The list of supported platforms for each framework is now more explicit.
  Android has been added to the list of supported platforms in all frameworks where this is the case.

### Baloo

- DocumentUrlDB::del Only assert when children of dir actually exist
- Ignore malformed Queries which have a binary operator without first argument

### Breeze Icons

- Many new or improved icons
- fix bug 364931 user-idle icon was not visible (bug 364931)
- Add a program to convert symbolically linked files to qrc aliases

### Extra CMake Modules

- Integrate relative library paths to APK
- Use "${BIN_INSTALL_DIR}/data" for DATAROOTDIR on Windows

### KArchive

- Ensure extracting an archive does not install files outside the extraction folder,
  for security reasons. Instead, extract such files to the root of the extraction folder.

### KBookmarks

- Cleanup KBookmarkManagerList before qApp exits, to avoid deadlocks with the DBus thread

### KConfig

- Deprecate authorizeKAction() in favor of authorizeAction()
- Fix reproducibility in builds by ensuring utf-8 encoding

### KConfigWidgets

- KStandardAction::showStatusbar: Return the intended action

### KDeclarative

- Make epoxy optional

### KDED

- [OS X] make kded5 an agent, and build it as a regular application

### KDELibs 4 Support

- Remove KDETranslator class, there's no kdeqt.po anymore
- Document the replacement for use12Clock()

### KDesignerPlugin

- Add support for KNewPasswordWidget

### KDocTools

- Allow KDocTools to always locate at least its own installed stuff
- Use CMAKE_INSTALL_DATAROOTDIR to look for docbook instead of share
- Update qt5options manpage docbook to qt 5.4
- Update kf5options manpage docbook

### KEmoticons

- Move glass theme to kde-look

### KGlobalAccel

- Use QGuiApplication instead of QApplication

### KHTML

- Fix applying inherit value for outline shorthand property
- Handle initial and inherit for border radius
- Discard property if we caught an invalid length|percent as background-size
- cssText must output comma separated values for those properties
- Fix parsing background-clip in shorthand
- Implement background-size parsing in shorthand
- Mark properties as set when repeating patterns
- Fix background properties inheritance
- Fix applying Initial and Inherit for background-size property
- Deploy the khtml kxmlgui file in a Qt resource file

### KI18n

- Also search catalogs for stripped variants of values in env var LANGUAGE
- Fix parsing of env var values WRT modifier and codeset, done in wrong order

### KIconThemes

- Add support for loading and using an icontheme in a RCC file automatically
- Document icon theme deployment on MacOS and Windows, see https://api.kde.org/frameworks/kiconthemes/html/index.html

### KInit

- Allow timeout in reset_oom_protection while waiting for SIGUSR1

### KIO

- KIO: add SlaveBase::openPasswordDialogV2 for better error checking, please port your kioslaves to it
- Fix KUrlRequester opening file dialog in wrong directory (bug 364719)
- Fix unsafe KDirModelDirNode* casts
- Add cmake option KIO_FORK_SLAVES to set default value
- ShortUri filter: fix filtering of mailto:user@host
- Add OpenFileManagerWindowJob to highlight file within a folder
- KRun: add runApplication method
- Add soundcloud search provider
- Fix an alignment issue with the OS X native "macintosh" style

### KItemModels

- Add KExtraColumnsProxyModel::removeExtraColumn, will be needed by StatisticsProxyModel

### KJS

- kjs/ConfigureChecks.cmake - set HAVE_SYS_PARAM_H properly

### KNewStuff

- Make sure we have a size to offer (bug 364896)
- Fix "Download dialog fails when all categories missing"

### KNotification

- Fix notify by taskbar

### KNotifyConfig

- KNotifyConfigWidget: add disableAllSounds() method (bug 157272)

### KParts

- Add switch to disable KParts' handling of window titles
- Add donate menu item to help menu of our apps

### Kross

- Fix name of QDialogButtonBox's enumerator "StandardButtons"
- Remove the first attempt to load library because we will try libraryPaths anyway
- Fix crash when a method exposed to Kross returns QVariant with non-relocatable data
- Do not use C-style casts into void* (bug 325055)

### KRunner

- [QueryMatch] Add iconName

### KTextEditor

- Show Scrollbar Text Preview after a delay of 250ms
- hide preview and stuff on view content scrolling
- set parent + toolview, I think this is needed to avoid task switcher entry in Win10
- Remove "KDE-Standard" from encoding box
- Folding preview on per default
- Avoid dashed underline for preview &amp; avoid poisoning of line layout cache
- Always enable "Show preview of folded text" option
- TextPreview: Adjust the grooveRect-height when scrollPastEnd is enabled
- Scrollbar preview: use groove rect if scrollbar does not use full height
- Add KTE::MovingRange::numberOfLines() just like KTE::Range has
- Code folding preview: set popup height so that all hidden lines fit
- Add option to disable preview of folded text
- Add modeline 'folding-preview' of type bool
- View ConfigInterface: support 'folding-preview' of type bool
- Add bool KateViewConfig::foldingPreview() and setFoldingPreview(bool)
- Feature: Show text preview when hovering over folded code block
- KateTextPreview: add setShowFoldedLines() and showFoldedLines()
- Add modelines 'scrollbar-minimap' [bool], and 'scrollbar-preview' [bool]
- Enable mini-map scrollbar by default
- New feature: Show text preview when hovering over the scrollbar
- KateUndoGroup::editEnd(): pass KTE::Range by const ref
- Fix vim-mode shortcut handling, after behaviour changes in Qt 5.5 (bug 353332)
- Autobrace: don't insert ' character in text
- ConfigInterface: add scrollbar-minimap config key to enable/disable scrollbar mini map
- Fix KTE::View::cursorToCoordinate() when top message widget is visible
- Refactoring of the Emulated Command Bar
- Fix drawing artifacts when scrolling while notifications are visible (bug 363220)

### KWayland

- Add a parent_window event to Plasma Window interface
- Properly handle destroying a Pointer/Keyboard/Touch resource
- [server] Delete dead code: KeyboardInterface::Private::sendKeymap
- [server] Add support for setting the clipboard selection DataDeviceInterface manually
- [server] Ensure that Resource::Private::get returns nullptr if passed a nullptr
- [server] Add resource check in QtExtendedSurfaceInterface::close
- [server] Unset SurfaceInterface pointer in referenced objects when being destroyed
- [server] Fix error message in QtSurfaceExtension Interface
- [server] Introduce a Resource::unbound signal emitted from unbind handler
- [server] Don't assert when destroying a still referenced BufferInterface
- Add destructor request to org_kde_kwin_shadow and org_kde_kwin_shadow_manager

### KWidgetsAddons

- Fix reading Unihan data
- Fix minimum size of KNewPasswordDialog (bug 342523)
- Fix ambiguous contructor on MSVC 2015
- Fix an alignment issue under the OS X native "macintosh" style (bug 296810)

### KXMLGUI

- KXMLGui: Fix merge indices when removing xmlgui clients with actions in groups (bug 64754)
- Don't warn about "file found in compat location" if it wasn't found at all
- Add donate menu item to help menu of our apps

### NetworkManagerQt

- Do not set peap label based on peap version
- Make network manager version checks in runtime (to avoid compile vs. run-time (bug 362736)

### Plasma Framework

- [Calendar] Flip arrow buttons on right-to-left languages
- Plasma::Service::operationDescription() should return a QVariantMap
- Don't include embedded contrainers in containmentAt(pos) (bug 361777)
- fix the color theming for the restart system icon (login screen) (bug 364454)
- disable taskbar thumbnails with llvmpipe (bug 363371)
- guard against invalid applets (bug 364281)
- PluginLoader::loadApplet: restore compatibility for misinstalled applets
- correct folder for PLASMA_PLASMOIDS_PLUGINDIR
- PluginLoader: improve error message about plugin version compatibility
- Fix check to keep QMenu on screen for multiscreen layouts
- New containment type for the systray

### Solid

- Fix check that CPU is valid
- Handle reading /proc/cpuinfo for Arm processors
- Find CPUs by subsystem rather than driver

### Sonnet

- Mark helper exe as non-gui app
- Allow nsspellcheck to be compiled on mac per default

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
