---
aliases:
- ../../kde-frameworks-5.74.0
date: 2020-09-06
layout: framework
libCount: 70
---

### Attica

+ Use Q_DECLARE_OPERATORS_FOR_FLAGS in same namespace as flags definition

### Baloo

+ Relicense many files to LGPL-2.0-or-later
+ Use common UDS creation code also for tags (bug 419429)
+ Factor out common UDS creation code from KIO workers
+ [balooctl] Show allowed formats in help text
+ [balooctl] Show current file in status output in indexing state
+ [balooctl] Set QDBusServiceWatcher mode from constructor
+ [balooctl] Formatting cleanup
+ Use Q_DECLARE_OPERATORS_FOR_FLAGS in same namespace as flags definition
+ [OrPostingIterator] Do not advance when wanted id is lower than current
+ [Extractor] Remove QWidgets dependency from extractor helper
+ [Extractor] Remove KAboutData from extractor helper executable
+ Update various references in README
+ [FileContentIndexer] Remove unused config constructor argument and member
+ [balooctl] Fix QProcess::start deprecated warning, provide empty args list
+ [Engine] Propagate transaction errors (bug 425017)
+ [Engine] Remove unused method hasChanges from {Write}Transaction
+ Don't index .ytdl files (bug 424925)

### Breeze Icons

+ Make the keepassxc icon more faithful to the official one
+ Add more symlinks for new keepassxc system tray icon names (bug 425928)
+ Add another alias for keepass icon (bug 425928)
+ Add icon for Godot project MIME type
+ Add icon for Anaconda installer
+ Add 96px places icons
+ Remove unused places to communicate
+ Run application-x-bzip-compressed-tar through <code>scour</code> (bug 425089)
+ Make application-gzip symlink to application-x-gzip (bug 425059)
+ Add aliases for MP3 icons (bug 425059)

### Extra CMake Modules

+ Strip leading zeros from numerical version numbers in C++ code
+ Add timeout for qmlplugindump calls
+ Add WaylandProtocols find module
+ invoke update-mime-database with -n

### Framework Integration

+ Clarify license statement according to KDElibs History

### KActivitiesStats

+ Use Boost::boost for older CMake versions

### KActivities

+ Drop empty X-KDE-PluginInfo-Depends

### KDE Doxygen Tools

+ Document dependencies in requirements.txt and install them in setup.py
+ Opt-In Display of Library License

### KAuth

+ Use Q_DECLARE_OPERATORS_FOR_FLAGS in same namespace as flags definition

### KBookmarks

+ KBookmarkManager: clear memory tree when the file is deleted

### KCalendarCore

+ Always store X-KDE-VOLATILE-XXX properties as volatile
+ Document the expected TZ transitions in Prague

### KCMUtils

+ KCModuleData: Fix headers, improve api doc and rename method
+ Extend KCModuleData with revertToDefaults and matchQuery functionality
+ Try to avoid horizontal scrollbar in KCMultiDialog
+ Add KCModuleData as base class for plugin
+ Allow extra button to be added to KPluginSelector (bug 315829)

### KConfig

+ Make KWindowConfig::allConnectedScreens() static and internal (bug 425953)
+ Add standard shortcut for "Create Folder"
+ Introduce method to query KConfigSkeletonItem default value
+ Remember window sizes on a per-screen-arrangement basis
+ Extract code to get list of connected screens into a re-usable function
+ Add functions to save and restore window positions on non-Wayland platforms (bug 415150)

### KConfigWidgets

+ Avoid swapping defaults to read KConfigSkeletonItem default value
+ Fix KLanguageName::nameForCodeInLocale for codes that QLocale doesn't know about
+ KLanguageName::allLanguageCodes: Take into account there can be more than one locale directory
+ KConfigDialog: Try to avoid horizontal scrollbars
+ Function that returns the list of Language Codes

### KContacts

+ Addressee::parseEmailAddress(): Check length of full name before trimming

### KCoreAddons

+ Add *.kcrash glob pattern to KCrash Report MIME type
+ Use Q_DECLARE_OPERATORS_FOR_FLAGS in same namespace as flags definition
+ Do not wait for fam events indefinitely (bug 423818)
+ [KFormat] Allow formatting values to arbitrary binary units
+ Make it possible to KPluginMetadata from QML
+ [KFormat] Fix binary example

### KDAV

+ metainfo.yaml: Add tier key and tweak description

### KDeclarative

+ [KKeySequenceItem] Make Meta+Shift+num shortcuts work
+ Expose checkForConflictsAgainst property
+ Add a new AbstractKCM class
+ Port KRunProxy away from KRun

### KDED

+ org.kde.kded5.desktop: Add missing required key "Name" (bug 408802)

### KDocTools

+ Update contributor.entities
+ Use placeholder markers

### KEmoticons

+ Recover EmojiOne license information

### KFileMetaData

+ Convert old Mac line endings in lyrics tags (bug 425563)
+ Port to ECM new FindTaglib module

### KGlobalAccel

+ Load service files for shortcuts for applications data dir as fallback (bug 421329)

### KI18n

+ Fix possible preprocessor race condition with i18n defines

### KIO

+ StatJob: make mostLocalUrl work only with protoclClass == :local
+ KPropertiesDialog: also load plugins with JSON metadata
+ Revert "[KUrlCompletion] Don't append / to completed folders" (bug 425387)
+ Simplify KProcessRunner constructor
+ Allow CCBUG and FEATURE keywords for bugs.kde.org (bug )
+ Update help text for editing the app command in a .desktop entry to comply with current spec (bug 425145)
+ KFileFilterCombo: Don't add the allTypes option if we only have 1 item
+ Use Q_DECLARE_OPERATORS_FOR_FLAGS in same namespace as flags definition
+ Fix potential crash deleting a menu in event handler (bug 402793)
+ [filewidgets] Fix KUrlNavigatorButton padding on breadcrumb (bug 425570)
+ ApplicationLauncherJob: tweak docs
+ Fix untranslated items in kfileplacesmodel
+ ApplicationLauncherJob: fix crash if there's no open-with handler set
+ Rename "Web Shortcuts" KCM to "Web Search Keywords"
+ KFileWidget: reparse config to grab dirs added by other instances of the app (bug 403524)
+ Move web shortcuts kcm to search category
+ Automatically remove trailing whitespace instead of showing a warning
+ Avoid systemd launched applications from closing forked children (bug 425201)
+ smb: fix share name availability check
+ smb: keep stderr of net commands on add/remove (bug 334618)
+ smb: rejigger guest allowed check and publish as areGuestsAllowed
+ KFileWidget: Clear urls before rebuilding the list
+ KFileWidget: remove default URLs top path combo
+ KFilePlacesModel: add default places when upgrading from older version
+ Fix 2-years regression in KUrlComboBox, setUrl() didn't append anymore

### Kirigami

+ kirigami.pri: Add managedtexturenode.cpp
+ make buddyfor custom positioning work again
+ Account for More button size if we already know it will be visible
+ Add a property to ToolBarLayout to control how to deal with item height (bug 425675)
+ make checkbox labels readable again
+ support displayComponent for actions
+ make submenus visible
+ Fix QSGMaterialType forward declaration
+ Make OverlaySheet header and footer use appropriate background colors
+ Check low power environment variable for value, not just if it is set
+ Add setShader function to ShadowedRectangleShader to simplify setting shaders
+ Add a low power version of the sdf functions file
+ Make overloads of sdf_render use the full-argument sdf_render function
+ Remove need for core profile defines in shadowedrect shader main
+ Base inner rectangle on outer rectangle for bordered shadowedrectangle
+ Use the right shaders for ShadowedTexture when using core profile
+ Add "lowpower" versions of the shadowedrectangle shaders
+ Initialize m_component member in PageRoute
+ Fix Material SwipeListItem
+ new logic to remove acceleration marks (bug 420409)
+ remove forceSoftwarerendering for now
+ just show the source item on software rendering
+ a software fallback for the shadowed texture
+ Use the new showMenuArrow property on background for the menu arrow
+ don't hide the header when overshooting up
+ move ManagedTextureNode in own file
+ ToolBarLayout: Add spacing to visibleWidth if we're displaying the more button
+ Do not override Heading pixel size in BreadCrumbControl (bug 404396)
+ Update app template
+ [passivenotification] Set explicit padding (bug 419391)
+ Enable clipping in the GlobalDrawer StackView
+ Remove opacity from disabled PrivateActionToolButton
+ Make ActionToolBar a control
+ swipenavigator: add control over which pages are displayed
+ Declare the underlying type of the DisplayHint enum to be uint
+ Add ToolBarLayout/ToolBarLayoutDelegate to pri file
+ kirigami.pro: Use source file list from kirigami.pri
+ Do not delete incubators in completion callback
+ Ensure menuActions remains an array instead of a list property
+ Add return type to DisplayHint singleton lambda
+ Account for item height when vertically centering delegates
+ Always queue a new layout, even if we are currently layouting
+ Rework InlineMessage layout using anchors
+ Use full width to check if all actions fit
+ Add a comment about the extra spacing for centering
+ Fix check for tooltip text in PrivateActionToolButton
+ Workaround qqc2-desktop-style ToolButton not respecting non-flat IconOnly
+ Prefer collapsing KeepVisible actions over hiding later KeepVisible actions
+ Hide all following actions when the first action gets hidden
+ Add a notify signal for ToolBarLayout::actions and emit it at the right time
+ Expose action to separator of ActionsMenu
+ Rename ActionsMenu loaderDelegate kirigamiAction property to action
+ Add missing loaderDelegate that checks visibility
+ Hide action delegates until they are positioned
+ ToolBarLayout: Replace custom lazy-loading of delegates with QQmlIncubator
+ Move displayHintSet to C++ so it can be called from there
+ Support right-to-left mode in ToolBarLayout
+ Add minimumWidth property to ToolBarLayout
+ Rework PrivateActionToolButton for improved performance
+ Deprecate ActionToolBar::hiddenActions
+ Set layout hints for ActionToolBar
+ Use non-deprecated DisplayHint in ToolBarPageHeader
+ Display component errors if delegate items fail instantiation
+ Hide delegates of actions that were removed
+ Cleanup full/icon delegate items on delegate destruction
+ Enforce full/icon delegate item visibility
+ Use ToolBarLayout for ActionToolBar's layouting
+ Add some comments to ToolBarLayout::maybeHideDelegate
+ Add visibleWidth property to ToolBarLayout
+ Introduce ToolBarLayout native object
+ Move DisplayHint from Action to C++ enums file
+ Add icons used in the about page to cmake icon packaging macro

### KNewStuff

+ Fix cache sync issues with QtQuick dialog (bug 417985)
+ Drop empty X-KDE-PluginInfo-Depends
+ Remove entries if they do not match filter anymore (bug 425135)
+ Fix edge case where KNS gets stuck (bug 423055)
+ Make the internal kpackage job task less fragile (bug 425811)
+ Remove downloaded file when using kpackage installation
+ Support a different style of kpackage knsrc for fallback
+ Handle /* notation for RemoveDeadEntries (bug 425704)
+ Make it easier to get the cache for an already initialised engine
+ Add a reverse lookup for entries based on their installed files
+ Use /* notation for subdir uncompression and allow subdir uncompression if file is archive
+ Fix flood of "Pixmap is a null pixmap" warnings
+ Remove slashes from entry name when interpreting it as path (bug 417216)
+ Fix a sometimes-crash with the KPackageJob task (bug 425245)
+ Use same spinner for loading and initializing (bug 418031)
+ Remove details button (bug 424895)
+ Run uninstall script async (bug 418042)
+ Disable search when not available
+ Hide load more spinner while updating/installing (bug 422047)
+ Do not append download target dir to entry files
+ Remove * character when passing dir to script
+ Do not focus first element in icon view mode (bug 424894)
+ Avoid unnecessary starting of uninstall job
+ Add a RemoveDeadEntries option for knsrc files (bug 417985)
+ [QtQuick dialog] Fix last instance of incorrect update icon
+ [QtQuick dialog] Use more appropriate uninstall icons

### KPackage Framework

+ Do not delete package root if package was deleted (bug 410682)

### KQuickCharts

+ Add a fillColorSource property to Line charts
+ Calculate smoothing based on item size and device pixel ratio
+ Use average of size instead of maximum to determine line smoothness
+ Base amount of smoothing in line charts on chart size

### KRunner

+ Add template for python runner
+ Update template for KDE Store compatibility and improve README
+ Add support for runner syntaxes to dbus runners
+ Save RunnerContext after each match session (bug 424505)

### KService

+ Implement invokeTerminal on Windows with workdir, command and envs
+ Fix application preference ordering for mimetypes with multiple inheritance (bug 425154)
+ Bundle the Application servicetype into a qrc file
+ Expand tilde character when reading working directory (bug 424974)

### KTextEditor

+ Vimode: Make the small delete register (-) directly accessible
+ Vimode: Copy the behavior of vim's numbered registers
+ Vimode: Simplify the append-copy implementation
+ Make "search for selection" search if there is no selection
+ Multiline mode makes sense only for multiline regex
+ Add comment about checking pattern.isEmpty()
+ Port the search interface from QRegExp to QRegularExpression
+ Vimode: Implement append-copy
+ Speed up a *lot* loading large files
+ Only show zoom level when it is not 100%
+ Add a zoom indicator to the status bar
+ Added a separate config option for bracket match preview
+ Show a preview of the matching open bracket's line
+ Allow more control over invocation of completion models when automatic invocation is not used

### KWallet Framework

+ Avoid clash with a macro in ctype.h from OpenBSD

### KWidgetsAddons

+ Add KRecentFilesMenu to replace KRecentFileAction

### KWindowSystem

+ Install platform plugins in a directory with no dots in file name (bug 425652)
+ [xcb] Scaled icon geometry correctly everywhere

### KXMLGUI

+ Allow opting out of remembering window positions on X11 (bug 415150)
+ Save and restore position of main window (bug 415150)

### Plasma Framework

+ [PC3/BusyIndicator] Avoid running invisible animation
+ Don't use highlightedTextColor for TabButtons
+ Remove Layout.minimumWidth from Button and ToolButton
+ Use the spacing property for the spacing between Button/ToolButton icons and labels
+ Add private/ButtonContent.qml for PC3 Buttons and ToolButtons
+ Change PC3 Button and ToolButton implicitWidth and implicitHeight to account for inset values
+ Add implicitWidth and implicitHeight to ButtonBackground
+ Fix incorrect default for PlasmaExtras.ListItem (bug 425769)
+ Don't let the background become smaller than the svg (bug 424448)
+ Use Q_DECLARE_OPERATORS_FOR_FLAGS in same namespace as flags definition
+ Make PC3 BusyIndicator visuals keep a 1:1 aspect ratio (bug 425504)
+ Use ButtonFocus and ButtonHover in PC3 ComboBox
+ Use ButtonFocus and ButtonHover in PC3 RoundButton
+ Use ButtonFocus and ButtonHover in PC3 CheckIndicator
+ Unify the flat/normal behavior of PC3 Buttons/ToolButtons (bug 425174)
+ Make Heading use PC3 Label
+ [PlasmaComponents3] Make checkbox text fill its layout
+ Give PC2 slider implicitWidth and implicitHeight
+ Copy files rather than broken symlinks
+ Fix toolbutton-hover margins in button.svg (bug #425255)
+ [PlasmaComponents3] very small refactor of ToolButton shadow code
+ [PlasmaComponents3] Fix reversed condition for flat ToolButton shadow
+ [PlasmaComponents3] Strip mnemonic ampersands from tooltip text
+ Only draw the focus indicator when we got focus via the keyboard (bug 424446)
+ Drop implicit minimum sizing from PC2 and PC3 Buttons
+ Add PC3 equivalent to PC2 ListItem
+ Fix toolbar svg
+ [pc3] Make ToolBar more aligned with qqc2-desktop-style one
+ Expose the applet metadata in AppletInterface
+ Don't truncate DPR to an integer in cache ID
+ Add timeout property to ToolTipArea
+ Set type to Dialog in flags if type is Dialog::Normal

### Purpose

+ Apply initial configuration data when loading config UI
+ Restore behaviour of AlternativesView
+ [jobcontroller] Disable separate process
+ Rework job view handling (bug 419170)

### QQC2StyleBridge

+ Fix StandardKey shortcut sequences in MenuItem showing as numbers
+ Don't use parent height/width for implicit ToolSeparator sizing (bug 425949)
+ Only use "focus" style for non-flat toolbuttons on press
+ Add top and bottom padding to ToolSeparator
+ Make ToolSeparator respect topPadding and bottomPadding values
+ Make MenuSeparator use background's calculated height not implicitheight
+ Fix toolbuttons with menus using newer Breeze
+ Draw the entire CheckBox control via the QStyle

### Solid

+ Adding static storageAccessFromPath method, needed by https://phabricator.kde.org/D28745

### Syndication

+ Correct license exception

### Syntax Highlighting

+ convert all themes to new keys for editor colors
+ change theme json format, use meta object enum names for editor colors
+ check kateversion &gt;= 5.62 for fallthroughContext without fallthrough="true" and use attrToBool for boolean attribute
+ Add syntax definition for todo.txt
+ fix matchEscapedChar(): the last character of a line is ignored
+ Fix isDigit(), isOctalChar() and isHexChar(): must only match ascii characters
+ generate themes overview
+ add theme meta data to header of HTML pages we generate
+ start to generate theme collection page
+ rename 'Default' to 'Breeze Light'
+ Varnish, Vala &amp; TADS3: use the default color style
+ Improve the Vim Dark color theme
+ Add Vim Dark color theme
+ add syntax-highlighting theme files to json highlighting
+ Ruby/Rails/RHTML: add spellChecking in itemDatas
+ Ruby/Rails/RHTML: use the default color style and other improvements
+ LDIF, VHDL, D, Clojure &amp; ANS-Forth94: use the default color style
+ ASP: use the default color style and other improvements
+ let objective-c win for .m files
+ .mm is more likely Objective-C++ then meta math
+ use notAsciiDelimiters only with a not ascii char
+ optimize isWordDelimiter(c) with ascii character
+ optimize Context::load
+ use std::make_shared which removes the allocation on the control block
+ add proper license to update scripty
+ move update script for kate-editor.org/syntax to syntax repository
+ SELinux CIL &amp; Scheme: update bracket colors for dark themes
+ POV-Ray: use default color style
+ use krita installer NSIS script as example input, taken from krita.git
+ add hint to update script for website
+ Add minimal Django Template example
+ update refs after latest hl changes
+ CMake: fix illegible colors in dark themes and other improvements
+ Add pipe and ggplot2 example
+ fix naming error for varnish hl
+ use right highlighting for 68k ASM: Motorola 68k (VASM/Devpac)
+ fix XML to be valid in respect to XSD
+ BrightScript: Add exception syntax
+ Improve comments in some syntax definitions (Part 3)
+ R Script: use default color style and other improvements
+ PicAsm: fix unknown preprocessor keywords highlighting
+ Improve comments in some syntax definitions (Part 2)
+ Modelines: remove LineContinue rules
+ quickfix broken hl file
+ Optimization: check if we are on a comment before using ##Doxygen which contains several RegExpr
+ Assembly languages: several fixes and more context highlighting
+ ColdFusion: use the default color style and replace some RegExpr rules
+ Improve comments in some syntax definitions, Pt. 1
+ Use angle brackets for context information
+ Revert removal of byte-order-mark
+ xslt: change color of XSLT tags
+ Ruby, Perl, QML, VRML &amp; xslt: use the default color style and improve comments
+ txt2tags: improvements and fixes, use the default color style
+ Fix errors and add fallthroughContext=AttrNormal for each context of diff.xml because all rules contain column=0
+ fix issues found by static checker
+ import Pure highlighting from https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/pure.xml
+ fix kateversion attribute
+ make highlighting lookup order independent of translations
+ fix version format + spacing issues
+ import Modula-3 highlighting from https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/modula-3.xml
+ fix version format
+ fix spacing stuff found by static checker
+ import LLVM highlighting from https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/llvm.xml
+ fix spacing stuff found by static checker
+ import Idris from https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/idris.xml
+ fix faults found by static checker
+ import ATS from https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/ats.xml
+ detach only for not freshly created data
+ create StateData on demand

### Security information


The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
