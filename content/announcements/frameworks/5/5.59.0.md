---
aliases:
- ../../kde-frameworks-5.59.0
date: 2019-06-08
layout: framework
libCount: 70
---

### Baloo

- Don't try to index SQL database dumps
- Exclude .gcode and virtual machine files from indexing consideration

### BluezQt

- Add Bluez API to DBus XML parser/generator

### Breeze Icons

- gcompris-qt too
- Make falkon icon a real SVG
- add missing icons from the apps, to be redone https://bugs.kde.org/show_bug.cgi?id=407527
- add icon for kfourinline from app, needs updating too
- add kigo icon https://bugs.kde.org/show_bug.cgi?id=407527
- add kwave icon from kwave, to be redone in breeze style
- Symlink arrow-_-double to go-_-skip, add 24px go-*-skip
- Change input-* device icon styles, add 16px icons
- Add dark version of new Knights icon which escaped from my previous commit
- Create new icon for Knights based on Anjuta's icon (bug 407527)
- add icons for apps which miss them in breeze, these should be updated to be more breezy but they are needed for the new kde.org/applications for now
- kxstitch icon from kde:kxstitch, to be updated
- don't glob everything and the kitchen sink
- make sure to also assert ScaledDirectories

### Extra CMake Modules

- Create specific directory for Qt logging categories file
- Don't enable QT_STRICT_ITERATORS on Windows

### Framework Integration

- ensure to search also in the legacy location
- search in the new location for knsrc files

### KArchive

- Test reading and seeking in KCompressionDevice
- KCompressionDevice: Remove bIgnoreData
- KAr: fix out-of-bounds read (on invalid input) by porting to QByteArray
- KAr: fix parsing of long filenames with Qt-5.10
- KAr: the permissions are in octal, not decimal
- KAr::openArchive: Also check ar_longnamesIndex is not &lt; 0
- KAr::openArchive: Fix invalid memory access on broken files
- KAr::openArchive: Protect against Heap-buffer-overflow in broken files
- KTar::KTarPrivate::readLonglink: Fix crash in malformed files

### KAuth

- Don't hardcode dbus policy install dir

### KConfigWidgets

- Use locale currency for donate icon

### KCoreAddons

- Fix compilation for python bindings (bug 407306)
- Add GetProcessList for retrieving the list of currently active processes

### KDeclarative

- Fix qmldir files

### KDELibs 4 Support

- Remove QApplication::setColorSpec (empty method)

### KFileMetaData

- Show 3 significant figures when displaying doubles (bug 343273)

### KIO

- Manipulate bytes instead of characters
- Fix kioslave executables never exiting, when setting KDE_FORK_SLAVES
- Fix desktop link to file or directory (bug 357171)
- Test current filter before setting a new one (bug 407642)
- [kioslave/file] Add a codec for legacy filenames (bug 165044)
- Rely upon QSysInfo to retrieve the system details
- Add Documents to the default list of Places
- kioslave: preserve argv[0], to fix applicationDirPath() on non-Linux
- Allow to drop one file or one folder on KDirOperator (bug 45154)
- Truncate long filename before creating a link (bug 342247)

### Kirigami

- [ActionTextField] Make QML tooltip consistent
- base on height for items that should have a top padding (bug 405614)
- Performance: compress color changes without a QTimer
- [FormLayout] Use even top and bottom spacing for separator (bug 405614)
- ScrollablePage: Make sure the scrolled view gets the focus when it's set (bug 389510)
- Improve keyboard-only usage of the toolbar (bug 403711)
- make the recycler a FocusScope

### KNotification

- Handle apps which set the desktopFileName property with filename suffix

### KService

- Fix assert (hash != 0) sometimes when a file is deleted by another process
- Fix another assert when the file disappears under us: ASSERT: "ctime != 0"

### KTextEditor

- Don't delete entire previous line by backspace in pos 0 (bug 408016)
- Use native dialog overwrite check
- Add action to reset font size
- show static word wrap marker always if requested
- Ensure highlighted range begin/end marker after unfold
- Fix: don't reset Highlight when saving some files (bug 407763)
- Auto indentation: Use std::vector instead of QList
- Fix: Use default indentation mode for new files (bug 375502)
- remove duplicated assignment
- honor auto-bracket setting for balance check
- improve invalid character check on loading (bug 406571)
- New menu of syntax highlighting in the status bar
- Avoid infinite loop in "Toggle Contained Nodes" action

### KWayland

- Allow compositors to send discrete axis values (bug 404152)
- Implement set_window_geometry
- Implement wl_surface::damage_buffer

### KWidgetsAddons

- KNewPasswordDialog: add periods to message widgets

### NetworkManagerQt

- Don't fetch device statistics upon construction

### Plasma Framework

- Make Breeze Light/Dark use more system colors
- Export SortFilterModel sort column to QML
- plasmacore: fix qmldir, ToolTip.qml no longer part of module
- signal availableScreenRectChanged for all applets
- Use simply configure_file to generate the plasmacomponents3 files
- Update *.qmltypes to current API of QML modules
- FrameSvg: also clear mask cache on clearCache()
- FrameSvg: make hasElementPrefix() also handle prefix with trailing -
- FrameSvgPrivate::generateBackground: generate background also if reqp != p
- FrameSvgItem: emit maskChanged also from geometryChanged()
- FrameSvg: prevent crash when calling mask() with no frame yet created
- FrameSvgItem: emit maskChanged always from doUpdate()
- API dox: note for FrameSvg::prefix()/actualPrefix() the trailing '-'
- API dox: point to Plasma5 versions on techbase if avail
- FrameSvg: l &amp; r borders or t &amp; b don't need to have same height resp. width

### Purpose

- [JobDialog] Also signal cancellation when window is closed by the user
- Report cancelling a configuration as finished with an error (bug 407356)

### QQC2StyleBridge

- Remove DefaultListItemBackground and MenuItem animation
- [QQC2 Slider Style] Fix wrong handle positioning when initial value is 1 (bug 405471)
- ScrollBar: Make it work as a horizontal scroll bar as well (bug 390351)

### Solid

- Refactor the way device backends are built and registered
- [Fstab] Use folder-decrypted icon for encrypting fuse mounts

### Syntax Highlighting

- YAML: only comments after spaces and other improvements/fixes (bug 407060)
- Markdown: use includeAttrib in code blocks
- fix highlighting of "0" in C mode
- Tcsh: fix operators and keywords
- Add syntax definition for the Common Intermediate Language
- SyntaxHighlighter: Fix foreground color for text without special highlighting (bug 406816)
- Add example app for printing highlighted text to pdf
- Markdown: Use color with higher contrast for lists (bug 405824)
- Remove .conf extension from "INI Files" hl, to determine the highlighter using MIME type (bug 400290)
- Perl: fix the // operator (bug 407327)
- fix casing of UInt* types in Julia hl (bug 407611)

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
