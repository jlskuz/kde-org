---
aliases:
- ../announce-4.1.3
date: '2008-11-05'
description: KDE Community Ships Third Maintenance Update for KDE 4.1.
title: KDE 4.1.3 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
  KDE Community Improves Desktop with KDE 4.1.3 Codenamed "Change"
</h3>

<p align="justify">
  <strong>
KDE Community Ships Third Translation and Service Release of the 4.1
Free Desktop, Containing Numerous Bugfixes, Performance Improvements and
Translation Updates
</strong>
</p>

<p align="justify">
 The <a href="/">KDE
Community</a> today announced the immediate availability of <em>"Change"</em>,
(a.k.a KDE 4.1.3), another
bugfix and maintenance update for the latest generation of the most advanced and powerful
free desktop. <em>Change</em> is a monthly update to <a href="../4.1.0/">KDE 4.1</a>. It
ships with desktop workspace and many cross-platform applications such as administration
programs, network tools, educational applications, utilities, multimedia software, games, artwork,
web development tools and more. KDE's award-winning tools and applications are
available in more than 50 languages.
</p>

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-screenie.png">
	<img src="/announcements/4/4.1.0/dolphin-screenie_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Dolphin's new selection mechanism</em>
</div>
<br/>

<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.1.3/">download.kde.org</a> and can
also be obtained on <a href="/download">CD-ROM</a>
or with any of the <a href="/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  <a id="changes">Enhancements</a>
</h4>
<p align="justify">
As a service release, the
<a href="/announcements/changelogs/changelog4_1_2to4_1_3">changelog</a>
contains a list of bugfixes and improvements</a>. Note that the changelog is
usually incomplete, for a complete list of changes that went into KDE 4.1.3, you
can browse the Subversion log. The most significant changes are:

<ul>
    <li>Two crashes fixed in the Dolphin filemanager.</li>
	<li>A large number of bugfixes and optimizations in the KHTML HTML rendering component.</li>
	<li>Several bugfixes in the <a href="http://kopete.kde.org/">Kopete</a> multi-protocol
        Instant Messenger.</li>
</ul>

KDE 4.1.3 also ships a more complete set of translations.

To find out more about the KDE 4.1.x desktop and applications, please refer to the
<a href="/announcements/4.1/">KDE 4.1.0</a> and
<a href="/announcements/4.0/">KDE 4.0.0</a> release
notes. KDE 4.1.3 is a recommended update for everyone running KDE 4.1.2 or earlier.
This release will be followed up by KDE 4.1.4 in December and
ultimately by a new feature release, KDE 4.2.0 this coming January.

<p />

<h4>Extragear</h4>
<p align="justify">
The KDE Extragear packages contains a number
of useful additional and alternative applications.
Since KDE 4.0.0, <a href="http://extragear.kde.org">Extragear</a> applications
are also part of regular KDE releases.
Extragear applications are KDE applications that are mature, but not part
of one of the other KDE packages.
</p>

<h4>
  Installing KDE 4.1.3 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.1.3
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.1.3/">download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4/4.1.3">KDE 4.1.3 Info
Page</a>.
</p>

<h4>
  Compiling KDE 4.1.3
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE 4.1.3 may be <a
href="http://download.kde.org/stable/4.1.3/src/">freely downloaded</a>.
Instructions on compiling and installing KDE 4.1.3
  are available from the <a href="/info/4/4.1.3#binary">KDE 4.1.3 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>


