---
aliases:
- ../4.4
date: '2010-02-09'
title: Anuncio de la publicación de KDE SC 4.4.0 Caikaku
---

<h3 align="center">
  La Compilación de Software KDE 4.4.0 introduce una nueva interfaz para <i>netbooks</i>, ventanas en pestañas y framework de autenticación
</h3>

<p align="justify">
  <strong>
    Publicada la Compilación de Software KDE 4.4.0 (nombre en código: <i>«Caikaku»</i>)
  </strong>
</p>

<p align="justify">
<a href="/">KDE</a> ha anunciado hoy la disponibilidad inmediata de la Compilación de Software KDE 4.4, <i>«Caikaku»</i>, que proporciona una innovadora colección de aplicaciones a los usuarios de software libre. Se han introducido importantes tecnologías nuevas, incluyendo redes sociales y funciones de colaboración en red, una nueva interfaz para <i>netbooks</i> e innovaciones en la infraestructura como el marco de autenticación KAuth. Según el sistema de seguimiento de fallos de KDE, se han corregido 7293 fallos, y se han implementado 1443 solicitudes de nuevas funciones.  La comunidad de KDE da las gracias a todos los que han ayudado a hacer que esta publicación sea posible.</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/general-desktop.png">
	<img src="/announcements/4/4.4.0/thumbs/general-desktop_thumb.png" class="img-fluid" alt="El espacio de trabajo del escritorio Plasma de KDE">
	</a> <br/>
	<em>El espacio de trabajo del escritorio Plasma de KDE</em>
</div>
<br/>


<p align="justify">
 Lea la <a href="/announcements/4/4.4.0/guide">Guía visual de la Compilación de Software KDE 4.4</a> para más detalles sobre las mejoras en 4.4, o siga leyendo para una visión general.</p>


<h3>
  El espacio de trabajo Plasma introduce funciones web y sociales
</h3>
<br />

<p align="justify">
 Los espacios de trabajo Plasma de KDE ofrecen las funciones básicas que necesita un usuario para iniciar y gestionar aplicaciones, archivos y opciones globales. Los desarrolladores del espacio de trabajo de KDE han desarrollado una nueva interfaz para <i>netbooks</i>, un trabajo artístico más refinado y un mejor flujo de trabajo para Plasma. La introducción de funciones de servicios en red y sociales surge de la naturaleza cooperativa de la comunidad de KDE como equipo de software libre.</p>

<!-- Plasma Screencast -->

<div class="text-center">
	<em>Interacción mejorada con la interfaz de escritorio Plasma </em><br/>
<a href="http://blip.tv/file/get/Sebasje-KDEPlasmaIn44540.ogg">descargar</a></div>
<br/>


<p align="justify">
<ul>
  <li>
    <strong>Plasma Netbook</strong> se estrena en 4.4.0. Plasma Netbook es una interfaz alternativa al escritorio Plasma, diseñada específicamente para un uso ergonómico en <i>netbooks</i> y portátiles pequeños. El framework Plasma se ha construido desde el principio pensando también en los dispositivos que no son de escritorio. Plasma Netbook comparte muchos componentes con el escritorio Plasma, pero esta diseñado específicamente para hacer buen uso de un espacio reducido, y ser más adecuado también para pantallas táctiles.
    La interfaz Plasma Netbook incluye un lanzador de aplicaciones a pantalla completa y una interfaz de búsqueda, así como un periódico con componentes gráficos para mostrar contenido de la web y pequeñas utilidades ya conocidas de su hermano.
  </li>
  <li>
    La iniciativa del <strong>escritorio social</strong> trae mejoras al componente de Comunidad (anteriormente conocido como Escritorio Social), permitiendo a los usuarios enviar mensajes y buscar amigos directamente desde él. El nuevo componente Noticias Sociales muestra en directo lo que está sucediendo en la red social del usuario, y el nuevo componente Base de Conocimiento permite a los usuarios buscar respuestas y preguntas de diferentes proveedores, incluyendo la propia base de conocimiento de openDesktop.org.
  </li>
  <li>
    La nueva función de ventanas en pestañas de KWin permite al usuario <strong>agrupar ventanas</strong> en una interfaz de pestañs, facilitando y haciendo más eficiente el manejo de muchas aplicaciones. Otras mejoras del manejo de ventanas son el ajuste a un lado de la pantalla y su maximizado al arrastrarlas. El equipo de KWin ha colaborado con los desarrolladores de Plasma para mejorar la interacción entre los espacios de trabajo de las aplicaciones, proporcionando animaciones más suaves y mejorando el rendimiento. Finalmente, los diseñadores pueden desarrollar y compartir temas más fácilmente gracias al desarrollo un decorador de ventanas más configurable y con la capacidad de utilizar gráficos escalables.
  </li>
</ul>
</p>

<!-- Window grouping Screencast -->
<div class="text-center">
	<em>Gestión de ventanas más sencilla   </em><br/>
<a href="http://blip.tv/file/get/Sebasje-WindowManagingFeaturesInKDEPlasma44222.ogg">descargar</a></div>
<br/>

<h3>
  Innovaciones en las aplicaciones de KDE
</h3>
<p align="justify">
La comunidad de KDE ofrece un gran número de aplicaciones potentes y a la vez fáciles de usar. Esta versión introduce varias mejoras incrementales y algunas tecnologías innovadoras.
</p>
<p align="justify">
<ul>
  <li>
    En esta versión de la Compilación de Software de KDE se ha mejorado en gran medida la <strong>interfaz de GetHotNewStuff</strong>, como resultado de un proceso de diseño y planificación a largo plazo. Este framework está destinado a permitir que la gran cantidad de terceros que contribuyen con la comunidad de KDE se <strong>comuniquen más fácilmente</strong> con los millones de usuarios de sus contenidos. Los usuarios pueden descargar datos como <a href="http://dimsuz.wordpress.com/2009/11/11/katomic-new-feature-level-sets-support/">nuevos niveles de KAtomic</a>, nuevas estrellas o guiones de mejora de funcionalidad desde la misma aplicación. Como <strong>características sociales</strong> novedosas se incluyen los comentarios, la puntuación de contenido o el hacerse fan de un producto, con lo que se mostrarán al usuario actualizaciones de estos en el componente de Novedades Sociales. Los usuarios pueden <strong>subir los resultados</strong> de su propia creatividad a la web desde varias aplicaciones, eliminando el pesado proceso de empaquetarlos y subirlos a mano a un sitio web.
  </li>
  <li>
    Otros dos proyectos a largo plazo de la Comunidad KDE han dado por fin sus frutos en esta versión. Nepomuk, un desarrollo internacional financiado por la Comunidad Europea, ha alcanzado un nivel suficiente de estabilidad y rendimiento. <strong><a href="http://ppenz.blogspot.com/2009/11/searching.html">La integración en Dolphin de la búsqueda de escritorio</a> hace uso del framework semántico de Nepomuk para ayudar al usuario a ubicar y organizar sus datos. La nueva vista de línea temporal muetra los archivos usados recientemente organizados de manera cronológica. Mientras tanto, el equipo de KDE PIM ha portado las primeras aplicaciones para hacer uso del nuevo sistema de almacenamiento y consulta de datos, <strong>Akonadi</strong>. La libreta de direcciones de KDE ha sido reescrita con una nueva interfaz de 3 paneles. En futuras versiones de la Compilación de Software KDE se irán migrando más aplicaciones a estas nuevas tecnologías.
  </li>
  <li>
    Además de la integración con estas importantes tecnologías, los equipos de desarrollo han mejorado sus aplicaciones de muchas maneras. Los desarrolladores de KGet han añadido soporte para la comprobación de firmas digitales y la descarga de archivos de múltiples orígenes, y Gwenview incluye una herramienta sencilla de importación de fotografías. Además, en esta versión verán la luz aplicaciones nuevas y completamente reescritas. Palapeli es un juego que permite al usuario resolver puzzles en su equipo, y crear y compartir los suyos propios. Cantor es una interfaz intuitiva para software científico y estadístico (<a href="http://r-project.org/">R</a>, <a href="http://sagemath.org/">SAGE</a> y <a href="http://maxima.sourceforge.net/">Maxima</a>). La suite KDE PIM incluye Blogilo, una nueva aplicación de blogging.
  </li>
</ul>
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/dolphin-search.png">
	<img src="/announcements/4/4.4.0/thumbs/dolphin-search_thumb.png" class="img-fluid" alt="Búsqueda de escritorio integrada en el gestor de archivos Dolphin">
	</a> <br/>
	<em>Búsqueda de escritorio integrada en el gestor de archivos Dolphin</em>
</div>
<br/>


<h3>
  Plataforma de aceleración del desarrollo
</h3>
<p align="justify">
El fuerte enfoque de la comunidad de KDE en la construcción de una excelente tecnología ha resultado en una de las plataformas de desarrollo más completas, consistentes y eficientes. La versión 4.4 introduce en sus bibliotecas muchas tecnologías asociadas a las redes colaborativas y sociales. Hemos proporcionado unas alternativas poderosas, flexibles y abiertas a las tecnologías ya existentes. En lugar de atar a los usuarios a nuestros productos, nos centramos en permitir innovar tanto en los espacios de la red como del escritorio.
</p>
<p align="justify">
<ul>
  <li>
    La infraestructura subyacente en el software ha recibido significativas mejoras. Para empezar, <strong>Qt 4.6</strong> introduce el soporte a la plataforma Symbian, un nuevo framework de animaciones, <em>multitouch</em> y un mejor rendimiento. La <strong>tecnología del Escritorio Social</strong> presentada en la versión anterior ha sido mejorada con un gestor centralizado de identidades y con el acceso transparente a servicios web mediante la biblioteca libattica.
<strong>Nepomuk</strong> hace ahora uso de un motor mucho más estable, convirtiéndolo en la elección perfecta para búsqueda, indexación y metadatos de aplicaciones.
  </li>
  <li>
    Con KAuth se presenta un nuevo framework de autenticación. <strong>KAuth proporciona autenticación segura</strong> y elementos relacionados con la interfaz de usuario para desarrolladores que desean ejecutar tareas con privilegios elevados. En Linux, KAuth utiliza PolicyKit como motor, proporcionando una <strong>integración transparente con múltiples escritorio</strong>. KAuth ya se utiliza en algunos cuadros de diálogo de las Preferencias del Sistema, y durante los próximos meses se integrará con el Escritorio Plasma de KDE y las Aplicaciones KDE. KAuth acepta políticas de aceptación y rechazo complejas, cacheo de contraseñas y varios elementos de IU dinámicos con consejos visuales para su uso en aplicaciones.
  </li>
  <li>
    <strong>Akonadi</strong>, la caché de software colaborativo de Free Desktop se introduce en las Aplicaciones de KDE en esta versión 4.4.0. La libreta de direcciones de KDE SC 4.4 es la primera aplicación de KDE que hace uso de la nueva infraestructura de Akonadi. KAdressbook acepta libretas de direcciones locales y varios servidores colaborativos. KDE SC 4.4.0 marca el comienzo de la era de Akonadi en KDE SC, con más aplicaciones que harán uso de ella en futuras versiones. Akonadi está destinada a ser la interfaz centralizada para correo electrónico, contactos, calendarios y otra información personal, actuando como caché transparente para el servidores de correo o colaborativos, y otros recursos en línea.
  </li>
</ul>
Liberado bajo la licencia LGPL (permitiendo el desarrollo de código abierto y propietario) y multiplataforma (Linux, UNIX, Mac y MS Windows).
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/social-web-widgets.png">
	<img src="/announcements/4/4.4.0/thumbs/social-web-widgets_thumb.png" class="img-fluid" alt="La web y las redes sociales en el Escritorio Plasma">
	</a> <br/>
	<em>La web y las redes sociales en el Escritorio Plasma</em>
</div>
<br/>

<h4>
Más cambios
</h4>
<p align="justify">
Como se mencionó, lo anterior es solo una selección de los cambios y mejoras en el escritorio KDE, las aplicaciones de KDE y el Framework de Desarrollo de Aplicaciones de KDE. Una lista más amplia, aunque aún incompleta, se puede encontrar en el <a href="http://techbase.kde.org/Schedules/KDE4/4.4_Feature_Plan">plan de características de KDE SC 4.4</a> en <a href="http://techbase.kde.org">TechBase</a>. Puede encontrar información sobre las aplicaciones desarrolladas por la comunidad de KDE fuera de las suites de aplicaciones de KDE en la <a href="https://www.kde.org/family/">web de la familia KDE</a> y en el <a href="http://kde-apps.org">sitio web kde-apps</a>.
</p>


<h4>
    Corra la voz y vea lo que ocurre
</h4>
<p align="justify">
La comunidad de KDE anima a todo el mundo a <strong>correr la voz</strong> en la web social. Envíe artículos a sitios web, use canales como delicious, digg, reddit, twitter o identi.ca. Suba capturas de pantalla a servicios como Facebook, FlickR, ipernity o Picasa y publíquelas en los grupos adecuados. Cree demostraciones en vídeo y envíelas a YouTube, Blip.tv, Vimeo y otros sitios. No olvide etiquetar el material que envíe con la <em>etiqueta <strong>kde</strong></em> para que sea más fácil para todo el mundo encontrar el material, y para el equipo de KDE compilar recortes del anuncio de KDE SC 4.4. <strong>¡Ayúdenos a correr la voz, sea parte del proyecto!</strong></p>

<p align="justify">
Puedes seguir en directo lo que ocurre en torno a la liberación de KDE SC 4.4 en la web social a través del nuevo <a href="http://buzz.kde.org"><strong>livefeed de la Comunidad de KDE</strong></a>. Este sitio agrega en tiempo real lo que sucede en identi.ca, twitter, youtube, flickr, picasaweb, blogs y en muchas otras redes sociales. Puede encontrar el livefeed en <strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/linux_unix/KDE_Software_Compilation_4_4_0_Introduces_new_innovations"><img src="/announcements/buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/technology/comments/azx29/kde_software_compilation_440_introduces_netbook/"><img src="/announcements/buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde44"><img src="/announcements/buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde44"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde44"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde44"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="/announcements/buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde44"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>


<h4>
  Instalación de KDE SC 4.4.0
</h4>
<p align="justify">
KDE, incluyendo todas sus bibliotecas y aplicaciones, está disponible gratuitamente bajo licencias de software libre. El software de KDE se puede ejecutar sobre varias configuraciones de hardware, sistemas operativos y cualquier gestor de ventanas o entorno de escritorio. Además de Linux y otros sistemas operativos basados en UNIX, en el sitio web de <a href="http://windows.kde.org">KDE for Windows</a> dispone de versiones para Microsoft Windows de la mayoría de aplicaciones, y versiones para Mac OS X en el sitio de <a href="http://mac.kde.org/">KDE on Mac</a>. También puede encontrar compilaciones experimentales de las aplicaciones de KDE para plataformas móviles como MS Windows Mobile y Symbian en la web, pero no están soportadas.
<br />
KDE se puede obtener en código fuente y varios formatos binarios en <a href="http://download.kde.org/stable/4.4.0/">download.kde.org</a> y también en <a href="/download">CD-ROM</a> o con cualquiera de las <a href="/distributions">principales distribuciones de GNU/Linux y sistemas UNIX</a> actuales. 
</p>
<p align="justify">
  <em>Empaquetadores</em>.
  Algunos proveedores de sistemas operativos Linux/UNIX han tenido la amabilidad de proporcionar paquetes binarios de KDE SC 4.4.0 para algunas versiones de su distribución, y en otros casos lo han hecho voluntarios de la comunidad. <br />
Algunos de estos paquetes binarios se pueden descargar gratuitamente desde <a href="http://download.kde.org/binarydownload.html?url=/stable/4.4.0/">download.kde.org</a>. Durante las próximas semanas habrá disponibles más paquetes binarios, así como actualizaciones para los paquetes que ya hay disponibles ahora. 
</p>

<p align="justify">
  <a id="package_locations"><em>Ubicaciones de los paquetes</em></a>.
  Visite la <a href="/info/4/4.4.0">página de información sobre KDE SC 4.4.0</a> para obtener una lista actualizada de los paquetes binarios disponibles de los que ha sido informado el Proyecto KDE</a>.
</p>


<h4>
  Compilación de KDE SC 4.4.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  El código fuente completo de KDE SC 4.4.0 se puede <a href="http://download.kde.org/stable/4.4.0/src/">descargar libremente</a>. Las instrucciones para compilar e instalar KDE SC 4.4.0 están disponibles en la <a href="/info/4/4.4.0#binary">página de información sobre KDE SC 4.4.0</a>.
</p>



