---
aliases:
- ../4.2
date: '2009-01-27'
title: Anunci de la publicació del KDE 4.2.0
---

<h3 align="center">
  La comunitat del KDE millora l'experiència de l'usuari amb KDE 4.2
</h3>

<p align="justify">
  <strong>
    KDE 4.2 (Nom en clau: <em>"La Resposta"</em>) porta millores a l'experiència de
    l'usuari, les aplicacions i la plataforma de desenvolupament
  </strong>
</p>

<p align="justify">
La <a href="/">comunitat del KDE</a> ha 
anunciat avui el llançament de <em>"La resposta"</em>, (també coneguda com a KDE 4.2.0), 
deixant a punt aquest escriptori lliure per als usuaris finals. El KDE 4.2 està construït fent ús de la tecnologia introduïda amb el KDE 4.0 el gener del 2008. Després del llançament del KDE 4.1, el qual anava destinat a usuaris ocasionals, la comunitat del KDE creu que ja pot oferir una oferta convincent per a la majoria dels usuaris finals.
</p>

<p><strong><a href="./guide">Consulteu la Guia Visual del KDE 4.2</a></strong> per més detalls sobre les novetats de 4.2 o continueu per un resum.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/desktop.png">
	<img src="/announcements/4/4.2.0/desktop_thumb.png" class="img-fluid">
	</a> <br/>
	<em>L'escriptori KDE 4.2</em>
</div>
<br/>

<h3>
    L'escriptori millora l'experiència de l'usuari
</h3>
<p align="justify">

<ul>
    <li>
        <p align="justify">
	Els últims refinaments de la interfície de l'escriptori Plasma permeten organitzar
	el vostre espai de treball encara més fàcilment.
        <strong>Miniaplicacions noves i millorades</strong> com ara un llançador ràpid d'aplicacions, informació meteorològica,
        agregador de notícies, còmics, compartició de fitxers ràpida mitjançant serveis "pastebin". Les miniaplicacions de Plasma ara poden
        utilitzar-se sobre del salvapantalles permetent, per exemple, deixar una nota a l'usuari mentre no hi és.
        Si es vol, Plasma pot actuar com un <strong>escriptori - gestor de fitxers tradicional</strong>.
        S'hi ha afegit una vista prèvia pels fitxers i la persistència de la posició de les icones. <br />
        El panell Plasma <strong>ara agrupa tasques</strong> i les mostra en diverses files. La safata del sistema millorada <strong>fa un seguiment de les tasques que duren més temps</strong> com ara les descarregues. Les <strong>notificacions</strong> de les aplicacions i del sistema es mostren amb un estil unificat mitjançant la safata del sistema.
        Les icones de la safata del sistema ara es poden amagar per tal d'estalviar espai. Ara el panell pot
        <strong>amagar-se automàticament</strong> per alliberar espai de la pantalla. Els estris poden mostrar-se tan als panells com a l'escriptori.<br />
        </p>
    </li>
    <li>
        <p align="justify">
        El KWin ofereix una gestió de finestres fluida i eficient. Al KDE 4.2 utilitza <strong>física del moviment</strong> per donar un aire més natural als vells i <strong>nous efectes</strong> com ara el "cub" i la "llàntia màgica". El KWin només activa per defecte els efectes de l'escriptori en ordinadors que són capaços de suportar-los.
        La <strong>fàcil configuració</strong> permet a l'usuari seleccionar diferents efectes com ara l'intercanviador de finestres, permetent més eficiència a l'hora de moure-us entre finestres.
        </p>
    </li>
    <li>
        <p align="justify">
        Les noves i millorades eines de l'espai de treball augmenten la productivitat. El PowerDevil facilita la vida mòbil oferint un modern sistema no intrusiu de <strong>gestió d'energia</strong> als ordinadors portàtils i dispositius mòbils. L'Ark ofereix una <strong>extracció i creació</strong> elegant d'arxius, i les noves eines d'impressió permeten a l'usuari <strong>gestionar impressores</strong> i imprimir treballs fàcilment.
        </p>
    </li>
</ul>
A més a més, s'han afegit les traduccions per uns quants idiomes, expandint el nombre d'usuaris que tenen disponible el KDE en la seva llengua materna en uns 700 milions. Entre els nous idiomes hi ha l'àrab, islandès, basc, hebreu, romanès, tadjik i varis idiomes de la Índia (Indi bengalí, gujarati, kanarès, maithili, marathi), fet que indica un augment de la popularitat en aquesta part d'Àsia.
</p>

<div class="text-center">
	<em>The Plasma desktop interface in KDE 4.2</em><br/>
<a href="http://blip.tv/file/get/Sebasje-ThePlasmaDesktopShellInKDE42312.ogv">Ogg Theora version</a></div>
<br/>

<h3>
    Un pas endavant per les aplicacions
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
        La gestió de fitxers esdevé més ràpida i eficient. Dolphin, el gestor de fitxers, ara té un botó de desplaçament per a <strong>ajustar la mida de les icones</strong> fàcilment.
        Altres millores de la interfície d'usuari inclouen <strong>suggeriments amb previsualitzacions</strong> i un indicador de la capacitat pels dispositius d'emmagatzematge externs. Aquests canvis també s'han aplicat als <strong>diàlegs de selecció de fitxers</strong> del KDE, fent més fàcil localitzar el fitxer que s'està buscant.
        </p>
    </li>
    <li>
        <p align="justify">
	Les vistes en llista dels correus electrònics del KMail han estat re-dissenyades per un estudiant del "Google Summer of Code". Ara l'usuari pot configurar la <strong>visualització d'informació addicional</strong> optimitzant el flux de treball per a cada carpeta  individualment. També s'ha millorat la implementació per a l'<strong>IMAP i altres protocols</strong> fent el KMail molt més ràpid.
        </p>
    </li>
    <li>
        <p align="justify">
        La navegació per la xarxa ha millorat. El navegador web Konqueror ha millorat el funcionament dels <strong>gràfics vectorials escalables</strong> i ha rebut moltes millores en el rendiment. S'ha fet un <strong>nou diàleg de cerca</strong> menys intrusiu per a cercar dins de les webs. Ara el Konqueror <strong>mostra els seus punts</strong> com a pàgina inicial.
        </p>
    </li>
</ul>
</p>

<div class="text-center">
	<em>Window Management in KDE 4.2</em><br/>
<a href="http://blip.tv/file/get/Sebasje-WindowManagementInKDE42153.ogv">Ogg Theora version</a></div>
<br/>

<h3>
    La plataforma accelera el desenvolupament
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
        <strong>Funcionament millorat per als scripts</strong>. Les miniaplicacions per a Plasma ja poden ser escrites en JavaScript, Python i Ruby. Aquestes es poden distribuir per la xarxa mitjançant serveis web i eines de col·laboració en línia com ara l'OpenDesktop.org.
        Els GoogleGadgets es poden utilitzar al Plasma, i el funcionament de les miniaplicacions pel "dashboard" de Mac OS X a Plasma s'ha millorat molt.
        </p>
    </li>
    <li>
        <p align="justify">
        Hi ha disponibles versions prèvies de diverses aplicacions del KDE per a <strong>Windows</strong> i <strong>Mac OS X</strong>, algunes aplicacions gairebé estan llestes pel llançament mentre que altres necessiten una mica més de treball depenent de les funcionalitats que implementin. El funcionament a l'OpenSolaris està millorant i s'acosta a la qualitat d'una versió estable. El KDE4 per al FreeBSD continua madurant.
        </p>
    </li>
    <li>
        <p align="justify">
        Quan les Qt s'hagin alliberat sota els termes de la <strong>LGPL</strong>, tan les biblioteques del KDE com les Qt estaran disponibles sota aquesta llicència més laxa, fent del KDE una plataforma més atractiva pel desenvolupament comercial de programari privatiu.
        </p>
    </li>
</ul>
</p>

<h4>
  Instal·lant el KDE 4.2.0
</h4>
<p align="justify">
 El KDE, inclòs totes les seves llibreries i aplicacions, està disponible gratuïtament sota llicències de programari lliure. Es pot obtenir el codi font del KDE i varis formats binaris a <a
href="http://download.kde.org/stable/4.2.0/">download.kde.org</a>. També es pot aconseguir en <a href="/download">CD-ROM</a>
o amb qualsevol de les <a href="/distributions">distribucions principals de GNU/Linux i sistemes UNIX </a> actuals.
</p>
<p align="justify">
  <em>Empaquetadors</em>.
  Alguns proveïdors de sistemes operatius Linux/UNIX han proporcionat amablement paquets binaris pel KDE 4.2.0 per algunes versions de llurs distribucions, i en altres casos han estat voluntaris de les comunitats els qui ho han fet.
  Alguns d'aquests fitxers binaris es poden descarregar gratuïtament des de <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.2.0/">download.kde.org</a>.
  Durant les pròximes setmanes podran haver-hi altres paquets binaris i actualitzacions dels que ja estan disponibles.
</p>

<p align="justify">
  <a id="package_locations"><em>Ubicació dels paquets</em></a>.
  Per veure la llista actualitzada dels paquets binaris disponibles dels què el projecte KDE té notícia, consulteu la <a href="/info/4/4.2.0">pàgina d'informació del KDE 4.2.0</a>.
</p>

<p align="justify">
Els problemes de rendiment amb els controladors binaris de <em>NVidia</em> han estat <a href="http://techbase.kde.org/User:Lemma/KDE4-NVIDIA">resolts</a> a l'últim llançament de la versió beta del controlador disponible de NVidia.
</p>

<h4>
  Compilant KDE 4.2.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  El codi font complet del KDE 4.2.0 es pot<a
href="http://download.kde.org/stable/4.2.0/src/">descarregar lliurement</a>.
Les instruccions per compilar i instal·lar el KDE 4.2.0
  estan disponibles a la <a href="/info/4/4.2.0#binary">pàgina d'informació del KDE 4.2.0</a>.
</p>

<h4>
    Feu córrer la veu
</h4>
<p align="justify">
L'equip del KDE anima a tothom a difondre la notícia a les xarxes socials. Envieu articles als portals web, utilitzeu canals com la tafanera, delicious, digg, reddit, twitter,
identi.ca. Pugeu captures de pantalla a serveis com Facebook, FlickR,
ipernity i Picasa i publiqueu-les als grups apropiats. Creeu vídeos,
pugeu-los a YouTube, Blip.tv, Vimeo i altres. No oblideu d'etiquetar el material que publiqueu amb l'<em>etiqueta <strong>kde42</strong></em> per tal que sigui més fàcil per tothom trobar el material, i per l'equip del KDE de compilar les crítiques i la cobertura que es faci del llançament del KDE 4.2. Aquesta és la primera vegada que l'equip del KDE intenta coordinar esforços per utilitzar les xarxes i mitjans socials per fer arribar el seu missatge. Ajuda'ns a difondre la idea, forma'n part.
</p>
<p>
Als fòrums, informeu a tothom sobre les noves i atractives característiques, ajudeu als altres a començar amb el seu nou entorn d'escriptori, ajudeu-nos a difondre la informació.
</p>


