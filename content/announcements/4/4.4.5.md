---
aliases:
- ../announce-4.4.5
date: '2010-06-30'
description: KDE Ships New KDE SC 4.4.5.
title: KDE Software Compilation 4.4.5 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>


<h3 align="center">
  KDE Software Compilation 4.4.5 Released: Codename "Ceilidh"
</h3>

<p align="justify">
  <strong>
KDE Community Ships Fifth Translation and Service Release of the 4.4
Free Desktop, Containing Numerous Bugfixes and Translation Updates
</strong>
</p>

<p align="justify">
 Today, KDE has released a new version of the KDE Software Compilation (KDE SC). This is expected to be the final bugfix and translation update to KDE SC 4.4. KDE SC 4.4.5 is a recommended update for everyone running KDE SC 4.4.4 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. Users around the world will appreciate that KDE SC 4.4.5 multi-language support is more complete. KDE SC 4 is already translated into more than 55 languages, with more to come.
</p>

<p>To download source code or packages to install go to the <a href="/info/4/4.4.5">KDE SC 4.4.5 Info Page</a>.</p>

<p>
KDE SC 4.4.5 brings a number of improvements:
</p>
<ul>
    <li>
    Several bugs in encoding and refreshing in Konsole have been fixed
    </li>
    <li>
    A couple of crashes in Okular's PDF viewer have been fixed
    </li>
    <li>
    Alarms have received some fixes in KDE PIM
    </li>
</ul>
<p>
The <a href="/announcements/changelogs/changelog4_4_4to4_4_5">changelog</a> lists more, if not all improvements since KDE SC 4.4.4.  There are no more scheduled releases in the 4.4 series.  The next feature release will be KDE SC 4.5.0, to be released in August this year.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/general-desktop.png">
	<img src="/announcements/4/4.4.0/thumbs/general-desktop_thumb.png" class="img-fluid" alt="The KDE Plasma Desktop Workspace">
	</a> <br/>
	<em>The KDE Plasma Desktop Workspace</em>
</div>
<br/>


<p align="justify">
Note that the changelog is usually incomplete. For a complete list of
changes that went into KDE SC 4.4.5, you can browse the Subversion log.
KDE SC 4.4.5 also ships a more complete set of translations for many of the 55+ supported languages.</p>

<p>To find out more about the KDE 4.4 Workspace and Applications, please refer to the
<a href="/announcements/4.4/">KDE SC 4.4.0</a>,
<a href="/announcements/4.3/">KDE SC 4.3.0</a>,
<a href="/announcements/4.2/">KDE SC 4.2.0</a>,
<a href="/announcements/4.1/">KDE SC 4.1.0</a> and
<a href="/announcements/4.0/">KDE SC 4.0.0</a> release
notes. KDE SC 4.4.5 is a recommended update for everyone running KDE SC 4.4.4 or earlier versions.
<p />


<p align="justify">
KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.4.5/">download.kde.org</a> 
or with any of the <a href="/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing KDE SC 4.4.5 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE SC 4.4.5
for some versions of their distribution, and in other cases community volunteers
have done so.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"></a><em>Package Locations</em>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4/4.4.5">KDE SC 4.4.5 Info
Page</a>.
</p>

<h4>
  Compiling KDE SC 4.4.5
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE SC 4.4.5 may be <a
href="http://download.kde.org/stable/4.4.5/src/">freely downloaded</a>.
Instructions on compiling and installing KDE SC 4.4.5
  are available from the <a href="/info/4/4.4.5#binary">KDE SC 4.4.5 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>

<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information or 
become a KDE e.V. supporting member through our new 
<a href="http://jointhegame.kde.org/">Join the Game</a> initiative. </p>


