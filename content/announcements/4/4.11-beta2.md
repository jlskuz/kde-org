---
aliases:
- ../announce-4.11-beta2
date: '2013-06-27'
description: KDE Ships Second Beta of Plasma Workspaces, Applications and Platform
  4.11.
title: KDE Ships Second Beta of Plasma Workspaces, Applications and Platform 4.11
---

June 27, 2013. Today KDE released the second beta of the new versions of Workspaces, Applications, and Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

{{< figure src="/announcements/4/4.11-beta1/announce-4.11-beta1.png" caption="New KWalletManager user interface; changes to Okular" width="600px" >}}

The 4.11 releases include the following highlights and more:

- <strong>Qt Quick in Plasma Workspaces</strong>—Qt Quick is continuing to make its way into the Plasma Workspaces. Plasma Quick, KDE&quot;s extensions on top of Qt Quick, allow deeper integration and more powerful apps and Plasma components. One of the central Plasma widgets, the task manager, was completely rewritten in Plasma Quick. It got quite a few bug fixes on its way to the new QML version. The Battery widget was overhauled. It now shows information about all the batteries (e.g. mouse, keyboard) in a system. And the battery icons can now show a finer-grained load-status.

* <strong>Faster Nepomuk indexing</strong>—The Nepomuk semantic engine received massive performance optimizations (e.g., reading data is 6 or more times faster). Indexing happens in two stages: the first stage retrieves general information (such as file type and name) immediately; additional information like MP3 tags, author information and similar is extracted in a second, somehow slower stage. Metadata display is now much faster. In addition, the Nepomuk backup and restore system was improved. The system also got new indexers for documents like odt or docx.

- <strong>Kontact improvements</strong>—Kontact got a faster indexer for its PIM data with improvements to Nepomuk, and a <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>new theme editor</a> for email headers. The way it handles email images now allows it to resize pictures on the fly. The whole KDE PIM suite got a lot of bug fixes, such as the way it deals with Google Calender resources. The PIM Import Wizard allows users to import settings and data from <a href='http://trojita.flaska.net/'>Trojita</a> (the Qt IMAP email client) and all other importers were improved as well.

* <strong>KWin and Path to Wayland</strong>—Initial experimental support for Wayland was added to KWin. KWin also got many OpenGL improvements including support being added for creating an OpenGL 3.1 core context and robustness from using the new functionality provided by the GL_ARB_robustness extension. Numerous KWin optimizations are aimed at reducing CPU and memory overhead in the OpenGL backend. Some desktop effects have been re-written in JavaScript to ease maintenance.

More improvements can be found in <a href='http://techbase.kde.org/Schedules/KDE4/4.11_Feature_Plan'>the 4.11 Feature Plan</a>.

With the large number of changes, the 4.11 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the 4.11 team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.

### KDE Software Compilation 4.11 Beta2

The KDE Software Compilation, including all its libraries and its applications, is available for free under Open Source licenses. KDE's software can be obtained in source and various binary formats from <a href='http://download.kde.org/unstable/4.10.90/'>download.kde.org</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

#### Installing 4.11 Beta2 Binary Packages

<em>Packages</em>.
Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11 Beta2 (internally 4.10.90) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>.
For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11_Beta_2_.284.10.90.29'>Community Wiki</a>.

#### Compiling 4.11 Beta2

The complete source code for 4.11 Beta2 may be <a href='http://download.kde.org/unstable/4.10.90/src/'>freely downloaded</a>. Instructions on compiling and installing 4.10.90 are available from the <a href="/info/4/4.10.90">4.10.90 Info Page</a>.

#### Supporting KDE

KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='http://jointhegame.kde.org/'>Join the Game</a> initiative.


