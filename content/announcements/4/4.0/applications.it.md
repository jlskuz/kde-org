---
custom_about: true
custom_contact: true
hidden: true
title: KDE 4.0 Applications
---

<h2>Dolphin, il gestore di filer</h2>
<p>
Dolphin è il nuovo gestore dei file per KDE4. Naviga, trova, apri, copia e muovi i tuoi file con Dolphin. Dolphin è focalizzato sulla facilità d'uso e rimpiazza il componente di Konqueor per la gestione dei file, utilizzato su KDE3. Nonostante Konqueror possa ancora essere impostato come gestore dei file, ed effettivamente può essere combinato con Dolphin, la squadra di KDE ha deciso di introdurre un applicazione che fosse ottimizzata per quel compito.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/dolphin-splitview.png">
<img src="/announcements/4/4.0/dolphin-splitview_thumb.png" class="img-fluid">
</a> <br/>
<em>Copia i tuoi file con Dolphin</em>
</div>
<br/>

<p>
Per le cartelle contenenti molte immagini, premi il pulsante di anteprima che si trova nella barra degli strumenti di Dolphin e avrai le anteprime delle immagini contenute in quella cartella. Per muoverti velocemente fra le cartelle puoi fare click sul percorso situato proprio sopra alla visualizzazione dei file. Facendo click su una delle frecce situate fra i nomi contenuti nel percorso avrai la possibilità di muoverti fra le sottocartelle. Per una visuale doppia che rende la copia dei file fra le cartelle più semplice utilizza il pulsante «Dividi». Dolphin ricorda le impostazioni di ogni cartella, ma puoi anche impostare dei parametri generali secondo i tuoi gusti tramite il menu «Impostazioni» e scegliendo la voce «Configura Dolphin».
</p>

<div class="text-center">
<a href="/announcements/4/4.0/dolphin-groups.png">
<img src="/announcements/4/4.0/dolphin-groups_thumb.png" class="img-fluid">
</a> <br/>
<em>Raggruppamento dei file con Dolphin</em>
</div>
<br/>

<p>
Nella parte sinisstra, Dolphin fornisce un accesso veloce alle risorse più frequentemente utilizzate. Trascna semplicemente una cartella nel pannello, sarà così possiblie raggiungerla molto più facilmente, sia da Dolphin che da KickOff, ma anche dala finestra di dialogo «Apri...» di tuttle le applicazioni.
</p>

<p>
La barra delle informazioni fornisce informazionin aggiuntive sul file selezionato. Può anche essere utilizzata per aggiungere commenti ed etichette ai file per catalogarli meglio. Abilita l'opzione «Mostra in gruppi» nel menu «Visualizza» per impostare la visualizzazione in gruppi ordinati per grandezza, tipo o altre caratteristiche.
</p>

<h2>Okular e Gwenview: Visualizza i tuoi documenti ed immagini</h2>

<div class="text-center">
<a href="/announcements/4/4.0/gwenview.png">
<img src="/announcements/4/4.0/gwenview_thumb.png" class="img-fluid">
</a> <br/>
<em>View your images with Gwenview</em>
</div>
<br/>

<p>
<strong>Gwenview</strong> è il visualizzatore di immagini di KDE. Nonostante fosse già presente in KDE3, nella nuova versione per KDE4 ne è stata di molto semplificata l'interfaccia utente, rendendola più agevole per navigare velocemente fra le immagini. Gwenview dispone di una gradevole funzionalità per visualizzare le immagini a shcermo intero, come una presentazione.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/okular.png">
<img src="/announcements/4/4.0/okular_thumb.png" class="img-fluid">
</a> <br/>
<em>Okular is KDE 4.0's fast and versatile document reader</em>
</div>
<br/>

<p>
<strong>Okular</strong> è il visualizzatore dei documenti di KDE4. Supporta molti formadi che vanno dal PDF al formato OpenDocument. Okular però, non si limita solamente alla visualizzazione del testo. Il nuovo strumento di revisione permette l'annotazione sui documenti. Premi F6, scegli uno strumento e inizia a scarabbocchiare, aggiungere note e commenti ai documenti. Okular è ispirato a KPdf, il visualizzatore di pdf di KDE 3. Okular è focalizzato sull'usabilità e supporta molti formati.
</p>

<h2>Impostazioni di sistema</h2>
<p>
Impostazioni si Sistema è il nuoco centro di controllo per KDE 4. Quì si possono modificare le impostazioni del desktop, delle applicazioni e degli utenti. Si possono gestire le impostazioni di rete e amministrare il computer in generale.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/systemsettings-appearance.png">
<img src="/announcements/4/4.0/systemsettings-appearance_thumb.png" class="img-fluid">
</a> <br/>
<em>Cambia l'aspetto del desktop su Impostazioni di Sistema</em>
</div>
<br/>

<p>
Apri «Aspetto» per cambiare lo schema dei colori delle applicazioni per dare un contrasto maggiore o per rispecchiare le tue preferenze personali. Quì potrai anche cambiare il carattere e la dimensione del testo utilizzato nelle applicazioni. Nonostante KDE4 habbia come impostazione predefinita una grafica accattivante, potrebbe non corrispondere ai gusti di tutti. Le impostazioni di questa finestra ti permettono di cambiare completamente l'aspetto di KDE 4.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/solid.png">
<img src="/announcements/4/4.0/solid_thumb.png" class="img-fluid">
</a> <br/>
<em>Integrazione Hrdware tramite l'infrastruttura di Solid</em>
</div>
<br/>

<p>
«Impostazioni di Sistema» fornisce anche gli strumenti per contrllare il sistema operativo. Solid si occupa di cose come il risparmio energetico, la gestione delle periferiche rimovibili e le interfacce di rete. Solid dispone di una infrastruttura che può essere configurata a seconda del sistema operativo.
</p>

<h2>Konsole</h2>

<p>
Un esempio di applicazione che è stata sottoposta a intensivi miglioramenti è l'emulatore di terminale Konsole. La finestra delle configurazioni è stata semplificata ma senza rinunciare alle funzioni avanzate di Konsole. I miglioramenti includono:
</p>

<div class="text-center">
<a href="/announcements/4/4.0/konsole.png">
<img src="/announcements/4/4.0/konsole_thumb.png" class="img-fluid">
</a> <br/>
<em>Konsole, KDE's terminal emulator</em>
</div>
<br/>

<p>
<ul>
	<li>
		L'interfaccia grafica è stata riorganizzata e pulita, molte scorciatoie da tastiera sono state aggiunte per rendere Konsole più efficente.
	</li>
	<li>
		La vista può essere divisa in più parti per permettere all'utente di tenere sotto controllo l'output di un comando.
	</li>
	<li>
		I titoli delle schede ora cambiano automaticamente, per renderne più facile il riconoscimento.
	</li>
	<li>
		I risultati delle ricerche sono evidenziati per permette un utilizzo più semplice delle ricerche. Premi CTRL+SHIFT+F in una schefa di Konsole per effettuare ricerche al suo interno.
	</li>
	<li>
		Le prestazioni sono state migliorate nello scorrimenti dei terminali di maggiori dimensioni e nelle ricerche con molti dati.
	</li>
	<li>
		Le finestre del terminale hanno ora trasparenze reali che possono essere abilitate nella scheda dell'Aspetto.
		(Nota che potrebbe essere necessario avviare la prima volta il terminale con il comando "konsole --enable-transparency".)
	</li>
</ul>
</p>
<p>
Maggiori informazioni su Konsole si trovano nel suo<a href="http://websvn.kde.org/branches/KDE/4.0/kdebase/apps/konsole/CHANGES-4.0?view=markup">
changelog</a>.
</p>

<h2>Altre applicazioni</h2>
<p>
Il rilascio di KDE 4.0 porta anche novità per gli utenti delle applicazioni che si trovano in extragear. Le applicazioni che si trovano nei moduli extragear normalmente hanno un loro ciclo di rilascio, ma d'ora in avanti sarà anche possibile seguire il ciclo di rilascio di KDE. La squadra dei rilasci di KDE ha perciò aumentato i suoi doveri e fornirà i pacchetti per le applicazioni che 
<a href="http://techbase.kde.org/Projects/extragearReleases">si aggiungeranno a questa pagina</a> per ogni rilascio di KDE. Questo primo insieme di pacchetti comprende alcune applicazioni extragear più conosciute, like: <a href="http://techbase.kde.org/Projects/Summer_of_Code/2007/Projects/KAider">Kaider</a>, 
<a href="http://ktorrent.org/">KTorrent[3]</a>, 
<a href="http://ktown.kde.org/kphotoalbum/">KPhotoAlbum</a> e <a href="http://rsibreak.org/">RSIBreak</a>. 
Il porting a KDE4 potrebbe non essere completo per queste applicazioni, ma gli sviluppatori saranno felici di ricevere segnalazioni di bug.
</p>

<table width="100%">
	<tr>
		<td width="50%">
				<a href="../desktop">
				<img src="/announcements/4/4.0/images/desktop-32.png" />
				Nella pagina precedente: Desktop
				</a>		
		</td>
		<td align="right" width="50%">
				<a href="../education">Nella prossima pagina: Applicazioni educative di KDE 4.0
				<img src="/announcements/4/4.0/images/education-32.png" /></a>		
				</td>
	</tr>
</table>