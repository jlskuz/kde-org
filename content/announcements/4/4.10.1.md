---
aliases:
- ../announce-4.10.1
date: 2013-03-05
description: KDE Ships Plasma Workspaces, Applications and Platform 4.10.1.
title: KDE Ships March Updates to Plasma Workspaces, Applications and Platform
---

Today KDE released updates for its Workspaces, Applications and Development Platform. These updates are the first in a series of monthly stabilization updates to the 4.10 series. 4.10.1 updates bring many bugfixes and translation updates on top of the 4.10 release and are recommended updates for everyone running the initial 4.10 releases. As this release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone.

The over 100 recorded bugfixes include improvements to the Personal Information Management suite Kontact, the Window Manager KWin, and others. KDE's Development Platform has received a number of updates that affect multiple applications. The changes are listed on <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2011-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.10.1&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=">KDE's issue tracker</a>. For a detailed list of changes that went into 4.10.1, you can browse the Subversion and Git logs.

To download source code or packages to install go to the <a href="/info/4/4.10.1">4.10.1 Info Page</a>. If you want to find out more about the 4.10 versions of KDE Workspaces, Applications and Development Platform, please refer to the <a href='/announcements/4.10/'>4.10 release notes</a>.

{{< figure src="/announcements/4/4.10.0/plasma-tasks.png" caption="KDE's Dolphin File Manager" width="600px" >}}

KDE software, including all libraries and applications, is available for free under Open Source licenses. KDE's software can be obtained as source code and various binary formats from <a href='http://download.kde.org/stable/4.10.1/'>download.kde.org</a> or from any of the <a href="/distributions">major GNU/Linux and UNIX systems</a> shipping today.

#### Installing 4.10.1 Binary Packages

<em>Packages</em>.
Some Linux/UNIX OS vendors have kindly provided binary packages of 4.10.1 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>.
For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href="/info/4/4.10.1#binary">4.10.1 Info Page</a>.

#### Compiling 4.10.1

The complete source code for 4.10.1 may be <a href='http://download.kde.org/stable/4.10.1/src/'>freely downloaded</a>. Instructions on compiling and installing 4.10.1 are available from the <a href="/info/4/4.10.1">4.10.1 Info Page</a>.

#### Supporting KDE

KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='http://jointhegame.kde.org/'>Join the Game</a> initiative.


