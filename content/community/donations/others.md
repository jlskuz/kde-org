---
title: "Other ways to Donate"
hidden: true
---

### Donate through Paypal

This is the preferred way to [donate](../) to KDE.


### Donate through Money Transfer

You may remit your donation to the account below. Be aware of applicable fees. Since July 2003 new EU regulations apply for cross-border transfers in euros and the fees charged by banks for such transactions may no longer exceed the fees that apply to domestic transfers. Please read the [FAQ on cross-border money transfers within the EU](http://europa.eu/rapid/pressReleasesAction.do?reference=MEMO/03/140) to make sure that you do not incur unnecessary fees. Contact your bank for details and the fees that apply to you.

```
K Desktop Environment e.V.
Account-Nr. 0 66 64 46
BLZ 200 700 24
Deutsche Bank Privat und Geschäftskunden
Hamburg, Germany

IBAN : DE82 2007 0024 0066 6446 00
SWIFT-BIC: DEUTDEDBHAM
```

<img src="epc-qr-code.png" width="202" alt="Scan to donate">


### Donate through Personal Check

You can send regular US checks to the following address:

```
K Desktop Environment e.V.
c/o Celeste Lyn Paul
1864 Michael Faraday Drive
Reston, VA 20190 
```

Use "KDE e.V. - Celeste Lyn Paul" in the "Pay to the order of..." line.

### Donate through "crypto" coins

KDE e.V. does not accept donations of "crypto" coins: Bitcoin, Etherium,
Litecoin, Dogecoin, and all the others. The tax regime that covers German
non-profit associations considers those coins a form of speculation which
would endanger our non-profit status, so regretfully you cannot donate them directly.
Cash out and then use a traditional method to donate.

### Benevity

Some companies offer donation matching though Benevity.

If yours does you can select KDE e.V. at [https://causes.benevity.org/causes/276-2767060345](https://causes.benevity.org/causes/276-2767060345).


### GitHub Sponsors

You can donate through the [GitHub Sponsors](https://github.com/sponsors/KDE/) portal. There are various levels of sponsorship possible.


<hr style="margin-top: 2em; margin-bottom: 0.5em; border-style: inset; border-width: 1px;">

Please contact the <a href="ma&#x69;&#x6c;&#116;&#x6f;:k&#100;&#101;-ev&#00045;&#116;r&#101;asu&#x72;er&#064;&#x6b;d&#x65;&#46;o&#114;&#103;">KDE e.V. treasurer</a> for assistance or for any questions that you might have. Your contribution is very much appreciated! 
