---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: ကေဒီအီးသည် ကေဒီအီးအပ္ပလီကေးရှင်းများ ၁၈.၀၈.၂ တင်ပို့ခဲ့သည်
layout: application
title: ကေဒီအီးသည် ကေဒီအီးအပ္ပလီကေးရှင်းများ ၁၈.၀၈.၂ တင်ပို့ခဲ့သည်
version: 18.08.2
---
October 11, 2018. Today KDE released the second stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than a dozen recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, KCalc, Umbrello, among others.

တိုးတက်မှုများ -

- Dragging a file in Dolphin can no longer accidentally trigger inline renaming
- KCalc again allows both 'dot' and 'comma' keys when entering decimals
- A visual glitch in the Paris card deck for KDE's card games has been fixed
