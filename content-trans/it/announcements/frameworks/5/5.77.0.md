---
aliases:
- ../../kde-frameworks-5.77.0
date: 2020-12-12
layout: framework
libCount: 83
qtversion: 5.13
---
### Attica

* Fix crash in provider loading by checking reply pointer before deref (bug 427974)

### Baloo

* [DocumentUrlDB] Delete child list entry from DB if empty
* Add Presentation document type for Office OpenXML slideshow and template
* [MetaDataMover] Fix lookup of parent document id
* [DocumentUrlDB] Add method for trivial renames and moves
* [MetaDataMover] Make renames a DB only operation
* [Document] Add parent document ID and populate it
* Replace direct syslog call with categorized logging message

### Icone Brezza

* Add text-field variants: -frameless (-> text-field), -framed
* Add symbolic name symlink for input-* icons
* Add icon for True Type XML font
* Add add-subtitle
* Change MathML icon to use a formula and use specific mime type
* Add icon for QEMU disk image and SquashFS image
* Add edit-move action icon
* Add icon for core dumps
* Add a bunch of subtitle mime types
* Remove useless blur from kontrast icon

### Moduli CMake aggiuntivi

* Fix category extraction from desktop files
* Define install dir variable for file templates
* Add fastlane metadata generation for Android builds
* (Qt)WaylandScanner: Properly mark files as SKIP_AUTOMOC

### KActivitiesStats

* ResultModel: expose resource MimeType
* Add event filtering for files and directories (bug 428085)

### KCalendarCore

* Corretto il responsabile, che si suppone sia Allen, non io :)
* Add support for CONFERENCE property
* Add alarmsTo convenience method to Calendar
* Check that by day recurrences do not precede dtStart

### KCMUtils

* Remove hack that broke multi level kcms in icon mode

### KConfig

* Fix KConfigGroup::copyTo with KConfigBase::Notify (bug 428771)

### KCoreAddons

* Avoid to crash when factory is empty (when we return an error)
* KFormat: Add more relative date time cases
* Enable KPluginFactory to optionally pass KPluginMetaData to plugins

### KDAV

* Remove it as it creates too many errors

### KDeclarative

* Sync margins from AbstractKCM into SimpleKCM
* Remove obsolete license text
* Relicense file to LGPL-2.0-or-later
* Relicense file to LGPL-2.0-or-later
* Relicense file to LGPL-2.0-or-later
* Relicense file to LGPL-2.0-or-later
* Rewrite KeySequenceItem (and helper) to use KeySequenceRecorder (bug 427730)

### KDESU

* Properly parse escaped double quotes
* Add OpenBSD's doas(1) support

### KFileMetaData

* Fix some leaks in OpenDocument and Office OpenXML extractors
* Add several subtypes for OpenDocument and OpenXML document

### KGlobalAccel

* Load statically linked kglobalacceld interface plugins

### KDE GUI Addons

* Make shortcut inhibition work from the get-go (bug 407395)
* Fix potential crash in wayland inhibitor teardown (bug 429267)
* CMake: Find Qt5::GuiPrivate when Wayland support is enabled
* Add KeySequenceRecorder as base for KKeySequenceWidget and KeySequenceItem (bug 407395)

### KHolidays

* Fix rounding of sun position events less than 30s before the next hour
* Avoid parsing each holiday file twice in defaultRegionCode()
* Compute the astro seasons only once per occurrence
* Fix holiday region lookup for ISO 3166-2 codes
* Make HolidayRegion copyable/movable
* Add support for calculating civil twilight times

### KIdleTime

* Load statically linked system poller plugins

### KImageFormats

* No longer decrease color depth to 8 for 16 bit uncompressed PSD files

### KIO

* NewFile Dialog: allow to accept directory creation before stat has run (bug 429838)
* Don't leak DeleteJob thread
* KUrlNavBtn: make opening subdirs from dropdown menu with the keyboard work (bug 428226)
* DropMenu: Use translated shortcuts
* [ExecutableFileOpenDialog] focus Cancel button
* Places view: highlight place only when it is displayed (bug 156678)
* Add property to display plugin actions in "Actions" submenu
* Remove newly introduced method
* KIO::iconNameForUrl: resolve icon for remote files based on name (bug 429530)
* [kfilewidget] Use new standard shortcut for "Create Folder"
* Refactor context menu loading and make it more scalable
* RenameDialog: allow to overwrite when files are older (bug 236884)
* DropJob: use new edit-move icon for 'Move Here'
* Fix moc_predefs.h gen when ccache is enabled through -DCMAKE_CXX_COMPILER=ccache CMAKE_CXX_COMPILER_ARG1=g++
* We require Qt 5.13 now, so remove ifdef
* Port KComboBox to QComboBox
* Doc fix requested by Méven Car @meven
* Refactor some loops using modern C++
* Cleanup dead code
* Remove redundant check if key exists
* Simplify code for RequiredNumberOfUrls
* Remove some blank lines
* Simplify code and make it more consistent
* ioslaves: fix remote:/ root permissions
* KFileItem: isWritable use KProtocolManager for remote files
* Add overload to prepend actions to the "Actions" menu
* Use modern code style
* Do not add unnecessary separators (bug 427830)
* Use a braced initializer_list instead of << operator
* MkPathJob: rewrite conditionally compiled code to improve readablity

### Kirigami

* Make GlobalDrawer header set the position of ToolBars/TabBars/DialogButtonBoxes
* inViewport attached property
* fix header slide on touchscreen
* Refactor AbstractapplicationHeader with a new "ScrollIntention" concept
* on desktop always fill anchors to parent
* [controls/BasicListItem]: Don't anchor to nonexistent item
* Don't display avatar text on small size
* Remove # and @ from the extraction process of avatar initial
* Initialise property in sizeGroup
* use mouse interaction on isMobile for easier testing
* Hotfix leading/trailing using trailing values for leading separator margin
* [controls/BasicListItem]: Add leading/trailing properties
* fix sheet positioning on contents resize
* Apply old behavior in wide mode and don't add topMargin in FormLayout
* Fix form layout on small screen
* You can't use an AbstractListItem in a SwipeListItem
* Fix "Unable to assign [undefined] to int" in OverlaySheet
* [overlaysheet]: Don't do a janky transition when content height changes
* [overlaysheet]: Animate height changes
* Fix overlay sheet positioning
* always set index when clicking on a page
* fix FAB s dragging in RTL mode
* Refine list separator appearance (bug 428739)
* fix drawer handles in RTL mode
* Fix rendering borders the proper size with software fallback (bug 427556)
* Don't place software fallback item outside of shadowed rectangle item bounds
* Use fwidth() for smoothing in low power mode (bug 427553)
* Also render a background color in low power mode
* Enable transparent rendering for Shadowed(Border)Texture in lowpower
* Do not cancel alpha components for shadowed rectangle in low power mode
* Don't use a lower smoothing value when rendering ShadowedBorderTexture's texture
* Remove "cut out" steps from shadowed rectangle and related shaders
* Use icon.name instead of iconName in the doc
* [Avatar] Make icon use an icon size close to the text size
* [Avatar] Make the initials use more space and improve vertical alignment
* [Avatar] Expose sourceSize and smooth properties for anyone who wants to animate the size
* [Avatar] Set the source size to prevent images from being blurry
* [Avatar] Set the foreground color once
* [Avatar] Change background gradient
* [Avatar] Change border width to 1px to match other bother widths
* [Avatar] Make padding, verticalPadding and horizontalPadding always work
* [avatar]: Add gradient to colours

### KItemModels

* KRearrangeColumnsProxyModel: only column 0 has children

### KMediaPlayer

* Install player & engine servicetype def. files by file name matching type

### KNewStuff

* Fix uninstalling when the entry isn't cached
* When we call to check for updates, we expect updates (bug 418082)
* Reuse QWidgets dialog (bug 429302)
* Wrap compatibility block in KNEWSTUFFCORE_BUILD_DEPRECATED_SINCE
* Do not write cache for intermediate states
* Use enum for uncompression instead of string values
* Fix entry disappearing too early from updatable page (bug 427801)
* Add DetailsLoadedEvent enum to new signal
* Rework adoption API (bug 417983)
* Fix a couple of stragglers with old provider url
* Remove entry from cache before inserting new entry (bug 424919)

### KNotification

* Don't pass transient hint (bug 422042)
* Fix case-sensitive error of AppKit header on macOS
* Do not invoke invalid notification actions
* Fix memory handling for notifybysnore

### KPackage Framework

* Drop X-KDE-PluginInfo-Depends

### KParts

* Deprecate embed() method, for lack of usage
* Make KParts use KPluginMetaData instead of KAboutData

### KQuickCharts

* Rework line smoothing algorithm
* Move applying interpolation to the polish step
* Properly center point delegates on line chart and size them with line width
* Add a "smooth" checkbox to line chart example
* Ensure line chart point delegates are properly cleaned up
* Also show name in tooltip on Line chart page example
* Document LineChartAttached and fix a typo in LineChart docs
* Add name and shortName properties to LineChartAttached
* Document pointDelegate property more thoroughly
* Remove previousValues member and fix stacked line charts
* Use pointDelegate in Line chart example to display values on hover
* Add support for "point delegate" to Line charts
* LineChart: Move point calculation from updatePaintNode to polish

### KRunner

* Deprecate KDE4 package remnants
* Make use of new KPluginMetaData plugin constructor support of KPluginLoader

### KService

* [kapplicationtrader] Fix API docs
* KSycoca: recreate DB when version < expected version
* KSycoca: Keep track of resources Files of KMimeAssociation

### KTextEditor

* Port KComboBox to QComboBox
* use KSyntaxHighlighting themeForPalette
* fix i18n call, missing argument (bug 429096)
* Improve the automatic theme selection

### KWidgetsAddons

* Don't emit the passwordChanged signal twice
* Add KMessageDialog, an async-centric variant of KMessageBox
* Restore the old default popup mode of KActionMenu
* Port KActionMenu to QToolButton::ToolButtonPopupMode

### KWindowSystem

* Load statically linked integration plugins
* Port away from pid() to processId()

### KXMLGUI

* Introduce HideLibraries and deprecate HideKdeVersion
* Rewrite KKeySequenceWidget to use KeySequenceRecorder (bug 407395)

### Plasma Framework

* [Representation] Only remove top/bottom padding when header/footer is visible
* [PlasmoidHeading] Use technique from Representation for inset/margins
* Add a Representation component
* [Desktop theme] Rename hint-inset-side-margin to hint-side-inset
* [FrameSvg] Rename insetMargin to inset
* [PC3] Use PC3 Scrollbar in ScrollView
* [Breeze] Report inset hint
* [FrameSvg*] Rename shadowMargins to inset
* [FrameSvg] Cache shadow margins and honor prefixes
* Finish the animation before changing the length of the progressbar highlight (bug 428955)
* [textfield] Fix clear button overlapping text (bug 429187)
* Show drop menu at correct global position
* Use gzip -n to prevent embedded buildtimes
* Use KPluginMetaData to list containmentActions
* Port packageStructure loading from KPluginTrader
* Use KPluginMetaData to list DataEngines
* [TabBar] Add highlight on keyboard focus
* [FrameSvg*] Expose shadows margins
* Make MarginAreasSeparator value more clear
* [TabButton] Align center icon and text when text is beside the icon
* [SpinBox] Fix logic error in scroll directionality
* fix mobile scrollbar in RTL mode
* [PC3 ToolBar] Don't disable borders
* [PC3 ToolBar] Use correct svg margin properties for padding
* [pc3/scrollview] Remove pixelAligned
* Add margin areas

### Scopo

* [bluetooth] Fix sharing multiple files (bug 429620)
* Read translated plugin action label (bug 429510)

### QQC2StyleBridge

* button: rely on down, not pressed for styling
* Reduce the size of round buttons on mobile
* fix mobile scrollbar in RTL mode
* fix progressbar in RTL mode
* fix RTL display for RangeSlider

### Solid

* Include errno.h for EBUSY/EPERM
* FstabBackend: return DeviceBusy where umount failed on EBUSY (bug 411772)
* Fix detection of recent libplist and libimobiledevice

### Evidenziazione della sintassi

* fix dependencies of the generated files
* indexer: fix some issues and disable 2 checkers (capture group and keyword with delimiter)
* indexer: load all xml files in memory for easy checking
* C++ highlighting: update to Qt 5.15
* relaunch syntax generators when the source file is modified
* systemd unit: update to systemd v247
* ILERPG: simplify and test
* Zsh, Bash, Fish, Tcsh: add truncate and tsort in unixcommand keywords
* Latex: some math environments can be nested (bug 428947)
* Bash: many fixes and improvements
* add --syntax-trace=stackSize
* php.xml: Fix matching endforeach
* Move bestThemeForApplicationPalette from KTextEditor here
* debchangelog: add Trixie
* alert.xml: Add `NOQA` yet another popular alert in source code
* cmake.xml: Upstream decided to postpone `cmake_path` for the next release

### Informazioni di sicurezza

Il codice rilasciato è stato firmato con GPG utilizzando la chiave seguente: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Impronta della chiave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
