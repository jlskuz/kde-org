---
date: 2013-08-14
hidden: true
title: Plasma Workspaces 4.11 continua ad affinare l'esperienza utente
---
{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/empty-desktop.png" caption=`KDE Plasma Workspaces 4.11` width="600px" >}}

Nella versione 4.11 di Plasma Workspaces il gestore dei processi — uno degli oggetti di Plasma più utilizzati — <a href='http://blogs.kde.org/2013/07/29/kde-plasma-desktop-411s-new-task-manager'>è stato riscritto usando QtQuick</a>. Il nuovo gestore dei processi, pur conservando l'aspetto e le funzionalità della sua vecchia controparte, ha un comportamento più consistente e fluido. La conversione ha risolto un certo numero di bug presenti da tempo. L'indicatore della batteria (che prima poteva cambiare la luminosità dello schermo) adesso supporta anche la luminosità della tastiera e può gestire diverse batterie nelle periferiche, come ad esempio quelle dei mouse e delle tastiere senza fili. Mostra la carica della batteria per ognuno di questi dispositivi e avvisa quando il livello di una di esse è basso. Il menu Kickoff adesso mostra per alcuni giorni le applicazioni installate di recente. Infine, ma non meno importante, le notifiche a comparsa adesso dispongono di un pulsante di configurazione da cui è possibile modificare le impostazioni per quel particolare tipo di notifica.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/notifications.png" caption=`Gestione migliorata delle notifiche` width="600px" >}}

KMix, il mixer audio di KDE, ha ricevuto modifiche significative nelle prestazioni e nella stabilità così come <a href='http://kmix5.wordpress.com/2013/07/26/kmix-mission-statement-2013/'>il supporto completo per il controllo dei lettori multimediali</a> basato sullo standard MPRIS2.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/battery-applet.png" caption=`Il programma della batteria ridisegnato in azione` width="600px" >}}

## Gestore delle finestre e compositore KWin

Il nostro gestore delle finestre, KWin, ha ricevuto ancora una volta degli aggiornamenti significativi, allontanandosi da tecnologie obsolete e incorporando il protocollo di comunicazione 'XCB'. Questo si riflette in una gestione delle finestre più fluida e veloce. È stato anche introdotto il supporto per OpenGL 3.1 e OpenGL ES 3.0. Questo rilascio incorpora inoltre il supporto iniziale sperimentale per Wayland, il successore di X11. Questo permette di usare KWin con X11 al di sopra di un sistema Wayland. Per maggiori informazioni su come usare questa modalità sperimentale consultare <a href='http://blog.martin-graesslin.com/blog/2013/06/starting-a-full-kde-plasma-session-in-wayland/'>questo articolo</a>. L'interfaccia degli script di KWin ha visto notevoli miglioramenti, ed ora fornisce il supporto per un'interfaccia di configurazione, nuove animazioni ed effetti grafici e molti piccoli miglioramenti. Questo rilascio porta una maggiore capacità di gestire i monitor multipli (incluso un'opzione di sfumatura del bordo per i «bordi attivi»), miglioramento al &quot;tiling&quot; rapido (con aree di affiancamento configurabili) ed il solito quantitativo di correzioni di bug e ottimizzazioni. Vedi <a href='http://blog.martin-graesslin.com/blog/2013/06/what-we-did-in-kwin-4-11/'>qui</a> e <a href='http://blog.martin-graesslin.com/blog/2013/06/new-kwin-scripting-feature-in-4-11/'>qui</a> per maggiori dettagli.

## Gestione del monitor e scorciatoie del web

La configurazione del monitor nelle Impostazioni di sistema è stata <a href='http://www.afiestas.org/kscreen-1-0-released/'>sostituita dal nuovo strumento KScreen</a>. KScreen introduce un supporto più intelligente ai monitor in Plasma Workspaces, configurando automaticamente i nuovi schermi e ricordando le impostazioni per i monitor configurati manualmente. Presenta un'interfaccia intuitiva e visuale e gestisce la risistemazione dei monitor tramite semplice trascinamento.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kscreen.png" caption=`La nuova gestione del monitor con KScreen` width="600px" >}}

Le scorciatoie web, il modo più veloce di trovare rapidamente quello che cerchi sul web, sono stati ripulite e migliorate. Molte sono state aggiornate per utilizzare delle connessioni sicure cifrate (TLS/SSL), nuove scorciatoie sono state aggiunte e alcune obsolete rimosse. Anche il processo di aggiunta delle scorciatoie personalizzate è stato migliorato. Puoi trovare maggiori dettagli <a href='https://plus.google.com/108470973614497915471/posts/9DUX8C9HXwD'>qui</a>.

Questo rilascio segna la fine di Plasma Workspaces 1, parte della serie di rilasci di KDE SC 4. Per facilitare la transizione alla nuova generazione questo rilascio sarà supportato per almeno due anni. L'attenzione sullo sviluppo delle funzionalità si sposta ora su Plasma Workspaces 2; i miglioramenti alle prestazioni e la correzione di bug si concentreranno sulla serie 4.11.

#### Installare Plasma

Il software KDE, incluse tutte le sue librerie e le sue applicazioni, è disponibile liberamente e gratuitamente sotto licenze Open Source. Il software KDE funziona su varie configurazioni hardware e architetture CPU come ARM e x86, su vari sistemi operativi e funziona con ogni tipo di gestore di finestre o ambiente desktop. Oltre a Linux e altri sistemi operativi basati su UNIX puoi trovare versioni per Microsoft Windows di buona parte delle applicazioni KDE sul sito del <a href='http://windows.kde.org'>software KDE per Windows</a> e versioni Apple Mac OS X sul sito del <a href='http://mac.kde.org/'>software KDE per Mac</a>. Versioni sperimentali di applicazioni KDE per varie piattaforme mobili come MeeGo, MS Windows Mobile e Symbian possono essere trovate sul web ma non sono al momento supportate. <a href='http://plasma-active.org'>Plasma Active</a> fornisce l'esperienza utente per una vasta gamma di dispositivi, come tablet e altro hardware mobile.

Il software KDE si può scaricare come sorgente e in vari formati binari da <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> e può essere anche ottenuto su <a href='/download'>CD-ROM</a> o con qualsiasi <a href='/distributions'>principale distribuzione GNU/Linux o sistema UNIX</a> attualmente disponibile.

##### Pacchetti

Alcuni fornitori di sistemi Linux/UNIX hanno gentilmente messo a disposizione pacchetti binari di 4.11.0 per alcune versioni delle rispettive distribuzioni, e in altri casi dei volontari della comunità hanno provveduto. <br />

##### Posizione dei pacchetti

Per l'elenco aggiornato dei pacchetti binari disponibili di cui la squadra di rilascio di KDE è stata informata, visita il <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>wiki Community</a>.

Il codice sorgente completo per 4.11.0 può essere <a href='/info/4/4.11.0'>scaricato liberamente</a>. Le istruzioni su come compilare e installare il software KDE 4.11.0 sono disponibili dalla <a href='/info/4/4.11.0#binary'>pagina di informazioni di 4.11.0</a>.

#### Requisiti di sistema

Per poter ottenere il massimo da questi rilasci, raccomandiamo l'uso di una versione recente di Qt, come 4.8.4. Questo è necessario per poter assicurare un'esperienza stabile e più efficiente, dato che alcuni miglioramenti del software KDE sono stati in realtà effettuati sul framework Qt sottostante.<br /> Per usare in pieno le funzionalità del software KDE raccomandiamo inoltre di usare i driver grafici più recenti disponibili per il tuo sistema, in quanto l'esperienza utente può migliorare notevolmente, sia nelle funzionalità opzionali, sia nelle prestazioni sia nella stabilità globale.

## Oggi sono stati annunciati anche:

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Application 4.11 introduce grandi passi avanti nella gestione delle informazioni personali (PIM) e miglioramenti globali</a>

Questo rilascio vede imponenti miglioramenti in KDE PIM che forniscono migliori prestazioni e molte nuove funzioni. Kate migliora la produttività degli sviluppatori Python e Javascript con nuove estensioni, Dolphin è diventato più veloce e le applicazioni didattiche hanno varie nuove funzionalità.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 porta prestazioni migliori</a>

Questo rilascio di KDE Platform 4.11 continua a concentrarsi sulla stabilità. Nuove funzionalità sono in corso di implementazione in KDE Frameworks 5.0, il nostro futuro rilascio, ma in questa versione stabile siamo riusciti a far entrare delle ottimizzazioni per la nostra infrastruttura Nepomuk.
