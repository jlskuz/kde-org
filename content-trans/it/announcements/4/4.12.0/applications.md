---
date: '2013-12-18'
hidden: true
title: KDE Application 4.12 introduce grandi passi avanti nella gestione delle informazioni
  personali (PIM) e miglioramenti globali
---
La comunità KDE è fiera di annunciare i più recenti aggiornamenti delle applicazioni (Applications) che portano nuove funzionalità e correzioni. Questo rilascio vede imponenti miglioramenti in KDE PIM che forniscono migliori prestazioni e molte nuove funzioni. In Kate è stata rifinita l'integrazione delle estensioni in Python e aggiunto il supporto iniziale per le macro di Vim, e i giochi e le applicazioni didattiche includono molte nuove funzionalità.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/kate.png" width="600px">}}

Kate, l'editor di testi grafico più avanzato per Linux, ha ancora una volta ricevuto degli aggiornamenti nel completamento del codice, questa volta con l'introduzione <a href='http://scummos.blogspot.com/2013/10/advanced-code-completion-filtering-in.html'>del codice per la ricerca avanzata di corrispondenze, la gestione di abbreviazioni e la ricerca parziale di corrispondenze nelle classi</a>. Ad esempio, il nuovo codice può trovare la corrispondenza tra «QualIdent», inserito dall'utente, e «QualifiedIdentifier». Kate ha ricevuto inoltre un <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-18th-august-2013'>supporto iniziale per le macro di Vim</a>. Ancora meglio, questi miglioramenti arrivano anche in KDevelop e nelle altre applicazioni che usano la tecnologia di Kate.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/okular.png" width="600px">}}

Il visualizzatore dei documenti Okular <a href='http://tsdgeos.blogspot.com/2013/10/changes-in-okular-printing-for-412.html'>adesso considera i margini hardware della stampante</a>, ha il supporto per audio e video negli ePub, una migliore ricerca e può gestire più trasformazioni incluse quelle dei metadati Exif delle immagini. Nello strumento per i diagrammi UML Umbrello, le associazioni possono ora essere <a href='http://dot.kde.org/2013/09/20/kde-commit-digest-1st-september-2013'>tracciate con un aspetto diverso</a> e Umbrello <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-25th-august-2013'>aggiunge un feedback visuale se un certo oggetto è documentato</a>.

Il custode della privacy KGpg mostra più informazioni agli utenti e KWalletManager, lo strumento per salvare le tue password, adesso può <a href='http://www.rusu.info/wp/?p=248'>salvarle in formato GPG</a>. Konsole introduce il supporto per una nuova funzionalità: Ctrl-clic per eseguire direttamente l'URL presente nell'output della console. Può anche <a href='http://martinsandsmark.wordpress.com/2013/11/02/mangonel-1-1-and-more/'>elencare i processi quando avvisa riguardo la chiusura del programma</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

KWebKit aggiunge l'abilità di <a href='http://dot.kde.org/2013/08/09/kde-commit-digest-7th-july-2013'>riscalare automaticamente i contenuti in modo da corrispondere alla risoluzione del desktop</a>. Il gestore di file Dolphin introduce una serie di miglioramenti alla prestazioni nell'ordinare e mostrare i file, riducendo l'uso della memoria e accelerando le operazioni. KRDC dispone della riconnessione automatica in VNC e KDialog fornisce ora l'accesso al riquadri informativi «detailedsorry» e «detailederror» per script in console più informativi. Kopete ha aggiornato la propria estensione OTR e il protocollo Jabber supporta «XEP-0264: File Transfer Thumbnails». A parte queste funzionalità l'attenzione si è concentrata sulla ripulitura del codice e la correzione degli avvisi in fase di compilazione.

### Giochi e software educativo

I giochi di KDE sono stati oggetto di lavoro in molte aree. KReversi è <a href='http://tsdgeos.blogspot.ch/2013/10/kreversi-master-is-now-qt-quick-based.html'>adesso basato su QML e Qt Quick</a>, ottenendo così un aspetto più gradevole e un'esperienza di gioco più fluida. KNetWalk è <a href='http://tsdgeos.blogspot.ch/2013/08/knetwalk-portedx-to-qtquick.html'>stato pure migrato</a> con gli stessi vantaggi e l'abilità di impostare una griglia con larghezza e altezza personalizzate. Konquest ora dispone di un'intelligenza artificiale come giocatore chiamata «Becai».

Tra le applicazioni didattiche ci sono stati alcuni notevoli cambiamenti. KTouch <a href='http://blog.sebasgo.net/blog/2013/11/12/what-is-new-for-ktouch-in-kde-sc-4-dot-12/'>introduce il supporto per lezioni personalizzate e vari nuovi corsi</a>; KStars dispone di un nuovo, più accurato <a href='http://knro.blogspot.ch/2013/10/demo-of-ekos-alignment-module.html'>modulo di allineamento per i telescopi</a>, vedi <a href='http://www.youtube.com/watch?v=7Dcn5aFI-vA'>questo video su youtube</a> con le nuove funzionalità. Cantor, che fornisce una semplice ma potente interfaccia utente per vari motori matematici, adesso dispone di motori <a href='http://blog.filipesaraiva.info/?p=1171'>per Python2 e Scilab</a>. Leggi altre informazioni sul potente motore Scilab <a href='http://blog.filipesaraiva.info/?p=1159'>qui</a>. Marble aggiunge l'integrazione con ownCloud (configurabile tramite le Impostazioni) e il supporto per il tracciamento di disegni sovrapposti. KAlgebra rende possibile l'esportazione di disegni 3D in PDF, fornendo così un ottimo modo per condividere il tuo lavoro. Infine, ma non meno importante, molti bug sono stati corretti in varie applicazioni di KDE Education.

### Posta, calendario ed informazioni personali

KDE PIM, l'insieme di applicazioni di KDE per la gestione della posta, del calendario e di altre informazioni personali, è stato oggetto di molto lavoro.

Partendo con il client di posta elettronica KMail, adesso è disponibile il <a href='http://dot.kde.org/2013/10/11/kde-commit-digest-29th-september-2013'>supporto AdBlock</a> (quando è attivo HTML) e un supporto migliorato per il riconoscimento di messaggi truffaldini (scam) estendendo gli URL abbreviati. Un nuovo agente Akonadi chiamato FolderArchiveAgent (agente di archiviazione per cartelle) permette agli utenti di archiviare i messaggi letti in cartelle specifiche e l'interfaccia della funzionalità di invio posticipato è stata ripulita. KMail beneficia inoltre del supporto migliorato per i filtri Sieve. Sieve permette di filtrare i messaggi di posta lato server e adesso puoi <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-script-parsing-22/'>creare e modificare i filtri sui server</a> e <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-12/'>convertire i filtri KMail esistenti in filtri lato server</a>. <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-mboximporter/'>È stato pure migliorato</a> il supporto mbox di KMail.

In altre applicazioni varie modifiche rendono il lavoro più semplice e piacevole. È stato introdotto un nuovo strumento, <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>l'editor dei temi dei contatti</a>, che permette la creazione di temi Grantlee di KAddressBook per mostrare i contatti. La rubrica può mostrare anteprime prima di stampare i dati. KNotes è stato oggetto di un <a href='http://www.aegiap.eu/kdeblog/2013/11/what-news-in-kdepim-4-12-knotes/'>notevole lavoro di risoluzione di bug</a>. Lo strumento per i blog Blogilo può gestire le traduzioni e ci sono una grande quantità di correzioni e miglioramenti in tutte le applicazioni di KDE PIM.

A beneficio di tutte le applicazioni, il sottostante sistema di cache dei dati di KDE PIM è <a href='http://ltinkl.blogspot.ch/2013/11/this-month-october-in-red-hat-kde.html'>stato oggetto di notevole lavoro sulle prestazioni, la stabilità e la scalabilità</a>, correggendo <a href='http://www.progdan.cz/2013/10/akonadi-1-10-3-with-postgresql-fix/'>il supporto per PostgreSQL con le ultime Qt 4.8.5</a>. Ed è disponibile un nuovo strumento a riga di comando, calendarjanitor, che controlla tutti i dati dei calendari per occorrenze sbagliate e aggiunge una finestra di debug per la ricerca. Ringraziamenti davvero speciali vanno a Laurent Montel per il lavoro che porta avanti sulle funzionalità di KDE PIM!

#### Installare le applicazioni KDE

Il software KDE, incluse tutte le sue librerie e le sue applicazioni, è disponibile liberamente e gratuitamente sotto licenze Open Source. Il software KDE funziona su varie configurazioni hardware e architetture CPU come ARM e x86, su vari sistemi operativi e funziona con ogni tipo di gestore di finestre o ambiente desktop. Oltre a Linux e altri sistemi operativi basati su UNIX puoi trovare versioni per Microsoft Windows di buona parte delle applicazioni KDE sul sito del <a href='http://windows.kde.org'>software KDE per Windows</a> e versioni Apple Mac OS X sul sito del <a href='http://mac.kde.org/'>software KDE per Mac</a>. Versioni sperimentali di applicazioni KDE per varie piattaforme mobili come MeeGo, MS Windows Mobile e Symbian possono essere trovate sul web ma non sono al momento supportate. <a href='http://plasma-active.org'>Plasma Active</a> fornisce l'esperienza utente per una vasta gamma di dispositivi, come tablet e altro hardware mobile.

Il software KDE si può scaricare come sorgente e in vari formati binari da <a href='http://download.kde.org/stable/4.12.0'>download.kde.org</a> e può essere anche ottenuto su <a href='/download'>CD-ROM</a> o con qualsiasi <a href='/distributions'>principale distribuzione GNU/Linux o sistema UNIX</a> attualmente disponibile.

##### Pacchetti

Alcuni fornitori di sistemi Linux/UNIX hanno gentilmente messo a disposizione pacchetti binari di 4.12.0 per alcune versioni delle rispettive distribuzioni, e in altri casi dei volontari della comunità hanno provveduto.

##### Posizione dei pacchetti

Per l'elenco aggiornato dei pacchetti binari disponibili di cui la squadra di rilascio di KDE è stata informata, visita il <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.12.0'>wiki Community</a>.

Il codice sorgente completo per 4.12.0 può essere <a href='/info/4/4.12.0'>scaricato liberamente</a>. Le istruzioni su come compilare e installare il software KDE 4.12.0 sono disponibili dalla <a href='/info/4/4.12.0#binary'>pagina di informazioni di 4.12.0</a>.

#### Requisiti di sistema

Per poter ottenere il massimo da questi rilasci, raccomandiamo l'uso di una versione recente di Qt, come 4.8.4. Questo è necessario per poter assicurare un'esperienza stabile e più efficiente, dato che alcuni miglioramenti del software KDE sono stati in realtà effettuati sul framework Qt sottostante.

Per poter usare in pieno le funzionalità del software KDE raccomandiamo inoltre di usare i più recenti driver grafici disponibili per il tuo sistema, perché questo può migliorare notevolmente l'esperienza utente sia nelle funzionalità opzionali sia nelle prestazioni sia nella stabilità globale.
