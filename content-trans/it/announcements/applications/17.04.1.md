---
aliases:
- ../announce-applications-17.04.1
changelog: true
date: 2017-05-11
description: KDE rilascia KDE Applications 17.04.1
layout: application
title: KDE rilascia KDE Applications 17.04.1
version: 17.04.1
---
11 maggio 2017. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../17.04.0'>KDE Applications 17.04</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 20 errori corretti includono, tra gli altri, miglioramenti a kdepim, dolphin, gwenview, kate e kdenlive.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.32.
