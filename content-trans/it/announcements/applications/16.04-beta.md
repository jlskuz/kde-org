---
aliases:
- ../announce-applications-16.04-beta
custom_spread_install: true
date: 2016-03-24
description: KDE rilascia Applications 16.04 Beta.
layout: application
release: applications-16.03.80
title: KDE rilascia la beta di KDE Applications 16.04
---
24 marzo 2016. Oggi KDE ha rilasciato la beta della nuova versione di KDE Applications. Con il &quot;congelamento&quot; di dipendenze e funzionalità, l'attenzione degli sviluppatori KDE è adesso concentrata sulla correzione dei bug e sull'ulteriore rifinitura del sistema.

Controlla le <a href='https://community.kde.org/Applications/16.04_Release_Notes'>note di rilascio della comunità</a> per informazioni sui nuovi archivi tar, sui programmi che ora sono basati su KF5 e sui problemi noti. Un annuncio più completo verrà reso disponibile in concomitanza con la versione finale

Dato che molte applicazioni sono ora basate su KDE Frameworks 5, i rilasci di KDE Applications 16.04 hanno bisogno di una verifica accurata per poter mantenere e migliorare la qualità e l'esperienza utente. Gli utenti &quot;reali&quot; sono fondamentali per mantenere la qualità in KDE, perché gli sviluppatori non possono testare completamente ogni possibile configurazione. Contiamo su di voi per aiutarci a trovare i bug al più presto possibile perché possano essere eliminati prima della versione finale. Valutate la possibilità di contribuire al gruppo di sviluppo installando la versione &quot;beta&quot; <a href='https://bugs.kde.org/'>e segnalando ogni problema</a>.

#### Installazione dei pacchetti binari di KDE Applications 16.04 Beta

<em>Pacchetti</em>. Alcuni fornitori di sistemi Linux/UNIX hanno gentilmente messo a disposizione pacchetti binari di KDE Applications 16.04 Beta (internamente 16.03.80) per alcune versioni delle rispettive distribuzioni, e in altri casi hanno provveduto i volontari della comunità. Altri pacchetti binari, così come aggiornamenti ai pacchetti ora disponibili, potrebbero essere disponibili nelle prossime settimane.

<em>Posizioni dei pacchetti</em>. Per l'elenco aggiornato dei pacchetti binari disponibili di cui il progetto KDE è stato informato, visita <a href='http://community.kde.org/KDE_SC/Binary_Packages'>il wiki Community</a>.

#### Compilazione di KDE Applications 16.04 Beta

Il codice sorgente completo per KDE Applications 16.04 Beta può essere <a href='http://download.kde.org/unstable/applications/16.03.80/src/'>scaricato liberamente</a>. Le istruzioni per la compilazione e l'installazione sono disponibili dalla <a href='/info/applications/applications-16.03.80'>pagina di informazioni di KDE Applications 16.04 Beta</a>.
