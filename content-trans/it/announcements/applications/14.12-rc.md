---
aliases:
- ../announce-applications-14.12-rc
custom_spread_install: true
date: '2014-11-27'
description: KDE rilascia Applications 14.12 Release Candidate.
layout: application
title: KDE rilascia la Release Candidate della versione 14.12 delle applicazioni (Applications)
---
27 novembre 2014. Oggi KDE ha rilasciato la release candidate della nuova versione delle applicazioni KDE (Applications). Con il &quot;congelamento&quot; di dipendenze e funzionalità, l'attenzione degli sviluppatori KDE è adesso concentrata sulla correzione dei bug e sull'ulteriore rifinitura del sistema.

Dato che molte applicazioni sono ora basate su KDE Frameworks 5, i rilasci di KDE Applications 1412 hanno bisogno di una verifica accurata per poter mantenere e migliorare la qualità e l'esperienza utente Gli utenti &quot;reali&quot; sono fondamentali per mantenere la qualità di KDE, perché gli sviluppatori non possono testare completamente ogni possibile configurazione. Contiamo su di voi per aiutarci a trovare i bug al più presto possibile perché possano essere eliminati prima della versione finale. Valutate la possibilità di contribuire al gruppo di sviluppo installando la versione &quot;release candidate&quot; <a href='https://bugs.kde.org/'>e segnalando ogni problema</a>.

#### Installazione dei pacchetti binari di KDE Applications 14.12 Release Candidate

<em>Pacchetti</em>. Alcuni fornitori di sistemi Linux/UNIX hanno gentilmente messo a disposizione pacchetti binari di KDE Applications 14.12 Release Candidate (internamente 14.11.97) per alcune versioni delle rispettive distribuzioni, e in altri casi hanno provveduto i volontari della comunità. Altri pacchetti binari, così come aggiornamenti ai pacchetti ora disponibili, potrebbero essere disponibili nelle prossime settimane.

<em>Posizioni dei pacchetti</em>. Per l'elenco aggiornato dei pacchetti binari disponibili di cui il progetto KDE è stato informato, visita <a href='http://community.kde.org/KDE_SC/Binary_Packages'>il wiki Community</a>.

#### Compilazione di KDE Applications 14.12 Release Candidate

Il codice sorgente completo per KDE Applications 14.12 Release Candidate può essere <a href='http://download.kde.org/unstable/applications/14.11.97/src/'>scaricato liberamente</a>. Le istruzioni per la compilazione e l'installazione sono disponibili dalla <a href='/info/applications/applications-14.11.97'>pagina di informazioni di KDE Applications 14.12 Release Candidate</a>.
