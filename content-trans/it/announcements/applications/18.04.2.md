---
aliases:
- ../announce-applications-18.04.2
changelog: true
date: 2018-06-07
description: KDE rilascia KDE Applications 18.04.2
layout: application
title: KDE rilascia KDE Applications 18.04.2
version: 18.04.2
---
7 giugno 2018. Oggi KDE ha rilasciato il secondo aggiornamento di stabilizzazione per <a href='../18.04.0'>KDE Applications 18.04</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Circa venticinque errori corretti includono, tra gli altri, miglioramenti a Kontact, Cantor, Dolphin, Gwenview, KGpg, Kig, Konsole, Lokalize, Okular.

I miglioramenti includono:

- Le operazioni sulle immagini in Gwenview ora possono essere rifatte dopo averle annullate
- KGpg non crea errori durante la decrittazione dei messaggi senza un'intestazione di versione
- È stata corretta l'esportazione dei fogli di calcolo Cantor in LaTeX per le matrici Maxima
