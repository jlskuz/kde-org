---
aliases:
- ../announce-applications-16.04.1
changelog: true
date: 2016-05-10
description: KDE vydává Aplikace KDE 16.04.1
layout: application
title: KDE vydává Aplikace KDE 16.04.1
version: 16.04.1
---
May 10, 2016. Today KDE released the first stability update for <a href='../16.04.0'>KDE Applications 16.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 25 recorded bugfixes include improvements to kdepim, ark, kate, dolphin. kdenlive, lokalize, spectacle, among others.

This release also includes Long Term Support version of KDE Development Platform 4.14.20.
