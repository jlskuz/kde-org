---
aliases:
- ../announce-applications-15.04-rc
date: '2015-03-26'
description: KDE Uygulamalar 15.04  Sürüm Adayını Gönderdi.
layout: application
title: KDE, KDE Uygulamalar 15.04 Sürüm Adayını Gönderdi
---
March 26, 2015. Today KDE released the release candidate of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

With the various applications being based on KDE Frameworks 5, the KDE Applications 15.04 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the release <a href='https://bugs.kde.org/'>and reporting any bugs</a>.
