---
aliases:
- ../announce-applications-15.12.3
changelog: true
date: 2016-03-16
description: KDE, KDE Uygulamaları 15.12.3'yi Gönderdi
layout: application
title: KDE, KDE Uygulamaları 15.12.3'yi Gönderdi
version: 15.12.3
---
March 15, 2016. Today KDE released the third stability update for <a href='../15.12.0'>KDE Applications 15.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 15 recorded bugfixes include improvements to kdepim, akonadi, ark, kblocks, kcalc, ktouch and umbrello, among others.

This release also includes Long Term Support version of KDE Development Platform 4.14.18.
