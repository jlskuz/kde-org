---
aliases:
- ../announce-applications-18.04.3
changelog: true
date: 2018-07-12
description: KDE, KDE Uygulamaları 18.04.3'ü Gönderdi
layout: application
title: KDE, KDE Uygulamaları 18.04.3'ü Gönderdi
version: 18.04.3
---
July 12, 2018. Today KDE released the third stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kaydedilen yaklaşık 20 hata düzeltmesi, diğerleri arasında Kontak, Ark, Cantor, Dolphin, Gwenview, KMag için iyileştirmeler içerir.

İyileştirmeler şunları içerir:

- Yeteneklerini duyurmayan IMAP sunucularıyla uyumluluk geri yüklendi
- Ark artık klasörler için uygun girişleri olmayan ZIP arşivlerini çıkarabilir
- Ekrandaki K Notlar notları, taşınırken yine fare imlecini takip eder
