---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: KDE выпускает KDE Applications 18.08.1
layout: application
title: KDE выпускает KDE Applications 18.08.1
version: 18.08.1
---
September 6, 2018. Today KDE released the first stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Зафиксировано более 10 исправлений ошибок в электронном секретаре Kontact, системе компьютерной алгебры Cantor, приложении для просмотра изображений Gwenview, приложении для просмотра документов Okular, UML-редакторе Umbrello и других приложениях.

Некоторые из улучшений:

- The KIO-MTP component no longer crashes when the device is already accessed by a different application
- Sending mails in KMail now uses the password when specified via password prompt
- Okular now remembers the sidebar mode after saving PDF documents
