---
aliases:
- ../../kde-frameworks-5.78.0
date: 2021-01-09
layout: framework
libCount: 83
qtversion: 5.14
---
### Attica

* Respetar el trabajo que se está interrumpiendo inmediatamente (error 429939).

### Baloo

* [ExtractorProcess] Mover señal de DBus del proceso auxiliar al principal.
* [cronología] Consolidar el código para las estadísticas y el listado de la carpeta raíz.
* Hacer que las entradas UDS de nivel superior del esclavo de E/S sean de solo lectura.
* Evitar errores de lanzamiento de aplicaciones si no se había creado previamente un índice de Baloo.
* [BasicIndexingJob] Eliminar la barra sobrante de las carpetas (error 430273).

### Iconos Brisa

* Nuevo icono de acción «compass».
* Se ha añadido al tema el icono «image-missing».
* Se ha añadido el icono para imágenes WIM.

### Módulos CMake adicionales

* Indicarle a MSVC que nuestros archivos de código fuente tienen codificación UTF-8.
* Se ha añadido «Findepoxy.cmake».
* Considerar locales los recursos de imagen «fastlane».
* «Tarballs» reproducibles solo con «tar» de GNU.
* Preservar el subconjunto de texto enriquecido permitido por F-Droid.
* Se ha modificado la versión necesaria de «cmake» para «Android.cmake» (error 424392).
* Detectar automáticamente las dependencias de bibliotecas de complementos en Android.
* Comprobar que el archivo existe antes de eliminar el archivo «fastlane».
* Borrar la carpeta de imágenes y el archivo antes de descargarlos o generarlos.
* Mantener el orden de capturas de pantalla del archivo «appstream».
* Windows: Se ha corregido «QT_PLUGIN_PATH» para las pruebas.
* No fallar cuando no se encuentran categorías.
* Hacer que «KDEPackageAppTemplates» pueda crear un tarball reproducible.

### KActivitiesStats

* Eliminar la funcionalidad «lastQuery» que no funciona, corrige fallos de «krunner» para mí.

### KCalendarCore

* CMakeLists.txt: Se ha incrementado la versión mínima de «libical» a 3.0.

### KCMUtils

* Implementar el indicador de resaltado predeterminado en «KPluginSelector».
* kcmoduleqml: No enlazar la anchura de las columnas a la anchura de la vista (error 428727).

### KCompletion

* [KComboBox] Se ha corregido un fallo al llamar a «setEditable(false)» con un menú de contexto abierto.

### KConfig

* Se ha corregido que las ventanas se maximicen de forma no apropiada durante el lanzamiento (error 426813).
* Se ha corregido el formato de la cadena de ventana maximizada.
* Se ha corregido el tamaño y la posición de las ventanas en Windows (error 429943).

### KConfigWidgets

* KCodecAction: Se han añadido las señales sin sobrecarga «codecTriggered» y «encodingProberTriggered».

### KCoreAddons

* Se ha adaptado «KJobTrackerInterface» a la sintaxis de conexión de Qt5.
* KTextToHtml: Se ha corregido una aserción debida a un desbordamiento de límites en una llamada a «at()».
* Usar jerarquía plana para las rutas de complementos en Android.
* Conversión de archivo de escritorio a JSON: Ignorar la entrada «Actions=».
* Se ha marcado como obsoleto «KProcess::pid()».
* ktexttohtml: Se ha corregido el uso de «KTextToHTMLHelper».

### KCrash

* Usar «std::unique_ptr<char[]>» para evitar fugas de memoria.

### KDeclarative

* Cambiar al «Findepoxy» proporcionado por ECM.
* KCMShell: Se ha añadido el uso de argumentos.
* Solución alternativa para el fallo con la detección de GL y «kwin_wayland».
* [KQuickAddons] Usar «QtQuickSettings::checkBackend()» como último recurso al que recurrir en el motor de software (error 346519).
* [abstractkcm] Se ha corregido la importación de la versión en el código de ejemplo.
* Evitar la definición de «QSG_RENDER_LOOP» si ya se había definido previamente.
* ConfigPropertyMap : Cargar el valor predeterminado de las propiedades en el mapa.

### KDocTools

* Se ha añadido una entidad para el acrónimo «MathML».
* Se ha cambiado el nombre «Batalla naval» a «KNavalBattle» para satisfacer requisitos legales.

### KGlobalAccel

* Evitar el reinicio automático de «kglobalaccel» durante el apagado del sistema (error 429415).

### KHolidays

* Actualización de las festividades japonesas.

### KIconThemes

* Omitir la advertencia de algunos iconos Adwaita para compatibilidad hacia atrás.
* Se ha introducido «QSvgRenderer::setAspectRatioMode()» en Qt 5.15.

### KImageFormats

* Se ha añadido AVIF a la lista de formatos permitidos.
* Se ha añadido un complemento para el formato de imagen AV1 (AVIF).

### KIO

* [KFileItemDelegate] No malgastar espacio para los iconos que no existen en las columnas distintas de la primera.
* KFilePlacesView, KDirOperator: Se han adaptado para usar el método asíncrono «askUserDelete()».
* Se ha cambiado la forma de trabajo de «CopyJob» para encontrar las extensiones «JobUiDelegate».
* Se ha introducido «AskUserActionInterface», una API asíncrona para los diálogos de cambio de nombre y de omisión.
* RenameDialog: Llamar «compareFiles()» solo con archivos.
* kcm/webshortcuts: Se ha corregido el botón «Reinicio».
* KUrlNavigatorMenu: Se ha corregido el manejo del clic central.
* Eliminar el elemento «knetattach» de la vista del esclavo de E/S «remote://» (error 430211).
* Tarea de copia: Se ha adaptado para usar «AskUserActionInterface».
* Tareas: Se ha añadido la señal no sobrecargada «mimeTypeFound» para marcar como obsoleta «mimetype».
* RenameDialog: Se ha añadido una inicialización a «nullptr» que faltaba (error 430374).
* KShortUriFilter: No filtrar las cadenas «../» y relacionadas.
* No realizar una aserción si se ha proporcionado una URL sin esquema a «KIO::rawErrorDetail()» (error 393496).
* KFileItemActions: Se ha corregido una condición, ya que solo queremos excluir los directorios remotos (error 430293).
* KUrlNavigator: Se ha eliminado el uso de «kurisearchfilter».
* KUrlNavigator: Hacer que funcionen las terminaciones automáticas de rutas relativas (error 319700).
* KUrlNavigator: Resolver las rutas de directorio relativas (error 319700).
* Silenciar las advertencias producidas por problemas de configuración de Samba cuando no se usa Samba explícitamente.
* KFileWidget: Permitir que se puedan seleccionar los archivos que comienzan por «:» (error 322837).
* [KFileWidget] Se ha corregido la posición del botón de marcadores en la barra de herramientas.
* KDirOperator: Marcar como obsoleto «mkdir(const QString &, bool)».
* KFilePlacesView: Permitir definir un tamaño de icono estático (error 182089).
* KFileItemActions: Se ha añadido un nuevo método para las acciones «Abrir con» (error 423765).

### Kirigami

* [controles/SwipeListItem]: Mostrar siempre las acciones en el escritorio predeterminado.
* [overlaysheet] Usar una posición más condicional para el botón de cierre (error 430581).
* [controles/avatar]: Abrir el «AvatarPrivate» interno como la API pública «NameUtils».
* [controles/avatar]: Exponer el color generado.
* Se ha añadido el componente «Hero».
* [controles/Card]: Eliminar la animación que se produce al situar el ratón encima.
* [controles/ListItem]: Eliminar la animación que se produce al situar el ratón encima.
* Mover «ListItems» para que use «veryShortDuration» al situar el cursor encima en lugar de «longDuration».
* [controles/Unidades]: Se ha añadido «veryShortDuration».
* Coloreado del icono «ActionButton».
* Usar mapas de bits de iconos solo tan grandes como se necesiten.
* [controles/avatar]: Mejor aspecto predeterminado.
* [controles/avatar]: Se han corregido fallos visuales.
* Se ha creado el componente «CheckableListItem».
* [controles/avatar]: Escalar el borde según el tamaño del avatar.
* Se ha revertido «[Avatar] Cambiar el degradado del fondo».
* Revertir «[Avatar] Cambiar el ancho del borde a 1 píxel para que coincida con otros anchos».
* [controles/avatar]: Hacer que el avatar disponga de funciones de accesibilidad.
* [controles/avatar]: Se ha incrementado el relleno del icono al que recurrir.
* [controles/avatar]: Hacer que el modo de imagen defina «sourceSize».
* [controles/avatar]: Se ha ajustado el tamaño del texto.
* [controles/avatar]: Se han ajustado las técnicas usadas para las formas circulares.
* [controles/avatar]: Se han añadido las acciones primaria y secundaria al avatar.
* Codificar el relleno del elemento de cabecera de «OverlaySheet».
* Compilación con qmake: Añadir el archivo de código fuente/cabecera «sizegroup».
* Colorear iconos, no botones (error 429972).
* Se ha corregido que los botones «avanzar» y «retroceder» no tengan un ancho.
* [BannerImage]: Corregir título de cabecera no centrado verticalmente con temas que no son de Plasma.

### KItemModels

* Se ha añadido la propiedad «count», que permite enlazar «rowCount» en QML.

### KItemViews

* «KWidgetItemDelegate» permite disparar un «resetModel» desde «KPluginSelector».

### KNewStuff

* Se ha marcado como obsoletos los métodos «standardAction» y «standardActionUpload».
* Se ha corregido el modelo de QtQuick si solo hay una carga útil, sin enlaces de descarga.
* Añadir un puntero «dptr» a «Cache» y mover el temporizador de aceleración a él para corregir un fallo (error 429442).
* Se ha refactorizado «KNS3::Button» para que use el nuevo diálogo internamente.
* Se ha creado una clase envolvente para diálogos QML.
* Comprobar si la versión está vacía antes de concatenarla.

### KNotification

* Se ha mejorado la documentación de la API de «KNotification».

### KParts

* Marcar como obsoleto «BrowserHostExtension».

### KQuickCharts

* Usar una macro personalizada para marcar como obsoletos mensajes en QML.
* Usar «ECMGenerateExportHeader» para las macros de marcar como obsoleto y usarlas.
* El cambio del intervalo no necesita borrar el historial.
* Cambiar el gráfico de líneas continuas al ejemplo de fuente de proxy del historial.
* Marcar como obsoleto «Model/ValueHistorySource».
* Se ha introducido «HistoryProxySource» como sustituto de «Model/ValueHistorySource».
* Añadir categorías de registro a las gráficas y usarlas en las advertencias existentes.

### KRunner

* [Lanzador de DBus] Permitir el uso de iconos de mapas de bits personalizados en los resultados.
* Añadir una clave para comprobar si la configuración se ha migrado.
* Separar archivos de configuración y de datos.
* Nueva API para ejecutar búsquedas y para el historial.
* No compilar «RunnerContextTest» en Windows.

### KService

* KSycoca: Evitar que se reconstruya la base de datos si «XDG_CONFIG_DIRS» contiene duplicados.
* KSycoca: Asegurar que los archivos adicionales se ordenan para la comparación (error 429593).

### KTextEditor

* Se ha cambiado el nombre de «Variable:» a «Document:Variable:».
* Expansión de variables: Se ha corregido la búsqueda de coincidencias de prefijos con múltiples signos de dos puntos.
* Mover el dibujo de «KateTextPreview» a «KateRenderer».
* Asegurarse de que solo se usan las líneas que se ven para pintar el pixmap.
* Usar «KateTextPreview» para representar la imagen.
* Expansión de variables: Permitir el uso de «%{Document:Variable:<name>}».
* Mostrar el texto al arrastrarlo (error 398719).
* Corregir la desconexión de «TextRange::fixLookup()».
* No pintar el fondo de la línea actual si existe una selección solapada.
* KateRegExpSearch: Corregir la lógica al añadir «\n» entre líneas de intervalo.
* Se ha cambiado el nombre de la acción a «Intercambiar con el contenido del portapapeles».
* Añadir una acción para desencadenar «copiar y pegar» como una acción.
* Funcionalidad: Se ha añadido el icono de la acción «text-wrap» para el ajuste dinámico de palabras.
* Deshacer sangrado en un paso (error 373009).

### KWidgetsAddons

* KSelectAction: Añadir las señales sin solapamiento «indexTriggered» y «textTriggered».
* KFontChooserDialog: Contemplar el borrado del diálogo por su padre durante «exec()».
* KMessageDialog: Llamar a «setFocus()» en el botón predeterminado.
* Se ha dejado de usar «QLocale::Norwegian» en favor de «QLocale::NorwegianBokmal».
* Se ha portado «KToolBarPopupActionTest» a «QToolButton::ToolButtonPopupMode».

### KXMLGUI

* KXmlGui: Al actualizar un archivo «.rc» local, mantener las nuevas barras de herramientas de la aplicación.
* Corregir la grabación de teclas de «setWindow» antes de que comience la captura (error 430388).
* Se ha eliminado la dependencia no usada de «KWindowSystem».
* Borrar «KXMLGUIClient» en el documento XML en memoria tras guardar los accesos rápidos en el disco.

### Iconos de Oxígeno

* Se ha añadido «upindicator».

### Framework de Plasma

* Exponer la información de error al plasmoide de error de una manera más estructurada.
* [componentes] Conectar mnemónicos.
* [svg] Iniciar siempre el temporizador «SvgRectsCache» desde el hilo correcto.
* [Barra de avance PC3] Definir enlace para la anchura (error 430544).
* Se ha corregido la compilación en Windows y la inversión de variables.
* [PlasmaComponents/TabGroup] Se ha corregido la comprobación de si el elemento hereda de «Page».
* Se han adaptado diversos componentes para que usen «veryShortDuration» al situar el cursor encima de ellos.
* Mover «ListItems» para que use «veryShortDuration» al situar el cursor encima en lugar de «longDuration».
* Se ha añadido «veryShortDuration».
* No permitir años negativos en el calendario (error 430320).
* Se ha corregido el fondo dañado (error 430390).
* Sustituir los ID en caché de «QString» con una versión basada en una estructura.
* [TabGroup] Invertir las animaciones en el modo RTL.
* Eliminar los accesos rápidos solo al eliminar la miniaplicación, no cuando se destruye,
* Ocultar las acciones de contexto desactivadas de «ExpandableListItem».

### Purpose

* KFileItemActions: se ha añadido «windowflag» al menú.
* «fileitemplugin» de compartir: usar el widget padre como padre del menú (error 425997).

### QQC2StyleBridge

* Se ha actualizado «org.kde.desktop/Dialog.qml».
* Dibujar «ScrollView» usando «Frame» en lugar de «Edit» (error 429601).

### Sonnet

* Se ha mejorado el rendimiento de «createOrderedModel» usando «QVector».
* Evitar la advertencia de tiempo de ejecución si no existe ninguna suposición.

### Resaltado de sintaxis

* Resaltado de C++: «QOverload» y otros relacionados.
* Se ha corregido que las etiquetas que empiezan por un punto no se resaltaran en GAS.
* Resaltado de C++: se ha añadido la macro «qGuiApp».
* Se ha mejorado el tema «Drácula».
* Corrección #5: Bash, Zsh: «!» con «if», «while» y «until» ; Bash: estilo de patrón para «${var,patt}» y «${var^patt}».
* Corrección #5: Bash, Zsh: Comentarios dentro de una matriz.
* Sintaxis de la funcionalidad «Cucumber».
* Zsh: Se ha incrementado la versión de la sintaxis.
* Zsh: Se ha corregido la expansión de llaves en las órdenes.
* Añadir «weakDeliminator» y «additionalDeliminator» con palabra clave, «WordDetect», «Int», «Float», «HlCOct» y «HlCHex».
* Indexador: Reiniciar «currentKeywords» y «currentContext» al abrir una nueva definición.
* Zsh: numerosas correcciones y mejoras.
* Bash: Se han corregido los comentarios en «case», expresión de secuencia y «)» tras «]».
* Manejar correctamente el color de importación en la base y especializado para C/C++.
* Se ha actualizado el tema «Monokai».
* Verificar la corrección o la presencia de estilos personalizados en los temas.
* Se han añadido los temas «GitHub» oscuro y claro.
* Incrementar la versión, no cambiar la versión de Kate hasta tener claro para qué se necesita.
* Se han añadido licencias.
* Se han añadido los temas «Atom One» oscuro y claro.
* Faltaba incrementar la versión al realizar el cambio.
* Se ha corregido el atributo «monokai» y el color del operador.
* Se ha añadido el tema «Monokai».
* CMake: Se han añadido las variables de 3.19 que faltaban, así como otras nuevas de 3.19.2.
* Kotlin: se han corregido varios problemas y se han realizado otras mejoras.
* Groovy: se han corregido varios problemas y se han realizado otras mejoras.
* Scala: se han corregido varios problemas y se han realizado otras mejoras.
* Java: se han corregido varios problemas y se han realizado otras mejoras.
* Se ha corregido el uso de «&&» y de «||» como subcontenido, así como el patrón de nombre de funciones.
* Se ha añadido «QRegularExpression::DontCaptureOption» cuando no existe ninguna regla dinámica.
* Bash: Se han añadido (...), ||, && en [[ ... ]] ; se ha añadido «backquote» en [ ... ] y [[ ... ]]

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
