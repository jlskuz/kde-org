---
aliases:
- ../../kde-frameworks-5.29.0
date: 2016-12-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nueva framework

Este lanzamiento incluye Prison, una nueva infraestructura para la generación de códigos de barras (incluidos los códigos QR).

### General

Se ha añadido FreeBSD a «metainfo.yaml» en todas las plataformas que se ha comprobado que funcionan en FreeBSD.

### Baloo

- Mejoras de rendimiento al escribir (se ha cuadruplicado la velocidad al escribir datos)

### Iconos Brisa

- Hacer que «BINARY_ICONS_RESOURCE» sea «ON» de forma predeterminada.
- Añadir el tipo MIME «vnd.rar» para «shared-mime-info» 1.7 (error 372461).
- Se ha añadido el icono para Claws (fallo 371914)
- Añadir el icono «gdrive» en lugar de usar un icono genérico de la nube (error 372111).
- Se ha corregido el error «list-remove-symbolic usa una imagen errónea» (error 372119).
- Otras novedades y mejoras

### Módulos CMake adicionales

- Omitir la prueba de enlaces de Python si PyQt no está instalado.
- Añadir la prueba solo si se encuentra Python.
- Reducir la versión mínima necesaria de CMake.
- Añadir el módulo «ecm_win_resolve_symlinks».

### Integración con Frameworks

- Encontrar «QDBus», necesitado por el manejador del «kpackage» de «appstream».
- Permitir que «KPackage» tenga dependencias de «packagekit» y de «appstream».

### KActivitiesStats

- Envío correcto del evento enlazado del recurso.

### Herramientas KDE Doxygen

- Adaptar para el cambio de quickgit -&gt; cgit.
- Se ha corregido el error si el nombre del grupo no está definido. Todavía puede dar fallos en ciertas condiciones.

### KArchive

- Añadir el método «errorString()» para proporcionar información de error.

### KAuth

- Se ha añadido la propiedad «timeout» (fallo 363200)

### KConfig

- kconfig_compile: Generar código con «overrides».
- Analizar correctamente las palabras clave de la función (error 371562).

### KConfigWidgets

- Asegurarse de que las acciones del menú obtengan el «MenuRole» deseado.

### KCoreAddons

- KTextToHtml: Se ha corregido el fallo «[1] añadido al final de un hiperenlace» (error 343275).
- KUser: Buscar un avatar solo si «loginName» no está vacío.

### KCrash

- Alinear con «KInit» y no usar «DISPLAY» en Mac.
- No cerrar todos los descriptores de archivo en OS X.

### KDesignerPlugin

- src/kgendesignerplugin.cpp: Añadir «overrides» al código generado.

### KDESU

- No definir «XDG_RUNTIME_DIR» en los procesos ejecutados con «kdesu».

### KFileMetaData

- Encontrar realmente «libpostproc» de FFMpeg.

### KHTML

- Java: Aplicar los nombres a los botones correctos.
- Java: Definir los nombres en el diálogo de permisos.

### KI18n

- Comprobar correctamente la desigualdad del puntero de «dngettext» (error 372681).

### KIconThemes

- Permitir mostrar iconos de todas las categorías (error 216653).

### KInit

- Definir las variables de entorno desde «KLaunchRequest» al iniciar un nuevo proceso.

### KIO

- Se ha portado al registro categorizado.
- Corregir la compilación con WinXP SDK.
- Permitir la coincidencia de sumas de verificación en mayúsculas en la pestaña de «Sumas de verificación» (error 372518).
- No ajustar nunca la última columna (=fecha) en el diálogo de archivos (error 312747).
- Importar y actualizar los «docbooks» de «kcontrol» para el código de «kio» desde la rama «master» de «kde-runtime».
- [OS X] Hacer que la papelera de KDE use la papelera de OS X.
- SlaveBase: Añadir documentación sobre bucles de eventos, notificaciones y módulos de «kded».

### KNewStuff

- Añadir una nueva opción de gestión de archivos comprimidos («subdir») a «knsrc».
- Consumir las nuevas señales de error (configurar errores de trabajo).
- Manejar la rareza con respecto a los archivos que desaparecen cuando se acaban de crear.
- Instalar realmente las cabeceras principales, con «CamelCases».

### KNotification

- [KStatusNotifierItem] Guardar/restaurar la posición de los widgets al ocultar/restaurar su ventana (error 356523).
- [KNotification] Permitir anotar las notificaciones con URL.

### Framework KPackage

- Seguir instalando «metadata.desktop» (error 372594).
- Cargar los metadatos manualmente si se pasa una ruta absoluta.
- Se ha corregido un fallo potencial cuando el paquete no es compatible con «appstream».
- Permitir que «KPackage» tenga noticia de «X-Plasma-RootPath».
- Se ha corregido la generación del archivo «metadata.json».

### KPty

- Más búsqueda de rutas «utempter» (incluyendo «/usr/lib/utempter/»).
- Añadir la ruta de la biblioteca para poder encontrar el binario «utempter» en Ubuntu 16.10.

### KTextEditor

- Evitar las advertencias de Qt sobre un modo de portapapeles no permitido en Mac.
- Usar las definiciones de sintaxis de «KF5::SyntaxHighlighting».

### KTextWidgets

- No sustituir los iconos de las ventanas con el resultado de una búsqueda fallida.

### KWayland

- [cliente] Se ha corregido una derreferencia a «nullptr» en «ConfinedPointer» y en «LockedPointer».
- [cliente] Instalar «pointerconstraints.h».
- [servidor] Se ha corregido una regresión en «SeatInterface::end/cancelPointerPinchGesture».
- Implementación del protocolo «PointerConstraints».
- [servidor] Reducir la sobrecarga de «pointersForSurface».
- Devolver «SurfaceInterface::size» en el espacio del compositor global.
- [herramientas/generador] Generar el enumerador «FooInterfaceVersion» en el lado servidor.
- [herramientas/generador] Envolver los argumentos de la petición «wl_fixed» en «wl_fixed_from_double».
- [herramientas/generador] Generar la implementación de peticiones del lado cliente.
- [herramientas/generador] Generar factorías de recursos del lado cliente.
- [herramientas/generador] Generar «callbacks» y «listener» del lado cliente.
- [herramientas/generador] Pasar «this» como «QPointer» a «Client::Resource::Private».
- [herramientas/generador] Generar «Private::setup(wl_foo *arg)» en el lado cliente.
- Implementación del protocolo «PointerGestures».

### KWidgetsAddons

- Impedir un bloqueo en Mac.
- No sustituir los iconos con el resultado de una búsqueda fallida.
- KMessageWidget: Corregir el diseño cuando «wordWrap» está activado sin acciones.
- KCollapsibleGroupBox: No ocultar los widgets; redefinir la política del foco en lugar de ello.

### KWindowSystem

- [KWindowInfo] Añadir «pid()» y «desktopFileName()».

### Iconos de Oxígeno

- Se ha añadido el icono para «application-vnd.rar» (fallo 372461)

### Framework de Plasma

- Comprobar la validez de los metadatos en «settingsFileChanged» (error 372651).
- No invertir la disposición de la barra de pestañas si es vertical.
- Eliminar «radialGradient4857» (error 372383).
- [AppletInterface] No quitar nunca el foco de «fullRepresentation» (error 372476).
- Corregir el prefijo de identificación de icono SVG (fallo 369622)

### Solid

- winutils_p.h: Restaurar la compatibilidad con WinXP SDK.

### Sonnet

- Buscar también «hunspell-1.5».

### Resaltado de sintaxis

- Normalizar los valores de los atributos de licencia en XML.
- Sincronizar las definiciones de sintaxis de «ktexteditor».
- Se ha corregido la fusión de la región de plegado.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
