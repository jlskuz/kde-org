---
aliases:
- ../../kde-frameworks-5.16.0
date: 2015-11-13
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Biblioteca de monitor: usar Kformat::spelloutDuration para localizar la cadena de hora
- Utilizar KDE_INSTALL_DBUSINTERFACEDIR para instalar las interfaces dbus
- UnindexedFileIndexer: gestionar los archivos que se hubieran movido cuando baloo_file no se estaba ejecutando
- Eliminar Transaction::renameFilePath y añadir DocumentOperation en su lugar.
- Constructores Make con un solo parámetro explícito
- UnindexedFileIndexer: indexar solo las partes necesarias del archivo
- Transacción: añadir un método para devolver la estructura timeInfo
- Añadir los tipos MIME excluidos a la configuración de balooctl
- Bases de datos: utilizar QByteArray::fromRawData cuando se pasan datos a un codec
- Balooctl: mover la orden «status» a su propia clase
- Balooctl: mostrar el menú de ayuda si la orden no se reconoce
- Balooshow: permitir buscar archivos por su inode + devId
- Monitor de Balooctl: detener si baloo se detiene
- MonitorCommand: utilizar tanto la señal de iniciado como la de finalizado
- Monitor balooctl: moverlo a una clase de órdenes adecuada
- Añadir una notificación de DBus cuando se empieza y se termina de indexar un archivo.
- FileIndexScheduler: Matar los hilos forzosamente al salir.
- Envío de WriteTransaction: Evitar la extracción de «positionList» a menos que se solicite.
- WriteTransaction: Aserciones adicionales en «replaceDocument».

### BluezQt

- «isBluetoothOperational» ahora también depende de «rfkill» sin bloquear.
- Se ha corregido la detección del estado global del interruptor «rfkill».
- API de QML: Marcar como constantes las propiedades sin señales de notificación.

### Módulos CMake adicionales

- Mostrar una advertencia en lugar de un error si «ecm_install_icons» no encuentra iconos (error 354610).
- Hacer que se pueda compilar KDE Frameworks 5 con qt 5.5.x instalado mediante el instalador normal de qt.io en macOS.
- No eliminar la definición de variables de caché en «KDEInstallDirs» (error 342717).

### Integración con Frameworks

- Definir el valor por omisión para WheelScrollLines
- Se han corregido las preferencias de «WheelScrollLines» con Qt &gt;= 5.5 (error 291144).
- Cambiar al tipo de letra Noto en Plasma 5.5

### KActivities

- Corregir la compilación con Qt 5.3
- Se ha movido el archivo de cabecera «boost.optional» al lugar que lo usa.
- Se ha sustituido el uso de «boost.optional» en continuaciones con una estructura «optional_view» más delgada.
- Se ha añadido soporte para un orden personalizado de los resultados enlazados.
- Permitir que QML pueda llamar al módulo de control de actividades
- Se ha añadido soporte para el borrado de actividades en el KCM de actividades.
- Nueva interfaz gráfica de configuración de actividades
- Nueva interfaz gráfica de configuración que permite añadir una descripción y un fondo de pantalla
- La interfaz gráfica de preferencias está ahora correctamente dividida en módulos

### KArchive

- Corregir KArchive por un cambio de comportamiento introducido en Qt 5.6
- Se han corregido fugas de memoria y se ha bajado el consumo de memoria.

### KAuth

- Manejar los mensajes «qInfo» de proxys.
- Esperar la finalización de la aplicación auxiliar de inicio de llamadas asíncronas antes de verificar la respuesta (fallo 345234).
- Se ha corregido un nombre de variable; en caso contrario, no hay forma de que funcione el archivo de cabecera.

### KConfig

- Corregir el uso de ecm_create_qm_loader.
- Se ha corregido la variable de inclusión.
- Usar la variante «KDE*INSTALL_FULL*» para que no exista ambigüedad.
- Permitir que KConfig use recursos como archivos de configuración de respaldo.

### KConfigWidgets

- Hacer que «KConfigWidgets» sea autónomo, empaquetar el archivo global en un recurso.
- Hacer doctools opcional

### KCoreAddons

- KAboutData: corregir en la documentación de la API «is is» -&gt;«is»; addCredit(): ocsUserName -&gt; ocsUsername.
- «KJob::kill(Quiet)» también debe salir del bucle de eventos.
- Permitir el uso de nombres de archivos de escritorio a «KAboutData».
- Usar carácter de escape correcto
- Reducir algunas asignaciones.
- Simplificar KAboutData::translators/setTranslators
- Corregir el código de ejemplo de setTranslator
- desktopparser: omitir Encoding= key
- desktopfileparser: Atender los comentarios de revisión.
- Permitir la definición de tipos de servicios en «kcoreaddons_desktop_to_json()».
- desktopparser: Se ha corregido el análisis de valores «double» y «bool».
- Añadir KPluginMetaData::fromDesktopFile()
- desktopparser: Permitir el análisis de rutas relativas a archivos de tipo de servicio.
- desktopparser: Usar más registro categorizado.
- «QCommandLineParser» usa «-v» para «--version», así que usar exactamente «--verbose».
- Se ha eliminado gran cantidad de código duplicado para «desktop{tojson,fileparser}.cpp».
- Analizar archivos de «ServiceType» al leer archivos «.desktop».
- Hacer que «SharedMimeInfo» sea un requisito opcional.
- Eliminar llamada a QString::squeeze()
- desktopparser: evitar descodificación utf8 innecesaria
- desktopparser: no añadir otra entrada si la entrada termina con un separador
- KPluginMetaData: advertir cuando la lista de entrada no es una lista JSON
- Añadir mimeTypes() a KPluginMetaData

### KCrash

- Mejorar la búsqueda de «drkonqui» y mantenerlo silenciado por omisión si no se encuentra.

### KDeclarative

- Ahora se pueden consultar en «ConfigPropertyMap» las opciones de configuración inmutables usando el método «isImmutable(clave)».
- Desempaquetar «QJSValue» en un mapa de propiedades de configuración.
- EventGenerator: Permitir el envío de eventos de la rueda.
- Se ha corregido la pérdida de «initialSize» en «QuickViewSharedEngine» durante la inicialización.
- corregir una regresión crítica en QuickViewSharedEngine por el commit 3792923639b1c480fd622f7d4d31f6f888c925b9
- Hacer que el tamaño de vista preajustado preceda al tamaño inicial del objeto en QuickViewSharedEngine.

### KDED

- Hacer doctools opcional

### Soporte de KDELibs 4

- No intentar almacenar un QDateTime en memoria asignada con mmap
- Sincronizar y adopta uriencode.cmake de kdoctools.

### KDesignerPlugin

- Añadir KCollapsibleGroupBox

### KDocTools

- Actualización de las entidades pt_BR

### KGlobalAccel

- No realizar un XOR con desplazamiento en «KP_Enter» (fallo 128982).
- Se toman todas las claves para un símbolo (fallo 331198).
- No obtener keysyms dos veces en cada pulsación de tecla

### KHTML

- Se ha corregido la impresión desde «KHTMLPart» definiendo correctamente el padre de «printSetting».

### KIconThemes

- kiconthemes permite ahora el uso de temas integrados en recursos Qt con el prefijo «:/icons», como hace Qt en «QIcon::fromTheme».
- Añadir las dependencias necesarias que faltaban

### KImageFormats

- Reconocer «image/vnd.adobe.photoshop» en lugar de «image/x-psd».
- Revertir parcialmente d7f457a para impedir un fallo durante la salida de la aplicación.

### KInit

- Hacer doctools opcional

### KIO

- Guardar la URL del proxy con el esquema correcto.
- Empaquetar las «nuevas plantillas de archivo» en la biblioteca «kiofilewidgets» usando un archivo «.qrc» (error 353642).
- Manejar correctamente el clic central en el menú del navegador.
- Hacer que «kio_http_cache_cleaner» se pueda desplegar en instaladores/paquetes de aplicaciones.
- KOpenWithDialog: Se ha corregido la creación del archivo de escritorio con tipo MIME vacío.
- Leer información del protocolo de los metadatos del complemento.
- Permitir el despliegue local de «kioslave».
- Añadir un «.protocol» a los JSON convertidos.
- Se ha corregido la doble emisión de resultado y la advertencia ausente cuando la lista llega a una carpeta inaccesible (fallo 333436).
- Preservar los destinos de los enlaces relativos al copiar enlaces simbólicos (error 352927).
- Uso de iconos adecuados para las carpetas predeterminadas de la carpeta personal del usuario (fallo 352498).
- Añadir una interfaz que permite que el complemento muestre iconos superpuestos personalizados.
- Hacer que la dependencia de KNotifications en KIO (kpac) sea opcional
- Hacer opcional doctools + wallet
- Evitar cuelgues de kio si no se está ejecutando un servidor de dbus
- Añadir «KUriFilterSearchProviderActions» para mostrar una lista de acciones para buscar un texto usando accesos rápidos de la web.
- Mover las entradas del menú «Crear nuevo» de «kde-baseapps/lib/konq» a «kio» (error 349654).
- Mover konqpopupmenuplugin.desktop de kde-baseapps a kio (error 350769)

### KJS

- Usar la variable global «_timezone» en MSVC en lugar de «timezone». Corrige la compilación con MSVC 2015.

### KNewStuff

- Corregir el archivo de escritorio y la URL de la página web del «Gestor de particiones de KDE»

### KNotification

- Ahora que «kparts» ya no necesita «knotifications», solo las cosas que realmente quieran notificaciones requieren de esta infraestructura.
- Se ha añadido descripción + propósito del habla + phonon.
- Hacer que la dependencia de «phonon» sea opcional (cambio puramente interno), como se hace con el habla.

### KParts

- Usar deleteLater en Part::slotWidgetDestroyed().
- Se eliminan las KNotifications de KParts
- Usar la función para consultar la ubicación de «ui_standards.rc» en lugar de programarla en el código fuente, permitiendo que funcione recurrir a recursos.

### KRunner

- RunnerManager: simplificar el código de carga de complementos

### KService

- KBuildSycoca: guardar siempre, incluso cuando no se advierta ningún cambio en el archivo «.desktop» (error 353203).
- Hacer doctools opcional
- kbuildsycoca: Analizar todos los archivos «mimeapps.list» que se mencionan en la nueva especificación.
- Uso de la marca de tiempo mayor en el subdirectorio como marca de tiempo del directorio de recursos.
- Se mantienen los tipos MIME aparte cuando se convierte KPluginInfo en KPluginMetaData

### KTextEditor

- highlighting: gnuplot: añadir la extensión .plt
- Se ha corregido la sugerencia de validación, gracias a «Thomas Jarosch» &lt;thomas.jarosch@intra2net.com&gt;; también se ha añadido una sugerencia sobre la validación de tiempos de compilación.
- Ya no se detiene cuando la orden no está disponible.
- Corrección del error #307107
- Resaltado de variables de Haskell que empiezan por «_».
- Simplificar «git2 init», ya se requiere una versión lo suficientemente reciente (error 353947).
- Empaquetar lasa configuraciones predeterminadas en el recurso.
- Resaltado de sintaxis (d-g): usar los estilos predeterminados en lugar de colores codificados.
- Mejor búsqueda de *scripts*, primero en las cosas locales del usuario, luego en nuestros recursos y finalmente en el resto de cosas; así, el usuario puede sobrescribir los *scripts* que facilitamos con otros locales.
- Empaquetar también todos los «js» en recursos, solo faltan 3 archivos de configuración y ktexteditor se puede usar como una biblioteca sin ningún archivo empaquetado.
- Siguiente intento: empaquetar todos los archivos de sintaxis XML en un recurso.
- Añadir el acceso rápido de cambio de modo de entrada (error 347769).
- Empaquetar archivos XML en el recurso.
- Resaltado de sintaxis (a-c): migrar a nuevos estilos predeterminados, eliminar colores codificados.
- Resaltado de sintaxis: eliminar colores codificados y usar en su lugar los estilos predeterminados.
- Resaltado de sintaxis: usar los nuevos estilos predeterminados (eliminar colores codificados).
- Mejor estilo predeterminado para «Import».
- Se ha introducido «Guardar como, con codificación» para guardar un archivo con una codificación distinta usando el práctico menú agrupado de codificaciones del que disponemos y sustituir todos los diálogos de guardar archivo con los correctos del sistema operativo sin perder esta importante funcionalidad.
- Empaquetar el archivo «ui» en la biblioteca, usando mi extensión para «xmlgui».
- La impresión vuelve a respetar el tipo de letra y el esquema de color seleccionados (error 344976).
- Usar los colores de Brisa para las líneas guardadas y modificadas.
- Se han mejorado los colores predeterminados para el borde de iconos del esquema «Normal».
- Paréntesis automáticos: Insertar un paréntesis solo cuando la siguiente letra está vacía o no es alfanumérica.
- Paréntesis automáticos: Si se elimina el paréntesis inicial con un retroceso, eliminar también el final.
- Paréntesis automáticos: Solo se establece la conexión una vez.
- Paréntesis automáticos: Comerse los paréntesis de cierre bajo ciertas condiciones.
- Corregir que «shortcutoverride» no se reenvíe a la ventana principal.
- Se ha corregido el color predeterminado de «resaltado de paréntesis» que era difícil de distinguir (se ha corregido el esquema normal) (error 342659).
- Añadir colores predeterminados adecuados para el color del «número de línea actual».
- Concordancia de corchetes y corchetes automáticos: Compartir el código.
- concordancia de paréntesis: protegerse de maxLines negativas
- Concordancia de corchetes: Solo porque el nuevo intervalo coincida con el antiguo no significa que no sea necesaria ninguna actualización.
- Se ha añadido la anchura de medio espacio para permitir pintar el cursor en «EOL».
- Se han corregido varios problemas de HiDPI en el borde de los iconos.
- Eliminar espacios sobrantes también el la línea del cursor (error 310712).
- Mostrar el mensaje «Marca definida» solo cuando el modo VI está activo.
- Se ha eliminado «&amp;» del texto de los botones (error 345937).
- Se ha corregido la actualización del color del número de línea actual (error 340363).
- Implementar la inserción de corchetes al escribir un corchete sobre una selección (error 350317).
- Paréntesis automáticos (bug 350317)
- Se ha corregido la alerta HL (error 344442).
- No desplazar la columna con el ajuste de línea dinámico activado.
- Recordar si el usuario había definido que el resaltado se usara entre sesiones para que no se perdiera al guardar tras restaurar (fallo 332605).
- Se ha corregido el plegado de TeX (error 328348).
- Se ha corregido la detección del final de comentario de estilo C (error 327842).
- Guardar/restaurar el ajuste de palabras dinámico al guardar/restaurar la sesión (error 284250).

### KTextWidgets

- Se ha añadido un nuevo submenú a «KTextEdit» para cambiar el idioma de la comprobación ortográfica.
- Se ha corregido la carga de las preferencias por omisión de Sonnet.

### Framework KWallet

- Utilizar KDE_INSTALL_DBUSINTERFACEDIR para instalar las interfaces dbus
- Se han corregido las advertencias del archivo de configuración de «KWallet» durante el inicio de sesión (error 351805).
- Usar el prefijo correcto para la salida de «kwallet-pam».

### KWidgetsAddons

- Se ha añadido un widget de contenedor plegable, «KCollapsibleGroupBox».
- KNewPasswordWidget: faltaba la inicialización del color.
- Se ha introducido «KNewPasswordWidget».

### KXMLGUI

- KMainWindow: rellenar previamente la información sobre el traductor cuando esté disponible (error 345320).
- Permitir que se pueda enlazar la tecla del menú de contexto (inferior derecha) a los accesos rápidos (fallo 165542).
- Se ha añadido una función para consultar la ubicación de archivos XML estándares.
- Permitir que la infraestructura «KXmlGui» se pueda usar sin ningún archivo instalado.
- Añadir las dependencias necesarias que faltaban

### Framework de Plasma

- Se han corregido los elementos de la barra de pestañas se mostraran apiñados tras su creación inicial, lo que se podía observar (por ejemplo) en Kickoff tras iniciar Plasma.
- Se ha corregido que al soltar archivos sobre el escritorio o el panel no se ofrezca una selección de las posibles acciones.
- Tener en cuenta «QApplication::wheelScrollLines» de «ScrollView».
- Usar «BypassWindowManagerHint» solo en la plataforma X11.
- Borrar el antiguo fondo del panel.
- Ruleta más legible en tamaños pequeños.
- Vista del historial a color.
- calendario: Hacer que todo el área de la cabecera se pueda pulsar.
- calendario: No usar el número de día actual en «goToMonth».
- calendario: se ha corregido la actualización del resumen de la década.
- Usar iconos Brisa del tema cuando se cargan mediante «IconItem».
- Se ha corregido la propiedad «minimumWidth» de los botones (error 353584).
- Se ha introducido la señal «appletCreated».
- Icono Brisa de Plasma: Touchpad añade elementos «id» de SVG.
- Icono Brisa de Plasma: cambiar el tamaño de Touchpad a 22x22 píxeles.
- Iconos Brisa: Añadir el icono «widget» a las notas.
- Un script para sustituir colores codificados con hojas de estilos.
- Aplicar «SkipTaskbar» a «ExposeEvent».
- No definir «SkipTaskbar» en todos los eventos.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
