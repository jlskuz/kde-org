---
aliases:
- ../announce-applications-16.04-beta
custom_spread_install: true
date: 2016-03-24
description: KDE lanza la Beta de las Aplicaciones 16.04.
layout: application
release: applications-16.03.80
title: KDE lanza la beta para las Aplicaciones 16.04
---
Hoy, 24 de marzo de 2016, KDE ha lanzado la versión beta de las Aplicaciones KDE. Una vez congeladas las dependencias y las funcionalidades, el equipo de KDE se ha centrado en corregir errores y en pulir aún más la versión.

Consulte las <a href='https://community.kde.org/Applications/16.04_Release_Notes'>notas de lanzamiento de la comunidad</a> para obtener información sobre nuevos paquetes que ahora se basan en KF5 y sobre los problemas conocidos. Se realizará un anuncio más completo para la versión final.

Debido a las numerosas aplicaciones que se basan en KDE Frameworks 5, es necesario probar la versión 16.04 de manera exhaustiva con el fin de mantener y mejorar la calidad y la experiencia del usuario. Los usuarios reales son de vital importancia para mantener la alta calidad de KDE porque los desarrolladores no pueden probar todas las configuraciones posibles. Contamos con ustedes para ayudar a encontrar errores de manera temprana de forma que se puedan corregir antes de la versión final. Considere la idea de unirse al equipo instalando la beta <a href='https://bugs.kde.org/'>e informando de cualquier error que encuentre</a>.

#### Instalación de paquetes binarios de las Aplicaciones de KDE 16.04 Beta

<em>Paquetes</em>. Algunos distribuidores del SO Linux/UNIX tienen la gentileza de proporcionar paquetes binarios de 16.04 Beta (internamente, 16.03.80) para algunas versiones de sus distribuciones y en otros casos han sido voluntarios de la comunidad los que lo han hecho posible. En las próximas semanas estarán disponibles paquetes binarios adicionales, así como actualizaciones de los paquetes disponibles en este momento.

<em>Ubicación de paquetes</em>. Para obtener una lista actual de los paquetes binarios disponibles de los que el Equipo de Lanzamiento de KDE tiene conocimiento, visite la <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Wiki de la Comunidad</a>.

#### Compilación de las aplicaciones de KDE 16.04 Beta

La totalidad del código fuente de las Aplicaciones 16.04 Beta se puede <a href='http://download.kde.org/unstable/applications/16.03.80/src/'>descargar libremente</a>. Dispone de instrucciones sobre la compilación y la instalación en la <a href='/info/applications/applications-16.03.80'>página de información las aplicaciones de KDE Beta</a>.
