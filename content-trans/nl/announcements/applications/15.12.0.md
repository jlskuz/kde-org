---
aliases:
- ../announce-applications-15.12.0
changelog: true
date: 2015-12-16
description: KDE stelt KDE Applicaties 15.12 beschikbaar.
layout: application
release: applications-15.12.0
title: KDE stelt KDE Applicaties 15.12.0 beschikbaar
version: 15.12.0
---
16 december 2015. Vandaag heeft KDE KDE Frameworks 15.12 uitgebracht.

Met enthousiasme kondigt KDE de uitgave van KDE Applications 15.12, de december update van KDE Applications, aan. Deze uitgave brengt een nieuwe toepassing en toevoegingen aan functies, en reparaties van bugs over het gehele aanbod van bestaande toepassingen. Het team streeft er naar om de beste kwaliteit op uw bureaublad te brengen en van deze toepassingen. We rekenen op u om ons uw terugkoppeling te zenden.

In deze uitgave hebben we een grote hoeveelheid toepassingen bijgewerkt om het nieuwe <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a> te gebruiken, inclusief <a href='https://edu.kde.org/applications/all/artikulate/'>Artikulate</a>, <a href='https://www.kde.org/applications/system/krfb/'>Krfb</a>, <a href='https://www.kde.org/applications/system/ksystemlog/'>KSystemLog</a>, <a href='https://games.kde.org/game.php?game=klickety'>Klickety</a> en meer uit KDE Games, los van het KDE plug-in-interface voor afbeeldingen en zijn ondersteunde bibliotheken. Dit brengt het totale aantal toepassingen dat KDE Frameworks 5 gebruikt op 126.

### Een spectaculaire nieuw toevoeging

Na 14 jaar onderdeel te zijn van KDE, is KSnapshot met pensioen gegaan en vervangen door een nieuwe toepassing voor het maken van een schermafdruk, Spectacle.

Met nieuwe functies en een volledig nieuwe UI, maakt Spectacle het maken van schermafdrukken net zo makkelijk en eenvoudig als het maar kan zijn. Naast wat u kon doen met KSnapshot, kunt u met Spectacle nu samengestelde schermafdrukken maken van pop-upmenu's samen met hun oudervenster of schermafdrukken van het gehele scherm (of het nu actieve venster) zonder zelfs Spectacle te starten, eenvoudig door de sneltoetsen respectievelijk Shift+PrintScreen en Meta+PrintScreen te gebruiken.

We hebben ook agressief de opstarttijd van de toepassing geoptimaliseerd, om de tijdvertraging absoluut te minimaliseren tussen het starten van de toepassing en wanneer de afbeelding wordt gevangen.

### Oppoetsen overal

Veel toepassingen zijn aanzienlijk opgepoetst in deze cyclus, naast reparaties voor stabiliteit en bugs.

<a href='https://userbase.kde.org/Kdenlive'>Kdenlive</a>, de niet-lineaire videobewerker, kreeg grote reparaties aan zijn gebruikersinterface. U kunt nu items kopiëren en plakken op uw tijdlijn en gemakkelijk transparantie omschakelen in een gegeven track. De kleuren van de pictogrammen passen zich automatisch aan aan het thema van het hoofdvenster, waardoor ze gemakkelijker zijn te zien.

{{<figure src="/announcements/applications/15.12.0/ark1512.png" class="text-center" width="600px" caption=`Ark kan nu ZIP-commentaar tonen`>}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, de beheerder van archieven, kan nu commentaar ingebed in ZIP en RAR archieven tonen. We hebben ondersteuning voor Unicode tekens in bestandsnamen in ZIP-archieven verbeterd en u kunt nu beschadigde archieven detecteren en zo veel mogelijk gegevens daaruit herstellen. U kunt ook gearchiveerde bestanden in hun standaard toepassing openen en we hebben slepen en laten vallen naar het bureaublad gerepareerd evenals bekijken van XML-bestanden.

### Alles spelen en geen werk

{{<figure src="/announcements/applications/15.12.0/ksudoku1512.png" class="text-center" width="600px" caption=`Nieuw verwelkomingsscherm voor KSudoku (zoals op a Mac OS X)`>}}

De ontwikkelaars van KDE Games hebben in voorbije maanden hard gewerkt aan het optimaliseren van onze spellen voor een soepelere en rijkere ervaring, en we hebben heel wat nieuwe spullen op dit gebied ontvangen voor het plezier van onze gebruikers.

In <a href='https://www.kde.org/applications/games/kgoldrunner/'>KGoldrunner</a> hebben we twee nieuwe sets levels toegevoegd, een die graven tijdens vallen toestaat en de ander niet. We hebben oplossingen voor verschillende bestaande sets levels. Voor een toegevoegde uitdaging staan we geen graven tijdens vallen toe in sommige oudere sets levels.

In <a href='https://www.kde.org/applications/games/ksudoku/'>KSudoku</a>, kunt u nu Mathdoku en Killer Sudoku puzzels afdrukken. De nieuwe multi-kolom indeling in het welkomstscherm van KSudoku maakt het gemakkelijker om meer van de vele beschikbare typen puzzels te zien en de kolmmen passen zich automatisch aan als het venster van grootte wijzigt. We hebben hetzelfde in Palapeli gedaan, waarmee het makkelijker is om in een oogopslag meer van uw legpuzzelverzameling te zien.

We hebben ook reparaties voor stabiliteit ingebracht voor spellen zoals KNavalBattle, Klickety, <a href='https://www.kde.org/applications/games/kshisen/'>KShisen</a> en <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a> en de ervaring van het geheel is nu erg verbeterd. KTuberling, Klickety en KNavalBattle zijn ook bijgewerkt om het nieuwe KDE Frameworks 5 te gebruiken.

### Belangrijke reparaties

Vindt u het niet heel erg vervelend wanneer uw favoriete toepassing crasht op het meest ongelegen moment? Wij ook, en om dat op te lossen hebben we hard gewerkt om voor u heel wat bugs te repareren, maar waarschijnlijk niet allemaal, dus vergeet niet ze te <a href='https://bugs.kde.org'>rapporteren</a>!

In onze bestandsbeheerder <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a> hebben we verschillende reparaties voor stabiliteit en enkele reparaties om scrollen soepeler te maken. In <a href='https://www.kde.org/applications/education/kanagram/'>Kanagram</a> hebben we een vervelend probleem met witte tekst op een witte achtergrond gerepareerd. In <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a> hebben we gepoogd om een crash te repareren die optrad bij afsluiten, naast het opschonen van de UI en toevoeging van een nieuwe opschoning van de cache.

De <a href='https://userbase.kde.org/Kontact'>Kontact Suite</a> heeft vele toegevoegde functie gekregen, belangrijke reparaties en verbetering van prestaties. In feite is er zoveel ontwikkeling in deze cyclus geweest dat we het versienummer hebben opgehoogd naar 5.1. Het team is hard aan het werk en ziet uit naar al uw terugkoppeling.

### Naar de toekomst

Als onderdeel van de inspanning om, dat wat we bieden, te moderniseren, hebben we enige toepassingen uit KDE Applications laten vallen en deze worden niet langer uitgegeven als vanaf KDE Applications 15.12

We hebben 4 toepassingen uit de uitgave - Amor, KTux, KSnapshot en SuperKaramba laten vallen. Zoals boven opgemerkt is KSnapshot verbangen door Spectacle en in essentie vervangt Plasma SuperKaramba als een widget-engine. Alleenstaande schermbeveiliging is vervallen omdat schermvergrendeling op moderne bureaubladen heel anders wordt behandeld.

We hebben 3 illustratiepakketten (kde-base-artwork, kde-wallpapers en kdeartwork) laten vallen; hun inhoud was lange tijd niet gewijzigd.

### Volledige log met wijzigingen
