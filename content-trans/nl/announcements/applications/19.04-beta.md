---
aliases:
- ../announce-applications-19.04-beta
date: 2019-03-22
description: KDE stelt KDE Applicaties 19.04 Beta beschikbaar.
layout: application
release: applications-19.03.80
title: KDE stelt de beta van KDE Applicaties 19.04 beschikbaar
version_number: 19.03.80
version_text: 19.04 Beta
---
22 maart 2019. Vandaag heeft KDE de beta van de nieuwe versies van KDE Applications vrijgegeven. Met het bevriezen van afhankelijkheden en functies, is het team van KDE nu gefocust op repareren van bugs en verder oppoetsen.

Kijk in de <a href='https://community.kde.org/Applications/19.04_Release_Notes'>uitgavenotities van de gemeenschap</a> voor informatie over tarballs en bekende problemen. Een meer complete aankondiging zal beschikbaar zijn voor de uiteindelijke uitgave

De uitgave KDE Applications 19.04 heeft grondig testen nodig om de kwaliteit en gebruikservaring te handhaven en te verbeteren. Echte gebruikers zijn kritiek in het proces om de hoge kwaliteit van KDE te handhaven, omdat ontwikkelaars eenvoudig niet elke mogelijke configuratie kunnen testen. We rekenen op u om in een vroeg stadium bugs te vinden zodat ze gekraakt kunnen worden voor de uiteindelijke vrijgave. Ga na of u met het team mee kunt doen door de beta te installeren <a href='https://bugs.kde.org/'>en elke bug te rapporteren</a>.
