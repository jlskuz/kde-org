---
aliases:
- ../announce-applications-17.04.2
changelog: true
date: 2017-06-08
description: KDE stelt KDE Applicaties 17.04.2 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 17.04.2 beschikbaar
version: 17.04.2
---
8 juni 2017. Vandaag heeft KDE de tweede update voor stabiliteit vrijgegeven voor <a href='../17.04.0'>KDE Applicaties 17.04</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 15 aangegeven reparaties van bugs, die verbeteringen bevatten aan kdepim, ark, dolphin, gwenview, kdenlive, naast andere.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.33.
