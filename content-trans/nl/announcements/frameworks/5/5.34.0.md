---
aliases:
- ../../kde-frameworks-5.34.0
date: 2017-05-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- balooctl, baloosearch, balooshow: volgorde van aanmaken van QCoreApplication object repareren (bug 378539)

### Breeze pictogrammen

- Pictogrammen voor hotspot (https://github.com/KDAB/hotspot) toevoegen
- Beter versiebeheersysteem voor pictogrammen (bug 377380)
- Pictogram voor plasmate toevoegen (bug 37678072)
- Pictogrammen voor microfoongevoeligheid bijwerken (bug 377012)
- De standaard voor pictogrammen in het paneel verhogen naar 48

### Extra CMake-modules

- Sanitizers: Geen GCC-achtige vlaggen gebruiken voor bijv. MSVC
- KDEPackageAppTemplates: verbeteringen van documentatie
- KDECompilerSettings: geef -Wvla &amp; -Wdate-time door
- Oudere qmlplugindump versies ondersteunen
- ecm_generate_qmltypes introduceren
- Projecten toestaan het bestand een tweede keer in te voegen
- rx repareren die overeenkomt met projectnamen uit de git uri
- Bouwcommando fetch-translations introduceren
- -Wno-gnu-zero-variadic-macro-arguments meer gebruiken

### KActivities

- We gebruiken alleen Tier 1 frameworks, ga dus naar Tier 2
- KIO verwijderd uit de afhankelijkheden

### KAuth

- Beveiligingsreparatie: verifieer dat wie ons aanroept ook echt is wie hij zegt hij is

### KConfig

- Berekening van relatief pad in KDesktopFile::locateLocal() repareren (bug 345100)

### KConfigWidgets

- Stel het pictogram in voor de donatieactie
- Beperkingen voor verwerken van QGroupBoxes verminderen

### KDeclarative

- Stel ItemHasContents niet in in DropArea
- Accepteer geen hovergebeurtenissen in de DragArea

### KDocTools

- Workaround voor MSVC en laden van catalogus
- Een zichtbaarheidsconflict voor meinproc5 oplossen (bug 379142)
- Zet aanhalingstekens om een paar andere variabelen met pad (problemen met spaties voorkomen)
- Zet aanhalingstekens om een paar variabelen met pad (problemen met spaties voorkomen)
- Schakel de lokale documentatie tijdelijk uit op Windows
- FindDocBookXML4.cmake, FindDocBookXSL.cmake - zoek in huisgemaakte installaties

### KFileMetaData

- maakt KArchive optioneel en bouw geen extractors die het nodig hebben
- compilatiefout met gedupliceerde symbolen repareren met mingw op Windows

### KGlobalAccel

- build: KService afhankelijkheid verwijderen

### KI18n

- basename behandeling van po bestanden repareren (bug 379116)
- ki18n bootstrapping repareren

### KIconThemes

- Probeer zelfs niet pictogrammen met lege grootte aan te maken

### KIO

- KDirSortFilterProxyModel: breng natuurlijke sortering terug (bug 343452)
- UDS_CREATION_TIME vullen met de waarde van st_birthtime op FreeBSD
- http slave: foutpagina verzenden na mislukte autorisatie (bug 373323)
- kioexec: upload delegeren naar een kded-module (bug 370532)
- KDirlister Gui-test, die een URL-schema twee keer instelt, repareren
- kiod-modules verwijderen bij verlaten
- Een moc_predefs.h bestand genereren voor KIOCore (bug 371721)
- kioexec: ondersteuning voor --suggestedfilename repareren

### KNewStuff

- Meerdere categorieën toestaan met dezelfde naam
- KNewStuff: informatie over bestandsgrootte tonen in rasterdelegatie
- Als een grootte van een item bekend is, toon het in de lijstweergave
- Registeer en declareer KNSCore::EntryInternal::List als een metatype
- Niet door de schakelaar vallen. Dubbele items? Nee aub
- het gedownloade bestand altijd sluiten na downloaden

### KPackage-framework

- Pad van include repareren in KF5PackageMacros.cmake
- Waarschuwingen negeren bij appdata generatie (bug 378529)

### KRunner

- Sjabloon: topniveausjablooncategorie  wijzigen naar "Plasma"

### KTextEditor

- KAuth integratie in opslaan van document - vol. 2
- Toekennen repareren bij toepassen van code invouwen die cursorpositie wijzigt
- Niet verouderde root-element &lt;gui&gt; gebruiken in bestand ui.rc
- scroll-bar-marks ook toevoegen aan het ingebouwde zoeken&amp;vervangen
- KAuth integratie in opslaan van document

### KWayland

- Oppervlak is geldig velideren bij verzenden van gebeurtenis TextInput verlaten

### KWidgetsAddons

- KNewPasswordWidget: zichtbaarheidsactie niet verbergen in modus platte tekst (bug 378276)
- KPasswordDialog: zichtbaarheidsactie niet verbergen in modus platte tekst (bug 378276)
- KActionSelectorPrivate::insertionIndex() repareren

### KXMLGUI

- kcm_useraccount is dood, lang leve user_manager
- Reproduceerbaar bouwen: laat versie vallen uit XMLGUI_COMPILING_OS
- Reparatie: DOCTYPE naam moet overeenkomen met type root-element
- Verkeerd gebruik van ANY in kpartgui.dtd
- Niet verouderde root-element &lt;gui&gt; gebruiken
- API dox reparaties: vervang 0 door de nullptr of verwijder waar niet toegepast

### NetworkManagerQt

- Crash repareren bij ophalen van lijst met actieve verbindingen (bug 373993)
- Standaard waarde instellen voor automatisch onderhandelen gebaseerd op actieve NM versie

### Oxygen-pictogrammen

- Pictogram voor hotspot (https://github.com/KDAB/hotspot) toevoegen
- De standaard voor pictogrammen in het paneel verhogen naar 48

### Plasma Framework

- Pictogram opnieuw laden wanneer usesPlasmaTheme wijzigt
- Plasma Components 3 installeren zodat ze gebruikt kunnen worden
- Introduceer units.iconSizeHints.* om hints te leveren voor door gebruiker te configureren pictogramgroottes (bug 378443)
- [TextFieldStyle] textField is geen gedefinieerde fout repareren
- De ungrabMouse hack voor Qt 5.8 bijwerken
- Bewaking tegen niet laden door applet van AppletInterface (bug 377050)
- Agenda: juiste taal gebruiken voor namen van maand en dag
- plugins.qmltypes bestanden genereren voor de plug-ins die we installeren
- als de gebruiker een impliciete grootte instelde, behoud het

### Solid

- Include toevoegen die nodig is in msys2

### Accentuering van syntaxis

- Arduino extensie toevoegen
- LaTeX: onjuiste beëindiging van commentaar met iffalse (bug 378487)

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
