---
aliases:
- ../../kde-frameworks-5.11.0
date: 2015-06-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Extra CMake-modules

- Nieuwe argumenten voor ecm_add_tests(). (bug 345797)

### Frameworkintegratie

- De juiste initialDirectory voor de KDirSelectDialog gebruiken
- Ga na dat het schema gespecificeerd is bij overschrijven van de startwaarde van de URL
- Alleen bestaande mappen accepteren in modus FileMode::Directory

### KActivities

(geen log met wijzigingen geleverd)

### KAuth

- KAUTH_HELPER_INSTALL_ABSOLUTE_DIR beschikbaar maken aan alle KAuth-gebruikers

### KCodecs

- KEmailAddress: overladen toevoegen voor extractEmailAddress en firstEmailAddress die een foutmelding teruggeeft.

### KCompletion

- Ongewenste selectie repareren bij bewerken van de bestandsnaam in de bestandsdialoog (bug 344525)

### KConfig

- Crash voorkomen als QWindow::screen() nul is
- KConfigGui::setSessionConfig() toevoegen (bug 346768)

### KCoreAddons

- Nieuwe KPluginLoader::findPluginById() gemaks-API

### KDeclarative

- aanmaken van ConfigModule uit KPluginMetdata ondersteunen
- gebeurtenissen pressAndhold repareren

### Ondersteuning van KDELibs 4

- QTemporaryFile gebruiken in plaats van een hard gecodeerd tijdelijk bestand.

### KDocTools

- Vertalingen bijwerken
- Bijwerken van customization/ru
- Items met verkeerde koppelingen repareren

### KEmoticons

- Het thema in de cache zetten in de plug-in voor integratie

### KGlobalAccel

- [runtime] platformspecifieke code verplaatsen naar plug-ins

### KIconThemes

- KIconEngine::availableSizes() optimaliseren

### KIO

- Probeer geen gebruikers aan te vullen en toe te kennen als het voorvoegsel niet leeg is. (bug 346920)
- KPluginLoader::factory() gebruiken bij laden van KIO::DndPopupMenuPlugin
- Deadlock repareren bij gebruik van netwerkproxies (bug 346214)
- KIO::suggestName gerepareerd om bestandsextensies te behouden
- kbuildsycoca4 starten bij bijwerken van sycoca5.
- KFileWidget: geen bestanden accepteren in modus alleen-mappen
- KIO::AccessManager: het mogelijk maken om sequentiële QIODevice asynchroon te behandelen

### KNewStuff

- Nieuwe methode fillMenuFromGroupingNames toevoegen
- KMoreTools: veel nieuwe groeperingen toevoegen
- KMoreToolsMenuFactory: behandeling voor "git-clients-and-actions"
- createMenuFromGroupingNames: URL-parameter optioneel maken

### KNotification

- Crash in NotifyByExecute repareren wanneer geen widget is ingesteld (bug 348510)
- Behandeling van meldingen die gesloten worden verbeteren (bug 342752)
- Gebruik van QDesktopWidget door QScreen vervangen
- Zeker maken dat KNotification gebruikt kan worden vanuit een niet-GUI thread

### Pakket Framework

- De toegang tot de structuur qpointer bewaken (bug 347231)

### KPeople

- QTemporaryFile gebruiken in plaats van een hard gecodeerde /tmp.

### KPty

- tcgetattr &amp; tcsetattr gebruiken indien beschikbaar

### Kross

- Laden van Kross modules "forms" en "kdetranslation" repareren

### KService

- Bij uitvoeren als root handhaaf dan het eigendom van bestaande cache-bestanden (bug 342438)
- Tegen niet in staat zijn een stream te openen bewaken (bug 342438)
- Controle op ongeldig recht op schrijven in bestand repareren (bug 342438)
- Afvragen van ksycoca op pseudo-mimetypes x-scheme-handler/* repareren. (bug 347353)

### KTextEditor

- Zoals in KDE 4.x tijden toestaan om apps/plugins van derden te installeren met eigen XML-bestanden voor accentuering in katepart5/syntax
- KTextEditor::Document::searchText() toevoegen
- Gebruik van KEncodingFileDialog terugbrengen (bug 343255)

### KTextWidgets

- Een methode om decorator te wissen toevoegen
- Gebruik van eigen sonnet-decorator toestaan
- "Vorige zoeken" in KTextEdit implementeren.
- Ondersteuning voor spraak-naar-tekst opnieuw toevoegen

### KWidgetsAddons

- KAssistantDialog: de Help-knop die aanwezig was in de KDELibs4-versie opnieuw toevoegen

### KXMLGUI

- Sessiebeheer voor KMainWindow toevoegen (bug 346768)

### NetworkManagerQt

- WiMAX ondersteuning voor NM 1.2.0+ laten vallen

### Plasma Framework

- Agendacomponenten kunne nu weeknummers tonen (bug 338195)
- QtRendering gebruiken voor lettertypen in wachtwoordvelden
- Opzoeken met AssociatedApplicationManager repareren wanneer een mimetype heeft (bug 340326)
- Kleuring van paneelachtergrond repareren (bug 347143)
- Bericht "Kon applet niet laden" kwijtraken
- Mogelijkheid om QML-kcms te laden in plasmoid config-vensters
- De DataEngineStructure voor Applets niet gebruiken
- libplasma zoveel mogelijk weghalen uit sycoca
- [plasmacomponents] SectionScroller er voor laten zorgen dat de ListView.section.criteria worden gevolgd
- Schuifbalken niet langer automatisch verbergen wanneer een aanraakscherm aanwezig is (bug 347254)

### Sonnet

- Eén centrale cache voor de SpellerPlugins.
- Tijdelijke toekenningen verminderen.
- Optimaliseren: veeg de dict-cache niet schoon bij kopiëren van spellingobjecten.
- save() aanroepen wegoptimaliseren door het eenmalig aan het eind aan te roepen indien nodig.

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
