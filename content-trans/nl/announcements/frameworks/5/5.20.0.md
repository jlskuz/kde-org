---
aliases:
- ../../kde-frameworks-5.20.0
date: 2016-03-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Breeze pictogrammen

- Veel nieuwe pictogrammen
- Virtualbox MIME-type pictogrammen toegevoegd en enige andere ontbrekende MIME-typen
- Synaptic en octopi pictogram ondersteuning toegevoegd
- Pictogrammen voor knippen repareren (bug 350612)
- Naam van audio-headphones.svg (+=d) gerepareerd
- Pictogrammen voor waardering met kleiner marges (1px)

### Frameworkintegratie

- Mogelijke bestandsnaam in KDEPlatformFileDialog::setDirectory() verwijderen
- Niet op naam filteren als we MIME-typen hebben

### KActivities

- Afhankelijkheid van Qt5::Widgets verwijderen
- Afhankelijkheid van KDBusAddons verwijderen
- Afhankelijkheid van KI18n verwijderen
- Ongebruikte includes verwijderen
- Uitvoer van shell-scripts verbetert
- Het gegevensmodel (ActivitiesModel) toegevoegd die de activiteiten aan de bibliotheek toont
- Standaard alleen de bibliotheek bouwen
- De service- en workspace-componenten uit de build verwijdert
- De bibliotheek naar src/lib uit src/lib/core verplaatst
- CMake-waarschuwingen repareren
- Crash in contextmenu van activiteiten gerepareerd (bug 351485)

### KAuth

- kded5 dead-lock gerepareerd wanneer een programma dat kauth gebruikt bestaat

### KConfig

- KConfigIniBackend: dure detach in lookup gerepareerd

### KCoreAddons

- Kdelibs4 migratie van configuratie voor Windows gerepareerd
- API toegevoegd om runtime-versieinformatie van Frameworks te krijgen
- KRandom: Niet tot 16K van /dev/urandom gebruiken om rand() te voeden (bug 359485)

### KDeclarative

- Geen null-object-pointer aanroepen (bug 347962)

### KDED

- Het mogelijk maken om te compileren met -DQT_NO_CAST_FROM_ASCII

### Ondersteuning van KDELibs 4

- Sessiebeheer voor op KApplication gebaseerde toepassingen repareren (bug 354724)

### KDocTools

- Unicode tekens gebruiken voor opbellen

### KFileMetaData

- KFileMetadata kan nu informatie opvragen en opslaan over het originele e-mailbericht waarvan een opgeslagen bestand een bijlage kan zijn

### KHTML

- Cursor bijwerken in weergave repareren
- Gebruik van geheugen voor tekenreeksen beperken
- KHTML java-applet-viewer: gebroken DBus aanroep naar kpasswdserver repareren

### KI18n

- Overdraagbare import macro voor nl_msg_cat_cntr gebruiken
- Bij opzoeken van vertalingen voor een toepassing . and .. overslaan
- Gebruik van _nl_msg_cat_cntr beperken naar implementaties van GNU gettext
- KLocalizedString::languages() toevoegen
- Gettext aanroepen alleen plaatsen als catalog is gelokaliseerd

### KIconThemes

- Ga na dat variabele is geïnitialiseerd

### KInit

- kdeinit: geef de voorkeur aan laden van bibliotheken uit RUNPATH
- "Qt5 TODO: QUrl::fromStringList gebruiken" implementeren

### KIO

- KIO app-slave verbinding afbreken als appName een '/' bevat repareren (bug 357499)
- Meerdere authenticatiemethoden proberen in het geval van mislukkingen
- help: mimeType() bij get() repareren
- KOpenWithDialog: mimetype-naam en toelichting tonen in tekst bij keuzevakje "Herinneren" (bug 110146)
- Een serie wijzigingen om opnieuw een list van een map te maken nadat hernoemen van een bestand in meer gevallen (bug 359596)
- http: hernoem m_iError naar m_kioError
- kio_http: lees en gooi body weg na een 404 met errorPage=false
- kio_http: MIME-type bepaling repareren wanneer URL eindigt met '/'
- FavIconRequestJob: accessor hostUrl() toevoegen zodat konqueror kan bepalen waar de job voor was, in het slot
- FavIconRequestJob: hangende job repareren bij afbreken vanwege een te grote favicon
- FavIconRequestJob: errorString() repareren, het had alleen de URL
- KIO::RenameDialog: herstel ondersteuning voor vooruitblik, datum en grootte labels toevoegen (bug 356278)
- KIO::RenameDialog: gedupliceerde code opschonen
- Verkeerde pad-naar-QUrl conversies repareren
- kf5.kio gebruiken in de categorienaam om overeen te komen met andere categorieën

### KItemModels

- KLinkItemSelectionModel: nieuwe standaard constructor toegevoegd
- KLinkItemSelectionModel: het gekoppelde selectiemodel instelbaar maken
- KLinkItemSelectionModel: wijzigingen aan het model selectionModel behandelen
- KLinkItemSelectionModel: model niet lokaal opslaan
- KSelectionProxyModel: bug in iteratie repareren
- Reset status KSelectionProxyModel indien nodig
- Een eigenschap aangevend of de modellen een verbonden ketting vormen toevoegen
- KModelIndexProxyMapper: logica van verbonden controle vereenvoudigen

### KJS

- Gebruik van geheugen voor tekenreeksen beperken

### KNewStuff

- Een waarschuwing tonen als er een fout in de Engine is

### Pakket Framework

- KDocTools optioneel laten blijven in KPackage

### KPeople

- Gebruik van verouderd API repareren
- actionType toevoegen aan de declaratieve plug-in
- De logica van filtering in PersonsSortFilterProxyModel omdraaien
- Het QML voorbeeld iets bruikbaarder maken
- actionType toevoegen aan het PersonActionsModel

### KService

- Code vereenvoudigen, pointer-dereferences verminderen, aan container gerelateerde verbeteringen
- kmimeassociations_dumper toevoegen aan testprogramma, geïnspireerd door bug 359850
- chromium/wine apps die niet laden op sommige distributies repareren (bug 213972)

### KTextEditor

- Accentuering van alle voorkomens in ReadOnlyPart
- Niet itereren over een QString alsof het een QStringList was
- Statische QMaps op de juiste manier initialiseren
- Geef de voorkeur aan toDisplayString(QUrl::PreferLocalFile)
- Surrogaat teken sturen uit invoermethode ondersteunen
- Niet crashen bij afsluiten wanneer tekstanimatie nog actief is

### KWallet Framework

- Ga na dat KDocTools is opgezocht
- Geef geen negatief getal door aan dbus, het wordt toegekend in libdbus
- cmake-bestanden opschonen
- KWallet::openWallet(Synchronous): geen time-out na 25 seconden

### KWindowSystem

- _NET_WM_BYPASS_COMPOSITOR ondersteunen (bug 349910)

### KXMLGUI

- Naam in niet-landseigen taal als terugval gebruiken
- Sessiebeheer gebroken sinds KF5 / Qt5 repareren (bug 354724)
- Sneltoetsschema's: globaal geïnstalleerde schema's ondersteunen
- qHash(QKeySequence) van Qt gebruiken bij bouwen tegen Qt 5.6+
- Sneltoetsschema's: bug repareren waar twee KXMLGUIClients met dezelfde naam elkaars schemabestand overschrijven
- kxmlguiwindowtest: sneltoetsendialoog toevoegen, voor het testen van de sneltoetsschema's editor
- Sneltoetsschema's: bruikbaarheid verbeteren door teksten in de GUI te wijzigen
- Sneltoetsschema's: schemalijstcombo verbeteren (automatische grootte, niet wissen bij onbekend schema)
- Sneltoetsschema's: de guiclientnaam niet vooraan toevoegen aan de bestandsnaam
- Sneltoetsschema's: map maken voor het proberen om een nieuw sneltoetsschema op te slaan
- Sneltoetsschema's: marge van indeleing herstellen, het ziet er anders ingekrompen uit
- Geheugenlek in KXmlGui opstarthaak repareren

### Plasma Framework

- IconItem: bron niet overschrijven bij gebruik van QIcon::name()
- ContainmentInterface: gebruik van QRect right() en bottom() repareren
- Effectief gedupliceerd codepad voor behandeling van QPixmaps verwijderen
- API documenten voor IconItem toevoegen
- Stylesheet repareren (bug 359345)
- Venstermasker niet wissen bij elke wijziging in geometrie wanneer compositing actief is en geen masker is ingesteld
- Applet: niet crashen bij verwijderen van paneel (bug 345723)
- Thema: pixmapcache weggooien bij wijziging van thema (bug 359924)
- IconItemTest: overslaan wanneer grabToImage mislukt
- IconItem: kleur wijzigen van svg-pictogrammen geladen uit pictogramthema repareren
- Oplossen van svg-iconPath in IconItem repareren
- Als het pad voorbij is, pak de staart (bug 359902)
- Eigenschappen configurationRequired en reden toevoegen
- contextualActionsAboutToShow naar Applet verplaatsen
- ScrollViewStyle: geen marges van het om te klappen item gebruiken
- DataContainer: controles op slots repareren voor verbinden/verbreken
- ToolTip: meerdere wijzigingen in geometrie voorkomen bij wijziging van inhoud
- SvgItem: geen Plasma::Theme gebruiken uit rendering thread
- AppletQuickItem: zoeken naar eigen bijgevoegde layout repareren (bug 358849)
- Kleinere expander voor de taakbalk
- ToolTip: timer voor tonen stoppen als hideTooltip wordt aangeroepen (bug 358894)
- Animatie van pictogrammen in plasma tekstballonnen uitschakelen
- Animaties in tekstballonnen vervallen
- Standaard thema volgt kleurschema
- Niet laden van non-theme pictogrammen door IconItem met naam repareren (bug 359388)
- De voorkeur geven aan andere containers dan bureaublad in containmentAt()
- WindowThumbnail: glx-pixmap in stopRedirecting() verwerpen (bug 357895)
- Het oude filter van applets verwijderen
- ToolButtonStyle: vertrouw niet op een buitenstaande ID
- Neem niet aan dat we een corona vinden (bug 359026)
- Agenda: juiste terug/vooruit knoppen toevoegen en een "Vandaag" knop (bugs 336124, 348362, 358536)

### Sonnet

- Schakel taaldetectie niet uit eenvoudig vanwege een ingestelde taal
- Automatisch uitschakelen van standaard automatische spellingcontrole uitschakelen
- TextBreaks repareren
- Zoekpad van Hunspell woordenboek mist '/' repareren (bug 359866)
- &lt;app dir&gt;/../share/hunspell toevoegen naar zoekpad van woordenboek

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
