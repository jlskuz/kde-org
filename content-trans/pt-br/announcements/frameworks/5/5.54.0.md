---
aliases:
- ../../kde-frameworks-5.54.0
date: 2019-01-12
layout: framework
libCount: 70
---
### Attica

- Notificação se um fornecedor predefinido não conseguir ser transferido

### Baloo

- Mover o utilitário 'typesForMimeType' do BasicIndexingJob para um espaço de nomes anónimo
- Adição do "image/svg" como Type::Image ao BasicIndexingJob
- Uso da formatação JSON compacta para guardar os meta-dados dos documentos

### Ícones Breeze

- Mudança do 'preferences-system-network' para uma ligação simbólica
- Uso de um ícone do Kolf com um fundo e sombreado bonitos
- Cópia de alguns ícones alterados do Brisa para o Brisa Escuro
- Adição dos ícones do YaST e de novas preferências
- Adição de um ícone 'python-bytecode' adequado, uso de cores consistentes nos ícones do Python (erro 381051)
- Remoção das ligações simbólicas de 'bytecodes' de Python como preparação para terem ícones próprios
- Transformação do ícone do tipo MIME do Python como base e do ícone de 'bytecodes' de Python como uma ligação para o mesmo
- Adição de ícones de dispositivos para portas RJ11 e RJ45
- Adição do separador de pastas em falta (erro 401836)
- Uso de um ícone correcto para os programas em Python 3 (erro 402367)
- Mudança dos ícones a cores da rede/Web para um estilo consistente
- Adição de um novo nome para os ficheiros SQLite, para que os ícones apareçam de facto (erro 402095)
- Adição dos ícones 'drive-*' para o Gestor de Partições do YaST
- Adição do ícone 'view-private' (erro 401646)
- Adição de ícones de acção da lanterna
- Melhoria do simbolismo para os ícones de estado desligado e silenciado (erro 398975)

### Módulos extra do CMake

- Adição de pesquisa do módulo da 'libphonenumber' da Google

### KActivities

- Correcção da versão no ficheiro 'pkgconfig'

### Ferramentas Doxygen do KDE

- Correcção do desenho de Markdown do Doxygen

### KConfig

- Escape dos 'bytes' que sejam maiores que 127 nos ficheiros de configuração

### KCoreAddons

- Macros do CMake: Migração da variável obsoleta ECM do kcoreaddons_add_plugin (erro 401888)
- Possibilidade de traduzir as unidades e os prefixos do 'formatValue'

### KDeclarative

- não mostrar separadores nos dispositivos móveis
- 'root.contentItem' em vez de ter apenas 'contentItem'
- Adição da API em falta para os KCM's multi-nível para controlar as colunas

### KFileMetaData

- Falha no teste de escrita se o tipo MIME não for suportado pelo módulo de extracção
- Correcção da extracção do número do disco no APE
- Implementação da extracção de capas dos ficheiros ASF
- Extensão da lista de tipos MIME suportados para a extracção de imagens incorporadas
- Remodelação da extracção de imagens incorporadas para uma melhor capacidade de extensão
- Adição do tipo MIME 'wav' em falta
- Extracção de mais marcas dos meta-dados EXIF (erro 341162)
- Correcção da extracção da altitude do GPS para os dados EXIF

### KGlobalAccel

- Correcção da compilação do KGlobalAccel com a versão pré-lançamento do Qt 5.13

### KHolidays

- README.md - adição de instruções básicas para testar os ficheiros de feriados
- diversos calendários - correcção dos erros de sintaxe

### KIconThemes

- ksvg2icns : uso da API de QTemporaryDir do Qt 5.9+

### KIdleTime

- [xscreensaverpoller] Limpeza após reiniciar o protector de ecrã

### KInit

- Uso de um 'rlimit' leve para o número de descritores abertos. Isto corrige o arranque muito lento do Plasma com a última versão do 'systemd'.

### KIO

- Reversão do "Esconder a antevisão do ficheiro quando o ícone é demasiado pequeno"
- Mostrar um erro em vez de falhar de forma silenciosa ao pedir para criar uma pasta que já exista (erro 400423)
- Mudança da localização de cada item das sub-pastas numa mudança de nome da pasta (erro 401552)
- Extensão da filtragem de expressões regulares do 'getExtensionFromPatternList'
- [KRun] quando se pede para abrir uma ligação num navegador externo, recair no 'mimeapps.list' se nada estiver definido no 'kdeglobals' (erro 100016)
- Tornar a funcionalidade de abertura de URL numa página mais fácil de encontrar (erro 402073)
- [kfilewidget] Devolução de um navegador de URL's editável para o modo de selecção, caso esteja em primeiro plano e esteja tudo seleccionado e quando se carrega em Ctrl+L
- [KFileItem] Correcção da verificação 'isLocal' no 'checkDesktopFile' (erro 401947)
- SlaveInterface: Parar o 'speed_timer' após matar uma tarefa
- Avisar o utilizador antes da tarefa de cópia/movimento caso o tamanho do ficheiro ultrapasse o tamanho máximo dos ficheiros nos sistemas de ficheiros FAT32 (4GB) (erro 198772)
- Evitar aumentar de forma constante a fila de eventos do Qt nos 'KIO slaves'
- Suporte para o TLS 1.3 (parte do Qt 5.12)
- [KUrlNavigator] Correcção do 'firstChildUrl' ao voltar do pacote

### Kirigami

- Certificação de que não é substituído o QIcon::themeName quando não é suposto
- Introdução de um objecto DelegateRecycler em anexo
- correcção das margens da grelha, tendo em consideração as barras de deslocamento
- Capacidade de o AbstractCard.background reagir ao AbstractCard.highlighted
- Simplificação do código no MnemonicAttached
- SwipeListItem: mostrar sempre os ícones se '!supportsMouseEvents'
- Consideração se o 'needToUpdateWidth' se baseia no 'widthFromItem' e não no 'height'
- Ter a barra de deslocamento em conta na margem do ScrollablePage (erro 401972)
- Não tentar posicionar de novo a ScrollView quando tiver uma altura com problemas (erro 401960)

### KNewStuff

- Mudança da ordenação predefinida na janela de transferências os "Mais transferidos" (erro 399163)
- Notificação se o fornecedor não for carregado

### KNotification

- [Android] Falha mais ordeira ao compilar com a API &lt; 23
- Adição da infra-estrutura de notificações do Android
- Compilação sem o Phonon e o D-Bus no Android

### KService

- applications.menu: remoção da categoria não usada X-KDE-Edu-Teaching
- applications.menu: remoção do &lt;KDELegacyDirs/&gt;

### KTextEditor

- Correcção da programação para o Qt 5.12
- Correcção do programa 'emmet' ao usar números HEX em vez de OCT nos textos (erro 386151)
- Correcção de Emmet inválido (erro 386151)
- ViewConfig: Adição da opção 'Mudança de Linha Dinâmica em Marcação Estática'
- correcção do fim da região de dobragem e adição de código de fim no intervalo
- evitar uma pintura exagerada com o canal 'alfa'
- Não marcar de novo as palavras adicionadas/ignoradas do dicionário como erradas (erro 387729)
- KTextEditor: Adição de acção para a mudança de linha estática (erro 141946)
- Não esconder a acção 'Limpar os Intervalos do Dicionário'
- Não pedir uma confirmação ao carregar de novo (erro 401376)
- classe Message: Uso da inicialização de membros dentro da classe
- Exposição do KTextEditor::ViewPrivate:setInputMode(InputMode) ao KTextEditor::View
- Melhoria da performance das pequenas acções de edição, p.ex. correcção das grandes acções para substituição de tudo (erro 333517)
- Só invocar o updateView() no visibleRange() quando o endPos() for inválido

### KWayland

- Adição de esclarecimento sobre o uso tanto do ServerDecoration do KDE como o XdgDecoration
- Suporte para Decorações do XDG
- Correcção das instalações de ficheiros de inclusão Client do XDGForeign
- [servidor] Suporte para arrastamento por toque
- [servidor] Possibilidade de várias interfaces tácteis por cliente

### KWidgetsAddons

- [KMessageBox] Correcção do tamanho mínimo da janela quando são pedidos mais detalhes (erro 401466)

### NetworkManagerQt

- Adição das configurações do 'DCB', 'macsrc', 'match', 'tc', 'ovs-patch' e 'ovs-port'

### Plasma Framework

- [Calendário] Exposição do 'firstDayOfWeek' no MonthView (erro 390330)
- Adição do 'preferences-system-bluetooth-battery' ao preferences.svgz

### Purpose

- Adição de tipo de 'plugin' para a partilha de URL's

### QQC2StyleBridge

- Correcção da largura do item de menu quando o delegado é substituído (erro 401792)
- Rotação do indicador de ocupado no sentido dos ponteiros do relógio
- Obrigar as opções de marcação/exclusivas a serem quadradas

### Solid

- [UDisks2] Uso do MediaRemovable para determinar se é possível ejectar o dispositivo
- Suporte para baterias Bluetooth

### Sonnet

- Adição de método para o BackgroundChecker adicionar uma palavra à sessão
- DictionaryComboBox: Manter os dicionários preferidos pelo utilizador no topo (erro 302689)

### Realce de sintaxe

- Actualização do suporte da sintaxe do PHP
- WML: correcção do código de Lua incorporado &amp; uso dos novos estilos predefinidos
- Realce dos ficheiros .cu e .cuh do CUDA como C++
- TypeScript &amp; TS/JS React: melhoria na detecção dos tipos, correcção dos pontos flutuantes &amp; outras melhorias/correcções
- Haskell: Realce de comentários vazios a seguir ao 'import'
- WML: correcção de ciclo infinito na mudança de contextos &amp; só realçar as marcas com nomes válidos (erro 402720)
- BrightScript: Adição de alternativa para o realce do 'endsub' do QtCreator, adição de dobragem de funções/procedimentos
- suporte de mais variantes dos literais numéricos em C (erro 402002)

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
