---
aliases:
- ../../kde-frameworks-5.0
- ./5.0
customIntro: true
date: '2014-07-07'
description: O KDE Lança a Primeira Versão das Plataformas 5.
layout: framework
qtversion: 5.2
title: Primeira versão do KDE Frameworks 5
---
7 de julho de 2014. O KDE Community orgulha-se em anunciar o KDE Frameworks 5.0. Frameworks 5 é a próxima geração das bibliotecas do KDE, modulares e otimizadas para uma integração simples com aplicativos em Qt. Os Frameworks oferecem uma grande variedade de funcionalidades comuns em bibliotecas maduras, revistas e bem testadas com licenças amigáveis. Existem cerca de 50 Frameworks diferentes nesta versão, oferecendo soluções que incluem a integração com o hardware, suporte para formatos de arquivos, widgets adicionais, funções de gráficos, verificação ortográfica, entre outras. Muitos Frameworks são multiplataforma e têm pouca ou nenhuma dependência extra, tornando-os simples de compilar e adicionar a qualquer aplicativo em Qt.

KDE Frameworks representa um esforço para remodelar as poderosas bibliotecas do KDE Platform 4 em um conjunto de módulos independentes e multiplataforma, que estão disponíveis para todos os desenvolvedores, de forma a acelerar e reduzir o custo do desenvolvimento em Qt. Cada Frameworks é multiplataforma, bem documentado e testado e sua utilização será familiar para os programadores em Qt, seguindo o estilo e padrões impostos pelo Projeto Qt. Os Frameworks são desenvolvidos segundo o modelo de governança do KDE, com um calendário de versões previsível, um processo de contribuições claro, independente de fornecedores e com uma licença flexível (LGPL).

As Plataformas têm uma estrutura de dependências clara, dividida em Categorias e Níveis. As Categorias dizem respeito às dependências durante a execução:

- Os elementos <strong>funcionais</strong> não têm dependências durante a execução.
- A <strong>Integração</strong> define o código que poderá necessitar de dependências durante a execução para a integração, dependendo do que o SO ou a plataforma oferece.
- As <strong>Soluções</strong> têm dependências durante a execução obrigatórias.

Os <strong>Níveis</strong> referem-se às dependências durante a compilação de outras Plataformas. As Plataformas de Nível 1 não têm dependências dentro das Plataformas e só necessitam do Qt ou de outras bibliotecas relevantes. As Plataformas de Nível 2 só podem depender do Nível 1. As Plataformas de Nível 3 podem depender de outras Plataformas de Nível 3, assim como de Nível 2 e Nível 1.

A transição da Plataforma para o Frameworks está em andamento há cerca de 3 anos, guiada pelos principais desenvolvedores do KDE.
Aprenda mais sobre o Frameworks 5 <a href='http://dot.kde.org/2013/09/25/frameworks-5'>neste artigo do ano passado</a>.

## Destaques

Há mais de 50 Frameworks atualmente disponíveis. Consulte a lista completa na <a href='http://api.kde.org/frameworks-api/frameworks5-apidocs/'>documentação online da API</a>. Abaixo, disponibilizamos alguns exemplos de funcionalidades que os Frameworks oferecem aos desenvolvedores de aplicativos em Qt.

<strong>KArchive</strong> oferece suporte para muitos codecs de compressão populares, em uma biblioteca de extração e arquivamento independente, cheio de recursos e fácil de usar. Apenas alimente-a com arquivos; não há necessidade de reinventar uma função de arquivamento em seu aplicativo baseado no Qt!

<strong>ThreadWeaver</strong> oferece uma API de alto nível para gerenciar <i>threads</i> de processamento usando interfaces baseadas em tarefas e filas de execução. Permite o escalonamento simples da execução de <i>threads</i>, indicando as dependências entre elas e executando-as à medida que essas dependências forem satisfeitas, simplificando, de forma significativa, o uso de várias <i>threads</i>.

<strong>KConfig</strong> é um framework para lidar com armazenamento e consulta de definições de configuração. Oferece uma API orientada por grupos. Ele funciona com arquivos INI e pastas em cascata compatíveis com o XDG. Ele gera código com base em arquivos XML.

O <strong>Solid</strong> oferece a detecção de 'hardware' e poderá informar uma aplicação sobre os dispositivos e volumes de armazenamento, o processador, o estado da bateria, a gestão de energia, o estado e as interfaces da rede e o Bluetooth. Para as partições encriptadas, a gestão de energia e a rede, são necessários alguns serviços em execução.

O <strong>KI18n</strong> adiciona o suporte do Gettext às aplicações, tornando-se mais fácil integrar o fluxo de trabalho de traduções do Qt na infra-estrutura geral de traduções dos diversos projectos.
