---
aliases:
- ../../kde-frameworks-5.14.0
date: 2015-09-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Em vários frameworks

- Mudança do nome das classes privadas para evitar a exportação acidental

### Baloo

- Adição da interface org.kde.baloo ao objeto raiz, por motivo de compatibilidade
- Instalação de um org.kde.baloo.file.indexer.xml falso para corrigir a compilação do plasma-desktop 5.4
- Reorganização das interfaces D-Bus
- Uso dos metadados JSON no plugin do <i>kded</i> e correção do nome do plugin
- Criação de uma instância Database por processo (erro <a href='https://bugs.kde.org/show_bug.cgi?id=350247'>350247</a>)
- Evitar que o baloo_file_extractor seja finalizado durante o envio
- Geração de um arquivo de interface em XML usando o qt5_generate_dbus_interface
- Correções do monitor do Baloo
- Mudança da exportação da URL de arquivo para a <i>thread</i> principal
- Confirmação de que as configurações encadeadas são consideradas
- Não instalar o <i>namelink</i> nas bibliotecas privadas
- Instalação das traduções, detectado por Hrvoje Senjan

### BluezQt

- Não encaminhar o sinal <i>deviceChanged</i> após a remoção do dispositivo (erro <a href='https://bugs.kde.org/show_bug.cgi?id=351051'>351051</a>)
- Respeitar o -DBUILD_TESTING=OFF

### Módulos extra do CMake

- Adição de macro para gerar as declarações de categorias de registro do Qt5
- ecm_generate_headers: Adição da opção COMMON_HEADER e da funcionalidade de múltiplos cabeçalhos
- Adição do -pedantic no código do KF5 (ao usar o <i>gcc</i> ou o <i>clang</i>)
- KDEFrameworkCompilerSettings: ativar os iteradores restritos apenas no modo de depuração
- Ativação da visibilidade padrão para o código em C ocultar

### Integração do Framework

- Propagação dos títulos das janelas para as caixas de diálogo de arquivos apenas com pastas

### KActivities

- Só iniciar um carregador de ações (processamentos paralelos) quando as ações do FileItemLinkingPlugin não foram inicializadas (erro <a href='https://bugs.kde.org/show_bug.cgi?id=351585'>351585</a>)
- Correção dos problemas de compilação introduzidos ao renomear as classes Private (<a href='https://quickgit.kde.org/?p=kactivities.git&amp;a=commit&amp;h=11030ffc0'>11030ffc0</a>)
- Adição do caminho de inclusão do <i>boost</i> ausente na compilação no OS X
- A configuração dos atalhos foi movida para as configurações da atividade
- A configuração do modo privado da atividade agora funciona
- Remodelação da interface de configuração
- Os métodos básicos de atividades estão funcionais
- Interface das mensagens de configuração e exclusão de atividades
- Interface básica da seção de criação/exclusão/configuração de atividades no KCM
- Aumento do tamanho dos blocos para carregar os resultados
- Adição de uma inclusão ausente para o std::set

### Ferramentas Doxygen do KDE

- Correção no Windows: Remoção dos arquivos existentes antes de serem substituídos com o os.rename
- Usar os caminhos nativos ao chamar o Python para corrigir as compilações do Windows

### KCompletion

- Correção de um comportamento inválido / erro de memória no Windows (erro <a href='https://bugs.kde.org/show_bug.cgi?id=345860'>345860</a>)

### KConfig

- Otimização do readEntryGui
- Evitar o QString::fromLatin1() no código gerado
- Minimização das chamadas ao dispendioso QStandardPaths::locateAll()
- Término da migração para o QCommandLineParser (tem agora um addPositionalArgument)

### KDELibs 4 Support

- Migração do plugin solid-networkstatus do kded para os metadados em JSON
- KPixmapCache: Criação da pasta, caso não exista

### KDocTools

- Sincronização do <i>user.entities</i> Catalão com a versão em Inglês (en)
- Adicionado <i>entities</i> para 'sebas' e 'plasma-pa'

### KEmoticons

- Desempenho: Criação de uma instância do KEmoticons e não do KEmoticonsTheme

### KFileMetaData

- PlainTextExtractor: Ativação da ramificação O_NOATIME nas plataformas <i>libc</i> da GNU
- PlainTextExtractor: Fazer a ramificação Linux funcionar também sem o O_NOATIME
- PlainTextExtractor: Correção do erro na verificação de falhas no open(O_NOATIME)

### KGlobalAccel

- Só iniciar o kglobalaccel5 se for necessário

### KI18n

- Lidar de forma elegante com a ausência de fim de linha no fim do arquivo <i>pmap</i>

### KIconThemes

- KIconLoader: Correção do reconfigure(), ignorando os temas herdados e os pastas de aplicativos
- Melhor adesão à especificação do carregamento de ícones

### KImageFormats

- eps: correção das inclusões relacionadas com o Registro por Categorias do Qt

### KIO

- Uso do Q_OS_WIN em vez do Q_OS_WINDOWS
- Fazer o KDE_FORK_SLAVES funcionar no Windows
- Desativação da instalação do arquivo <i>desktop</i> do módulo ProxyScout do <i>kded</i>
- Fornecer uma ordenação determinista para o KDirSortFilterProxyModelPrivate::compare
- Mostrar novamente os ícones das pastas personalizadas (erro <a href='https://bugs.kde.org/show_bug.cgi?id=350612'>350612</a>)
- Mover o <i>kpasswdserver</i> do <i>kded</i> para o <i>kiod</i>
- Correção dos erros de migração no <i>kpasswdserver</i>
- Remoção de código obsoleto para falar com versões muito antigas do <i>kpasswdserver</i>
- KDirListerTest: Uso do QTRY_COMPARE em ambas as instruções, para corrigir um erro de concorrência demonstrado pelo CI
- KFilePlacesModel: Implementação da antiga tarefa pendente sobre o uso do <i>trashrc</i>, em vez de um KDirLister completo

### KItemModels

- Novo modelo proxy: KConcatenateRowsProxyModel
- KConcatenateRowsProxyModelPrivate: Correção do tratamento do <i>layoutChanged</i>
- Mais verificações sobre a seleção após a ordenação
- KExtraColumnsProxyModel: Correção de um erro no sibling() que danificou, por exemplo, as seleções

### Package Framework

- O <i>kpackagetool</i> pode desinstalar um pacote a partir do seu arquivo respectivo
- O <i>kpackagetool</i> é agora mais inteligente para encontrar o tipo de serviço correto

### KService

- KSycoca: Verificação das datas/horas e execução do <i>kbuildsycoca</i> se for necessário. Não há mais dependências do <i>kded</i>
- Não fechar o <i>ksycoca</i> logo após a sua abertura
- O KPluginInfo agora lida corretamente com os metadados do FormFactor

### KTextEditor

- União da alocação do <i>TextLineData</i> com o bloco de contagem de referências
- Correção do atalho de teclado padrão para "ir para a linha de edição anterior"
- Correção dos comentários no realce de sintaxe de Haskell
- Melhoria de desempenho para apresentação da mensagem de completação de código
- minimap: Tentativa de melhora na aparência e comportamento (erro <a href='https://bugs.kde.org/show_bug.cgi?id=309553'>309553</a>)
- Comentários encadeados no realce de sintaxe do Haskell
- Correção do problema com a incorreta remoção do recuo no Python (erro <a href='https://bugs.kde.org/show_bug.cgi?id=351190'>351190</a>)

### KWidgetsAddons

- KPasswordDialog: Permissão para o usuário alterar a visibilidade da senha (erro <a href='https://bugs.kde.org/show_bug.cgi?id=224686'>224686</a>)

### KXMLGUI

- Correção do KSwitchLanguageDialog que não mostrava vários idiomas

### KXmlRpcClient

- Evitar o uso do QLatin1String sempre que alocar memória de dados

### ModemManagerQt

- Correção do conflito de metadados com a última alteração do <i>nm-qt</i>

### NetworkManagerQt

- Adição de novas propriedades das últimas versões do NM

### Plasma Framework

- Mudar novamente para intermitente, se possível
- Correção da listagem de pacotes
- Plasma: Correção em que as ações do miniaplicativo podem ser um ponteiro nulo (erro <a href='https://bugs.kde.org/show_bug.cgi?id=351777'>351777</a>)
- O sinal <i>onClicked</i> do PlasmaComponents.ModelContextMenu agora funciona corretamente
- O PlasmaComponents.ModelContextMenu pode agora criar seções do tipo Menu
- Migração do plugin <i>platformstatus</i> do <i>kded</i> para metadados em JSON...
- Tratamento de metadados inválidos no PluginLoader
- Permissão ao RowLayout para descobrir o tamanho da legenda de texto
- Sempre mostrar o menu de edição quando o cursor estiver visível
- Correção de <i>loop</i> no ButtonStyle
- Não alterar o modo plano de um botão quando for clicado
- As barras de rolagem em telas sensíveis ao toque ou dispositivos móveis são transitórias
- Ajuste na velocidade e aceleração da intermitência à resolução DPI
- Delegação de cursores personalizada apenas em dispositivos móveis
- Cursor de texto amigável em telas sensíveis ao toque
- Correção dos itens-pai e política e aparição
- Declaração do __editMenu
- Adição das delegações de tratamento de cursor ausente
- Reescrita da implementação do EditMenu
- Uso do menu móvel apenas de forma condicional
- Mudança do item-pai do menu para o topo

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
