---
aliases:
- ../announce-applications-16.04-rc
custom_spread_install: true
date: 2016-04-05
description: O KDE disponibiliza a versão Release Candidate do KDE Applications 16.04
layout: application
release: applications-16.03.90
title: O KDE disponibiliza a versão Release Candidate do KDE Applications 16.04
---
7 de abril de 2016. Hoje o KDE disponibilizou o Release Candidate da nova versão do KDE Applications. Com as dependências e funcionalidades estabilizadas, o foco das equipes do KDE agora é a correção de erros e pequenos ajustes.

Com diversos aplicativos sendo preparados para o KDE Frameworks 5, as versões do KDE Applications 16.04 tem qualidade e experiência do usuário. Precisamos dos usuários atuais para manter a alta qualidade do KDE, porque os desenvolvedores simplesmente não conseguem testar todas as configurações possíveis. Contamos com você para nos ajudar a encontrar erros antecipadamente, para que possam ser corrigidos antes da versão final. Por favor, considere juntar-se à equipe, instalando esta versão e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.

#### Instalando os pacotes binários do KDE Applications 16.04 Release Candidate

<em>Pacotes</em>. Alguns distribuidores de SO's Linux/UNIX forneceram simpaticamente alguns pacotes binários do 16.04 RC (internamente 16.03.90) para algumas versões das suas distribuições e, em alguns casos, outros voluntários da comunidade também o fizeram. Os pacotes binários adicionais, assim como as actualizações dos pacotes agora disponíveis, poderão aparecer nas próximas semanas.

<em>Localizações dos Pacotes</em>. Para uma lista actualizada dos pacotes binários disponíveis, dos quais a Equipa de Versões do KDE foi informada, visite por favor o <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Wiki da Comunidade</a>.

#### Compilando o KDE Applications 16.04 Release Candidate

Poderá <a href='http://download.kde.org/unstable/applications/16.03.90/src/'>transferir à vontade</a> o código-fonte completo do 16.04 RC (pré-lançamento). As instruções de compilação e instalação estão disponíveis na <a href='/info/applications/applications-16.03.90'>Página de Informações das Aplicações do KDE RC</a>.
