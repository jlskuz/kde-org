---
aliases:
- ../announce-applications-19.04.0
changelog: true
date: 2019-04-18
description: O KDE Lança as Aplicações do KDE 19.04.
layout: application
release: applications-19.04.0
title: O KDE disponibiliza o KDE Applications 19.04.0
version: 19.04.0
version_number: 19.04.0
version_text: '19.04'
---
{{%youtube id="1keUASEvIvE"%}}

{{% i18n_date %}}

A comunidade do KDE tem o prazer de anunciar o lançamento das Aplicações do KDE 19.04.

A nossa comunidade trabalha continuamente na melhoria das aplicações incluídas na nossa série de Aplicações do KDE. Em conjunto com as novas funcionalidades, melhorámos o visual, a usabilidade e a estabilidade de todos os nossos utilitários, jogos e ferramentas de criatividade. O nosso objectivo é tornar a sua vida mais fácil, tornando as aplicações do KDE mais agradáveis de usar. Esperemos que goste de todas as novas melhorias e correcções de erros que encontrar no 19.04!

## O que há de novo nas Aplicações do KDE 19.04

Foram resolvidos mais de 150 erros. Estas correcções reimplementam funcionalidades desactivadas, normalizam atalhos e resolvem estoiros, tornando as Aplicações do KDE mais amigáveis e permitindo-lhe ser mais produtivo.

### Gerenciador de arquivos

{{<figure src="/announcements/applications/19.04.0/app1904_dolphin01.png" width="600px" >}}

O <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a> é o gestor de ficheiros do KDE. Também se liga a serviços de rede, como o SSH, o FTP e o Samba, vindo com ferramentas avançadas para procurar e organizar os seus dados.

Funcionalidades novas:

+ Expandimos o suporte para miniaturas, pelo que o Dolphin consegue agora mostrar miniaturas para diversos tipos de ficheiros novos: ficheiros do <a href='https://phabricator.kde.org/D18768'>Microsoft Office</a>, ficheiros de <a href='https://phabricator.kde.org/D18738'>eBooks em .epub e .fb2</a>, ficheiros do <a href='https://bugs.kde.org/show_bug.cgi?id=246050'>Blender</a> e ficheiros <a href='%4'>PCX</a>. Adicionalmente, as miniaturas dos ficheiros de texto agora mostram o <a href='%5'>realce de sintaxe</a> do texto dentro da miniatura. </li>
+ Poderá agora escolher <a href='https://bugs.kde.org/show_bug.cgi?id=312834'>qual a área dividida a fechar</a> quando carregar no botão para 'Fechar a área dividida'. </li>
+ Esta versão do Dolphin introduz uma <a href='https://bugs.kde.org/show_bug.cgi?id=403690'>colocação de separadores de páginas mais inteligente</a>. Quando abrir uma pasta numa nova página, a nova página será agora colocada imediatamente à direita da actual, em vez de ser sempre no fim da barra de páginas. </li>
+ A <a href='https://phabricator.kde.org/D16872'>marcação de itens</a> agora é muito mais prática, dado que as marcas podem ser adicionadas ou removidas com o menu de contexto. </li>
+ <a href='https://phabricator.kde.org/D18697'>Melhorámos os parâmetros predefinidos de ordenação</a> para algumas pastas mais usadas. Por omissão, a pasta de Transferências está agora ordenada por datas com o agrupamento activo e a área de Documentos Recentes (acessível em recentdocuments:/) está ordenada por data com a vista em lista escolhida. </li>

As correções de erros incluem:

+ Ao usar uma versão mais moderna do protocolo SMB, poderá agora <a href='https://phabricator.kde.org/D16299'>descobrir as partilhas do Samba</a> para as máquinas de Mac e Linux. </li>
+ <a href='https://bugs.kde.org/show_bug.cgi?id=399430'>A reorganização dos itens no painel de Locais</a> volta a funcionar de novo quando alguns dos itens ficam escondidos. </li>
+ Depois de abrir uma página nova no Dolphin, é essa página nova que ganha automaticamente o <a href='https://bugs.kde.org/show_bug.cgi?id=401899'>foco do teclado</a>. </li>
+ O Dolphin agora avisa-o quando tentar sair enquanto o <a href='https://bugs.kde.org/show_bug.cgi?id=304816'>painel do terminal estiver aberto</a> com um programa em execução dentro dele. </li>
+ Corrigimos muitas fugas de memória, melhorando a performance global do Dolphin.</li>

O <a href='https://cgit.kde.org/audiocd-kio.git/'>AudioCD-KIO</a> permite a outras aplicações do KDE lerem o áudio dos CD's e convertê-lo automaticamente para outros formatos.

+ O AudioCD-KIO agora suporta a extracção para <a href='https://bugs.kde.org/show_bug.cgi?id=313768'>Opus</a>. </li>
+ Tornámos a <a href='https://bugs.kde.org/show_bug.cgi?id=400849'>informação de texto do CD</a> realmente transparente para a visualização. </li>

### Edição de Vídeo

{{<figure src="/announcements/applications/19.04.0/app1904_kdenlive.png" width="600px" >}}

Esta é uma versão importante para o editor de vídeo do KDE. O <a href='https://kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> passou por uma remodelação extensa do seu código de base, dado que mais de 60% do seu código interno foi alterado, melhorando a sua arquitectura global.

As melhorias incluem:

+ A linha temporal foi remodelada para tirar partido do QML.
+ Quando colocar um 'clip' na linha temporal, o áudio e o vídeo irão sempre para faixas separadas.
+ A linha temporal agora suporta a navegação pelo teclado: poderá mover os 'clips', composições e imagens-chave com o teclado. De igual forma, a altura das faixas em si é ajustável.
+ Nesta versão do Kdenlive, a gravação do áudio na faixa vem com uma nova funcionalidade de <a href='https://bugs.kde.org/show_bug.cgi?id=367676'>sobreposição de voz</a>.
+ Melhorámos a cópia/colagem: funciona entre janelas de projectos diferentes. A gestão de 'clips' indirectos também foi melhorada, dado que os 'clips' agora podem ser apagados de forma individual.
+ A versão 19.04 vê o regresso do suporte para os monitores externos BlackMagic e existem também novos guias de predefinições no monitor.
+ Melhorámos o tratamento das imagens-chave, dando-lhe um aspecto e funcionamento mais consistente. O gerador de títulos também foi melhorado, fazendo com que os botões de alinhamento se ajustem a áreas seguras, adicionando guias e cores de fundo configuráveis e mostrando os itens em falta.
+ Corrigimos o erro de destruição da linha temporal, o qual perdia ou colocava em sítios errados os 'clips', sendo este erro desencadeado quando movia um grupo de 'clips'.
+ Corrigimos o erro de imagem do JPG que desenhava as imagens como ecrãs brancos no Windows. Também corrigimos os erros que afectavam a captura do ecrã no Windows.
+ À parte de tudo acima, adicionámos muitas pequenas melhorias de usabilidade que tornarão o uso do Kdenlive mais fácil e suave.

### Escritório

{{<figure src="/announcements/applications/19.04.0/app1904_okular.png" width="600px" >}}

O <a href='https://kde.org/applications/graphics/okular/'>Okular</a> é o visualizador multifunções do KDE. É ideal para ler e anotar PDF's, também pode abrir ficheiros ODF (usados pelo LibreOffice e OpenOffice), os livros electrónicos publicados em ficheiros ePub, a maioria dos ficheiros de banda desenhada comuns, ficheiros em PostScript, entre muitos outros.

As melhorias incluem:

+ Para o ajudar a garantir que os seus documentos ficam perfeitamente ajustados, adicionámos <a href='https://bugs.kde.org/show_bug.cgi?id=348172'>opções de escala</a> à janela de Impressão do Okular.
+ O Okular agora suporta a visualização e verificação de <a href='https://cgit.kde.org/okular.git/commit/?id=a234a902dcfff249d8f1d41dfbc257496c81d84e'>assinaturas digitais</a> nos ficheiros PDF.
+ Graças à integração melhorada entre aplicações, o Okular agora suporta a edição de documentos em LaTeX no <a href='https://bugs.kde.org/show_bug.cgi?id=404120'>TexStudio</a>.
+ Um suporte melhorado para a <a href='https://phabricator.kde.org/D18118'>navegação em ecrãs tácteis</a> significa que será capaz de se mover para trás e para a frente com um ecrã táctil quando estiver no modo de Apresentação.
+ Os utilizadores que preferirem manipular os documentos a partir da linha de comandos serão capazes de efectuar <a href='https://bugs.kde.org/show_bug.cgi?id=362038'>pesquisas de texto</a> inteligentes com a nova opção da linha de comandos que lhe permite abrir um documento e realçar todas as ocorrências de um dado pedaço de texto.
+ O Okular mostra agora de forma adequada as ligações nos <a href='https://bugs.kde.org/show_bug.cgi?id=403247'>documentos em Markdown</a> que se propagam em mais que uma linha.
+ As <a href='https://bugs.kde.org/show_bug.cgi?id=397768'>ferramentas de recorte</a> têm novos ícones bonitos.

{{<figure src="/announcements/applications/19.04.0/app1904_kmail.png" width="600px" >}}

O <a href='https://kde.org/applications/internet/kmail/'>KMail</a> é o cliente de e-mail do KDE para a protecção da sua privacidade. Fazendo parte do <a href='https://kde.org/applications/office/kontact/'>pacote de 'groupware' Kontact</a>, o KMail suporta todos os sistemas de e-mail e permite-lhe organizar as suas mensagens numa caixa de entrada virtual e partilhada ou em contas separadas (à sua escolha). Suporta todos os tipos de codificação e assinatura de mensagens e permite-lhe partilhar os dados, como os contactos, as datas de reuniões e as informações de viagens com outras aplicações do Kontact.

As melhorias incluem:

+ Chega para lá, Grammarly! Esta versão do KMail vem com suporte para o 'languagetools' (verificação gramatical) e o 'grammalecte' (verificação gramatical apenas em Francês).
+ Os números de telefone nos e-mails são agora detectados e podem ser marcados directamente através do <a href='https://community.kde.org/KDEConnect'>KDE Connect</a>.
+ O KMail agora tem uma opção para <a href='https://phabricator.kde.org/D19189'>iniciar directamente na bandeja do sistema</a> sem abrir a janela principal.
+ Foi melhorado o 'plugin' de suporte para Markdown.
+ A recepção de e-mails por IMAP já não bloqueia mais quando falha a autenticação.
+ Também fizemos diversas correcções na infra-estrutura do KMail Akonadi, para melhorar a fiabilidade e a performance.

O <a href='https://kde.org/applications/office/korganizer/'>KOrganizer</a> é o componente de calendário do Kontact, fazendo a gestão de todos os seus eventos.

+ Os eventos recorrentes do <a href='https://bugs.kde.org/show_bug.cgi?id=334569'>Google Calendar</a> são agora sincronizados de novo correctamente.
+ A janela de chamada de atenção de eventos agora recorda que é para <a href='https://phabricator.kde.org/D16247'>aparecer em todos os ecrãs</a>.
+ Foi tornada mais moderna a aparência das <a href='https://phabricator.kde.org/T9420'>áreas de eventos</a>.

O <a href='https://cgit.kde.org/kitinerary.git/'>Kitinerary</a> é o novo assistente de viagens do Kontact que o ajudará a obter a sua localização e aconselhá-lo durante o seu percurso.

- Existe uma nova extracção genérica para bilhetes RCT2 (p.ex. usados pelas empresas ferroviárias como a DSB, ÖBB, SBB, NS).
- A detecção e remoção de ambiguidades nos nomes dos aeroportos foi bastante melhorada.
- Adicionámos diversos módulos de extracção personalizados para fornecedores anteriormente não suportados (p.ex., BCD Travel, Grupo NH), e melhorámos as variantes de formatos/línguas dos fornecedores já suportados (p.ex., SNCF, Easyjet, Booking.com, Hertz).

### Desenvolvimento

{{<figure src="/announcements/applications/19.04.0/app1904_kate.png" width="600px" >}}

O <a href='https://kde.org/applications/utilities/kate/'>Kate</a> é o editor de texto completo do KDE e é ideal para programação, graças a funcionalidades como as páginas, modo de ecrãs divididos, realce de sintaxe, um terminal incorporado, completação de palavras, pesquisa e substituição de expressões regulares e muito mais através da infra-estrutura flexível de 'plugins'.

As melhorias incluem:

- O Kate consegue agora mostrar todos os caracteres de espaços invisíveis, e não apenas alguns.
- Poderá activar e desactivar facilmente a Mudança de Linha Estática, usando o seu próprio item de menu, para cada documento, sem ter de modificar a configuração global.
- Os menus de contexto dos ficheiros e páginas agora incluem um conjunto de novas acções úteis, como o Mudar o Nome, Apagar, Abrir a Pasta Respectiva, Copiar a Localização do Ficheiro, Comparar [com outro ficheiro aberto] e Propriedades.
- Esta versão do Kate vem com mais 'plugins' activados por omissão, incluindo a funcionalidade conhecida e útil do Terminal incorporado.
- Ao sair, o Kate não lhe pede mais para confirmar os ficheiros que foram modificados por outro processo, como uma mudança no controlo de versões.
- A árvore de 'plugins' agora mostra correctamente todos os itens de menu para os elementos do Git que tenham tremas (") nos seus nomes.
- Ao abrir vários ficheiros com a linha de comandos, os ficheiros são abertos nas páginas novas pela ordem com que foram indicados na linha de comandos.

{{<figure src="/announcements/applications/19.04.0/app1904_konsole.png" width="600px" >}}

O <a href='https://kde.org/applications/system/konsole/'>Konsole</a> é o emulador de terminal do KDE. Suporta páginas, fundos translúcidos, modo de ecrã dividido, esquemas de cores personalizados e atalhos de teclado, favoritos de pastas e SSH, entre muitas outras coisas.

As melhorias incluem:

+ A gestão de páginas recebeu um conjunto de melhorias que o ajudarão a ser mais produtivo. Poderá criar as novas páginas se carregar no botão do meio em zonas em branco da barra de páginas; existe também uma opção que lhe permite fechar as páginas com o botão do meio sobre elas. Os botões de fecho aparecem nas paǵinas por omissão e os ícones só aparecerão se usar um perfil com um ícone personalizado. Por último, o atalho Ctrl+Tab permite-lhe mudar rapidamente entre a página anterior e a seguinte.
+ A janela para Editar o Perfil recebeu uma grande <a href='https://phabricator.kde.org/D17244'>reorganização da interface do utilizador</a>.
+ O esquema de cores Brisa agora é usado como esquema de cores predefinido, sendo que foram melhorados o seu contraste e consistência com o tema Brisa do sistema.
+ Foram resolvidos os problemas ao mostrar texto em negrito.
+ O Konsole agora mostra correctamente o cursor de estilo sublinhado.
+ melhorada Foi a apresentação dos <a href='https://bugs.kde.org/show_bug.cgi?id=402415'>caracteres de caixa e linha</a>, assim como os <a href='https://bugs.kde.org/show_bug.cgi?id=401298'>caracteres Emoji</a>.
+ Os atalhos de mudança de perfil agora mudam o perfil da página actual em vez de abrir uma nova página com o outro perfil.
+ As páginas inactivas que receberem uma notificação e tiverem a cor do texto do seu título alteradas agora repõem a cor do texto do título das páginas normais quando for activada a página.
+ A funcionalidade para 'Variar o fundo para cada página' agora funciona quando a cor de fundo é muito escura ou é preta.

O <a href='https://kde.org/applications/development/lokalize/'>Lokalize</a> é um sistema de traduções auxiliado pelo computador que se foca na produtividade e garantia de qualidade. Foca-se na tradução de aplicações, mas também integra ferramentas de conversão externas para traduzir documentos do Office.

As melhorias incluem:

- O Lokalize agora suporta a visualização do código da tradução com um editor personalizado.
- Melhorámos a localização dos DockWidgets, bem como a forma como a configuração é gravada e reposta.
- A posição nos ficheiros .po agora é preservada quando filtrar as mensagens.
- Corrigimos um conjunto de erros na GUI (comentários dos programadores, botão da RegExp na substituição em massa, o número de mensagens vazias aproximadas, …).

### Utilitários

{{<figure src="/announcements/applications/19.04.0/app1904_gwenview.png" width="600px" >}}

O <a href='https://kde.org/applications/graphics/gwenview/'>Gwenview</a> é um visualizador e organizador de imagens avançado com ferramentas de edição intuitivas e fáceis de usar.

As melhorias incluem:

+ Esta versão do Gwenview que vem com as Aplicações 19.04 inclui o <a href='https://phabricator.kde.org/D13901'>suporte total para ecrãs tácteis</a>, com gestos para deslizar, ampliar, deslocar, entre outros.
+ Outra melhoria adicionada ao Gwenview é o <a href='https://bugs.kde.org/show_bug.cgi?id=373178'>suporte total para High DPI</a>, o que fará com que as imagens pareçam excelentes em ecrãs de alta resolução.
+ Foi melhorado o suporte para os <a href='https://phabricator.kde.org/D14583'>botões do rato para avançar e recuar</a>, que lhe permitem navegar entre imagens ao carregar nesses botões.
+ Também poderá usar o Gwenview para abrir os ficheiros de imagens criados com o <a href='https://krita.org/'>Krita</a> – a ferramenta de pintura digital favorita de todos.
+ O Gwenview agora suporta <a href='https://phabricator.kde.org/D6083'>miniaturas gigantes de 512 px</a>, permitindo-lhe antever as suas imagens de forma mais simples.
+ O Gwenview agora usa o <a href='https://bugs.kde.org/show_bug.cgi?id=395184'>atalho-padrão de teclado Ctrl+L</a> para passar o foco para o campo do URL.
+ Poderá agora usar a <a href='https://bugs.kde.org/show_bug.cgi?id=386531'>funcionalidade para Filtrar por Nome</a> com o atalho Ctrl+I, como acontece no Dolphin.

{{<figure src="/announcements/applications/19.04.0/app1904_spectacle.jpg" width="600px" >}}

O <a href='https://kde.org/applications/graphics/spectacle/'>Spectacle</a> é a aplicação de capturas do ecrã no Plasma. Poderá capturar ambientes de trabalho completos que ocupam vários ecrãs, ecrãs individuais, janelas, secções de janelas ou mesmo regiões personalizadas com a funcionalidade de selecção rectangular.

As melhorias incluem:

+ Alargámos o modo de Região Rectangular com algumas opções novas. Pode ser configurado para <a href='https://bugs.kde.org/show_bug.cgi?id=404829'>aceitar automaticamente</a> a caixa arrastada em vez de lhe pedirmos para a ajustar primeiro. Existe também uma opção nova predefinida para recordar a <a href='https://phabricator.kde.org/D19117'>região rectangular</a> actual, mas apenas até fechar o programa.
+ Poderá configurar o que acontece quando o atalho de captura da fotografia é carregado <a href='https://phabricator.kde.org/T9855'>enquanto o Spectacle já estiver em execução</a>.
+ O Spectacle permite-lhe alterar o <a href='https://bugs.kde.org/show_bug.cgi?id=63151'>nível de compressão</a> para formatos de imagens com perdas.
+ A configuração da gravação mostra-lhe <href='https://bugs.kde.org/show_bug.cgi?id=381175'>como ficará o nome de um ficheiro de fotografia</a>. Também poderá afinar facilmente o <a href='https://bugs.kde.org/show_bug.cgi?id=390856'>modelo de nomes de ficheiros</a> de acordo com as suas preferências, carregando para tal nos itens de substituição.
+ O Spectacle já não mostra mais as opções “Ecrã Completo (Todos os Monitores)” e “Ecrã Actual” quando o seu computador só tiver um ecrã.
+ O texto de ajuda no modo da Região Rectangular agora aparece no meio do ecrã principal, em vez de aparecer dividido entre os ecrãs.
+ Quando executado no Wayland, o Spectacle só inclui as funcionalidades que funcionam.

### Jogos e Educação

{{<figure src="/announcements/applications/19.04.0/app1904_kmplot.png" width="600px" >}}

A nossa série de aplicações inclui diversos <a href='https://games.kde.org/'>jogos</a> e <a href='https://edu.kde.org/'>aplicações educativas</a>.

O <a href='https://kde.org/applications/education/kmplot'>KmPlot</a> é um desenhador de funções matemáticas. Tem um processador incorporado poderoso. Os gráficos podem ser coloridos e a visualização é escalável, o que lhe permite ampliar até ao nível que precisa. Os utilizadores conseguem desenhar funções diferentes em simultâneo e combiná-las para criar funções novas.

+ Poderá agora ampliar se carregar no Ctrl enquanto usa a <a href='https://bugs.kde.org/show_bug.cgi?id=159772'>roda do rato</a>.
+ Nesta versão do Kmplot introduz a funcionalidade de <a href='https://phabricator.kde.org/D17626'>antevisão da impressão</a>.
+ O valor da raiz ou o par (x,y) podem agora ser copiados para a <a href='https://bugs.kde.org/show_bug.cgi?id=308168%'>área de transferência</a>.

O <a href='https://kde.org/applications/games/kolf/'>Kolf</a> é um jogo de golfe em miniatura.

+ Foi reposto o <a href='https://phabricator.kde.org/D16978'>suporte para som</a>.
+ O Kolf foi migrado com sucesso a partir das 'kdelibs4'.
