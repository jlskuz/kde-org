---
aliases:
- ../announce-applications-17.08.0
changelog: true
date: 2017-08-17
description: O KDE disponibiliza o KDE Applications 17.08.0
layout: application
title: O KDE disponibiliza o KDE Applications 17.08.0
version: 17.08.0
---
17 de agosto de 2017. O KDE Applications 17.08 chegou. Nós nos esforçamos em tornar tanto os aplicativos quanto as bibliotecas mais estáveis e fáceis de usar. Ao corrigir esses problemas e ouvir o seu feedback, nós conseguimos tornar os aplicativos do KDE muito mais amigáveis e menos suscetíveis a glitches. Aproveite seus novos apps!

### Mais migrações para o KDE Frameworks 5

Estamos contentes em anunciar que os seguintes aplicativos baseados no kdelibs4 foram migrados para o KDE Frameworks 5: kmag, kmousetool, kgoldrunner, kigo, konquest, kreversi, ksnakeduel, kspaceduel, ksudoku, kubrik, lskat e umbrello. Somos muito gratos pelos esforçados desenvolvedores que doaram seu tempo e suor para tornar isso possível.

### O que há de novo nas Aplicações do KDE 17.08

#### Dolphin

Os desenvolvedores do Dolphin relatam que o Dolphin agora exibe a data em que o documento foi deletado na Lixeira, assim como a Data de Criação caso o sistema operacional tenha suporte a isso, como é o caso dos BSDs.

#### KIO-Extras

O Kio-Extras agora fornece suporte melhorado aos compartilhamentos do Samba.

#### KAlgebra

Os desenvolvedores do KAlgebra melhoraram a interface Kirigami dele no desktop e implementaram a função autocompletar código.

#### Kontact

- No KMailtransport, os desenvolvedores reativaram o suporte ao akonadi transport, deram a possibilidade de criar plugins e recriaram o suporte ao sendmail mail transport.
- No SieveEditor, vários bugs dos scripts autocreate foram consertados e resolvidos. Além disso, um editor linear de regexp foi adicionado.
- No KMail, a função de abrir em editor externo foi reimplementada como extensão.
- O assistente de importação do Akonadi agora inclui o 'conversor converter tudo' como extensão, de modo a tornar mais fácil para desenvolvedores criarem novos conversores.
- Os aplicativos do KDE agora dependem do Qt 5.7. Os desenvolvedores consertaram vários erros de compilação no Windows. Nada do kdepim é capaz de ser compilado no Windows ainda, mas os desenvolvedores avançaram bastante nesse quesito. Pra começar, eles criaram instruções de construção para isso. Vários bugs foram reparados para modernizar o código (C++11). Suporte ao Wayland no Qt 5.9. Kdepim-runtime recebe um recurso facebook.

#### Kdenlive

No Kdenlive, a equipe consertou o "Efeito confelamento" quebrado. Em versões recentes, era impossível alterar o quadro congelado para o efeito congelamento. Agora um atalho de teclado para o recurso Extrair quadro é possível. Agora o usuário pode salvar capturas de tela da linha do tempo com um atalho de teclado e o nome sugerido é baseado no número do quadro <a href='https://bugs.kde.org/show_bug.cgi?id=381325'>https://bugs.kde.org/show_bug.cgi?id=381325</a>. O bug de a transição lumas baixada não aparecer na interface foi reparado: <a href='https://bugs.kde.org/show_bug.cgi?id=382451'>https://bugs.kde.org/show_bug.cgi?id=382451</a>. Problema de cliques no áudio foi reparado (por enquanto requer construir a dependência MLT do git até o lançamento do MLT): <a href='https://bugs.kde.org/show_bug.cgi?id=371849'>https://bugs.kde.org/show_bug.cgi?id=371849</a>.

#### Krfb

Os desenvolvedores terminaram de portar a extensão X11 para o Qt5 e o krfb voltou a funcionar usando o backend do X11 que é muito mais rápido que a extensão do Qt. Uma nova página de configurações foi criada permitindo ao usuário trocar a extensão preferida de framebuffer.

#### Konsole

O Konsole agora permite retorno de rolagem ilimitado até para além do limite de 2GB (32bit). Agora o Konsole permite que o usuário selecione qualquer local para guardar arquivos de armazenamento de histórico de rolagem. Uma regressão também foi consertada e o Konsole pode novamente permitir ao KonsolePart chamar a janela de Gerenciar Perfil.

#### KAppTemplate

No KAppTemplate, agora existe uma opção para instalar modelos novos a partir do sistema de ficheiros. Foram removidos mais modelos do KAppTemplate e, em vez disso, foram integrados em produtos relacionados; o modelo do 'plugin' do ktexteditor e do kpartsapp (migrados agora para o Qt5/KF5) tornaram-se parte do KTextEditor e do KParts das Plataformas do KDE desde a versão 5.37.0. Estas alterações deverão simplificar a criação de modelos nas aplicações do KDE.

### Pisoteando bugs

Mais de 80 bugs foram resolvidos nos aplicativos KDE, incluindo a suíte Kontact, Ark, Dolphin, K3b, Kdenlive, KGpg, Konsole e mais!

### Registro completo das alterações
