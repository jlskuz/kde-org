---
aliases:
- ../announce-applications-14.12-rc
custom_spread_install: true
date: '2014-11-27'
description: O KDE Lança as Aplicações do KDE 14.12 Pré-Lançamento.
layout: application
title: O KDE disponibiliza a versão Release Candidate do KDE Applications 14.12
---
27 de novembro de 2014. Hoje o KDE disponibilizou o Release Candidate da nova versão do KDE Applications. Com as dependências e funcionalidades estabilizadas, o foco das equipes do KDE agora é a correção de erros e pequenos ajustes.

Com diversos aplicativos sendo preparados para o KDE Frameworks 5, as versões do KDE Applications 14.12 precisam de testes aprofundados para manter e melhorar a qualidade e a experiência do usuário. Precisamos dos usuários atuais para manter a alta qualidade do KDE, porque os desenvolvedores simplesmente não conseguem testar todas as configurações possíveis. Contamos com você para nos ajudar a encontrar erros antecipadamente, para que possam ser corrigidos antes da versão final. Por favor, considere juntar-se à equipe, instalando a versão Release Candidate e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.

#### Instalando os pacotes binários do KDE Applications 14.12 Release Candidate

<em>Pacotes</em>. Alguns distribuidores de SO's Linux/UNIX forneceram simpaticamente alguns pacotes binários do 14.12 RC (internamente 14.11.97) para algumas versões das suas distribuições e, em alguns casos, outros voluntários da comunidade também o fizeram. Os pacotes binários adicionais, assim como as actualizações dos pacotes agora disponíveis, poderão aparecer nas próximas semanas.

<em>Localizações dos Pacotes</em>. Para uma lista actualizada dos pacotes binários disponíveis, dos quais a Equipa de Versões do KDE foi informada, visite por favor o <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Wiki da Comunidade</a>.

#### Compilando o KDE Applications 14.12 Release Candidate

Poderá <a href='http://download.kde.org/unstable/applications/14.11.97/src/'>transferir à vontade</a> o código-fonte completo das Aplicações do KDE 14.12 RC (pré-lançamento). As instruções de compilação e instalação estão disponíveis na <a href='/info/applications/applications-14.11.97'>Página de Informações das Aplicações do KDE RC</a>.
