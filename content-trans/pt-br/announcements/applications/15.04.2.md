---
aliases:
- ../announce-applications-15.04.2
changelog: true
date: 2015-06-02
description: O KDE disponibiliza o KDE Applications 15.04.2
layout: application
title: O KDE disponibiliza o KDE Applications 15.04.2
version: 15.04.2
---
2 de Junho de 2015. Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../15.04.0'>Aplicações do KDE 15.04</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Mais de 30 correções de erros registradas incluem melhorias no Gwenview, Kate, Kdenlive, kdepim, Konsole, Marble, Kgpg, Kig, ktp-call-ui e Umbrello.

Esta versão também inclui as versões de Suporte de Longo Prazo da Área de Trabalho do Plasma 4.11.20, a Plataforma de Desenvolvimento do KDE 4.14.9 e o pacote Kontact 4.14.9.
