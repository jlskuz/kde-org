---
aliases:
- ../announce-applications-15.04.2
changelog: true
date: 2015-06-02
description: KDE wydało Aplikacje KDE 15.04.2
layout: application
title: KDE wydało Aplikacje KDE 15.04.2
version: 15.04.2
---
2 czerwca 2015. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../15.04.0'>Aplikacji KDE 15.04</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 30 zarejestrowanych poprawek błędów uwzględnia ulepszenia do gwenview, kate, kdepim, kdenlive, konsole, marble, kgpg, kig, ktp-call-ui oraz umbrello.

To wydanie zawiera także wersje o długoterminowym wsparciu: Przestrzeni Roboczych 4.11.20, Platformę Programistyczną KDE 4.14.9 oraz Pakiet Kontact 4.14.9 .
