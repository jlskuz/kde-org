---
aliases:
- ../announce-applications-19.08.0
changelog: true
date: 2019-08-15
description: KDE wydało Aplikacje 19.08.
layout: application
release: applications-19.08.0
title: KDE wydało Aplikacje KDE 19.08.0
version: '19.08'
version_number: 19.08.0
---
{{< peertube "/6443ce38-0a96-4b49-8fc5-a50832ed93ce" >}}

{{% i18n_date %}}

Społeczność KDE z radością ogłasza wydanie Aplikacji KDE 19.08.

This release is part of KDE's commitment to continually provide improved versions of the programs we ship to our users. New versions of Applications bring more features and better-designed software that increases the usability and stability of apps like Dolphin, Konsole, Kate, Okular, and all your other favorite KDE utilities. Our aim is to ensure you remain productive, and to make KDE software easier and more enjoyable for you to use.

We hope you enjoy all the new enhancements you'll find in 19.08!

## Co nowego w Aplikacjach KDE 19.08

More than 170 bugs have been resolved. These fixes re-implement disabled features, normalize shortcuts, and solve crashes, making your applications friendlier and allowing you to work and play smarter.

### Dolphin

Dolphin is KDE's file and folder explorer which you can now launch from anywhere using the new global <keycap>Meta + E</keycap> shortcut. There is also a new feature that minimizes clutter on your desktop. When Dolphin is already running, if you open folders with other apps, those folders will open in a new tab in the existing window instead of a new Dolphin window. Note that this behavior is now on by default, but it can be disabled.

The information panel (located by default to the right of the main Dolphin panel) has been improved in several ways. You can, for example, choose to auto-play media files when you highlight them in the main panel, and you can now select and copy the text displayed in the panel. If you want to change what the information panel shows, you can do so right there in the panel, as Dolphin does not open a separate window when you choose to configure the panel.

We have also solved many of the paper cuts and small bugs, ensuring that your experience of using Dolphin is much smoother overall.

{{<figure src="/announcements/applications/19.08.0/dolphin_bookmark.png" alt=`Dolphin new bookmark feature` caption=`Dolphin new bookmark feature` width="600px" >}}

### Gwenview

Gwenview is KDE's image viewer, and in this release the developers have improved its thumbnail viewing feature across the board. Gwenview can now use a \"Low resource usage mode\" that loads low-resolution thumbnails (when available). This new mode is much faster and more resource-efficient when loading thumbnails for JPEG images and RAW files. In cases when Gwenview cannot generate a thumbnail for an image, it now displays a placeholder image rather than re-using the thumbnail of the previous image. The problems Gwenview had with displaying thumbnails from Sony and Canon cameras have also been solved.

Apart from changes in the thumbnail department, Gwenview has also implemented a new “Share” menu that allows sending images to various places, and correctly loads and displays files in remote locations accessed using KIO. The new version of Gwenview also displays vastly more EXIF metadata for RAW images.

{{<figure src="/announcements/applications/19.08.0/gwenview_share.png" alt=`Gwenview new “Share” menu` caption=`Gwenview new “Share” menu` width="600px" >}}

### Okular

Developers have introduced many improvements to annotations in Okular, KDE's document viewer. Apart from the improved UI for annotation configuration dialogs, line annotation can now have various visual flourishes added to the ends, allowing them to be turned into arrows, for example. The other thing you can do with annotations is expand and collapse them all at once.

Okular's support for EPub documents has also received a push in this version. Okular doesn't crash anymore when attempting to load malformed ePub files, and its performance with large ePub files has improved significantly. The list of changes in this release includes improved page borders and Presentation mode's Marker tool in High DPI mode.

{{<figure src="/announcements/applications/19.08.0/okular_line_end.png" alt=`Okular annotation tool settings with the new Line end option` caption=`Okular annotation tool settings with the new Line end option` width="600px" >}}

### Kate

Thanks to our developers, three annoying bugs have been squashed in this version of KDE's advanced text editor. Kate once again brings its existing window to the front when asked to open a new document from another app. The \"Quick Open\" feature sorts items by most recently used, and pre-selects the top item. The third change is in the \"Recent documents\" feature, which now works when the current configuration is set up not to save individual windows’ settings.

### Konsola

The most noticeable change in Konsole, KDE's terminal emulator application, is the boost to the tiling feature. You can now split the main pane any which way you want, both vertically and horizontally. The subpanes can then be split again however you want. This version also adds the ability to drag and drop panes, so you can easily rearrange the layout to fit your workflow.

Besides that, the Settings window has received an overhaul to make it clearer and easier to use.

{{< video src-webm="/announcements/applications/19.08.0/konsole-tabs.webm" >}}

### Spectacle

Spectacle is KDE's screenshot application and it is getting more and more interesting features with each new version. This version is no exception, as Spectacle now comes with several new features that regulate its Delay functionality. When taking a time-delayed screenshot, Spectacle will display the time remaining in its window title. This information is also visible in its Task Manager item.

Still on the Delay feature, Spectacle's Task Manager button will also show a progress bar, so you can keep track of when the snap will be taken. And, finally, if you un-minimize Spectacle while waiting, you will see that the “Take a new Screenshot” button has turned into a \"Cancel\" button. This button also contains a progress bar, giving you the chance to stop the countdown.

Saving screenshots has a new functionality, too. When you have saved a screenshot, Spectacle shows a message in the app that allows you to open the screenshot or its containing folder.

{{< video src-webm="/announcements/applications/19.08.0/spectacle_progress.webm" >}}

### Kontact

Kontact, KDE's email/calendar/contacts and general groupware suite of programs, brings Unicode color emoji and Markdown support to the email composer. Not only will the new version of KMail let you make your messages look good, but, thanks to the integration with grammar checkers such as LanguageTool and Grammalecte, it will help you check and correct your text.

{{<figure src="/announcements/applications/19.08.0/kontact_emoji.png" alt=`Emoji selector` caption=`Emoji selector` width="600px" >}}

{{<figure src="/announcements/applications/19.08.0/kmail_grammar.png" alt=`Integracja z modułem sprawdzania gramatyki KMail` caption=`Integracja z modułem sprawdzania gramatyki KMail` width="600px" >}}

When planning events, invitation emails in KMail are no longer deleted after replying to them. It is now possible to move an event from one calendar to another in the event editor in KOrganizer.

Last but not least, KAddressBook can now send SMS messages to contacts through KDE Connect, resulting in a more convenient integration of your desktop and mobile devices.

### Kdenlive

The new version of Kdenlive, KDE's video editing software, has a new set of keyboard-mouse combos that will help you become more productive. You can, for example, change the speed of a clip in the timeline by pressing CTRL and then dragging on the clip, or activate the thumbnail preview of video clips by holding Shift and moving the mouse over a clip thumbnail in the project bin. Developers have also put a lot of effort into usability by making 3-point editing operations consistent with other video editors, which you will surely appreciate if you're switching to Kdenlive from another editor.

{{<figure src="https://cdn.kde.org/screenshots/kdenlive/19-08.png" alt=`Kdenlive 19.08.0` caption=`Kdenlive 19.08.0` width="600px" >}}
