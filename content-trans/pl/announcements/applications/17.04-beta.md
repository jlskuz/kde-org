---
aliases:
- ../announce-applications-17.04-beta
date: 2017-03-24
description: KDE wydało Aplikacje 17.04 Beta.
layout: application
release: applications-17.03.80
title: KDE wydało betę Aplikacji KDE 17.04
---
24 marca 2017. Dzisiaj KDE wydało wersję beta nowej wersji Aplikacji KDE. Wersja ta zamraża wszelkie zmiany w zależnościach i funkcjonalności, a zespół KDE będzie się skupiał jedynie na naprawianiu w niej błędów i dalszym udoskonalaniu.

Sprawdź <a href='https://community.kde.org/Applications/17.04_Release_Notes'>listę zmian społeczności</a>, aby uzyskać informację dotyczącą archiwów, obecnie opartych na KF5 oraz ich znanych błędów. Bardziej kompletny komunikat będzie dostępny po wydaniu wersji ostatecznej

Aplikacje KDE, wydanie 17.04 potrzebuje wypróbowania go w szerokim zakresie, aby utrzymać i ulepszyć jakość i odczucia użytkownika. Obecnie użytkownicy są znaczącym czynnikiem przy utrzymywaniu wysokiej jakości KDE, bo programiści po prostu nie mogą wypróbować każdej możliwej konfiguracji. Liczymy, że wcześnie znajdziesz błędy, tak aby mogły zostać poprawione przed wydaniem końcowym. Proszę rozważyć dołączenie do zespołu poprzez zainstalowanie bety i <a href='https://bugs.kde.org/'>zgłaszanie wszystkich błędów</a>.
