---
aliases:
- ../../kde-frameworks-5.16.0
date: 2015-11-13
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Monitor lib: Use Kformat::spelloutDuration to localize time string
- Use KDE_INSTALL_DBUSINTERFACEDIR to install dbus interfaces
- UnindexedFileIndexer: Handle files that have been moved when baloo_file was not running
- Usunięto Transaction::renameFilePath oraz dodano do niej DocumentOperation.
- Make constructors with a single parameter explicit
- UnindexedFileIndexer: only index required parts of file
- Transakcja: dodano metodę zwracającą strukturę timeInfo
- Dodano wykluczane typy mime w ustawieniach balooctl
- Databases: Use QByteArray::fromRawData when passing data to a codec
- Balooctl: Move 'status' command to its own class
- Balooctl: Show help menu if the command is not recognized
- Balooshow: Allow us to lookup files by their inode + devId
- monitor Balooctl: zatrzymaj gdy baloo zawiesi się
- MonitorCommand: Use both the started and finished signals
- Balooctl monitor: Move to a proper command class
- Dodano powiadomienia dbus przy rozpoczynaniu/kończeniu indeksowania pliku
- FileIndexScheduler: Forcibly kill threads on exit
- WriteTransaction commit: Avoid fetching the positionList unless required
- WriteTransaction: Extra asserts in replaceDocument

### BluezQt

- isBluetoothOperational now also depends on unblocked rfkill
- Naprawiono ustalanie globalnego stanu przełącznika rfkill
- QML API: Mark properties without notify signal as constants

### Dodatkowe moduły CMake

- Warn instead of error if ecm_install_icons finds no icons. (bug 354610)
- make it possible to build KDE Frameworks 5 with a plain qt 5.5.x installed from the normal qt.io installer on mac os
- Nie usuwaj zmiennych z pamięci podręcznej w KDEInstallDirs. (błąd 342717)

### Integracja Szkieletów

- Ustawiono wartość domyślną dla WheelScrollLines
- Naprawiono ustawienie WheelScrollLines na Qt &gt;= 5.5 (błąd 291144)
- Przełączono na czcionkę Noto dla Plazmy 5.5

### KAktywności

- Naprawiono budowanie na Qt 5.3
- Przeniesiono plik dołączany boost.optional do miejsca, które go używa
- Zastąpiono wykorzystanie boost.optional w kontynuacjach ze szczuplejszą strukturą optional_view
- Dodano obsługę własnego porządkowania dowiązanych wyników
- QML może wywoływać KCM działań
- Dodano możliwość usunięcia działania dla KCMu działań
- Nowy interfejs ustawień działań
- Nowy interfejs ustawień obsługujący dodawanie opisów i tapety
- Interfejs ustawień jest teraz poprawnie podzielony na moduły

### KArchiwum

- Dostosowano KArchive do zmienionego zachowania w Qt 5.6
- Usunięto wycieki pamięci, zmniejszono wykorzystanie pamięci

### KAuth

- Obsługa w pośredniczeniu wiadomości qInfo
- Czekaj na ukończenie wywołania async rozpoczynającego pomocnika przed sprawdzeniem odpowiedzi (błąd 345234)
- Naprawiono nazwę zmiennej, w innym przypadku include nie zadziała

### KConfig

- Naprawiono użycie ecm_create_qm_loader.
- Naprawiono zmienną include
- Use KDE*INSTALL_FULL* variant, so there is no ambiguity
- Zezwolono KConfig na używanie zasobów jako zapasowych plików ustawień

### KConfigWidgets

- Uczyń KConfigWidgets zawartymi samymi w sobie, dostarczaj jeden globalny plik w zasobie
- Uczyniono doctools opcjonalnymi

### KCoreAddons

- KAboutData: apidoc "is is" -&gt; "is" addCredit(): ocsUserName -&gt; ocsUsername
- KJob::kill(Quiet) także powinien wyjść z pętli zdarzeń
- Dodano obsługę nazw plików desktop do KAboutData
- Użyto poprawnych znaków sterujących
- Zmniejszono kilka przydziałów
- Uczyniono KAboutData::translators/setTranslators prostym
- Naprawiono kod przykładowy setTranslator
- przetwarzanie na pulpicie: pominięto Encoding= key
- desktopfileparser: Zaadresuj komentarze recenzji
- Zezwolono na typy ustawień usługi w kcoreaddons_desktop_to_json()
- przetwarzanie na pulpicie: Naprawiono przetwarzanie wartości podwójnych i logicznych
- Dodano KPluginMetaData::fromDesktopFile()
- przetwarzanie na pulpicie: Zezwolono na przekazywanie względnych ścieżek dla plików typu usługa
- przetwarzanie na pulpicie: Użyto bardziej skategoryzowanego logowania
- QCommandLineParser używa -v dla --version więc po prostu użyj --verbose
- Usunięto dużo powielonego kodu dla desktop{tojson,fileparser}.cpp
- Przy czytaniu plików .desktop przetwarzaj pliki ServiceType
- Uczyniono SharedMimeInfo opcjonalnym wymaganiem
- Usunięto wywołanie do QString::squeeze()
- desktopparser: uniknięto niepotrzebnego dekodowania utf8
- desktopparser: Nie dodawaj kolejnego wpisu, jeśli wpis kończy się rozgranicznikiem
- KPluginMetaData: Gdy wpis listy nie jest listą JSON, to ostrzeż
- Dodano mimeTypes() do KPluginMetaData

### KCrash

- Ulepszono wyszukiwanie dla drkonqui i domyślnie uciszono je, gdy brak wyników

### KDeclarative

- ConfigPropertyMap można teraz odpytać o niezmienne opcje przy użyciu metody isImmutable(key)
- Rozpakuj QJSValue w mapie właściwości ustawień
- EventGenerator: Dodano obsługę do wysyłania zdarzeń wheel
- naprawiono utratę QuickViewSharedEngine initialSize przy inicjowaniu.
- naprawiono regresję krytyczną dla QuickViewSharedEngine wprowadzoną przez wdrożenie3792923639b1c480fd622f7d4d31f6f888c925b9
- nastawione rozmiary widoku mają pierwszeństwo przed początkowymi rozmiarami widoku w QuickViewSharedEngine

### KDED

- Uczyniono doctools opcjonalnymi

### Obsługa KDELibs 4

- Nie próbuj przechowywać QDateTime w pamięci mmap
- Synchronizuj i przystosuj uriencode.cmake z kdoctools.

### KDesignerPlugin

- Dodano KCollapsibleGroupBox

### KDocTools

- uaktualniono wpisy pt_BR

### KGlobalAccel

- Nie stosuj przesunięcia XOR dla KP_Enter (błąd 128982)
- Przechwytuj wszystkie klawisze dla symbolu (błąd 351198)
- Nie przechwytuj keysyms dwukrotnie przy każdym keypress

### KHTML

- Naprawiono poprawne drukowanie z KHTMLPart poprzez ustawienie opcji nadrzędnej printSetting

### KIconThemes

- kiconthemes teraz obsługuje wystroje osadzone w zasobach qt wewnątrz prefiks :/icons  tak jak Qt robi to sam dla QIcon::fromTheme
- Dodano brakującą, wymaganą zależność

### KImageFormats

- Rozpoznawaj image/vnd.adobe.photoshop zamiast image/x-psd
- Częściowo wycofaj d7f457a, aby uniknąć usterki przy wyjściu z aplikacji

### KInit

- Uczyniono doctools opcjonalnymi

### KIO

- Zapisz adres url pośrednika przy użyciu poprawnego schematu
- Dostarczono "szablony nowych plików" w bibliotece kiofilewidgets przy użyciu .qrc (błąd 353642)
- Poprawnie obsługuj kliknięcie środkowym w menu nawigacji
- Uczyniono kio_http_cache_cleaner użytecznym w aplikacji installer/bundles
- KOpenWithDialog: Naprawiono tworzenie pliku desktop z pustym mimetype
- Odczytuj informacje protokołu z metadanych wtyczki
- Zezwolono na lokalne używanie kioslave
- Dodano przekształcenie z .protocol na JSON
- Naprawiono dwukrotną emisję wyniku i brakujące ostrzeżenie przy natrafieniu na niedostępny katalog przez program wyszczególniający zawartość katalogu (błąd 333436)
- Zachowaj dowiązania względne do plików przy kopiowaniu dowiązań symbolicznych. (błąd 352927)
- Użyto odpowiednich ikon dla domyślnych katalogów w katalogu użytkownika (błąd 352498)
- Dodano interfejs umożliwiający wtyczce pokazwanie własnych ikon
- Uczyniono zależność KNotifications w KIO (kpac) opcjonalną
- Uczyniono doctools + wallet opcjonalnymi
- Uniknij usterki kio, gdy nie ma uruchomionego serwera dbus
- Dodano KUriFilterSearchProviderActions, do wyświetlania listy czynności przy wyszukiwaniu tekstu przy użyciu skrótów sieciowych
- Przeniesiono wpisy dla menu "Utwórz nowy" z kde-baseapps/lib/konq do kio (błąd 349654)
- Przeniesiono konqpopupmenuplugin.desktop z kde-baseapps do kio (błąd 350769)

### KJS

- Użyto zmiennej globalnej "_timezone" dla MSVC zamiast "timezone". Naprawiono budowanie z MSVC 2015.

### KNewStuff

- Naprawiono plik desktop i adres URL strony domowej 'Zarządzania Partycjami KDE'

### KNotification

- Teraz kparts już nie wymagają knotifications, tylko rzeczy, które na prawdę chcą powiadomień wymagają tego szkieletu
- Dodaj opis + powód do mowy +phonon
- Uczyniono zależność phonon opcjonalną, całkowicie wewnętrzna zmiana, zrobiono ją dla mowy.

### KParts

- Użyto deleteLater w Part::slotWidgetDestroyed().
- Usunięto zależność KNotifications z KParts
- Użyto funkcji do odpytania o położenie ui_standards.rc zamiast kodować je na stałe, umożliwia działanie zapasowego zasobu

### KRunner

- RunnerManager: Uproszczono kod wczytujący wtyczkę

### KService

- KBuildSycoca: zawsze zapisuj, nawet gdy nie odnotowano zmiany w pliku .desktop. (błąd 353203)
- Uczyniono doctools opcjonalnymi
- kbuildsycoca: wszystkie pliki mimeapps.list wymienione w nowym spec będą przetwarzane.
- Use largest timestamp in subdirectory as resource directory timestamp.
- Oddziel typy MIME przy przekształcaniu z KPluginInfo na KPluginMetaData

### KTextEditor

- podświetlanie: gnuplot: dodano rozszerzenie .plt
- poprawiono podpowiedź walidacji, dzięki "Thomas Jarosch" &lt;thomas.jarosch@intra2net.com&gt;, dodano podpowiedź o walidacji czasu kompilacji
- Nie powoduj usterki, gdy nie ma polecenia
- Naprawiono błąd #307107
- Zmienne Haskell zaczynające się od _ będą podświetlane
- uproszczono inicjalizacje git2, przy założeniu, ze potrzebujemy dosyć nowej wersji (błąd 353947)
- dostarczaj powiązane domyślne ustawienia w zasobach
- podświetlanie składni (d-g): użyto domyślnych stylów zamiast na stałe zakodowanych kolorów
- lepsze wyszukiwanie skryptów, najpierw wyszukiwanie lokalne, później w zasobach, później inne miejsca, tak aby użytkownik mógł zastępować nasze skrypty swoimi lokalnymi
- spakowano wszystkie rzeczy js do zasobów, bez dostarczania dodatkowych plików można użyć tylko 3 brakujących plików ustawień i ktexteditor jako biblioteki
- następna próba: umieszczono wszystkie dostarczane pliki składni xml w zasobie
- dodano skrót przełączania trybu wprowadzania (błąd 347769)
- w zasobie będzie dostarczany plik xml
- podświetlanie składni (a-c): przejście na nowy domyślny wygląd, usunięto na stałe zakodowane kolory
- podświetlanie składni: usunięto na stałe zakodowane kolory i użyto domyślnego wyglądu
- podświetlanie składni: użyto nowych domyślnych stylów (usunięto na stałe zakodowane kolory)
- Lepszy "import" domyślnego wyglądu
- Wprowadzono "Zapisz jako z kodowaniem" do zapisywania plików o innym kodowaniu. Używamy teraz ładnego menu z grupami kodowania, a także odpowiednich okien dialogowych plików bez tracenia ważnych funkcji.
- pliki ui będą dostarczane z lib, przy użyciu mojego rozszerzenia do xmlgui
- Przy drukowaniu znów uznawana jest wybrana czcionka &amp; zestaw kolorów (błąd 344976)
- Dla zapisanych i zmienionych wierszy używane są kolory bryzy
- Ulepszono domyślne kolory obramowań ikon dla "normalnego" zestawu kolorystycznego
- autoklamry: klamra zostanie wstawione tylko wtedy, gdy następna litera jest pusta lub niealfanumeryczna
- autoklamry: przy usuwaniu lewego nawiasu klawiszem backspace, prawy nawias także zostanie usunięty
- autoklamry: połączenie ustanawiaj tylko jeden raz
- autoklamry: zjadaj zamykający nawias przy pewnych warunkach
- Naprawiono nieprzekazywanie shortcutoverride do mainwindow
- Błąd 342659 - Trudno rozróżnić domyślny kolor "podświetlania nawiasów" (poprawiono normalny zestaw kolorystyczny) (błąd 342659)
- Dodano właściwe domyślne kolory dla koloru "Bieżący numer wiersza"
- dopasowywanie nawiasów &amp; autonawiasy: współdziel kod
- dopasowywanie nawiasów: strzeż się ujemnych maxLines
- dopasowywanie nawiasów: tylko dlatego, że nowy zakres pasuje do starego nie oznacza, że uaktualnienie nie jest potrzebne
- Dodaj szerokość połowy odstępu, aby umożliwić narysowanie kursora na końcu wiersza
- naprawiono kilka problemów z HiDPI w obramowaniu ikon
- naprawiono błąd #310712: usuwaj odstępy na końcu także w wierszu z kursorem (błąd 310712)
- będąc w trybie wprowadzania vi będzie wyświetlana tylko wiadomość "mark set" 
- usunięto &amp; z tekstu przycisku (błąd 345937)
- naprawiono uaktualnienie numeru koloru bieżącego wiersza (błąd 340363)
- zaimplementowano wstawianie nawiasów przy wstawianiu nawiasów na zaznaczenie (błąd 350317)
- auto nawiasy (błąd 350317)
- naprawiono ostrzeżenie HL (błąd 344442)
- brak przewijania kolumn przy włączonym dynamicznym zawijaniu wyrazów
- gdy użytkownik ustawił podświetlanie w sesjach, to będzie to zapamiętywane, aby nie stracić tego po przywróceniu sesji (błąd 332605)
- naprawiono zwijanie dla tex (błąd 328348)
- naprawiono błąd #327842: koniec komentarza C-style nie jest wykrywany (błąd 327842)
- zapisywanie/przywracanie dynamicznego zawijania wyrazów przy zapisywaniu/przywracaniu sesji (błąd 284250)

### KTextWidgets

- Dodano nowe podmenu do KTextEdit, aby móc przełączać pomiędzy językami sprawdzania pisowni
- Naprawiono wczytywanie domyślnych ustawień Sonnet

### Szkielet Portfela

- Use KDE_INSTALL_DBUSINTERFACEDIR to install dbus interfaces
- Pozbyto się ostrzeżeń pliku ustawień Portfela przy logowaniu (błąd 351805)
- Dodano poprawny przedrostek przed danymi wynikowymi kwallet-pam

### KWidgetsAddons

- Dodano zwijalny pojemnik, KCollapsibleGroupBox
- KNewPasswordWidget: brak czynności nadania wstępnego koloru
- Wprowadzono KNewPasswordWidget

### KXMLGUI

- kmainwindow: Gdy informacje o tłumaczu są dostępne to zostaną samoczynnie wprowadzone. (błąd 345320)
- Zezwolono na powiązywanie klawisza menu kontekstowego (na dole po prawej) do skrótów (błąd 165542)
- Dodano funkcję do odpytywania położenia pliku standardów xml
- Dodano szkielet kxmlgui, którego można używać bez wgrywania żadnych plików
- Dodano brakującą, wymaganą zależność

### Szkielety Plazmy

- Naprawiono zbicie elementów TabBar przy tworzeniu na początku, co można zaobserwować w np. Kickoff po uruchomieniu Plazmy
- Po upuszczeniu pliku na pulpit/panel teraz pojawi się pytanie o to co zrobić
- Weź pod uwagę QApplication::wheelScrollLines z ScrollView
- BypassWindowManagerHint będzie od teraz używany tylko na platformie X11
- usunięto tło starego panelu
- czytelniejsze pola obracane przy jeszcze mniejszych rozmiarach
- pokolorowana historia widoku
- kalendarz: Cały obszar nagłówka będzie od teraz klikalny
- kalendarz: W goToMont nie będzie od teraz używany numer bieżącego dnia
- kalendarz: Naprawiono uaktualniania przeglądu dekady
- Przy wczytywaniu przez IconItem zostaną użyte ikony z zestawu bryzy
- Naprawiono właściwość przycisku minimumWidth (błąd 353584)
- Wprowadzono sygnał appletCreated
- Ikona bryzy Plazmy: Dodano elementy id svg dla gładzika
- Ikona bryzy Plazmy: zmieniono gładzik do rozmiaru 22x22 pikseli
- Ikona bryzy: dodano ikonę elementu interfejsu do notatek
- Dodano skrypt zastępujący kolory zakodowane na stałe dla arkuszy stylów
- Dla ExposeEvent od teraz będzie stosowany SkipTaskbar
- SkipTaskbar od teraz nie będzie ustawiany dla każdego zdarzenia

Możesz przedyskutować i podzielić się pomysłami dotyczącymi tego wydania w dziale komentarzy <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artykułu dot</a>.
