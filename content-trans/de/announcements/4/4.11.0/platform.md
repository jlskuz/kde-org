---
date: 2013-08-14
hidden: true
title: Die KDE-Plattform 4.11 liefert eine bessere Leistung
---
Für die KDE-Plattform 4 wurden seit der Veröffentlichung der Version 4.9 keine neuen Funktionen mehr hinzugefügt. Folglich enthält diese Version ein Anzahl von Fehlerkorrekturen und Verbesserungen der Leistungsfähigkeit.

Die Leistung von Nepomuk, der semantischen Speicherung und Suche, wurde erheblich verbessert, wie z.B. Leseoptimierungen, wodurch das Einlesen von Daten bis zu sechsmal schneller ist. Die Indexierung arbeitet intelligenter, indem sie in zwei Phasen aufgeteilt wurde. Die erste Phase ermittelt sehr schnell allgemeine Informationen, wie z.B. Dateityp und -name. Zusätzliche Attribute wie Metadaten von Mediendateien, Urheberinformationen u.s.w. werden in der zweiten Phase ausgelesen, die etwas langsamer ist. Die Anzeige der Metadaten von neu erstellten oder heruntergeladenen Dateien erfolgt jetzt deutlich zügiger. Außerdem haben die Nepomuk-Entwickler am Sicherungs- und Wiederherstellungssystem gearbeitet. Zu guter Letzt beherrscht Nepomuk jetzt die Indexierung verschiedener Dokumentenformate einschließlich ODF und DOCX.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/dolphin.png" caption=`Anwendung von semantischen Funktionen in Dolphin` width="600px">}}

Wegen des optimierten Speicherformats und des neu geschriebenen Indizierungsprogramms für Nepomuk muss ein Teil des Inhalts der Festplatte neu indiziert werden. Daher wird während der erneuten Indizierung eine ungewöhnliche Auslastung des Rechners für eine gewisse Zeit auftreten, abhängig von der Menge der Daten, die neu indiziert werden müssen. Eine automatische Umwandlung der Nepomuk-Datenbank wird bei der ersten Anmeldung durchgeführt.

Es gibt noch weitere Fehlerbehebungen, die Sie in den <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'>Git-Protokollen</a> finden.

#### Installation der KDE-Entwicklerplattform

KDE-Software einschließlich all ihrer Bibliotheken und Anwendungen ist kostenlos unter Open-Source-Lizenzen erhältlich. KDE-Software läuft auf verschiedenen Betriebssystemen, Hardware-Konfigurationen und Prozessorarchitekturen wie z.B. ARM und x86, und es arbeitet mit jeder Art von Fensterverwaltung und Arbeitsumgebung zusammen. Neben Linux und anderen UNIX-basierten Betriebssystemen sind von den meisten KDE-Anwendungen auch Versionen für Microsoft Windows und Apple Mac OS X erhältlich. Sie sind auf den Seiten <a href='http://windows.kde.org'>KDE-Software für Windows</a> bzw. <a href='http://mac.kde.org/'>KDE-Software für den Mac</a> erhältlich. Experimentelle Versionen von KDE-Anwendungen für verschiedene Mobilplattformen wie MeeGo, MS Windows Mobile und Symbian sind im Internet erhältlich, werden zur Zeit aber nicht unterstützt. <a href='http://plasma-active.org'>Plasma Active</a> ist eine Benutzerumgebung für ein großes Spektrum an Geräten wie beispielsweise Tabletts und andere mobile Hardware.

Die KDE-Software kann als Quelltext und in verschiedenen binären Formaten von <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> heruntergeladen und auch auf <a href='/download'>CD-ROM</a> und von vielen großen <a href='http://download.kde.org/stable/%1'>GNU/Linux- und UNIX-Systemen</a> bezogen werden.

##### Pakete

Einige Anbieter von Linux-/UNIX-Betriebssystemen haben dankenswerterweise Binärpakete von 4.11.0für einige Versionen Ihrer Distributionen bereitgestellt, ebenso wie freiwillige Mitglieder der Gemeinschaft, <br />

##### Paketquellen

Eine aktuelle Liste aller Binärpakete, von denen das Release-Team in Kenntnis gesetzt wurde, finden Sie im <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community-Wiki</a>.

Der vollständige Quelltext für 4.11.0 kann <a href='/info/4/4.11.0'>hier</a> heruntergeladen werden. Anweisungen zum Kompilieren und Installieren von %1 finden Sie auf der <a href='/info/4/4.11.0#binary'>Infoseite zu 4.11.0</a>.

#### Systemanforderungen

Um den größtmöglichen Nutzen aus den Veröffentlichungen zu ziehen, empfehlen wir, eine aktuelle Version von Qt zu verwenden, etwa 4.8.4. Dies ist nötig, um eine stabile und performante Erfahrung zu bieten, weil einige KDE-Verbesserungen in Wahrheit am darunterliegenden Qt-Framework ansetzen.<br />Um den vollen Umfang an Fähigkeiten der KDE-Software zu nutzen, empfehlen wir außerdem, den neuesten Grafiktreiber für Ihr System zu verwenden, weil dies den Nutzungseindruck maßgeblich verbessert, sowohl bei wählbaren Funktionen als auch bei der allgemeinen Leistung und Stabilität.

## Heute ebenfalls angekündigt:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" />Plasma-Arbeitsbereich 4.11 verbessert weiter die Benutzererfahrung </a>

Mit dem Ziel der langfristigen Unterstützung im Auge wurde der Plasma-Arbeitsbereich in grundlegenden Funktionen weiter verbessert. So arbeitet die Fensterleiste flüssiger, die Batterieverwaltung wurde intelligenter und der Mixer erhielt Verbesserungen. Durch die Einführung von KScreen erhielt die Arbeitsfläche eine intelligente Verwaltung für Mehrbildschirmbetrieb. Insgesamt wurde die Nutzererfahrung sowohl durch große Leistungsverbesserungen als auch durch kleine Änderungen an der Nutzbarkeit verbessert,

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> Die KDE-Anwendungen 4.11 bringen erhebliche Verbesserungen in der Verwaltung persönlicher Daten und allgemeine Verbesserungen</a>

Diese Veröffentlichung bringt enorme Verbesserungen für die KDE-PIM-Anwendungen mit verbesserter Leistung und vielen neuen Funktionen. Für Kate gibt es verbesserte Produktivität für Python- und JavaScript-Entwicklern mit neuen Erweiterungen. Die Geschwindigkeit von Dolphin wurde gesteigert und für die Lernprogramme gibt es verschiedene neue Funktionen.
