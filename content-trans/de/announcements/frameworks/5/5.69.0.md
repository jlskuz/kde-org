---
aliases:
- ../../kde-frameworks-5.69.0
date: 2020-04-05
layout: framework
libCount: 70
---
### Baloo

- [SearchStore] Use categorized logging
- [QueryParser] Fix broken detection of end quote
- [EngineQuery] Provide toString(Term) overload for QTest
- [EngineQuery] Remove unused position member, extend tests
- [SearchStore] Avoid long lines and function nesting
- [baloosearch] Bail out early if specified folder is not valid
- [MTimeDB] Consolidate time interval handling code
- [AdvancedQueryParser] Test if quoted phrases are passed correctly
- [Term] Provide toString(Term) overload for QTest
- [ResultIterator] Remove unneeded SearchStore forward declaration
- [QueryTest] Make phrase test case data driven and extend
- [Inotify] Start the MoveFrom expire timer at most once per inotify batch
- [UnindexedFileIndexer] Only mark file for content indexing when needed
- [Inotify] Call QFile::decode only in a single place
- [QML] Correctly watch for unregistration
- [FileIndexScheduler] Update the content index progress more often
- [FileIndexerConfig] Replace config QString,bool pair with dedicated class
- [QML] Set the remaining time in the monitor more reliably
- [TimeEstimator] Correct batch size, remove config reference
- [FileIndexScheduler] Emit change to LowPowerIdle state
- [Debug] Improve readability of positioninfo debug format
- [Debug] Correct output of *::toTestMap(), silence non-error
- [WriteTransactionTest] Test removal of positions only
- [WriteTransaction] Extend position test case
- [WriteTransaction] Avoid growing m_pendingOperations twice on replace
- [FileIndexScheduler] Cleanup firstRun handling
- [StorageDevices] Fix order of notification connect and initialization
- [Config] Remove/deprecate disableInitialUpdate

### Breeze Icons

- Fix broken symlinks
- Move corner fold to top right in 24 icons
- Make find-location show a magnifier on a map, to be different to mark-location (bug 407061)
- Add 16px LibreOffice icons
- Fix configure when xmllint is not present
- Fix stylesheet linking in 8 icons
- Fix some stylesheet colors in 2 icon files
- Fix symlinks to incorrect icon size
- Add input-dialpad and call-voicemail
- Add buho icon
- Add calindori icon in the new pm style
- Add nota icon
- [breeze-icons] fix shadow in some user (applets/128) icons
- Add call-incoming/missed/outgoing
- Handle busybox's sed like GNU sed
- Add transmission-tray-icon
- Improve pixel alignment and margins of keepassxc systray icons
- Revert "[breeze-icons] Add telegram-desktop tray icons"
- Add small icons for KeePassXC
- [breeze-icons] add TeamViewer tray icons
- Add edit-reset
- Change document-revert style to be more like edit-undo
- Icons for emoji categories
- Add flameshot tray icons

### KAuth

- Fix type namespace requirement

### KBookmarks

- Decouple KBookmarksMenu from KActionCollection

### KCalendarCore

- Fix fallback to vCalendar loading on iCalendar load failure

### KCMUtils

- listen to passiveNotificationRequested
- workaround to never make applicationitem resize itself

### KConfig

- [KConfigGui] Check font weight when clearing styleName property
- KconfigXT: Add a value attribute to Enum field choices

### KCoreAddons

- kdirwatch: fix a recently introduced crash (bug 419428)
- KPluginMetaData: handle invalid mimetype in supportsMimeType

### KCrash

- Move setErrorMessage definition out of the linux ifdef
- Allow providing an error message from the application (bug 375913)

### KDBusAddons

- Check correct file for sandbox detection

### KDeclarative

- Introduce api for passive notifications
- [KCM Controls GridDelegate] Use <code>ShadowedRectangle</code>
- [kcmcontrols] Respect header/footer visibility

### KDocTools

- Use bold italic at 100% for sect4 titles, and bold 100% for sect5 titles (bug 419256)
- Update the list of the Italian entities
- Use the same style for informaltable as for table (bug 418696)

### KIdleTime

- Fix infinite recursion in xscreensaver plugin

### KImageFormats

- Port the HDR plugin from sscanf() to QRegularExpression. Fixes FreeBSD

### KIO

- New class KIO::CommandLauncherJob in KIOGui to replace KRun::runCommand
- New class KIO::ApplicationLauncherJob in KIOGui to replace KRun::run
- File ioslave: use better setting for sendfile syscall (bug 402276)
- FileWidgets: Ignore Return events from KDirOperator (bug 412737)
- [DirectorySizeJob] Fix sub-dirs count when resolving symlinks to dirs
- Mark KIOFuse mounts as Probably slow
- kio_file: honour KIO::StatResolveSymlink for UDS_DEVICE_ID and UDS_INODE
- [KNewFileMenu] Add extension to proposed filename (bug 61669)
- [KOpenWithDialog] Add generic name from .desktop files as a tooltip (bug 109016)
- KDirModel: implement showing a root node for the requested URL
- Register spawned applications as an independent cgroups
- Add "Stat" prefix to StatDetails Enum entries
- Windows: Add support for file date creation
- KAbstractFileItemActionPlugin: Add missing quotes in code example
- Avoid double fetch and temporary hex encoding for NTFS attributes
- KMountPoint: skip swap
- Assign an icon to action submenus
- [DesktopExecParser] Open {ssh,telnet,rlogin}:// urls with ktelnetservice (bug 418258)
- Fix exitcode from kioexec when executable doesn't exist (and --tempfiles is set)
- [KPasswdServer] replace foreach with range/index-based for
- KRun's KProcessRunner: terminate startup notification on error too
- [http_cache_cleaner] replace foreach usage with QDir::removeRecursively()
- [StatJob] Use A QFlag to specify the details returned by StatJob

### Kirigami

- Hotfix for D28468 to fix broken variable refs
- get rid of the incubator
- disable mousewheel completely in outside flickable
- Add property initializer support to PagePool
- Refactor of OverlaySheet
- Add ShadowedImage and ShadowedTexture items
- [controls/formlayout] Don't attempt to reset implicitWidth
- Add useful input method hints to password field by default
-  [FormLayout] Set compression timer interval to 0
- [UrlButton] Disable when there is no URL
- simplify header resizing (bug 419124)
- Remove export header from static install
- Fix about page with Qt 5.15
- Fix broken paths in kirigami.qrc.in
- Add "veryLongDuration" animation duration
- fix multi row notifications
- don't depend on window active for the timer
- Support multiple stacked Passive Notifications
- Fix enabling border for ShadowedRectangle on item creation
- check for window existence
- Add missing types to qrc
- Fix undefined check in global drawer menu mode (bug 417956)
- Fallback to a simple rectangle when using software rendering
- Fix color premultiply and alpha blending
- [FormLayout] Propagate FormData.enabled also to label
- Add a ShadowedRectangle item
- alwaysVisibleActions property
- don't create instances when the app is quitting
- Don't emit palette changes if the palette didn't change

### KItemModels

- [KSortFilterProxyModel QML] Make invalidateFilter public

### KNewStuff

- Fix layout in DownloadItemsSheet (bug 419535)
- [QtQuick dialog] Port to UrlBUtton and hide when there's no URL
- Switch to using Kirigami's ShadowedRectangle
- Fix update scenarios with no explicit downloadlink selected (bug 417510)

### KNotification

- New class KNotificationJobUiDelegate

### KNotifyConfig

- Use libcanberra as primary means of previewing the sound (bug 418975)

### KParts

- New class PartLoader as replacement to KMimeTypeTrader for parts

### KService

- KAutostart: Add static method to check start condition
- KServiceAction: store parent service
- Properly read the X-Flatpak-RenamedFrom string list from desktop files

### KTextEditor

- Make it compile against qt 5.15
- fix folding crash for folding of single line folds (bug 417890)
- [VIM Mode] Add g&lt;up&gt; g&lt;down&gt; commands (bug 418486)
- Add MarkInterfaceV2, to s/QPixmap/QIcon/g for symbols of marks
- Draw inlineNotes after drawing word wrap marker

### KWayland

- [xdgoutput] Only send initial name and description if set
- Add XdgOutputV1 version 2
- Broadcast application menu to resources when registering them
- Provide an implementation for the tablet interface
- [server] Don't make assumptions about the order of damage_buffer and attach requests
- Pass a dedicated fd to each keyboard for the xkb keymap (bug 381674)
- [server] Introduce SurfaceInterface::boundingRect()

### KWidgetsAddons

- New class KFontChooserDialog (based on KFontDialog from KDELibs4Support)
- [KCharSelect] Do not simplify single characters in search (bug 418461)
- If we readd items we need to clear it first. Otherwise we will see duplicate list
- Update kcharselect-data to Unicode 13.0

### KWindowSystem

- Fix EWMH non-compliance for NET::{OnScreenDisplay,CriticalNotification}
- KWindowSystem: deprecate KStartupInfoData::launchedBy, unused
- Expose application menu via KWindowInfo

### Plasma Framework

- Added Page element
- [pc3/busyindicator] Hide when not running
- Update window-pin, Add more sizes, Remove redundant edit-delete
- Create a new TopArea element using widgets/toparea svg
- Added plasmoid heading svg
- Make highlighted property work for roundbutton

### Prison

- Also expose the true minimum size to QML
- Add a new set of barcode size functions
- Simplify minimum size handling
- Move barcode image scaling logic to AbstractBarcode
- Add API to check whether a barcode is one- or two-dimensional

### QQC2StyleBridge

- [Dialog] Use <code>ShadowedRectangle</code>
- Fix sizing of CheckBox and RadioButton (bug 418447)
- Use <code>ShadowedRectangle</code>

### Solid

- [Fstab] Ensure uniqueness for all filesystem types
- Samba: Ensure to differentiate mounts sharing the same source (bug 418906)
- hardware tool: define syntax via syntax arg

### Sonnet

- Fix Sonnet autodetect failing on Indian langs
- Create ConfigView an unmanaged ConfigWidget

### Syntax Highlighting

- LaTeX: fix math parentheses in optional labels (bug 418979)
- Add Inno Setup syntax, including embedded Pascal scripting
- Lua: add # as additional deliminator to activate auto-completion with <code>#something</code>
- C: remove ' as digit separator
- add some comment about the skip offset stuff
- optimize dynamic regex matching (bug 418778)
- fix regex rules wrongly marked as dynamic
- extend indexer to detect dynamic=true regexes that have no place holders to adapt
- Add Overpass QL highlighting
- Agda: keywords updated to 2.6.0 and fix float points

### Sicherheitsinformation

Der veröffentlichte Quelltext wurde mit GPG mit folgendem Schlüssel signiert: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärer Fingerprint des Schlüssels: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
