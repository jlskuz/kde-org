---
aliases:
- ../../kde-frameworks-5.21.0
date: 2016-04-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
New framework: KActivitiesStats, a library for accessing the usage statistics data collected by the KDE activity manager.

### All frameworks

Qt &gt;= 5.4 is now required, i.e. Qt 5.3 is no longer supported.

###  Attica

- Add const variant to getter method

### Baloo

- Centralize batch size in config
- Remove code blocking indexing of text/plain files without .txt extension (bug 358098)
- Check both, filename and filecontent to determine mimetype (bug 353512)

### BluezQt

- ObexManager: Split error messages for missing objects

### Breeze Icons

- add breeze lokalize icons
- sync app icons between breeze and breeze dark
- update theme icons and remove application-system icon fix kicker groups
- add xpi support for firefox addons (bug 359913)
- update okular icon with the right one
- add ktnef app icon support
- add kmenueditor, kmouse and knotes icon
- change audio volume muted icon to use - for mute instead of only red color (bug 360953)
- add djvu mimetype support (bug 360136)
- add link instead of double entry
- add ms-shortcut icon for gnucash (bug 360776)
- change wallpaper background to generic one
- update icons to use an generic wallpaper
- add the icon for konqueror (bug 360304)
- add process-working icon for progress animation in KDE (bug 360304)
- add software install icon and update update icon with the right color
- add add and remove emblem icons for dolphin select, add mount icon
- Remove stylesheet from analogclock and kickerdash applet icons
- sync breeze and breeze dark (bug 360294)

### Extra CMake-Module

- Fix _ecm_update_iconcache to only update the install location
- Revert "ECMQtDeclareLoggingCategory: Include &lt;QDebug&gt; with the generated file"

### Framework-Integration

- Fallback to QCommonStyle implementation of standardIcon
- Set a default menu close timeout

### KActivities

- Removed compiler checks now that all frameworks require c++11
- Removed QML ResourceModel as it is superseeded by KAStats::ResultModel
- Inserting into empty QFlatSet returned an invalid iterator

### KCodecs

- Simplify code (qCount -&gt; std::count, homegrown isprint -&gt; QChar::isPrint)
- encoding detection: fix crash in wrong usage of isprint (bug 357341)
- Fix crash due to uninitialized variable (bug 357341)

### KCompletion

- KCompletionBox: force frameless window and don't set focus
- KCompletionBox should *not* be a tooltip

### KConfig

- Add support for get QStandardPaths locations inside desktop files

### KCoreAddons

- Fix kcoreaddons_desktop_to_json() on windows
- src/lib/CMakeLists.txt - fix linking to a Threads library
- Add stubs to allow compilation on Android

### KDBusAddons

- Avoid introspecting a DBus interface when we don't use it

### KDeclarative

- uniform use of std::numeric_limits
- [DeclarativeDragArea] Don't override "text" of mime data

### KDELibs 4 Support

- Fix obsolete link in kdebugdialog5 docbook
- Don't leak Qt5::Network as required lib for the rest of the ConfigureChecks

### KDESU

- Set feature macros to enable building on musl libc

### KEmoticons

- KEmoticons: fix crash when loadProvider fails for some reason

### KGlobalAccel

- Make kglobalaccel5 properly killable, fixing super slow shutdown

### KI18n

- Use qt system locale langs as fallback on non UNIX

### KInit

- Clean up and refactor the xcb port of klauncher

### KIO

- FavIconsCache: sync after write, so other apps see it, and to avoid crash on destruction
- Fix many threading issues in KUrlCompletion
- Fix crash in rename dialog (bug 360488)
- KOpenWithDialog: improve window title and description text (bug 359233)
- Allow for better cross-platform deployment of io slaves by bundling protocol info in plugin meta data

### KItemModels

- KSelectionProxyModel: Simplify row removal handling, simplify deselection logic
- KSelectionProxyModel: Recreate mapping on removal only if needed (bug 352369)
- KSelectionProxyModel: Only clear firstChild mappings for top-level
- KSelectionProxyModel: Ensure proper signalling when removing last selected
- Make DynamicTreeModel searchable by display role

### KNewStuff

- Do not crash if .desktop files are missing or broken

### KNotification

- Handle left-button clicking on legacy systray icons (bug 358589)
- Only use X11BypassWindowManagerHint flag on platform X11

### Package Framework

- After installing a package, load it
- if the package exists and up to date don't fail
- Add Package::cryptographicHash(QCryptographicHash::Algorithm)

### KPeople

- Set the contact uri as person uri in PersonData when no person exists
- Set a name for the database connection

### KRunner

- Import runner template from KAppTemplate

### KService

- Fix new kbuildsycoca warning, when a mimetype inherits from an alias
- Fix handling of x-scheme-handler/* in mimeapps.list
- Fix handling of x-scheme-handler/* in mimeapps.list parsing (bug 358159)

### KTextEditor

- Revert "Open/Save config page: Use term "Folder" instead of "Directory""
- enforce UTF-8
- Open/Save config page: Use term "Folder" instead of "Directory"
- kateschemaconfig.cpp: use correct filters with open/save dialogs (bug 343327)
- c.xml: use default style for control flow keywords
- isocpp.xml: use default style "dsControlFlow" for control flow keywords
- c/isocpp: add more C standard types
- KateRenderer::lineHeight() returns an int
- printing: use font size from selected printing schema (bug 356110)
- cmake.xml speedup: Use WordDetect instead of RegExpr
- Change tab width to 4 instead of 8
- Fix changing the current line number color
- Fix selecting completion item with the mouse (bug 307052)
- Add syntax highlighting for gcode
- Fix the MiniMap selection background painting
- Fix encoding for gap.xml (use UTF-8)
- Fix nested comment blocks (bug 358692)

### KWidgetsAddons

- Take content margins into account when calculating size hints

### KXMLGUI

- Fix editing toolbars loses plugged actions

### NetworkManagerQt

- ConnectionSettings: Initialize gateway ping timeout
- New TunSetting and Tun connection type
- Create devices for all known types

### Oxygen Icons

- Install index.theme to same directory it always was in
- Install into oxygen/base/ so icons move from apps don't clash with version installed by those apps
- Replicate symlinks from breeze-icons
- Add new emblem-added and emblem-remove icons for sync with breeze

### Plasma Framework

- [calendar] Fix calendar applet not clearing selection when hiding (bug 360683)
- update audio icon to use stylesheet
- update audio mute icon (bug 360953)
- Fixing the force-creation of applets when plasma is immutable
- [Fading Node] Don't mix opacity separately (bug 355894)
- [Svg] Don't reparse configuration in response to Theme::applicationPaletteChanged
- Dialog: Set SkipTaskbar/Pager states before showing window (bug 332024)
- Reintroduce busy property in Applet
- Make sure PlasmaQuick export file is properly found
- Don't import an nonexistent layout
- Make it possible for an applet to offer a test object
- Replace QMenu::exec with QMenu::popup
- FrameSvg: Fix dangling pointers in sharedFrames when theme changes
- IconItem: Schedule pixmap update when window changes
- IconItem: Animate active and enabled change even with animations disabled
- DaysModel: Make update a slot
- [Icon Item] Don't animate from previous pixmap when it has been invisible
- [Icon Item] Don't call loadPixmap in setColorGroup
- [Applet] Don't overwrite "Persistent" flag of Undo notification
- Allowing to override plasma mutability setting on containment creation
- Add icon/titleChanged
- Remove QtScript dependency
- Header of plasmaquick_export.h is in plasmaquick folder
- Install some plasmaquick headers

Sie können im Kommentarabschnitt des <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>Dot-Artikels</a> über diese Veröffentlichung diskutieren und Ideen einbringen.
