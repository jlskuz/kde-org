---
aliases:
- ../../kde-frameworks-5.55.0
date: 2019-02-09
layout: framework
libCount: 70
---
### Baloo

- [tags_kio] Disable access with a double slashed url, i.e. "tags://" (bug 400594)
- Instantiate QApplication before KCrash/KCatalog
- Ignore all non-storage deviceAdded signals from Solid
- Use the nicer K_PLUGIN_CLASS_WITH_JSON
- Remove Qt 5.10 checks now that we require it as min version

### Breeze Icons

- Add Cursors KCM icon
- Add and rename some YaST icons and symlinks
- Improve the Notification Bell Icon by using the KAlarm design (bug 400570)
- Add yast-upgrade
- Improve weather-storm-* icons (bug 403830)
- Add font-otf symlinks, just like the font-ttf symlinks
- Add proper edit-delete-shred icons (bug 109241)
- Add trim margins and trim to selection icons (bug 401489)
- Delete edit-delete-shred symlinks in preparation for replacing them with real icons
- Rename Activities KCM icon
- Add Activities KCM icon
- Add Virtual Desktops KCM icon
- Add icons for Touch Screen and Screen Edge KCMs
- Fix file sharing preference related icon names
- Add a preferences-desktop-effects icon
- Add a Plasma Theme Preferences Icon
- Improve contrast of preferences-system-time (bug 390800)
- Add a new preferences-desktop-theme-global icon
- Add tools icon
- document-new icon follow ColorScheme-Text
- Add a preferences-system-splash icon for the Splash Screen KCM
- Include applets/22
- Add Kotlin (.kt) mimetype icons
- Improve the preferences-desktop-cryptography icon
- Fill lock in preferences-desktop-user-password
- Consistently fill the lock in the encrypted icon
- Use a Kile Icon that is similar to the original

### Extra CMake-Module

- FindGperf: in ecm_gperf_generate set SKIP_AUTOMOC for generated file
- Move -Wsuggest-override -Wlogical-op to regular compiler settings
- Fix python binding generation for classes with deleted copy constructors
- Fix qmake module generation for Qt 5.12.1
- Use more https in links
- API dox: add missing entries for some find-modules &amp; modules
- FindGperf: improve api dox: mark-up usage example
- ECMGenerateQmlTypes: fix api dox: title needs more --- markup
- ECMQMLModules: fix api dox: title match module name, add missing "Since"
- FindInotify: fix api dox .rst tag, add missing "Since"

### KActivities

- fix for macOS

### KArchive

- Save two KFilterDev::compressionTypeForMimeType calls

### KAuth

- Remove support for passing gui QVariants to KAuth helpers

### KBookmarks

- Build without D-Bus on Android
- Const'ify

### KCMUtils

- [kcmutils] Add ellipsis to search labels in KPluginSelector

### KCodecs

- nsSJISProber::HandleData: Don't crash if aLen is 0

### KConfig

- kconfig_compiler: delete the assignment operator and copy constructor

### KConfigWidgets

- Build without KAuth and D-Bus on Android
- Add KLanguageName

### KCrash

- Comment why changing the ptracer is required
- [KCrash] Establish socket to allow change of ptracer

### KDeclarative

- [KCM Controls GridView] Add remove animation

### KDELibs 4 Support

- Fix some country flags to use all the pixmap

### KDESU

- handle wrong password when using sudo which asks for another password (bug 389049)

### KFileMetaData

- exiv2extractor: add support for bmp, gif, webp, tga
- Fix failing test of exiv gps data
- Test empty and zero gps data
- add support for more mimetypes to taglibwriter

### KHolidays

- holidays/plan2/holiday_ua_uk - updated for 2019

### KHTML

- Add JSON metadata to khtmlpart plugin binary

### KIconThemes

- Build without D-Bus on Android

### KImageFormats

- xcf: Fix fix for opacity being out of bounds
- Uncomment the qdebug includes
- tga: Fix Use-of-uninitialized-value on broken files
- max opacity is 255
- xcf: Fix assert in files with two PROP_COLORMAP
- ras: Fix assert because of ColorMapLength being too big
- pcx: Fix crash on fuzzed file
- xcf: Implement robustness for when PROP_APPLY_MASK is not on the file
- xcf: loadHierarchy: Obey the layer.type and not the bpp
- tga: Don't support more than 8 alpha bits
- ras: Return false if allocating the image failed
- rgb: Fix integer overflow in fuzzed file
- rgb: Fix Heap-buffer-overflow in fuzzed file
- psd: Fix crash on fuzzed file
- xcf: Initialize x/y_offset
- rgb: Fix crash in fuzzed image
- pcx: Fix crash on fuzzed image
- rgb: fix crash in fuzzed file
- xcf: initialize layer mode
- xcf: initialize layer opacity
- xcf: set buffer to 0 if read less data that expected
- bzero -&gt; memset
- Fix various OOB reads and writes in kimg_tga and kimg_xcf
- pic: resize header id back if didn't read 4 bytes as expected
- xcf: bzero buffer if read less data than expected
- xcf: Only call setDotsPerMeterX/Y if PROP_RESOLUTION is found
- xcf: initialize num_colors
- xcf: Initialize layer visible property
- xcf: Don't cast int to enum that can't hold that int value
- xcf: Do not overflow int on the setDotsPerMeterX/Y call

### KInit

- KLauncher: handle processes exiting without error (bug 389678)

### KIO

- Improve keyboard controls of the checksum widget
- Add helper function to disable redirections (useful for kde-open)
- Revert "Refactor SlaveInterface::calcSpeed" (bug 402665)
- Don't set CMake policy CMP0028 to old. We don't have targets with :: unless they are imported.
- [kio] Add ellipsis to search label in Cookies section
- [KNewFileMenu] Don't emit fileCreated when creating a directory (bug 403100)
- Use (and suggest using) the nicer K_PLUGIN_CLASS_WITH_JSON
- avoid blocking kio_http_cache_cleaner and ensure exit with session (bug 367575)
- Fix failing knewfilemenu test and underlying reason for its failure

### Kirigami

- raise only colored buttons (bug 403807)
- fix height
- same margins sizing policy as the other list items
- === operators
- support for the concept of expandible items
- don't clear when replacing all the pages
- horizontal padding is double than vertical
- take padding into account to compute size hints
- [kirigami] Do not use light font styles for headings (2/3) (bug 402730)
- Unbreak the AboutPage layout on smaller devices
- stopatBounds for breadcrumb flickable

### KItemViews

- [kitemviews] Change the search in Desktop Behavior/Activities to more in line with other search labels

### KJS

- Set SKIP_AUTOMOC for some generated files, to deal with CMP0071

### KNewStuff

- Fix semantics for ghns_exclude (bug 402888)

### KNotification

- Fix memory leak when passing icon data to Java
- Remove the AndroidX support library dependency
- Add Android notification channel support
- Make notifications work on Android with API level &lt; 23
- Move the Android API level checks to runtime
- Remove unused forward declaration
- Rebuild AAR when Java sources change
- Build the Java side with Gradle, as AAR instead of JAR
- Don't rely on the Plasma workspace integration on Android
- Search for notification event configuration in qrc resources

### KPackage-Framework

- Make translations work

### KPty

- Fix struct/class mismatch

### KRunner

- Remove explicit use of ECM_KDE_MODULE_DIR, is part of ECM_MODULE_PATH

### KService

- Build without D-Bus on Android
- Suggest people to use K_PLUGIN_CLASS_WITH_JSON

### KTextEditor

- Qt 5.12.0 has issues in the regex implementation in QJSEngine, indenters might behave incorrectly, Qt 5.12.1 will have a fix
- KateSpellCheckDialog: Remove action "Spellcheck Selection"
- Update JavaScript library underscore.js to version 1.9.1
- Fix bug 403422: Allow changing the marker size again (bug 403422)
- SearchBar: Add Cancel button to stop long running tasks (bug 244424)
- Remove explicit use of ECM_KDE_MODULE_DIR, is part of ECM_MODULE_PATH
- Review KateGotoBar
- ViewInternal: Fix 'Go to matching bracket' in override mode (bug 402594)
- Use HTTPS, if available, in links visible to users
- Review KateStatusBar
- ViewConfig: Add option to paste at cursor position by mouse (bug 363492)
- Use the nicer K_PLUGIN_CLASS_WITH_JSON

### KWayland

- [server] Generate correct touch ids
- Make XdgTest spec compliant
- Add option to use wl_display_add_socket_auto
- [server] Send initial org_kde_plasma_virtual_desktop_management.rows
- Add rows info to the plasma virtual desktop protocol
- [client] Wrap wl_shell_surface_set_{class,title}
- Guard resource deletion in OuptutConfiguration::sendApplied

### KWidgetsAddons

- [KWidgetsAddons] Do not use light font styles for headings (3/3) (bug 402730)

### KXMLGUI

- Build without D-Bus on Android
- Make KCheckAccelerators less invasive for apps that don't directly link to KXmlGui

### ModemManagerQt

- Fix QVariantMapList operator &gt;&gt; implementation

### Plasma Framework

- [Wallpaper templates] Add missing Comment= entry to desktop file
- Share Plasma::Theme instances between multiple ColorScope
- Make the clock svg's shadows more logically correct and visually appropriate (bug 396612)
- [frameworks] Do not use light font styles for headings (1/3) (bug 402730)
- [Dialog] Don't alter mainItem's visibility
- Reset parentItem when mainItem changes

### Purpose

- Use the nicer K_PLUGIN_CLASS_WITH_JSON

### QQC2StyleBridge

- Fix combobox initial sizing (bug 403736)
- Set combobox popups to be modal (bug 403403)
- partly revert 4f00b0cabc1230fdf
- Word-wrap long tooltips (bug 396385)
- ComboBox: fix default delegate
- Fake mousehover whilst combobox is open
- Set CombooBox QStyleOptionState == On rather than Sunken to match qwidgets (bug 403153)
- Fix ComboBox
- Always draw the tooltip on top of everything else
- Support a tooltip on a MouseArea
- do not force text display for ToolButton

### Solid

- Build without D-Bus on Android

### Sonnet

- Don't call this code if we have only space

### Syntax Highlighting

- Fix end of folding region in rules with lookAhead=true
- AsciiDoc: Fix highlighting of include directive
- Add AsciiDoc support
- Fixed Bug Which Caused Infinite Loop While Highlighting Kconfig Files
- check for endless context switches
- Ruby: fix RegExp after ": " and fix/improve detection of HEREDOC (bug 358273)
- Haskell: Make = a special symbol

### Sicherheitsinformation

Der veröffentlichte Quelltext wurde mit GPG mit folgendem Schlüssel signiert: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärer Fingerprint des Schlüssels: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
