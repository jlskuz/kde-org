---
aliases:
- ../announce-applications-16.12-beta
date: 2016-11-18
description: KDE veröffentlicht die Anwendungen 16.12 Beta.
layout: application
release: applications-16.11.80
title: KDE veröffentlicht die Beta-Version der KDE-Anwendungen 16.12
---
18. November 2016. Heute veröffentlicht KDE die Beta-Version der neuen Ausgaben der KDE-Anwendungen. Mit dem Einfrieren von Abhängigkeiten und Funktionen konzentriert sich das KDE-Team auf die Behebung von Fehlern und Verbesserungen.

In den <a href='https://community.kde.org/Applications/16.12_Release_Notes'>Veröffentlichungshinweisen der KDE-Gemeinschaft</a> finden Sie Informationen über neue Quelltextarchive, Portierungen zu KF5 und bekannte Probleme. Eine vollständigere Ankündigung erscheint mit der endgültigen Version.

Es sind sind gründliche Tests für die Veröffentlichung der KDE-Anwendungen 16.12 nötig, um die Qualität und Benutzererfahrung beizubehalten und zu verbessern. Benutzer, die KDE täglich benutzen, sind sehr wichtig, um die hohe Qualität der KDE-Software zu erhalten, weil Entwickler nicht jede mögliche Kombination von Anwendungsfällen testen können. Diese Benutzer können Fehler finden, so dass sie vor der endgültigen Veröffentlichung korrigiert werden können. Beteiligen Sie sich beim KDE-Team und installieren Sie die Beta-Version und berichten Sie alle <a href='https://bugs.kde.org/'>Fehler</a>.
