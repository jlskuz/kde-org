---
aliases:
- ../announce-applications-19.04.3
changelog: true
date: 2019-07-11
description: KDE veröffentlicht die Anwendungen 19.04.3.
layout: applications
major_version: '19.04'
release: applications-19.04.3
title: KDE veröffentlicht die KDE-Anwendungen 19.04.3
version: 19.04.3
---
{{% i18n_date %}}

Heute veröffentlicht KDE die erste Aktualisierung der <a href='../19.04.0'>KDE-Anwendungen 19.04</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Over sixty recorded bugfixes include improvements to Kontact, Ark, Cantor, JuK, K3b, Kdenlive, KTouch, Okular, Umbrello, among others.

Verbesserungen umfassen:

- Konqueror and Kontact no longer crash on exit with QtWebEngine 5.13
- Cutting groups with compositions no longer crashes the Kdenlive video editor
- The Python importer in Umbrello UML designer now handles parameters with default arguments
