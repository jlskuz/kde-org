---
aliases:
- ../announce-applications-18.08.3
changelog: true
date: 2018-11-08
description: KDE veröffentlicht die KDE-Anwendungen 18.08.3
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 18.08.3
version: 18.08.3
---
08.November 2018. Heute veröffentlicht KDE die dritte Aktualisierung der <a href='../18.08.0'>KDE-Anwendungen 18.08</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 20 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für Kontact, Ark, Dolphin, KDE-Spiele, Kate, Okular und Umbrello

Verbesserungen umfassen:

- HTML viewing mode in KMail is remembered, and again loads external images if allowed
- Kate now remembers meta information (including bookmarks) between editing sessions
- Automatic scrolling in the Telepathy text UI was fixed with newer QtWebEngine versions
