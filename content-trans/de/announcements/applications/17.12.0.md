---
aliases:
- ../announce-applications-17.12.0
date: 2017-12-14
description: KDE veröffentlicht die KDE-Anwendungen 17.12.0
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 17.12.0
version: 17.12.0
---
14 Dezember 2017. KDE veröffentlicht heute die KDE-Anwendungen 17.12.0.

We continuously work on improving the software included in our KDE Application series, and we hope you will find all the new enhancements and bug fixes useful!

### Neu in den KDE-Anwendungen 17.12

#### System

{{<figure src="/announcements/applications/17.12.0/dolphin1712.gif" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, our file manager, can now save searches and limit the search only to folders. Renaming files is now easier; simply double click on the file name. More file information is now at your hands, as the modification date and origin URL of downloaded files are now displayed in the information panel. Additionally, new Genre, Bitrate, and Release Year columns have been introduced.

#### Grafik

Our powerful document viewer <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a> gained support for HiDPI displays and Markdown language, and the rendering of documents that are slow to load is now shown progressively. An option is now available to share a document via email.

<a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a> image viewer can now open and highlight images in the file manager, zooming is smoother, keyboard navigation has been improved, and it now supports the FITS and Truevision TGA formats. Images are now protected from being accidentally removed by the Delete key when they are not selected.

#### Multimedia

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> now uses less memory when handling video projects which include many images, default proxy profiles have been refined, and an annoying bug related to jumping one second forward when playing backward has been fixed.

#### Dienstprogramme

{{<figure src="/announcements/applications/17.12.0/kate1712.png" width="600px" >}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>'s zip support in the libzip backend has been improved. <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a> has a new <a href='https://frinring.wordpress.com/2017/09/25/ktexteditorpreviewplugin-0-1-0/'>Preview plugin</a> that allows you to see a live preview of the text document in the final format, applying any available KParts plugins (e.g. for Markdown, SVG, Dot graph, Qt UI, or patches). This plugin also works in <a href='https://www.kde.org/applications/development/kdevelop/'>KDevelop.</a>

#### Entwicklung

{{<figure src="/announcements/applications/17.12.0/kuiviewer1712.png" width="600px" >}}

<a href='https://www.kde.org/applications/development/kompare/'>Kompare</a> now provides a context menu in the diff area, allowing for quicker access to navigation or modification actions. If you are a developer, you might find KUIViewers' new in-pane preview of UI object described by Qt UI files (widgets, dialogs, etc) useful. It now also supports KParts streaming API.

#### Büroprogramme

The <a href='https://www.kde.org/applications/office/kontact'>Kontact</a> team has been hard at work improving and refining. Much of the work has been modernizing the code, but users will notice that encrypted messages display has been improved and support has been added for text/pgp and <a href='https://phabricator.kde.org/D8395'>Apple® Wallet Pass</a>. There is now an option to select IMAP folder during vacation configuration, a new warning in KMail when a mail gets reopened and identity/mailtransport is not the same, new <a href='https://micreabog.wordpress.com/2017/10/05/akonadi-ews-resource-now-part-of-kde-pim/'>support for Microsoft® Exchange™</a>, support for Nylas Mail and improved Geary import in the akonadi-import-wizard, along with various other bug-fixes and general improvements.

#### Spiele

{{<figure src="/announcements/applications/17.12.0/ktuberling1712.jpg" width="600px" >}}

<a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a> can now reach a wider audience, as it has been <a href='https://tsdgeos.blogspot.nl/2017/10/kde-edu-sprint-in-berlin.html'>ported to Android</a>. <a href='https://www.kde.org/applications/games/kolf/'>Kolf</a>, <a href='https://www.kde.org/applications/games/ksirk/'>KsirK</a>, and <a href='https://www.kde.org/applications/games/palapeli/'>Palapeli</a> complete the porting of KDE games to Frameworks 5.

### Weitere Portierungen zu KDE Frameworks 5

Even more applications which were based on kdelibs4 have now been ported to KDE Frameworks 5. These include the music player <a href='https://www.kde.org/applications/multimedia/juk/'>JuK</a>, the download manager <a href='https://www.kde.org/applications/internet/kget/'>KGet</a>, <a href='https://www.kde.org/applications/multimedia/kmix/'>KMix</a>, utilities such as <a href='https://www.kde.org/applications/utilities/sweeper/'>Sweeper</a> and <a href='https://www.kde.org/applications/utilities/kmouth/'>KMouth</a>, and <a href='https://www.kde.org/applications/development/kimagemapeditor/'>KImageMapEditor</a> and Zeroconf-ioslave. Many thanks to the hard-working developers who volunteered their time and work to make this happen!

### Applications moving to their own release schedule

<a href='https://www.kde.org/applications/education/kstars/'>KStars</a> now has its own release schedule; check this <a href='https://knro.blogspot.de'>developer's blog</a> for announcements. It is worth noting that several applications such as Kopete and Blogilo are <a href='https://community.kde.org/Applications/17.12_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>no longer shipped</a> with the Application series, as they have not yet been ported to KDE Frameworks 5, or are not actively maintained at the moment.

### Bug Stomping

Mehr als 110 Fehler in Kontact, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello und weiteren Anwendungen wurden behoben.

### Vollständiges Änderungsprotokoll

If you would like to read more about the changes in this release, <a href='/announcements/changelogs/applications/17.12.0'>head over to the complete changelog</a>. Although a bit intimidating due to its breadth, the changelog can be an excellent way to learn about KDE's internal workings and discover apps and features you never knew you had.
