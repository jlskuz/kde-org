---
aliases:
- ../announce-applications-16.04-rc
custom_spread_install: true
date: 2016-04-05
description: KDE veröffentlicht den Freigabekandidaten der KDE-Anwendungen 16.04
layout: application
release: applications-16.03.90
title: KDE veröffentlicht den Freigabekandidaten der KDE-Anwendungen 16.04
---
07. April 2016. Heute veröffentlicht KDE den Freigabekandidaten der neuen Version der KDE-Anwendungen. Mit dem Einfrieren von Abhängigkeiten und Funktionen konzentriert sich das KDE-Team auf die Behebung von Fehlern und Verbesserungen.

Bei den verschiedenen Anwendungen auf der Grundlage der KDE Frameworks 5 sind gründliche Tests für die Veröffentlichung der KDE Anwendungen 16.04 nötig, um die Qualität und Benutzererfahrung beizubehalten und zu verbessern. Anwender, die KDE täglich benutzen, sind sehr wichtig, um die hohe Qualität der KDE-Software zu erhalten, weil Entwickler nicht jede mögliche Kombination von Anwendungsfällen testen können. Diese Benutzer können Fehler finden, so dass sie vor der endgültigen Veröffentlichung korrigiert werden können. Beteiligen Sie sich beim Team und installieren Sie die Version und berichten Sie alle <a href='https://bugs.kde.org/'>Fehler</a>.

#### Binärpakete für KDE-Anwendungen 16.04 (Freigabekandidat) installieren

<em>Pakete</em>. Einige Anbieter von Linux-/UNIX-Betriebssystemen haben dankenswerterweise Binärpakete des Veröffentlichungskandidaten der KDE-Anwendungen für 16.04 (intern 16.03.97) für einige Versionen Ihrer Distributionen bereitgestellt, ebenso wie freiwillige Mitglieder der Gemeinschaft. Zusätzliche binäre Pakete und Aktualisierungen der jetzt verfügbaren Pakete werden in den nächsten Wochen bereitgestellt.

<em>Paketquellen</em>. Eine aktuelle Liste aller Binärpakete, von denen das KDE-Projekt in Kenntnis gesetzt wurde, finden Sie im <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Community Wiki</a>.

#### Kompilieren des Freigabekandidaten der KDE-Anwendungen 16.04

Der vollständige Quelltext des Freigabekandidaten der KDE-Anwendungen 16.04 kann <a href='http://download.kde.org/unstable/applications/16.03.90/src/'>hier</a> heruntergeladen werden. Anweisungen zum Kompilieren und Installieren finden Sie auf der <a href='/info/applications/applications-16.03.90'>Infoseite des Freigabekandidaten der KDE-Anwendungen </a>.
