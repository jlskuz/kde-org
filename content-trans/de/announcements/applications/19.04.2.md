---
aliases:
- ../announce-applications-19.04.2
changelog: true
date: 2019-06-06
description: KDE veröffentlicht die Anwendungen 19.04.2.
layout: application
major_version: '19.04'
release: applications-19.04.2
title: KDE veröffentlicht die KDE-Anwendungen 19.04.2
version: 19.04.2
---
{{% i18n_date %}}

Heute veröffentlicht KDE die erste Aktualisierung der <a href='../19.04.0'>KDE-Anwendungen 19.04</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Nearly fifty recorded bugfixes include improvements to Kontact, Ark, Dolphin, JuK, Kdenlive, KmPlot, Okular, Spectacle, among others.

Verbesserungen umfassen:

- A crash with viewing certain EPUB documents in Okular has been fixed
- Secret keys can again be exported from the Kleopatra cryptography manager
- The KAlarm event reminder no longer fails to start with newest PIM libraries
