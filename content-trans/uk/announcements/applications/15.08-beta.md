---
aliases:
- ../announce-applications-15.08-beta
date: 2015-07-28
description: KDE випущено Програми 15.08 Beta.
layout: application
release: applications-15.07.80
title: KDE випущено тестову версію програм KDE 15.08
---
28 липня 2015 року. Сьогодні командою KDE випущено тестовий варіант нових версій програм. Від цього моменту заморожено залежності та список можливостей, — команда KDE зосереджує зусилля на виправлені вад та удосконаленні нової версії.

Через те, що частину програм побудовано на основі KDE Frameworks 5, випуск 15.08 потребує ретельного тестування з метою підтримання та поліпшення якості та зручності у користуванні. Користувачі є надзвичайно важливою ланкою у підтриманні високої якості випусків KDE, оскільки розробникам просто не вистачить часу перевірити всі можливі комбінацій обладнання та налаштувань системи. Ми розраховуємо на вашу допомогу у якомога швидшому виявленні вад, щоб уможливити виправлення цих вад до остаточного випуску. Будь ласка, долучіться до команди тестувальників , встановивши нову версію <a href='https://bugs.kde.org/'>і повідомивши про всі виявлені вади</a>.
