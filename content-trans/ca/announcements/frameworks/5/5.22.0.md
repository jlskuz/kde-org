---
aliases:
- ../../kde-frameworks-5.22.0
date: 2016-05-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Comprovar adequadament si un URL és un fitxer local

### Baloo

- Esmenes de compilació per al Windows

### Icones Brisa

- Diverses accions i icones d'aplicacions noves.
- Especificar les extensions oferides en canviar en el «kiconthemes»

### Mòduls extres del CMake

- Desplegament a l'Android: permetre projectes sense res a «share» ni a «lib/qml» (error 362578)
- Activar KDE_INSTALL_USE_QT_SYS_PATHS si hi ha el prefix CMAKE_INSTALL_PREFIX de les Qt5
- ecm_qt_declare_logging_category: millorar el missatge d'error en usar sense inclusió

### Integració del marc de treball

- Eliminar el connector «platformtheme», ja que està a «plasma-integration»

### KCoreAddons

- Proporcionar una manera de desactivar l'ús del «inotify» al KDirWatch
- Esmenar KAboutData::applicationData() per iniciar-se des de les metadades de la Q*Application actual
- Deixar clar que no es recomana el KRandom per a finalitats criptogràfiques

### KDBusAddons

- KDBusService: canviar «-» per «_» en els camins d'objectes

### KDeclarative

- No fallar si no hi ha context openGL

### Compatibilitat amb les KDELibs 4

- Proporcionar una MAXPATHLEN alternativa si no està definida
- Solucionar KDateTime::isValid() per a valors de ClockTime (error 336738)

### KDocTools

- S'han afegit entitats d'aplicacions

### KFileMetaData

- Fusionar la branca «externalextractors»
- S'han esmenat connectors externs i proves
- S'ha afegit la implementació per a connectors d'escriptura externs
- S'ha afegit la implementació del connector d'escriptura
- Afegir la implementació per al connector extractor extern

### KHTML

- Implementar «toString» per a l'Uint8ArrayConstructor i similars
- Fusió a diverses esmenes relacionades amb el Coverity
- Usar correctament QCache::insert
- Solucionar diverses fuites de memòria
- Verificació de prudència en l'anàlisi del tipus de lletra web als CSS, per evitar fuites de memòria potencials
- dom: afegir prioritats d'etiqueta per l'etiqueta «comment»

### KI18n

- libgettext: solucionar un ús potencial després d'alliberar usant compiladors no g++

### KIconThemes

- Usar un contenidor apropiat per a una matriu interna d'apuntadors
- Afegir l'oportunitat de reduir accessos a disc no necessaris, incorporar les KDE-Extensions
- Estalviar diversos accessos al disc

### KIO

- kurlnavigatortoolbutton.cpp - Usar «buttonWidth» a «paintEvent()»
- Menú de fitxer nou: filtra duplicats (p. ex. entre fitxers .grc i del sistema) (error 355390)
- Solucionar el missatge d'error en iniciar el KCM de Galetes
- Eliminar el «kmailservice5», que només pot fer mal en aquest punt (error 354151)
- Solucionar «KFileItem::refresh()» per als enllaços simbòlics. Es definien una mida, tipus de fitxer i permisos incorrectes
- Solucionar una regressió en el KFileItem: el «refresh()» perdia el tipus de fitxers, de manera que un directori esdevenia un fitxer (error 353195)
- Definir text al giny QCheckbox en lloc d'usar una etiqueta separada (error 245580)
- No activar el giny de permisos d'ACL si no és propietari del fitxer (error 245580)
- Solucionar la doble barra als resultats del KUriFilter quan s'ha definit un filtre de nom
- KUrlRequester: afegir el senyal «textEdited» (reenviat des de QLineEdit)

### KItemModels

- Esmenar la sintaxi de la plantilla per la generació de casos de proves
- Solucionar l'enllaçat amb les Qt 5.4 (#endif situat incorrectament)

### KParts

- Solucionar la disposició del diàleg BrowserOpenOrSaveQuestion

### KPeople

- Afegir una comprovació perquè PersonData sigui vàlid

### KRunner

- Esmenar metainfo.yaml: el KRunner ni és una ajuda d'adaptació ni és obsolet

### KService

- Eliminar una longitud màxima de cadena massa estricta a la base de dades KSycoca

### KTextEditor

- Usar una sintaxi de caràcter adequada «"» en lloc de «"»
- doxygen.xml: usar l'estil predeterminat «dsAnnotation» per les «Etiquetes personalitzades» també (menys colors fixos al codi)
- Afegir una opció per mostrar el comptador de paraules
- Contrast millorat del color de fons per al ressaltat de cerca i substitució
- Solucionar una fallada en tancar el Kate a través de D-Bus mentre el diàleg d'impressió està obert (error 356813)
- Cursor::isValid(): afegir una nota quant a «isValidTextPosition()»
- Afegir l'API {Cursor, Range}::{toString, static fromString}

### KUnitConversion

- Informar el client si no es coneix la relació de conversió
- Afegir la divisa ILS (nou xéquel israelià) (error 336016)

### Framework del KWallet

- Desactivació de la restauració de sessió per al kwalletd5

### KWidgetsAddons

- KNewPasswordWidget: eliminar la reserva de mida a l'espaiador, que ha portat a diversos espais sempre buits a la disposició
- KNewPasswordWidget: solucionar el QPalette quan el giny està desactivat

### KWindowSystem

- Solucionar la generació del camí al connector «xcb»

### Frameworks del Plasma

- [QuickTheme] Solucionar les propietats
- highlight/highlightedText des del grup de colors adequat
- ConfigModel: no intentar resoldre un camí d'origen buit des del paquet
- [calendari] Mostrar només les marques d'esdeveniments a la graella dels dies, no a la mensual ni a l'anual
- declarativeimports/core/windowthumbnail.h - esmenar un avís -Wreorder
- Recarregar adequadament el tema d'icones
- Escriure sempre el nom del tema al «plasmarc», també si es tria el tema predeterminat
- [calendari] Afegir una marca als dies que contenen un esdeveniment
- Afegir colors de text Positius, Neutres, Negatius
- ScrollArea: esmenar l'avís quan el «contentItem» no és «Flickable»
- Afegir una propietat i un mètode per alinear el menú a una cantonada del seu pare visual
- Permetre definir una amplada mínima en el Menú
- Mantenir l'ordre en una llista emmagatzemada d'elements
- Ampliar l'API per permetre el (re)posicionament dels elements del menú durant la inserció d'un procediment
- Vincular el color «highlightedText» en el Plasma::Theme
- Solucionar la restauració d'aplicacions/URL associades en el Plasma::Applets
- No exposar els símbols de la classe privada DataEngineManager
- Afegir un element «esdeveniment» en el SVG de calendari
- SortFilterModel: invalidar el filtre en canviar la crida de retorn del filtratge

### Sonnet

- Instal·lar l'eina «parsetrigrams» per a la compilació creuada
- hunspell: carregar/emmagatzemar un diccionari personal
- Implementar hunspell 1.4
- configwidget: notificar el canvi de configuració en actualitzar mots ignorats
- Configuració: no desar immediatament la configuració en actualitzar la llista d'ignorats
- configwidget: solucionar el desament en actualitzar la llista de mots ignorats
- Solucionar la fallada del problema de desar mots ignorats (error 355973)

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
