---
aliases:
- ../../kde-frameworks-5.49.0
date: 2018-08-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- No crea una instància «QStringRef» en una «QString» només per a cercar en una «QStringList»
- Es defineixen els elements quan estan declarats

### Baloo

- [tags_kio] S'han esmenat les múltiples còpies del nom de fitxer
- Reverteix «[tags_kio] Utilitza UDS_URL en comptes d'UDS_TARGET_URL»
- [tags_kio] Utilitza UDS_URL en comptes d'UDS_TARGET_URL
- [tags_kio] Consulta els camins de destinació en comptes d'afegir els camins a l'entrada UDS del fitxer
- S'han implementat els URL especials per a trobar fitxers d'un determinat tipus
- S'evita la manipulació de les llistes amb una complexitat quadràtica
- S'utilitza un «fastInsert» no obsolet al Baloo

### Icones Brisa

- S'ha afegit la icona <code>drive-optical</code> (error 396432)

### Mòduls extres del CMake

- Android: no codifica una versió aleatòria de l'SDK de l'Android
- ECMOptionalAddSubdirectory: proporciona una mica més de detall
- S'ha esmenat la comprovació de la definició de variable
- S'ha canviat la versió «since»
- Millora «ECMAddAppIconMacro»

### Integració del marc de treball

- Respecta BUILD_TESTING

### KActivities

- Respecta BUILD_TESTING

### KArchive

- Respecta BUILD_TESTING

### KAuth

- S'eviten els avisos per a les capçaleres de «PolkitQt5-1»
- Respecta BUILD_TESTING

### KBookmarks

- Respecta BUILD_TESTING

### KCodecs

- Respecta BUILD_TESTING

### KCompletion

- Respecta BUILD_TESTING

### KConfig

- Respecta BUILD_TESTING

### KConfigWidgets

- Respecta BUILD_TESTING

### KCoreAddons

- S'ha esmenat el desbordament en el codi d'arrodoniment (error 397008)
- API dox: s'ha eliminat el no-ha-d'estar «:» al darrere de «@note»
- API dox: parla de «nullptr», no de «0»
- KFormat: s'han substituït els unicode literals amb el punt de codificació unicode per a corregir la construcció de MSVC
- KFormat: s'ha corregit l'etiqueta «@since» per al nou «KFormat::formatValue»
- KFormat: permet l'ús de quantitats més enllà de bytes i segons
- S'han corregit els exemples de «KFormat::formatBytes»
- Respecta BUILD_TESTING

### KCrash

- Respecta BUILD_TESTING

### KDBusAddons

- No bloqueja sempre a «ensureKdeinitRunning»
- Respecta BUILD_TESTING

### KDeclarative

- S'assegura que sempre estem escrivint en el context arrel del motor
- Millor llegibilitat
- Millora una mica la documentació de l'API
- Respecta BUILD_TESTING

### Compatibilitat amb les KDELibs 4

- S'ha esmenat «qtplugins» al «KStandardDirs»

### KDocTools

- Respecta BUILD_TESTING

### KEmoticons

- Respecta BUILD_TESTING

### KFileMetaData

- API dox: s'afegeix «@file» a la capçalera només per a les funcions, perquè el «doxygen» ho cobreixi

### KGlobalAccel

- Respecta BUILD_TESTING

### Complements de la IGU del KDE

- Respecta BUILD_TESTING

### KHolidays

- Instal·la la capçalera de càlcul de la posta de sol/alba
- S'ha afegit el dia de l'any de traspàs com a festa (cultural) per a Noruega
- S'ha afegit l'entrada «name» als fitxers dels festius noruecs
- S'han afegit descripcions als fitxers dels festius noruecs
- Més actualitzacions dels festius japonesos des de «phanect»
- holiday_jp_ja, holiday_jp-en_us - actualitzats (error 365241)

### KI18n

- Es torna a emprar la funció que ja fa el mateix
- S'ha esmenat el maneig del catàleg i la detecció de la configuració regional a l'Android
- Llegibilitat, s'ometen les declaracions «no-op»
- S'ha esmenat el «KCatalog::translate» quan la traducció és la mateixa que el text original
- S'ha canviat el nom d'un fitxer
- Permet que el nom de fitxer de la macro del «ki18n» segueixi l'estil dels altres fitxers relacionats amb «find_package»
- S'ha esmenat la comprovació de la configuració per a «_nl_msg_cat_cntr»
- No genera els fitxers al directori d'origen
- libintl: determina si existeix «if _nl_msg_cat_cntr» abans d'usar-lo (error 365917)
- S'han esmenat les compilacions per a la fàbrica de binaris

### KIconThemes

- Respecta BUILD_TESTING

### KIO

- Ara instal·la el fitxer de categoria «kdebugsettings» relacionat amb «kio»
- S'ha canviat el nom de la capçalera privada a «_p.h»
- S'ha eliminat la selecció personalitzada d'icones per a la paperera (error 391200)
- Les etiquetes van alineades a dalt al diàleg de propietats
- Presenta un diàleg d'error quan l'usuari intenta crear un directori anomenat «.» o «..» (error 387449)
- API dox: parla de «nullptr», no de «0»
- Prova de referència «lstItems» al «kcoredirlister»
- [KSambaShare] Comprova el fitxer que ha canviat abans de tornar a carregar
- [KDirOperator] Utilitza els colors de fons alternatius per a les vistes amb múltiples columnes
- Evita les pèrdues de memòria en les tasques esclaves (error 396651)
- SlaveInterface: fa obsolet «setConnection/connection», ningú pot usar-los
- Un constructor UDS una mica més ràpid
- [KFilePlacesModel] S'admeten els bonics URL de cerca del Baloo
- S'ha eliminat la drecera web projects.kde.org
- S'ha canviat «KIO::convertSize()» a «KFormat::formatByteSize()»
- S'utilitza un «fastInsert» no obsolet al file.cpp (el primer dels molts per venir)
- Se substitueix la drecera web de Gitorious per la de GitLab
- No mostra el diàleg de confirmació per a l'acció predeterminada de la paperera (error 385492)
- No sol·licita contrasenyes al «kfilewidgettest»

### Kirigami

- Implementació per afegir i eliminar dinàmicament el títol (error 396417)
- S'ha introduït «actionsVisible» (error 396413)
- S'han adaptat els marges quan apareix/desapareix la barra de desplaçament
- Una millor gestió de la mida (error 396983)
- S'ha optimitzat la configuració de la paleta
- «AbstractApplciationItem» no hauria de tenir la seva pròpia mida, només implícit
- Senyals nous «pagePushed/pageRemoved»
- S'ha esmenat la lògica
- S'ha afegit l'element «ScenePosition» (error 396877)
- No cal emetre la paleta intermediària per a cada estat
- Oculta -&gt; mostra
- Mode plegable de la barra lateral
- kirigami_package_breeze_icons: no tracta les llistes com a elements (error 396626)
- S'han esmenat les expressions regulars de cercar/substituir (error 396294)
- L'animació d'un color produeix un efecte força desagradable (error 389534)
- Element centrat en un color per a la navegació amb el teclat
- S'ha eliminat la drecera ràpida
- S'ha eliminat la cadena «Encoding=UTF-8» -obsoleta des de fa molt de temps- del fitxer amb format «desktop»
- S'ha esmenat la mida de la barra d'eines (error 396521)
- S'han esmenat les nanses de canvi de mida
- Respecta BUILD_TESTING
- Mostra icones per a les accions que tenen un origen d'icona en comptes d'un nom d'icona

### KItemViews

- Respecta BUILD_TESTING

### KJobWidgets

- Respecta BUILD_TESTING

### KJS

- Respecta BUILD_TESTING

### KMediaPlayer

- Respecta BUILD_TESTING

### KNewStuff

- S'ha eliminat la cadena «Encoding=UTF-8» -obsoleta des de fa molt de temps- dels fitxers amb format «desktop»
- S'ha canviat l'ordre d'ordenació predeterminat al diàleg de baixada a Valoració
- S'han esmenat els marges de la finestra del diàleg de baixada per a complir amb els marges generals del tema
- Restaura el «qCDebug» eliminat accidentalment
- Utilitza l'API correcta de «QSharedPointer»
- Maneja les llistes de vista prèvia buides

### KNotification

- Respecta BUILD_TESTING

### Framework del KPackage

- Respecta BUILD_TESTING

### KParts

- API dox: parla de «nullptr», no de «0»

### KPeople

- Respecta BUILD_TESTING

### KPlotting

- Respecta BUILD_TESTING

### KPty

- Respecta BUILD_TESTING

### KRunner

- Respecta BUILD_TESTING

### KService

- API dox: parla de «nullptr», no de «0»
- Requereix una compilació fora del codi font
- S'ha afegit l'operador «subseq» per a fer coincidir les subseqüències

### KTextEditor

- Esmena adequada per a la cadena en brut, sagnada amb cometes automàtiques
- S'ha esmenat el sagnat per a fer front a un fitxer nou de sintaxi en el marc de treball «syntaxhighlighting»
- S'ha ajustat la prova al nou estat en el repositori del ressaltat de la sintaxi
- Mostra el missatge «Cerca ajustada» al centre de la vista per a una millor visibilitat
- S'ha esmenat l'avís, només fa servir «isNull()»
- S'ha ampliat l'API de creació de scripts
- S'ha esmenat una falla de segment en casos rars en què es produeix un vector buit durant el recompte de paraules
- Força la neteja de la vista prèvia a la barra de desplaçament en un document net (error 374630)

### KTextWidgets

- Documentació de l'API: s'ha revertit parcialment el commit anterior, en realitat no funcionava
- KFindDialog: dona el focus a la línia d'edició quan es mostra un diàleg reutilitzat
- KFind: restableix el recompte quan es canvia el patró (p. ex., al diàleg de cerca)
- Respecta BUILD_TESTING

### KUnitConversion

- Respecta BUILD_TESTING

### Framework del KWallet

- Respecta BUILD_TESTING

### KWayland

- Neteja les memòries intermèdies de «RemoteAccess» a «aboutToBeUnbound» en lloc de destruir els objectes
- S'ha implementat mostrar consells amb el cursor quan està bloquejat el punter
- Redueix els innecessaris temps d'espera llargs en els espies de senyal fallit
- S'ha esmenat la selecció i fa proves automàtiques dels seients
- S'han substituït els «include» globals compatibles amb V5 que mancaven
- S'ha afegit la implementació base del WM de l'XDG a la nostra API de l'XDGShell
- Fa compilable l'XDGShellV5 amb l'XDGWMBase

### KWidgetsAddons

- S'ha esmenat la màscara d'entrada de «KTimeComboBox» per als temps AM/PM (error 361764)
- Respecta BUILD_TESTING

### KWindowSystem

- Respecta BUILD_TESTING

### KXMLGUI

- S'ha esmenat la «KMainWindow» que desava una configuració incorrecta dels ginys (error 395988)
- Respecta BUILD_TESTING

### KXmlRpcClient

- Respecta BUILD_TESTING

### Frameworks del Plasma

- Si una miniaplicació no és vàlida, tindrà immediatament «UiReadyConstraint»
- [PluginLoader del Plasma] Desa els connectors a la memòria cau durant l'inici
- S'ha esmenat el node de l'esvaïment quan es troba atrapada una textura
- [Contenció] No carrega les accions de contenció amb restriccions plasma/containment_actions del KIOSK
- Respecta BUILD_TESTING

### Prison

- S'ha esmenat l'enganxament del mode mixt a superior en la generació de codi Aztec

### Purpose

- S'assegura que la depuració de «kf5.kio.core.copyjob» està desactivada per a la prova
- Reverteix «Prova: una manera més «atòmica» de comprovar si el senyal ha succeït»
- Prova: una manera més «atòmica» de comprovar si el senyal ha succeït
- S'ha afegit el connector per a Bluetooth
- [Telegram] No espera que es tanqui el Telegram
- Prepara per utilitzar els colors de l'estat d'Arc a la llista desplegable de la revisió
- Respecta BUILD_TESTING

### QQC2StyleBridge

- Millora el dimensionament dels menús (error 396841)
- Elimina la doble comparació
- L'adaptació s'allunya de les connexions basades en cadenes
- Comprova si hi ha una icona vàlida

### Solid

- Respecta BUILD_TESTING

### Sonnet

- Sonnet: «setLanguage» hauria de programar un ressaltat de la sintaxi si el ressaltat està activat
- Utilitza l'API actual del Hunspell

### Ressaltat de la sintaxi

- CoffeeScript: s'han esmenat les plantilles en el codi de JavaScript incrustat: afegeix escapades
- Exclou això a «Definition::includedDefinitions()»
- Utilitza la inicialització del membre a una classe quan sigui possible
- S'han afegit les funcions per accedir a les paraules clau
- S'ha afegit «Definition::::formats()»
- S'ha afegit «QVector&lt;Definition&gt; Definition::includedDefinitions() const»
- S'ha afegit «Theme::TextStyle Format::textStyle() const;»
- C++: s'han esmenat els punts flotants estàndard que són literals (error 389693)
- CSS: s'ha actualitzat la sintaxi i esmenat alguns errors
- C++: s'ha actualitzat per a c++20 i esmena alguns errors de sintaxi
- CoffeeScript i JavaScript: s'han esmenat els objectes dels membres. S'han afegit les extensions «.ts» al JS (error 366797)
- Lua: s'han esmenat les cadenes amb línies múltiples (error 395515)
- Spec de l'RPM: s'ha afegit el tipus MIME
- Python: s'han esmenat les escapades en els comentaris que estan entre cometes (error 386685)
- haskell.xml: no ressalta els constructors de dades Prelude de manera diferent d'altres
- haskell.xml: s'ha eliminat els tipus de la secció «prelude function»
- haskell.xml: ressalta els constructors de dades promocionats
- haskell.xml: s'han afegit les paraules clau «family», «forall», «pattern»
- Respecta BUILD_TESTING

### ThreadWeaver

- Respecta BUILD_TESTING

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
