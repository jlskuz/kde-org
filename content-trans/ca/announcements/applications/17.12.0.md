---
aliases:
- ../announce-applications-17.12.0
date: 2017-12-14
description: KDE distribueix les aplicacions 17.12.0 del KDE
layout: application
title: KDE distribueix les aplicacions 17.12.0 del KDE
version: 17.12.0
---
14 de desembre de 2017. S'han publicat les aplicacions 17.12.0 de KDE.

Treballem contínuament per a millorar el programari inclòs a les sèries de les aplicacions del KDE, i esperem que trobeu útils totes les millores i esmenes d'errors!

### Novetats a les Aplicacions 17.12 del KDE

#### Sistema

{{<figure src="/announcements/applications/17.12.0/dolphin1712.gif" width="600px" >}}

El <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, el nostre gestor de fitxers, ara pot desar cerques i limitar la cerca a només les carpetes. També és més fàcil reanomenar fitxers; simplement fent un clic doble en el nom del fitxer. Hi ha més informació del fitxer a les vostres mans, com la data de modificació i ara es mostra l'URL origen dels fitxers baixats al plafó d'informació. Addicionalment, s'han incorporat les noves columnes de gènere, taxa de bits i any de publicació.

#### Gràfics

El nostre potent visualitzador de documents <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a> ara permet pantalles HiDPI i el llenguatge Markdown, i la renderització de documents que són lents en carregar es mostra de manera progressiva. Ara hi ha disponible una opció per compartir un document via correu electrònic.

El visualitzador d'imatges <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a> ara pot obrir i ressaltar imatges al gestor de fitxers, el zoom és més suau, s'ha millorat la navegació des del teclat, i accepta els formats FITS i Truevision TGA. Les imatges ara estan protegides davant de l'eliminació accidental per la tecla Suprimir quan no estan seleccionades.

#### Multimèdia

El <a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> ara utilitza menys memòria quan gestiona projectes de vídeo que inclouen moltes imatges, s'han redefinit els perfils predeterminats dels servidors intermediaris, i s'ha esmenat un error molest relacionat en saltar un segon cap endavant en reproduir cap enrere.

#### Utilitats

{{<figure src="/announcements/applications/17.12.0/kate1712.png" width="600px" >}}

S'ha millorat la implementació del ZIP al dorsal «libzip» de l'<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>. El <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a> té un <a href='https://frinring.wordpress.com/2017/09/25/ktexteditorpreviewplugin-0-1-0/'>connector de vista prèvia</a> nou que permet veure una vista prèvia en directe del document de text en el format final, aplicant qualsevol connector disponible de les KPart (p. ex. per a Markdown, SVG, gràfics Dot, IU de les Qt o pedaços). Aquest connector també funciona al <a href='https://www.kde.org/applications/development/kdevelop/'>KDevelop</a>.

#### Desenvolupament

{{<figure src="/announcements/applications/17.12.0/kuiviewer1712.png" width="600px" >}}

El <a href='https://www.kde.org/applications/development/kompare/'>Kompare</a> ara proporciona un menú contextual a l'àrea «diff», que permet un accés ràpid a les accions de navegació o modificació. Si sou desenvolupador, podreu trobar útil la nova vista prèvia KUIViewers a la subfinestra dels objectes de la IU descrits pels fitxers IU de les Qt (ginys, diàlegs, etc.). També accepta l'API de fluxos de les KParts.

#### Oficina

L'equip del <a href='https://www.kde.org/applications/office/kontact'>Kontact</a> ha treballat força per millorar-lo i refinar. Hi ha molta feina modernitzant el codi, però els usuaris notaran que s'ha millorat la visualització dels missatges encriptats i s'ha afegit la implementació per a text/pgp i l'<a href='https://phabricator.kde.org/D8395'>Apple® Wallet Pass</a>. Ara hi ha una opció per seleccionar una carpeta IMAP durant la configuració de les absències, un avís nou al KMail quan es torna a obrir un correu i la identitat/«mailtransport» no és la mateixa, una implementació nova per a <a href='%[3'>Microsoft® Exchange™</a>, s'admet el Nylas Mail i una importació millorada del Geary a l'«akonadi-import-wizard», a més d'altres esmenes d'errors i millores generals.

#### Jocs

{{<figure src="/announcements/applications/17.12.0/ktuberling1712.jpg" width="600px" >}}

El <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a> ara pot arribar a una audiència més àmplia, ja que s'ha <a href='https://tsdgeos.blogspot.nl/2017/10/kde-edu-sprint-in-berlin.html'>adaptat a l'Android</a>. Els <a href='https://www.kde.org/applications/games/kolf/'>Kolf</a>, <a href='https://www.kde.org/applications/games/ksirk/'>KsirK</a>, i <a href='https://www.kde.org/applications/games/palapeli/'>Palapeli</a> completen l'adaptació dels jocs als Frameworks 5 del KDE.

### Més adaptacions als Frameworks 5 del KDE

També s'han adaptat més aplicacions basades en les «kdelibs4» als Frameworks 5 del KDE. Aquí s'inclouen el reproductor de música <a href='https://www.kde.org/applications/multimedia/juk/'>JuK</a>, el gestor de baixades <a href='https://www.kde.org/applications/internet/kget/'>KGet</a>, el <a href='https://www.kde.org/applications/multimedia/kmix/'>KMix</a>, utilitats com el <a href='https://www.kde.org/applications/utilities/sweeper/'>Sweeper</a> i el <a href='https://www.kde.org/applications/utilities/kmouth/'>KMouth</a>, i els <a href='https://www.kde.org/applications/development/kimagemapeditor/'>KImageMapEditor</a> i Zeroconf-ioslave. Moltes gràcies a tots els desenvolupadors que han fet una gran feina i que han ofert voluntàriament el seu temps i treball per fer que això sigui possible!

### Aplicacions que tindran la seva pròpia planificació de llançaments

Ara el <a href='https://www.kde.org/applications/education/kstars/'>KStars</a> té la seva pròpia planificació de llançaments; comproveu aquest <a href='https://knro.blogspot.de'>blog dels desenvolupadors</a> per als anuncis. Cal destacar que diverses aplicacions com el Kopete o el Blogilo <a href='https://community.kde.org/Applications/17.12_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>ja no es distribuiran</a> amb les sèries de les aplicacions, ja que no s'han adaptat als Frameworks 5 del KDE, o no estan mantingudes activament en aquest moment.

### Cacera d'errors

S'han resolt més de 110 errors a les aplicacions, incloses el paquet Kontact, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello i més!

### Registre complet de canvis

Si us agradaria llegir més quant als canvis d'aquest llançament, <a href='/announcements/changelogs/applications/17.12.0'>dirigiu-vos al registre complet de canvis</a>. Encara que és una mica intimidant a causa de la seva extensió, el registre de canvis pot ser una manera excel·lent d'aprendre pel que fa a les tasques internes del KDE i descobrir aplicacions i funcionalitats que mai hauríeu sospitat.
