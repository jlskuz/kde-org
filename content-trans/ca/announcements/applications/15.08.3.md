---
aliases:
- ../announce-applications-15.08.3
changelog: true
date: 2015-11-10
description: KDE distribueix les aplicacions 15.08.3 del KDE
layout: application
title: KDE distribueix les aplicacions 15.08.3 del KDE
version: 15.08.3
---
10 de novembre de 2015. Avui KDE distribueix la tercera actualització d'estabilització per a les <a href='../15.08.0'>aplicacions 15.08 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 20 esmenes registrades d'errors que inclouen millores als programes Ark, Dolphin, Kdenlive, Kdepim, Kig, Lokalize i Umbrello.

Aquest llançament també inclou les versions de suport a llarg termini de la plataforma de desenvolupament KDE 4.14.14.
