---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: KDE distribueix les aplicacions 18.08.1 del KDE
layout: application
title: KDE distribueix les aplicacions 18.08.1 del KDE
version: 18.08.1
---
6 de setembre de 2018. Avui KDE distribueix la primera actualització d'estabilització per a les <a href='../18.08.0'>aplicacions 18.08 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més d'una dotzena d'esmenes registrades d'errors que inclouen millores al Kontact, Cantor, Gwenview, Okular i Umbrello, entre d'altres.

Les millores inclouen:

- El component KIO-MTP no fallarà més quan el dispositiu ja està accedit per una aplicació diferent
- L'enviament de correus al KMail ara usa la contrasenya quan s'especifica via la pregunta de contrasenya
- Ara l'Okular recorda el mode de la barra lateral després de desar documents PDF
