---
aliases:
- ../announce-applications-19.08.1
changelog: true
date: 2019-09-05
description: Es distribueixen les aplicacions 19.08.1 del KDE.
layout: application
major_version: '19.08'
release: applications-19.08.1
title: Es distribueixen les aplicacions 19.08.1 del KDE
version: 19.08.1
---
{{% i18n_date %}}

Avui, KDE distribueix la primera actualització d'estabilització per a les <a href='../19.08.0'>Aplicacions 19.08 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, i proporciona una actualització segura i millor per a tothom.

Hi ha més de vint esmenes registrades d'errors que inclouen millores al Kontact, Dolphin, Kdenlive, Konsole i Step, entre d'altres.

Les millores inclouen:

- S'han corregit diverses regressions a la gestió de pestanyes del Konsole
- El Dolphin es torna a iniciar correctament en el mode de vista dividida
- La supressió d'un cos tou al simulador de física Step ja no provoca una fallada
