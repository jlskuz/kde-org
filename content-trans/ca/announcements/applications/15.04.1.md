---
aliases:
- ../announce-applications-15.04.1
changelog: true
date: '2015-05-12'
description: Es distribueixen les aplicacions 15.04.1 del KDE.
layout: application
title: KDE distribueix les aplicacions 15.04.1 del KDE
version: 15.04.1
---
12 de maig de 2015. Avui KDE distribueix la primera actualització d'estabilització per a les <a href='../15.04.0'>aplicacions 15.04 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 50 esmenes registrades d'errors que inclouen millores a les Kdelibs, Kdepim, Kdenlive, Okular, Marble i Umbrello.

Aquest llançament també inclou les versions de suport a llarg termini dels espais de treball Plasma 4.11.19, la plataforma de desenvolupament KDE 4.14.8 i el paquet Kontact 4.14.8.
