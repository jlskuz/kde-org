---
aliases:
- ../announce-applications-19.04.1
changelog: true
date: 2019-05-09
description: Es distribueixen les aplicacions 19.04.1 del KDE.
layout: application
major_version: '19.04'
release: applications-19.04.1
title: KDE distribueix les aplicacions 19.04.1 del KDE
version: 19.04.1
---
{{% i18n_date %}}

Avui, KDE distribueix la primera actualització d'estabilització per a les <a href='../19.04.0'>Aplicacions 19.04 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, i proporciona una actualització segura i millor per a tothom.

Hi ha una vintena d'esmenes registrades d'errors que inclouen millores al Kontact, Ark, Cantor, Dolphin, Kdenlive, Spectacle i Umbrello, entre d'altres.

Les millores inclouen:

- En etiquetar fitxers a l'escriptori ja no es trunca el nom d'etiqueta
- S'ha solucionat una fallada al connector de compartició de text del KMail
- S'han corregit diverses regressions a l'editor de vídeo Kdenlive
