---
aliases:
- ../announce-applications-18.04.0
changelog: true
date: 2018-04-19
description: KDE distribueix les aplicacions 18.04.0 del KDE
layout: application
title: KDE distribueix les aplicacions 18.04.0 del KDE
version: 18.04.0
---
19 d'abril de 2018. S'han publicat les aplicacions 18.04.0 de KDE.

Treballem contínuament per a millorar el programari inclòs a les sèries de les aplicacions del KDE, i esperem que trobeu útils totes les millores i esmenes d'errors!

## Novetats a les Aplicacions 18.04 del KDE

### Sistema

{{<figure src="/announcements/applications/18.04.0/dolphin1804.png" width="600px" >}}

La primera gran versió de 2018 del <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, el potent gestor de fitxers del KDE, té moltes millores als seus plafons:

- Les seccions del plafó «Llocs» ara es poden ocultar si preferiu no veure-les, i hi ha una nova secció «Xarxa» disponible per contenir les entrades a les ubicacions remotes.
- El plafó «Terminal» es pot acoblar a qualsevol banda de la finestra, i si l'intenteu obrir sense el Konsole instal·lat, el Dolphin mostrarà un avís i us ajudarà a instal·lar-lo.
- S'ha millorat la implementació del HiDPI per al plafó «Informació».

També s'han actualitzat la vista de carpeta i els menús:

- La carpeta Paperera ara mostra el botó «Buida la paperera».
- S'ha afegit un element de menú «Mostra la destinació» per ajudar a trobar les destinacions dels enllaços simbòlics.
- S'ha incrementat la integració del Git, ja que el menú contextual de les carpetes «git» ara mostra dues noves accions: «git log» i «git merge».

Les millores addicionals inclouen:

- S'ha presentat una drecera nova per donar-vos l'opció d'obrir la barra de filtre prement la tecla de barra (/).
- Ara podeu ordenar i organitzar les fotos per la data en què es van prendre.
- L'acció arrossega i deixa de molts fitxers petits amb el Dolphin ara és més ràpida, i els usuaris poden desfer els treballs de reanomenar per lots.

{{<figure src="/announcements/applications/18.04.0/konsole1804.png" width="600px" >}}

Perquè la feina des de la línia d'ordres sigui més agradable, el <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, l'aplicació d'emulació de terminal del KDE, ara pot lluir més:

- Podeu baixar esquemes de color via KNewStuff.
- La barra de desplaçament es fusiona millor amb l'esquema de color actiu.
- La barra de pestanyes es mostra només quan cal, de manera predeterminada.

Els col·laboradors del Konsole no s'han aturat aquí, i presenten moltes funcionalitats noves:

- S'ha afegit un mode nou de només lectura i una propietat de perfil per commutar la còpia de text com a HTML.
- Amb el Wayland, ara el Konsole admet el menú d'arrossegar i deixar anar.
- En relació amb el protocol ZMODEM, s'han produït diverses millores: el Konsole ara pot gestionar l'indicador B01 de pujada del zmodem, mostrarà el progrés en transferir dades, el botó Cancel·la del diàleg ara funciona com s'espera, i la transferència dels fitxers més grans funciona millor llegint-los a la memòria en fragments d'1MB.

Les millores addicionals inclouen:

- S'ha esmenat el desplaçament amb la roda del ratolí amb la «libinput», i s'impedeix la circulació per l'historial de l'intèrpret en desplaçar-se amb la roda del ratolí.
- Les cerques s'actualitzen després de canviar l'opció de cerca de coincidència amb expressió regular i en prémer «Ctrl» + «Retrocés» el Konsole canviarà al comportament de l'«xterm».
- S'ha esmenat la drecera «--background-mode».

### Multimèdia

El <a href='https://juk.kde.org/'>JuK</a>, el reproductor musical del KDE, ara admet el Wayland. Les noves funcionalitats de la IU inclouen la capacitat d'ocultar la barra de menús i tenir un indicador visual de la peça actual en reproducció. Encara que l'acoblament a la safata del sistema està desactivat, el JuK no tornarà a fallar en intentar sortir via la icona de tancament de la finestra i la interfície d'usuari romandrà visible.S'han solucionat els errors relacionats amb la reproducció automàtica del JuK en reprendre des de l'estat d'adormit al Plasma 5 i la gestió de la columna de la llista de reproducció.

Per al llançament 18.04, els col·laboradors del <a href='https://kdenlive.org/'>Kdenlive</a>, l'editor de vídeo no lineal del KDE, s'han centrat en el manteniment:

- La redimensió dels clips ja no provoca la corrupció dels esvaïments i dels fotogrames clau.
- Els usuaris de pantalles escalades en monitors HiDPI poden gaudir d'icones més nítides.
- S'ha esmenat una possible fallada en iniciar amb diverses configuracions.
- Ara es requereix tenir la versió 6.6.0 o posterior del MLT, i s'ha millorat la compatibilitat.

#### Gràfics

{{<figure src="/announcements/applications/18.04.0/gwenview1804.png" width="600px" >}}

En els darrers mesos, els col·laboradors del <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, el visualitzador i organitzador d'imatges del KDE, han treballat en moltes millores. Les principals són:

- S'ha afegit la implementació per als controladors MPRIS, de manera que ara es poden controlar els passis de diapositiva a pantalla completa via el KDE Connect, les tecles multimèdia del teclat, i el plasmoide Media Player.
- Ara es poden desactivar els botons en passar per sobre de les miniatures.
- L'eina de retall ha rebut diverses millores: el seu arranjament ara es recorda en canviar a una altra imatge, la forma del quadre de selecció ara es pot blocar prement les tecles «Maj» o «Ctrl» i també es pot blocar la relació d'aspecte de la imatge actualment mostrada.
- En mode de pantalla completa, ara es pot sortir amb la tecla «Esc», i la paleta de colors seguirà el tema de color actiu. Si sortiu del Gwenview en aquest mode, es recordarà aquesta configuració i les sessions noves també s'iniciaran en mode de pantalla completa.

L'atenció als detalls és important, així que el Gwenview s'ha polit a les àrees següents:

- El Gwenview mostrarà els camins dels fitxers d'una manera més intel·ligible a la llista de «Carpetes recents», mostrant el menú contextual apropiat per als elements de la llista de «Fitxers recents», i oblidant-ho tot correctament en usar la funcionalitat «Desactiva l'historial».
- En fer clic a una carpeta de la barra lateral, es permet canviar entre els modes d'exploració i visualització, i recorda el darrer mode usat en canviar entre carpetes, permetent una navegació més ràpida a col·leccions enormes d'imatges.
- La navegació amb el teclat s'ha racionalitzat més, indicant adequadament el focus del teclat en el mode exploració.
- La funcionalitat «Ajusta a l'amplada» s'ha substituït amb una funció «Omple» més generalitzada.

{{<figure src="/announcements/applications/18.04.0/gwenviewsettings1804.png" width="600px" >}}

Inclús les millores més petites sovint poden fer que els fluxos de treball dels usuaris siguin més agradables:

- Per millorar la coherència, ara les imatges SVG s'engrandeixen com totes les altres imatges quan «Vista de les imatges → Augmenta les imatges més petites» està activat.
- Després d'editar una imatge o desfer els canvis, la vista d'imatge i la miniatura ja no tornaran a quedar sense sincronitzar.
- En reanomenar imatges, l'extensió del nom de fitxer es desseleccionarà de manera predeterminada i el diàleg «Mou/Copia/Enllaça» ara mostrarà la carpeta actual per defecte.
- S'han corregit molts defectes visuals, p. ex. a la barra d'URL, a la barra d'eines de pantalla completa, i a les animacions dels consells d'eines de les miniatures. També s'han afegit les icones que mancaven.
- Finalment, però no menys important, el botó en passar per sobre de la pantalla completa visualitzarà directament la imatge en lloc de mostrar el contingut de la carpeta, i ara la configuració avançada permet més control sobre els intents de representació de color ICC.

#### Oficina

A l'<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, el visualitzador universal de documents del KDE, ara es pot cancel·lar la renderització del PDF i l'extracció del text si està instal·lat el Poppler versió 0.63 o superior, que permet que si teniu un fitxer PDF complex i canvieu el zoom mentre s'està renderitzant, es cancel·larà immediatament en lloc d'esperar la finalització de la renderització.

Trobareu un funcionament millorat del PDF JavaScript per l'«AFSimple_Calculate», i si teniu el Poppler versió 0.64 o superior, l'Okular permetrà canvis del PDF JavaScript en l'estat de només lectura de formularis.

La gestió de correus electrònics de confirmació de reserves al <a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, el potent client de correu del KDE, s'ha millorat significativament per permetre les reserves de tren, i usa una base de dades d'aeroports basada en la Wikidata per mostrar vols amb la informació correcta de la zona horària. Per a fer-ho més fàcil, s'ha implementat un extractor nou per a correus que no continguin dades de reserves estructurades.

Les millores addicionals inclouen:

- L'estructura del missatge es pot tornar a mostrar amb el nou connector «Expert».
- S'ha afegit un connector a l'editor Sieve per seleccionar correus des de la base de dades de l'Akonadi.
- El text de cerca a l'editor s'ha millorat amb l'admissió d'expressions regulars.

#### Utilitats

{{<figure src="/announcements/applications/18.04.0/spectacle1804.png" width="600px" >}}

S'ha millorat la interfície d'usuari de l'<a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, una eina versàtil de captura de pantalla del KDE, a les àrees següents:

- S'ha revisat la fila inferior de botons, i ara mostra un botó per obrir la finestra d'Arranjament i un botó nou d'«Eines» que mostra mètodes per obrir la darrera carpeta usada de captura de pantalla i llança un programa d'enregistrament.
- Ara es recorda de manera predeterminada el darrer mode usat per desar.
- La mida de la finestra ara s'adapta a la relació d'aspecte de la captura de pantalla, proporcionant miniatures de captura de pantalla més agradables i eficients en espai.
- S'ha simplificat substancialment la finestra d'Arranjament.

Addicionalment, els usuaris podran simplificar els seus fluxos de treball amb aquestes funcionalitats noves:

- Quan s'ha capturat una finestra específica, el seu títol es pot afegir automàticament al nom del fitxer de la captura de pantalla.
- L'usuari ara pot triar si l'Spectacle surt automàticament després de qualsevol operació de desament o copia.

Les esmenes d'errors importants inclouen:

- L'acció arrossega i deixa a finestres del Chromium ara funciona com s'espera.
- En usar de manera repetida les dreceres de teclat per desar una captura de pantalla, ja no tornarà a provocar un diàleg d'avís de drecera ambigua.
- Per a captures de pantalla de regió rectangular, la vora inferior de la selecció es pot ajustar amb més precisió.
- S'ha millorat la fiabilitat de la captura de finestres amb vores de pantalla tàctils quan la composició està desactivada.

{{<figure src="/announcements/applications/18.04.0/kleopatra1804.gif" width="600px" >}}

El <a href='https://www.kde.org/applications/utilities/kleopatra/'>Kleopatra</a>, la IGU per al gestor de certificats i de criptografia universal del KDE, ara pot generar claus EdDSA de Corba 25519 quan s'usa amb una versió recent del GnuPG. S'ha afegit una vista «Bloc de notes» per accions criptogràfiques basades en text i ara es pot signar/encriptar i desencriptar/verificar directament a l'aplicació. A la vista «Detalls del certificat» ara trobareu una acció d'exportació, que podeu usar per exportar com a text i copiar i enganxar. Encara més, podeu importar el resultat via la nova vista «Bloc de notes».

A l'<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, l'eina gràfica de compressió/descompressió de fitxers del KDE que admet formats múltiples, ara es pot aturar les compressions o extraccions mentre s'usa el dorsal «libzip» amb arxius ZIP.

### Aplicacions que s'afegeixen a la planificació de llançaments de les aplicacions del KDE

L'enregistrador de càmera web <a href='https://userbase.kde.org/Kamoso'>Kamoso</a> del KDE, i el programa de còpies de seguretat <a href='https://www.kde.org/applications/utilities/kbackup/'>KBackup</a> ara seguiran els llançaments de les Aplicacions. El programa de missatgeria instantània <a href='https://www.kde.org/applications/internet/kopete/'>Kopete</a> també s'ha tornat a introduir després d'adaptar-se als Frameworks 5 del KDE.

### Aplicacions que tindran la seva pròpia planificació de llançaments

L'editor hexadecimal <a href='https://community.kde.org/Applications/18.04_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>Okteta</a> tindrà la seva pròpia planificació de llançaments després d'una petició del seu mantenidor.

### Cacera d'errors

S'han resolt més de 170 errors a les aplicacions, incloses el paquet Kontact, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello i més!

### Registre complet de canvis
