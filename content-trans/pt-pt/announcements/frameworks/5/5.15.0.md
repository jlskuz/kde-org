---
aliases:
- ../../kde-frameworks-5.15.0
date: 2015-10-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Correcção do tratamento do limite/início no SearchStore::exec
- Vota a criar o índice do Baloo
- balooctl config: adição de opções para mudar/ver o 'onlyBasicIndexing'
- Migração de verificação do balooctl para funcionar com novas arquitecturas (erro 353011)
- FileContentIndexer: correcção da emissão duplicada do 'filePath'
- UnindexedFileIterator: O 'mtime' é um 'quint32', não um 'quint64'
- Transaction: correcção de outro erro do Dbi
- Transaction: Correcção do documentMTime() e do documentCTime(), que usavam o Dbis errado.
- Transaction::checkPostingDbInTermsDb: Optimização do código
- Correcção dos avisos do DBus
- Balooctl: Adição do comando 'checkDb'
- balooctl config: Adição do "filtro de exclusão"
- KF5Baloo: Garantia de que as interfaces de D-Bus são geradas antes de serem usadas. (erro 353308)
- Evitar o uso do QByteArray::fromRawData
- Remoção do 'baloo-monitor' do Baloo
- TagListJob: Emissão de erro quando não for possível abrir a base de dados
- Não ignorar os sub-termos, caso não seja encontrado
- Limpeza de código para a falha do Baloo::File::load(), caso não consiga aceder à BD.
- Alteração do 'balooctl' para usar o IndexerConfig em vez de manipular directamente o 'baloofilerc'
- Melhoria de internacionalização do 'balooshow'
- Fazer o 'balooshow' sair ordeiramente se não for possível abrir a base de dados.
- Falha do Baloo::File::load() se não abrir a base de dados. (erro 353049)
- IndexerConfig: adição do método refresh()
- inotify: Não simular um evento 'closedWrite' após a movimentação sem 'cookie'
- ExtractorProcess: Remoção da linha extra no fim do 'filePath'
- baloo_file_extractor: invocação do QProcess::close antes de destruir o QProcess
- baloomonitorplugin/balooctl: internacionalização do estado da indexação.
- BalooCtl: Adição da opção 'config'
- Tornar o 'baloosearch' mais apresentável
- Remover os ficheiros vazios do EventMonitor
- BalooShow: Mostrar mais informações quando os ID's não corresponderem
- BalooShow: Quando invocado sem uma verificação de ID, caso o ID esteja correcto
- Adição de uma classe FileInfo
- Adição de verificações de erros em diversos pontos, para que o Baloo não estoire se estiver desactivado. (erro 352454)
- Correcção do Baloo a não respeitar a opção de configuração "apenas a indexação básica"
- Monitor: Obtenção do tempo restante no arranque
- Uso de chamadas a métodos reais no MainAdaptor, em vez do QMetaObject::invokeMethod
- Adição da interface org.kde.baloo ao objecto de topo, por fins de compatibilidade
- Correcção do texto da data apresentado na barra de endereços, devido à migração para o QDate
- Adição de um atraso ao fim de cada ficheiro em vez de ser em cada lote
- Remoção da dependência do Qt::Widgets no 'baloo_file'
- Remoção de código não usado do 'baloo_file_extractor'
- Adição do monitor do Baloo ou do 'plugin' experimental em QML
- Tornar a "pesquisa do tempo restante" segura em multi-tarefa
- kioslaves: Adição da substituição em falta das funções virtuais
- Extractor: Configuração do 'applicationData' após a construção da aplicação
- Query: Implementação do suporte do 'offset' (início)
- Balooctl: Adição do --version e do --help (erro 351645)
- Remoção do suporte do KAuth para aumentar as monitorização máximas do 'inotify' quando a quantidade é demasiado baixa (erro 351602)

### BluezQt

- Correcção do estoiro do 'fakebluez' no 'obexmanagertest' com o ASAN
- Declaração prévia de todas as classes exportadas no types.h
- ObexTransfer: Emissão de erro quando a sessão de transferência é removida
- Utils: Armazenamento de ponteiros para as instâncias dos gestores
- ObexTransfer: Geração de erro quando o org.bluez.obex estoira

### Módulos Extra do CMake

- Actualização da 'cache' de ícones do GTK ao instalar os ícones.
- Remoção de resolução alternativa para atrasar a execução no Android
- ECMEnableSanitizers: O item de sanidade indefinido é suportado pelo gcc 4.9
- Desactivação da detecção do X11,XCB etc. no OS X
- Pesquisa pelos ficheiros no prefixo instalado em vez da localização do prefixo
- Uso do Qt5 para indicar qual o prefixo de instalação do Qt5
- Adição da definição ANDROID, de acordo com as necessidades, no qsystemdetection.h.

### Integração da Plataforma

- Correcção de problemas aleatórios na janela de ficheiros que impediam o seu aparecimento. (erro 350758)

### KActivities

- Uso de uma função de correspondência personalizada em vez do 'glob' do SQLite. (erro 352574)
- Correcção de problemas na adição de um novo recurso ao modelo

### KCodecs

- Correcção de estoiro no UnicodeGroupProber::HandleData com textos curtos

### KConfig

- Marcação do 'kconfig-compiler' como ferramenta não-gráfica

### KCoreAddons

- KShell::splitArgs: só o espaço em ASCII é um separador, não o espaço em Unicode U+3000 (erro 345140)
- KDirWatch: correcção de estoiro quando um destrutor estático e global usa o KDirWatch::self() (erro 353080)
- Correcção de estoiro quando o KDirWatch é usado no Q_GLOBAL_STATIC.
- KDirWatch: correcção da segurança em multi-tarefa
- Clarificação na definição dos argumentos do construtor do KAboutData.

### KCrash

- KCrash: passagem da pasta actual ao 'kdeinit' quando a aplicação é reiniciada automaticamente com o kdeinit. (erro 337760)
- Adição do KCrash::initialize(), para que as aplicações e o 'plugin' da plataforma possam activar o KCrash de forma explícita.
- Desactivação do ASAN, caso esteja activo

### KDeclarative

- Pequenas melhorias no ColumnProxyModel
- Possibilidade de as aplicações saberem a localização da 'homeDir'
- mover o EventForge para fora do contentor do ecrã
- Oferta da propriedade 'enabled' (activo) no QIconItem.

### KDED

- kded: simplificação da lógica à volta do sycoca; basta invocar o 'ensureCacheValid'.

### Suporte para a KDELibs 4

- Invocação do 'newInstance' a partir do filho da primeira invocação
- Usar as definições do kdewin.
- Não tentar descobrir o X11 no WIN32
- cmake: Correcção da verificação da versão da Taglib no FindTaglib.cmake.

### KDesignerPlugin

- O 'moc' do Qt não consegue lidar com as macros (QT_VERSION_CHECK)

### KDESU

- kWarning -&gt; qWarning

### KFileMetaData

- implementação dos meta-dados do utilizador no Windows

### Extensões da GUI do KDE

- Não pesquisar pelo X11/XCB faz sentido também para o WIN32

### KHTML

- Substituição do std::auto_ptr por std::unique_ptr
- khtml-filter: Eliminar as regras que contêm funcionalidades especiais do Adblock que não são ainda suportadas.
- khtml-filter: Reordenação do código, sem modificações funcionais.
- khtml-filter: Ignorar a expressão regular com opções, dado não serem suportadas.
- khtml-filter: Correcção da detecção do separador das opções do 'adblock'.
- khtml-filter: Limpeza dos espaços em branco finais.
- khtml-filter: Não eliminar linhas que comecem por '&amp;', dado não serem caracteres especiais do 'adblock'.

### KI18n

- remoção dos iteradores restritos para o MSVC, para que o ki18n consiga compilar

### KIO

- KFileWidget: o argumento 'parent' (pai) deverá ser por omissão 0, como em todos os outros itens gráficos.
- Garantia que o tamanho da lista de 'bytes' que foi descarregada na estrutura é grande o suficiente, antes de calcular o 'targetInfo', caso contrário estar-se-á a aceder a memória que não nos pertence
- Correcção da utilização do Qurl ao invocar o QFileDialog::getExistingDirectory()
- Actualização da lista de dispositivos do Solid, antes de efectuar uma pesquisa no kio_trash
- Permissão do uso de 'trash:', para além do 'trash:/' como URL do 'listDir' (que invoca o 'listRoot') (erro 353181)
- KProtocolManager: correcção de um bloqueio ao usar o EnvVarProxy (erro 350890)
- Não tentar descobrir o X11 no WIN32
- KBuildSycocaProgressDialog: uso do indicador de ocupação incorporado no Qt. (erro 158672)
- KBuildSycocaProgressDialog: execução do kbuildsycoca5 com o QProcess.
- KPropertiesDialog: correcção do caso em que o ~/.local é uma ligação simbólica, comparação de localizações canónicas
- Adição do suporte para partilhas de rede no kio_trash (erro 177023)
- Ligação aos sinais do QDialogButtonBox, não do QDialog (erro 352770)
- KCM de 'Cookies': actualização dos nomes de D-Bus para o kded5
- Uso de ficheiros JSON directamente, em vez do kcoreaddons_desktop_to_json()

### KNotification

- Não enviar um sinal de actualização de notificação duplicado
- Voltar a processar a configuração da notificação apenas se tiver sido alterada
- Não tentar descobrir o X11 no WIN32

### KNotifyConfig

- Modificação do método de carregamento das predefinições
- Envio do nome da aplicação, cuja configuração foi actualizada, em conjunto com o sinal de D-Bus
- Adição de método para reverter o 'kconfigwidget' aos valores predefinidos
- Não sincronizar a configuração 'n' vezes na gravação

### KService

- Usar a data mais recente na sub-pasta como data da pasta de recursos.
- KSycoca: armazenamento da hora de modificação para cada pasta de origem para detectar as alterações. (erro 353036)
- KServiceTypeProfile: remoção da criação de 'factory' desnecessária. (erro 353360)
- Simplificação e optimização do KServiceTest::initTestCase.
- tornar o nome de instalação do ficheiro 'applications.menu' uma variável em 'cache' do CMake
- KSycoca: o ensureCacheValid() só deverá criar a BD se não existir
- KSycoca: fazer funcionar a base de dados funcionar após o código recente de verificação de datas
- KSycoca: mudança do nome do ficheiro da BD para incluir a língua e o SHA1 das pastas de onde foi criada.
- KSycoca: fazer com que o ensureCacheValid() faça parte da API pública.
- KSycoca: adição de um ponteiro 'q' para remover mais utilizações de 'singletons'
- KSycoca: remoção de todos os métodos self() para as 'factories', guardando-os sim no KSycoca.
- KBuildSycoca: remoção da gravação do ficheiro ksycoca5stamp.
- KBuildSycoca: uso do qCWarning em vez do fprintf(stderr, ...) ou qWarning
- KSycoca: reconstrução do ksycoca num processo em vez de executar o kbuildsycoca5
- KSycoca: passagem de todo o código do 'kbuildsycoca' para a biblioteca, exceptuando o main().
- Optimização do KSycoca: só vigiar o ficheiro se a aplicação se ligar ao databaseChanged()
- Correcção de fugas de memória na classe KBuildSycoca
- KSycoca: substituição da notificação de D-Bus com a vigilância de ficheiros com o KDirWatch.
- kbuildsycoca: a opção --nosignal tornou-se obsoleta.
- KBuildSycoca: substituição do bloqueio baseado no D-Bus por um ficheiro de bloqueio.
- Não estoirar se encontrar informações do 'plugin' inválidas.
- Mudança dos ficheiros de inclusão para '_p.h' como preparação para a passagem para a biblioteca 'kservice'.
- Passagem do checkGlobalHeader() para dentro do KBuildSycoca::recreate().
- Remoção do código do --checkstamps e do --nocheckfiles.

### KTextEditor

- validação de mais expressões regulares
- correcção de expressões regulares nos ficheiros HL (erro 352662)
- sincronização do realce de OCaml com o estado do https://code.google.com/p/vincent-hugot-projects/, antes de o Google Code ter desaparecido; algumas pequenas correcções de erros
- adição de quebra de linha (erro 352258)
- validação da linha antes de invocar o código de dobragem (erro 339894)
- Correcção de problemas na contagem de palavras do Kate, integrando com o DocumentPrivate em vez do Document (erro 353258)
- Actualização do realce de sintaxe do Kconfig: adição dos novos operadores do Linux 4.2
- sincronização com a versão do Kate do KDE/4.14
- minimap: Correcção da pega da barra de deslocamento não ser desenhada com as marcações desligadas. (erro 352641)
- sintaxe: Adição da opção 'git-user' para o 'kdesrc-buildrc'

### Plataforma da KWallet

- Não é mais fechado automaticamente quando usado pela última vez

### KWidgetsAddons

- Correcção do aviso C4138 (MSVC): '*/' encontrado fora do comentário

### KWindowSystem

- Execução de uma cópia completa do QByteArray get_stringlist_reply
- Permitir a interacção com vários servidores de X nas classes de NETWM.
- [xcb] Considerar os modificadores no KKeyServer como inicializados em plataformas != x11
- Mudança do KKeyserver (X11) para registo de eventos por categorias

### KXMLGUI

- Possibilidade de importar/exportar esquemas de atalhos de forma simétrica

### NetworkManagerQt

- Correcção das introspecções, sendo que o LastSeen deverá estar no AccessPoint e não na ActiveConnection

### Plataforma do Plasma

- Esconder a janela de dicas quando o cursor entrar numa ToolTipArea inactiva
- se o ficheiro 'desktop' tiver Icon=/xpto.svgz, usar esse ficheiro do pacote
- adição um tipo de ficheiro "screenshot" (imagem) nos pacotes
- ter em conta o 'devicepixelration' na barra de deslocamento autónoma
- sem efeito de passagem em ecrãs móveis/por toque
- Uso das margens SVG do campo de edição no cálculo do 'sizeHint'
- Não desvanecer o ícone de animação nas dicas do Plasma
- Correcção do texto em reticências do botão
- Os menus de contexto das 'applets' dentro de um painel já não se sobrepõem mais à 'applet'
- Simplificação da obtenção da lista de aplicações associadas no AssociatedApplicationManager

### Sonnet

- Correcção do ID de 'plugin' do Hunspell para um carregamento adequado
- suporte da compilação estática no Windows, adição da localização dos dicionários do Hunspell e LibreOffice no Windows
- Não assumir dicionários do Hunspell codificados em UTF-8. (erro 353133)
- correcção do Highlighter::setCurrentLanguage() para o caso em que a língua anterior era inválida (erro 349151)
- suporte do /usr/share/hunspell como localização dos dicionários
- 'Plugin' baseado no NSSpellChecker

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
