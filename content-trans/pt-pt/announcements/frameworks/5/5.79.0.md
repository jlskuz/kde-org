---
aliases:
- ../../kde-frameworks-5.79.0
date: 2021-02-13
layout: framework
libCount: 83
qtversion: 5.14
---
### Attica

* Migração do QNetworkRequest::FollowRedirectsAttribute para o QNetworkRequest::RedirectPolicyAttribute

### Baloo

* [SearchStore] Remoção da dependência do sistema de ficheiros na propriedade 'includeFolder'
* [FileIndexerConfig] Correcção da verificação escondida para as pastas incluídas de forma explícita
* [FilteredDirIterator] Pedir os ficheiros escondidos do QDirIterator

### Ícones do Brisa

* novos ícones 'telegram-panel'
* Uso do estilo correcto para os ícones 'align-horizontal-left-out' (erro 432273)
* Adição de novos ícones do Kickoff (erro 431883)
* Adição do 'rating-half', 100% de opacidade para o 'rating-unrated', Cor do texto para a classificação no tema escuro
* Remoção dos ícones do KeePassXC (erro 431593)
* Correcção dos ícones @3x (erro 431475)
* Adição do ícone do Neochat

### Módulos Extra do CMake

* Só activar o GNU_TAR_FOUND quando o '--sort=name' estiver disponível
* Remoção de geração de meta-dados rápidos para um dado APK
* KDEFrameworksCompilerSettings: definição do -DQT_NO_KEYWORDS e -DQT_NO_FOREACH por omissão
* [KDEGitCommitHooks] Criação de uma cópia dos programas na pasta de origem
* Suporte também para a nova extensão de ficheiros do Appstream
* fetch-translations: Resolução do URL antes de o passar ao fetchpo.rb
* Ter em consideração os URL's de doações do Appstream para criar os meta-dados do F-Droid
* Correcção das permissões dos programas (erro 431768)
* ECMQtDeclareLoggingCategory: criação de ficheiros .categories na compilação, não na configuração
* Adição de uma função do 'cmake' para configurar as rotinas pré-commit do Git

### Integração da Plataforma

* Correcção da impossibilidade de desinstalar as decorações das janelas (erro 414570)

### Ferramentas de Doxygen do KDE

* Certificação de que não é utilizado o tipo de letra predefinido do Doxygen
* Melhoria da visualização do QDoc e correcção de alguns erros no tema escuro
* Actualização da informação do responsável de manutenção
* Tema completamente novo e consistente com o develop.kde.org/docs

### KCalendarCore

* Remoção do registo do MemoryCalendar como observador ao apagar uma incidência
* Uso do 'recurrenceId' para remover a ocorrência correcta
* Limpar também as associações a blocos de notas ao fechar um MemoryCalendar

### KCMUtils

* Garantia de um modo de coluna única

### KCodecs

* Remoção da utilização de textos não-UTF-8

### KCompletion

* Correcção de uma regressão provocada pela migração do 'operator+' para o 'operator|'

### KConfig

* Remodelação do código de gravação/carregamento da geometria da janela para ser menos frágil
* correcção do código de reposição da janela ao ser fechada enquanto maximizada (erro 430521)
* KConfig: preservação da componente de milisegundos do QDateTime

### KCoreAddons

* Adição do KFuzzyMatcher para a filtragem difusa de textos
* KJob::infoMessage: documentação de que o argumento 'richtext' será removido, por não ser usado
* KJobUiDelegate::showErrorMessage: implementação com o qWarning()
* Descontinuação dos método relacionados com o X-KDE-PluginInfo-Depends
* Eliminação das chaves X-KDE-PluginInfo-Depends

### KDeclarative

* Permitir itens em colunas únicas
* KeySequenceItem: Atribuição de um texto vazio ao limpar em vez de 'undefined' (erro 432106)
* Remoção de ambiguidade dos estados seleccionado vs coberto pelo cursor no GridDelegate (erro 406914)
* Uso do modo Único por omissão

### KFileMetaData

* ffmpegextractor: Uso do av_find_default_stream_index para encontrar a sequência de vídeo

### KHolidays

* Actualização dos feriados das Maurícias para 2021
* Actualização dos feriados de Taiwan

### KI18n

* Não definir a codificação do 'textstream' ao compilar com o Qt6

### KImageFormats

* Simplificação de parte do código de perfis de cores do NCLX
* [imagedump] Adição da opção para "listar o tipo MIME" (-m)
* Correcção de estoiro com ficheiros mal-formatados
* ani: Garantia de que o 'riffSizeData' é do tamanho correcto antes de fazer a dança de conversões para 'quint32_le'
* Adição de 'plugin' para os cursores animados do Windows (ANI)

### KIO

* Uso da macro Q_LOGGING_CATEGORY em vez de uma QLoggingCategory explícita (erro 432406)
* Correcção da codificação predefinida como "US-ASCII" nas aplicações do KIO (erro 432406)
* CopyJob: Correcção de estoiro ao ignorar/repetir (erro 431731)
* KCoreDirLister: não sobrepor os eventos canceled() e completed()
* KFilePreviewGenerator: tornar a base de código mais moderna
* KCoreDirLister: não sobrepor o evento clear()
* MultiGetJob: não sobrepor os eventos
* FileJob: não sobrepor o evento close()
* SkipDialog: descontinuação do evento result(SkipDialog *_this, int _botão)
* Correcção de bloqueio ao mudar o nome de um ficheiro na janela de propriedades (erro 431902)
* Descontinuação do addServiceActionsTo e do addPluginActionsTo
* [KFilePlacesView] Reduzir a opacidade dos itens escondidos em "mostrar tudo"
* Não mudar de pastas ao abrir URL's cuja listagem não seja possível
* Ajuste na lógica do KFileWidget::slotOk quando estiver no modo ficheiros+pasta
* FileUndoManager: correcção da anulação de cópia de uma pasta vazia
* FileUndoManager: não sobrepor os ficheiros ao desfazer
* FileUndoManager: descontinuação do método undoAvailable()
* ExecutableFileOpenDialog: tornar a legenda de texto mais genérica
* KProcessRunner: emitir o evento processStarted() apenas uma vez
* Reversão do "kio_trash: correcção da lógica onde não é definido nenhum tamanho-limite"

### Kirigami

* Uso de um ícone não-simbólico para a acção 'sair'
* Correcção da mudança para não-pressionados dos botões do menu da barra de ferramentas
* Uso de sub-secção em vez de secção
* [controls/BasicListItem]: Adição da propriedade 'reserveSpaceForSubtitle'
* tornar a altura implícita do botão de navegação explícita
* Correcção do alinhamento vertical do BasicListItem
* [basiclistitem] Garantia que os ícones são quadrados
* [controls/ListItem]: Remoção do separador indentado para os itens iniciais
* Voltar a adicionar a margem do separador do item da lista à direita quando existe um item anterior
* Não invocar manualmente o 'reverseTwinsChanged' ao destruir o FormLayout (erro 428461)
* [org.kde.desktop/Units] Fazer com que as durações correspondam aos controlos/Unidades
* [Unidades] Redução do 'veryLongDuration' para 400ms
* [Unidades] Redução do 'shortDuration' e do 'longDuration' em 50ms
* Não considerar os eventos do rato Sintético como sendo de um rato (erro 431542)
* usar de forma mais agressiva o 'implicitHeight' em vez do 'preferredHeight'
* Usar as texturas do Atlas nos ícones
* controls/AbstractApplicationHeader: centrar os filhos na vertical
* [actiontextfield] Correcção das margens e tamanho da acção incorporada
* actualizar correctamente o tamanho do cabeçalho (erro 429235)
* [controls/OverlaySheet]: Respeitar o Layout.maximumWidth (erro 431089)
* [controls/PageRouter]: Exposição dos parâmetros definidos ao apresentar as 'currentRoutes'
* [controls/PageRouter]: Exposição dos parâmetros de topo no mapa de propriedades 'params'
* passagem dos exemplos relacionados com o 'pagerouter' para uma sub-pasta
* Possibilidade de arrastar a janela nas áreas não interactivas
* AbstractApplicationWindow: Uso de uma janela ampla nos sistemas em computadores para todos os estilos
* Não esconder o separador do item da lista à passagem quando o fundo é transparente
* [controls/ListItemDragHandle] Correcção de organização errada no caso de não-deslocamento (erro 431214)
* [controls/applicationWindow]: Ter em conta as larguras das áreas de itens ao calcular o 'wideScreen'

### KNewStuff

* Remodelação do cabeçalho e rodapé do Page do KNSQuick para se adaptar ao Kirigami
* Adição da legenda do número à componente de Classificações para uma melhor legibilidade
* Redução do tamanho mínimo da janela do GHNS em QML
* Correspondência da aparência mais clara à passagem nas delegações de grelhas do KCM
* Garantia que a largura mínima do Dialog em QML é da largura do ecrã ou inferior
* Correcção da formatação da delegação do BigPreview
* Revalidação dos itens em 'cache' antes de mostrar a janela
* Adição do suporte para os URL's kns:/ na ferramenta 'knewstuff-dialog' (erro 430812)
* filecopyworker: Abrir os ficheiros antes da leitura/gravação
* Repor o item como actualizável quando não é identificado nenhum conteúdo para actualizar (erro 430812)
* Correcção de um estoiro ocasional devido à manutenção incorrecta de um ponteiro
* Descontinuação da classe DownloadManager
* Descontinuação da propriedade AcceptHtmlDownloads
* Descontinuação das propriedades ChecksumPolicy e SignaturePolicy
* Descontinuação da propriedade Scope
* Descontinuação da propriedade CustomName

### KNotification

* Emissão de NewMenu quando é definido um novo menu de contexto (erro 383202)
* Descontinuação do KPassivePopup
* Garantia que todas as infra-estruturas referenciam a notificação antes de fazer algo
* Fazer com que a aplicação de exemplo de notificações compile e funcione no Android
* Passagem do tratamento do ID da notificação para a classe KNotification
* Correcção da remoção de notificações pendentes da fila (erro 423757)

### Plataforma KPackage

* Documentação da pertença do PackageStructure ao usar o PackageLoader

### KPty

* Correcção da geração da localização completa no 'kgrantpty' no código para o ! HAVE_OPENPTY

### KQuickCharts

* Adição de um método "first" no ChartDataSource e uso do mesmo no Legend (erro 432426)

### KRunner

* Verificação da acção seleccionada no caso de uma correspondência informativa
* Correcção do texto de resultado vazio para a actividade actual
* Descontinuação das substituições dos ID's do QueryMatch
* [Execução do DBus] Teste do RemoteImage

### KService

* Descontinuação do KPluginInfo::dependencies()
* CMake: Definição das dependências do add_custom_command()
* Descontinuação explícita do substituto do KToolInvocation::invokeTerminal
* Adição de método para obter o KServicePtr da aplicação de terminal predefinida
* KService: adição de método para alterar a 'workingDirectory'

### KTextEditor

* [Modo VI] Não mudar de janela ao mudar a capitalização (comando '~') (erro 432056)
* Aumento da largura de indentação máxima para 200 (erro 432283)
* garantia de que é actualizado o mapeamento de intervalos p.ex. na invalidação de intervalos agora vazios
* Só mostrar o erro de caracteres dos favoritos quando estiver no modo VI (erro 424172)
* [Modo VI] Correcção do movimento para o item correspondente em uma unidade
* Retenção do texto de substituição enquanto a barra de pesquisa avançada não for fechada (erro 338111)
* KateBookMarks: tornar o código de base mais moderno
* Não ignorar o canal alfa quando estiver uma marcação
* Correcção do canal alfa ignorado ao ler da interface de configuração
* Evitar que a antevisão da correspondência de parêntesis não saia para fora da janela
* Evitar que a antevisão da correspondência de parêntesis não ande a mudar de posição depois de mudar para uma página diferente
* Tornar a antevisão da correspondência de parêntesis mais compacta
* Não mostrar a antevisão da correspondência de parêntesis se ela cobrir o cursor
* Maximização da largura da antevisão da correspondência de parêntesis
* Esconder a antevisão da correspondência de parêntesis ao deslocar-se
* evitar intervalos de realce duplicados que matam o desenho em ARGB
* exposição apenas para leitura do KSyntaxHighlighting::Repository global
* Correcção de erro de indentação quando a linha contém "for" ou "else"
* Correcção de um erro de indentação
* remoção da 'tagLine' com caso especial, dado que conduzia a problemas de actualização aleatórios em p.ex. 
* correcção do desenho de marcadores de mudança de linha + selecção
* Correcção da indentação para quando carregar em Enter e o parâmetro da função tiver uma vírgula no fim
* pintura do pequeno intervalo na cor da selecção, caso o fim da linha anterior esteja na selecção
* simplificação de código + correcção dos comentários
* reversão de erro no corte, onde apagava demasiado código para desenhos extra
* impedir a pintura da selecção de uma linha inteiro, mais em concordância com os outros editores
* adaptação da indentação para os ficheiros .hl modificados
* [Modo VI] Migração do Command para QRegularExpression
* [Modo VI] Migração do 'findPrevWordEnd' e 'findSurroundingBrackets' para QRegularExpression
* [Modo VI] Migração do QRegExp::lastIndexIn para QRegularExpression e do QString::lastIndexOf
* [Modo VI] Migração do ModeBase::addToNumberUnderCursor para QRegularExpression
* [Modo VI] Migração de usos simples do QRegExp::indexIn para o QRegularExpression e QString::indexOf
* Uso do rgba(r,g,b,aF) para lidar com o canal alfa na exportação para HTML
* Respeitar as cores do alfa ao exportar para HTML
* Introdução de um método auxiliar para obter correctamente o nome da cor
* Suporte para cores com alfa: Camada de configuração
* evitar que os marcadores de mudança de linha matem o realce da linha actual
* pintar o realce da linha actual também para lá do contorno de ícones
* Activação do canal alfa para as cores do editor
* Correcção do realce da linha actual que tinha um pequeno intervalo no início das linhas com mudança de linha dinâmica
* Não fazer cálculos inúteis, basta comparar os valores directamente
* Correcção do realce da linha actual, onde existia um pequeno intervalo no início
* [Modo VI] Não ignorar o intervalo dobrado quando o movimento termina dentro do mesmo
* Usar um ciclo 'for' com intervalos sobre o 'm_matchingItems' em vez de usar o QHash::keys()
* Migração do 'normalvimode' para QRegularExpression
* exportação adequada das dependências correctas
* exposição do tema do KSyntaxHighlighting
* adição do 'configChanged' também para o KTextEditor::Editor, para as mudanças de configuração globais
* pequena reordenação das partes de configuração
* correcção do tratamento de chaves
* adição de parâmetros 'doc/view' aos novos eventos, correcção dos 'connects' antigos
* permitir o acesso & alteração do tema actual através da interface de configuração
* Remoção da qualificação 'const' ao passar os LineRanges pelo código
* Uso do .toString(), dado que o QStringView não tem o .toInt() nas versões mais antigas do Qt
* Declaração da expressão constante toLineRange() como 'inline' sempre que possível
* Reutilização da versão do QStringView do QStringRev, remoção da qualificação 'const'
* Passagem do comentário TODO KF6 para fora do comentário do 'doxygen'
* Cursor, Range: Adição do substituto fromString(QStringView)
* Permitir a desactivação da "Indentação de Alinhamento da Mudança de Linha Dinâmica" (erro 430987)
* LineRange::toString(): Evitar o sinal '-', dado que nos números negativos é confuso
* Uso do KTextEditor::LineRange no mecanismo notifyAboutRangeChange()
* Migração do tagLines(), checkValidity() e do fixLookup() para LineRanges
* Adição do KTextEditor::LineRange
* Passagem da implementação do KateTextBuffer::rangesForLine() para o KateTextBlock e evitar construções de contentores desnecessárias
* [Modo VI] Correcção da pesquisa dentro de intervalos dobrados (erro 376934)
* [Modo VI] Correcção da Repetição de Finalização da Macro (erro 334032)

### KTextWidgets

* Ter mais classes privadas a herdarem das classes-mães

### KUnitConversion

* Definição da variável antes de a usar

### KWidgetsAddons

* Tirar partido do AUTORCC
* Ter mais classes privadas a herdarem das classes-mães
* Inclusão explícita do QStringList

### KWindowSystem

* Adição de métodos de conveniência de opacidade fraccionária
* Correcção efectiva das inclusões
* Correcção das inclusões
* xcb: Funcionar com o ecrã activo como indicado pelo QX11Info::appScreen()

### KXMLGUI

* Correcção das inclusões
* Adição do evento KXMLGUIFactory::shortcutsSaved
* Uso do URL correcto do KDE para 'Envolver-se' (erro 430796)

### Plataforma do Plasma

* [plasmoidheading] Uso da cor pretendida nos rodapés
* [plasmacomponents3/spinbox] Correcção da cor do texto seleccionado
* widgets>lineedit.svg: correcção de problemas de desalinhamento ao pixel (erro 432422)
* Correcção de preenchimento inconsistente à esquerda e à direita no PlasmoidHeading
* Actualização das cores 'breeze-light' e 'breeze-dark'
* Reversão do "[SpinBox] Correcção de erro lógico na direcção do deslocamento"
* [calendário] correcção dos nomes de importação em falta
* [ExpandableListItem] Fazer com que a área da lista de acções expandidas respeite o tipo de letra
* DaysCalendar: migração para o PC3/QQC2 onde for possível
* Remoção da animação à passagem dos botões planos, calendários, itens da lista, botões
* Apresentação dos erros do plasmóide para a consola
* Remoção de dependência cíclica do Units.qml
* [MenuItem do PlasmaComponents] Criar uma acção nula quando a acção é destruída
* não tornar as imagens maiores que o necessário
* Adição dos ficheiros de projectos dos IDE's da JetBrains aos ficheiros a ignorar
* Correcção dos avisos de Ligações
* Adição de RESET à propriedade 'globalShortcut' (erro 431006)

### Purpose

* [nextcloud] Remodelação da interface de configuração
* Avaliação da configuração inicial
* Recorte do ListViews na configuração do 'kdeconnect' e do Bluetooth
* Remoção de propriedades associadas desnecessárias do Layout
* [plugins/nextcloud] Uso do ícone da Nextcloud
* [cmake] Passagem do find_package para o CMakeLists.txt de topo

### QQC2StyleBridge

* [combobox] Correcção da velocidade de deslocamento do rato por toque (erro 400258)
* O 'qw' pode ser nulo
* Suporte para o QQuickWidget (erro 428737)
* permitir arrastar a janela nas áreas em branco

### Solid

* CMake: uso do configure_file() para garantir as compilações incrementais com 'noop'
* [Fstab] Ignorar as montagens sobrepostas pelo Docker (erro 422385)

### Sonnet

* Não fazer várias pesquisas quando basta uma

### Realce de Sintaxe

* adição de incremento em falta da versão do 'context.xml'
* Suporte para os fins de ficheiros oficiais - ver https://mailman.ntg.nl/pipermail/ntg-context/2020/096906.html
* correcção dos resultados da referência
* cor de operador menos vibrante para os temas Brisa
* correcção da sintaxe do novo 'hugo'
* correcção dos erros de sintaxe
* Melhorias na legibilidade do tema escuro Solarizado
* remoção de capturas desnecessárias com uma regra dinâmica
* junção das referências de actualização do realce do operador =>
* reunião do realce de operadores
* adição de métodos de substituição constantes para alguns métodos de leitura
* Bash, Zsh: correcção do comando;; num 'case' (erro 430668)
* Atom Claro, Brisa Claro/Escuro: nova cor para o Operator ; Brisa Claro: nova cor para o ControlFlow
* email.xml: Detecção de comentários encadeados e caracteres escapados (erro 425345)
* Actualização do Atom Claro para usar cores com alfa
* associação de alguns estilos do Symbol e Operator ao 'dsOperator'
* Bash: correcção do } in ${!xy*} e mais Operadores de Expansão de Parâmetros (# in ${#xy} ; !,*,@,[*],[@] in ${!xy*}) (erro 430668)
* Actualização das revisões do tema
* Actualização do realce do Kconfig para o Linux 5.9
* Uso do rgba(r,g,b,aF) para lidar com o canal alfa nos Qt/Browsers correctamente
* Não usar o 'rgba' quando estiver no 'themeForPalette'
* Garantia que o 'rgba' é respeitado no HTML e Format
* Correcções do Atom-One Escuro
* Mais algumas actualizações no Atom One Escuro
* Não verificar o 'rgba' ao procurar por uma melhor correspondência
* Permitir o uso do canal alfa nas cores dos temas
* Actualização das cores do Monokai e correcção de um azul incorrecto
* C++: correcção do sufixo 'us'
* Bash: correcção #5: $ no fim de um texto entre aspas
* VHDL: correcção de funções, procedimentos, intervalos/unidades dos tipos e outras melhorias
* Actualização do breeze-dark.theme
* Raku: #7: correcção dos símbolos que começam por Q
* correcção rápida de contraste em falta para o Extension
* Adição do esquema de cores Oblivion do GtkSourceView/Pluma/gEdit

### ThreadWeaver

* Correcção dos iteradores do mapa ao compilar com o Qt6
* Não inicializar explicitamente as exclusões mútuas como NonRecursive

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
