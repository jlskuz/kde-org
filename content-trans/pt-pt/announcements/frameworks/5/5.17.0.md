---
aliases:
- ../../kde-frameworks-5.17.0
date: 2015-12-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Correcção do filtro de dados usado pelo timeline://
- Balooctl: Devolver após os comandos
- Limpeza e protecção do Baloo::Database::open(), para lidar com mais condições de estoiro
- Adição de verificação no Database::open(OpenDatabase) para falhar se a BD não existir

### Ícones do Brisa

- Foram adicionados ou melhorados diversos ícones
- uso de folhas de estilo nos ícones do Brisa (erro 126166)
- ERRO: 355902 correcção e modificação do ecrã de bloqueio do sistema
- Adição da informação de janelas de 24px para a aplicações do GTK (erro 355204)

### Módulos Extra do CMake

- Não avisar quando os ícones SVG(Z) forem fornecidos em vários tamanhos/níveis de detalhe
- Certificação que as traduções são carregadas na tarefa principal. (errp 346188)
- Revisão geral do sistema de compilação ECM.
- Possibilidade de activar o Clazy em qualquer projecto do KDE
- Não encontrar a biblioteca XINPUT do XCB por omissão.
- Limpeza da pasta de exportação antes de gerar um APK de novo
- Usar o QuickGit para o URL do repositório do Git.

### Integração da Plataforma

- Adição de falha na instalação do plasmóide no plasma_workspace.notifyrc

### KActivities

- Correcção de um bloqueio no primeiro arranque do serviço
- Passagem da criação do QAction para a tarefa principal. (erro 351485)
- Em alguns casos, o 'clang-format' toma uma decisão errada (erro 355495)
- Correcção de problemas potenciais de sincronização
- Uso do org.qtproject em vez do com.trolltech
- Remoção da utilização da libkactivities a partir dos 'plugins'
- A configuração do KAStats foi removida da API
- Adição da associação/dissociação do ResultModel

### Ferramentas de Doxygen do KDE

- Tornar o kgenframeworksapidox mais robusto.

### KArchive

- Correcção do KCompressionDevice::seek(), invocado ao criar um KTar sobre um KCompressionDevice.

### KCoreAddons

- KAboutData: Permitir o https:// e os outros esquemas de URL's na página inicial. (erro 355508)
- Reparação da propriedade MimeType ao usar o kcoreaddons_desktop_to_json()

### KDeclarative

- Migração do KDeclarative para usar directamente o KI18n
- O 'delegateImage' do DragArea poderá agora ser um texto, do qual será criado automaticamente um ícone
- Adição de uma nova biblioteca CalendarEvents

### KDED

- Remoção da variável de ambiente SESSION_MANAGER em vez de a limpar

### Suporte para a KDELibs 4

- Correcção de algumas chamadas de 'i18n'.

### KFileMetaData

- Marcação do 'm4a' como legível pela Taglib

### KIO

- Janela de 'cookies': fazê-la funcionar como desejado
- Correcção da sugestão de nomes de ficheiros para algo aleatório, quando mudar o tipo MIME na gravação.
- Registo do nome de DBus do 'kioexec' (erro 353037)
- Actualização do KProtocolManager após a mudança da configuração.

### KItemModels

- Correcção do uso do KSelectionProxyModel no QTableView (erro 352369)
- Correcção da limpeza ou alteração do modelo de origem de um KRecursiveFilterProxyModel.

### KNewStuff

- O 'registerServicesByGroupingNames' pode agora definir mais itens por omissão
- Mudança do KMoreToolsMenuFactory::createMenuFromGroupingNames para carregamento posterior

### KTextEditor

- Adição do realce de sintaxe para o TaskJuggler e o PL/I
- Possibilidaade de desactivação a completação de palavras-chave pela interface de configuração.
- Mudança do tamanho da árvore quando o modelo de completação foi limpo.

### Plataforma da KWallet

- Tratamento correcto do caso em que o utilizador nos desactivou

### KWidgetsAddons

- Correcção de um pequeno problema do KRatingWidget em altas resoluções.
- Remodelação e correcção da funcionalidade introduzida no erro 171343

### KXMLGUI

- Não invocar o QCoreApplication::setQuitLockEnabled(true) no início.

### Plataforma do Plasma

- Adição de um plasmóide básico como exemplo para o guia de desenvolvimento
- Adição de um conjunto de modelos de plasmóides do kapptemplate/kdevelop
- [calendário] Atrasar a limpeza do modelo até que a vista esteja pronta (erro 355943)
- Não mudar de posição ao esconder. (erro 354352)
- [IconItem] Não estoirar com um tema KIconLoader nulo (erro 355577)
- A largada de ficheiros de imagens sobre um painel não oferece mais a opção de os definir como papel de parede do mesmo
- A largada de um ficheiro .plasmoid sobre um painel ou o ecrã instalá-lo-á e adicioná-lo-á
- remoção do módulo do 'kded' 'platformstatus', que já não é mais usado (erro 348840)
- permitir a colagem em campos de senhas
- correcção da posição do menu de edição; adição de um botão de selecção
- [calendário] usar a língua da UI para obter o nome do mês (erro 353715)
- [calendário] Ordenação dos eventos também pelo seu tipo
- [calendário] Passagem da biblioteca de 'plugins' para o KDeclarative
- [calendário] o 'qmlRegisterUncreatableType' precisa de mais alguns argumentos
- Possibilidade de adicionar mais categorias de configuração de forma dinâmica
- [calendário] Passagem do tratamento dos 'plugins' para uma classe separada
- Possibilidade de os 'plugins' fornecerem dados de eventos à 'applet' do Calendário (erro 349676)
- verificação da existência do 'slot' antes de se ligar ou desligar (erro 354751)
- [plasmaquick] Não se associar explicitamente ao OpenGL
- [plasmaquick] Remoção da dependência do XCB::COMPOSITE e DAMAGE

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
