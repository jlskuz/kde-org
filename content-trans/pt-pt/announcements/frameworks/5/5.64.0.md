---
aliases:
- ../../kde-frameworks-5.64.0
date: 2019-11-10
layout: framework
libCount: 70
---
### Attica

- Adição de alguns 'std::move' nas funções de alteração

### Baloo

- Possibilidade de compilação contra o Qt 5.15
- Uso do 'propertymap' para guardar as propriedades no Baloo::Result
- Adição de funções de conversão independentes ao PropertyMap para JSON e vice-versa
- [Base de Dados] Remodelação do tratamento das opções do ambiente
- Substituição de recursividade no FilteredDirIterator por uma iteração em ciclo

### Ícones do Brisa

- Alinhamento ao centro dos ícones de tipos MIME de áudio a 64px não-quadrados (erro 393550)
- Remoção de vestígios de código do Nepomuk
- Passagem do ícone 'help-about' colorido a 32px para a pasta 'actions' (erro 396626)
- Melhoria nos ícones de desenho (erro 399665)
- Remoção de ícone do Nepomuk
- Preenchimento da área do botão do meio do rato
- Adição do 'folder-recent', aumento do ponteiro do relógio no 'folder-temp'
- Uso de uma metáfora mais correcta e apropriada para o ícone "obter coisas novas" (erro 400500)
- Actualização do ícone do Elisa
- Uso de CSS em vez de SCSS como formato do resultado
- Correcção do desenho incorrecto do ícone 'edit-opacity' a 22px
- Adição de ícones do 'edit-opacity' (erro 408283)
- Ícones para o estado do tempo com vento (erro 412718)
- Correcção das margens incorrectas nos ícones multimédia a 16/22px
- Uso da cor do texto em vez da de realce para o emblema de classificação/estrelas
- Adição dos ícones 'draw-arrow' (erro 408283)
- Adição dos ícones de acções 'draw-highlight' (erro 408283)
- Adição do PATH/LD_LIBRARY_PATH à invocação do 'qrcAlias'
- Adição do ícone 'applications-network' para mudar o nome da categoria Internet para Rede
- Adição de ícones 'edit-line-width' (erro 408283)

### Módulos Extra do CMake

- Não definir as normas de C/C++ se já estiverem definidas
- Usar a forma moderna de definir a norma de C/CXX
- Elevação do requisito do CMake para o 3.5
- ECMAddQch: suporte para PREDEFINED_MACROS/BLANK_MACROS com espaços &amp; aspas

### Integração da Plataforma

- Adição de ícones-padrão para suportar todos os elementos no QDialogButtonBox (erro 398973)
- garantia de que o winId() não é invocado em elementos não-nativos (erro 412675)

### KActivitiesStats

- testes: correcção de erro de compilação no macOS
- Correcção da compilação em MSVC no Windows
- Adição de método de acesso auxiliar para obter um QUrl a partir de um ResultSet::Result

### KArchive

- Correcção de fuga de memória no KXzFilter::init
- Correcção de referência nula nas extracções com problemas
- ras: Correcção de estoiro em ficheiros com problemas
- KXzFilter::Private: remoção de propriedades não usadas
- K7Zip: Correcção do uso da memória no readAndDecodePackedStreams

### KCalendarCore

- Adição também da versão da 'libical'
- Definição explícita do construtor por cópia do Journal

### KCMUtils

- Apresentação condicional dos botões de navegação no cabeçalho dos KCM's multi-páginas
- não usar uma altura do cabeçalho personalizada (erro 404396)
- adição de inclusão extra
- Correcção de fuga de memória nos objectos KQuickAddons::ConfigModule (erro 412998)
- [KCModuleLoader] Mostrar um erro quando não foi possível carregar o QML

### KConfig

- kconfig_compiler: Mudança do local do KSharedConfig::Ptr onde estiver a ser usado
- Possibilidade de compilação com o Qt 5.15 sem métodos obsoletos
- Exposição do 'isImmutable' à introspecção (p.ex. QML)
- Adição de método de conveniência para os estados de predefinições/modificações no KCoreConfigSkeleton
- Mudança no 'kconfig_compiler' para gerar construtores com o argumento opcional 'parent'
- Mudança do preferences() para uma função pública

### KConfigWidgets

- Impedimento de substituição do KCModule::changed

### KContacts

- Instalação das traduções

### KCoreAddons

- KProcessInfoList -- adição da infra-estrutura 'proclist' para o FreeBSD

### KDeclarative

- Uso de 'connect' com verificação na altura da compilação
- Tornar o evento settingChanged() protegido
- Ler o KQuickAddons::ConfigModule para expor se está num estado predefinido
- Captura do teclado quando o KeySequenceItem estiver a gravar
- Adição do ManagedConfigModule
- Remoção de secção antiga sobre a expansão [$e]
- [ConfigModule] Exposição do estado do componente 'mainUi' e do texto do erro

### Suporte para a KDELibs 4

- Documentação da API do KLocale: simplificação da pesquisa para saber como migrar o código para não o usar

### KDocTools

- man: uso do &lt;arg&gt; em vez do &lt;group&gt;

### KFileMetaData

- Correcção de estoiro na recolha e limpeza do gravador

### KHTML

- Extensão do KHtmlView::print() para usar uma instância de QPrinter predefinida (erro 405011)

### KI18n

- Adição do KLocalizedString::untranslatedText
- Substituição de todas as chamadas 'qWarning' e relacionadas por um registo por categorias

### KIconThemes

- Correcção do uso das novas macros de descontinuação do 'assignIconsToContextMenu'
- Descontinuação do KIconTheme::assignIconsToContextMenu

### KIO

- Constante &amp; assinatura do novo SlaveBase::configValue introduzido
- Migração para a variante de QSslError do KSslInfoDialog
- Migração do código interno do KSSLD do KSslError para o QSslError
- Tornar explícitos os erros de SSL não-dissimuláveis
- activação automática do KIO_ASSERT_SLAVE_STATES também para as compilações do Git
- Migração (da maior parte) do KSslInfoDialog do KSslError para o QSslError
- kio_http: evitar a duplicação do 'Content-Type' e do 'Depth' quando for usado pelo KDAV
- Migração da interface de D-Bus do KSSLD do KSslError para o QSslError
- Substituição da utilização do SlaveBase::config()-&gt;readEntry pelo SlaveBase::configValue
- Remoção de duas variáveis-membro não usadas que usavam o KSslError
- Evitar o envio do KDirNotify::emitFilesAdded quando a tarefa 'emptytrashjob' terminar
- Descontinuação da variante baseada no KTcpSocket do SslUi::askIgnoreSslErrors
- Tratamento do "application/x-ms-dos-executable" como executável em todas as plataformas (erro 412694)
- Substituição do uso do SlaveBase::config() por SlaveBase::mapConfig()
- ftptest: substituição das cores do registo pelo 'logger'
- [SlaveBase] Uso do QMap em vez do KConfig para guardar a configuração do IO-Slave
- Migração do KSslErrorUiData para o QSslError
- exclusão da pasta 'ioslaves' da documentação da API
- ftptest: a marcação da substituição sem a opção respectiva não falha mais
- ftptest: remodelação do arranque do servidor para uma função auxiliar própria
- [SslUi] Adição da documentação da API para o askIgnoreSslErrors()
- consideração do 'ftpd' como não sendo obrigatório nesta altura
- migração do 'slave' de FTP para o novo sistema de notificação de erros
- correcção do carregamento da configuração do 'proxy'
- Implementação do KSslCertificateRule com o QSslError em vez do KSslError
- Migração (da maior parte) da interface do KSslCertificateRule para o QSslError
- Migração do KSslCertificateManager para o QSslError
- Actualização do teste 'kfileplacesviewtest' de acordo com o D7446

### Kirigami

- Garantia que o 'topContent' do GlobalDrawer fica sempre no topo (erro 389533)
- realce à passagem do rato apenas quando existir mais que uma página (erro 410673)
- Mudança de nome do Okular Active para Okular Mobile
- os itens ficam em primeiro plano na página quando não estiverem numa vista (erro 407524)
- Permitir que as 'contextualActions' fluam para a barra de ferramentas do cabeçalho
- Correcção do modelo Credits incorrecto no Kirigami.AboutPage
- Não mostrar a área de contexto se todas as acções estiverem invisíveis
- Correcção da imagem-modelo do Kirigami
- manter os contentores livres de itens apagados
- limitação do tamanho das margens de arrastamento
- Correcção da apresentação do botão de ferramentas do menu quando não estiver nenhuma área disponível
- Desactivação do arrastamento da área global quando estiver no modo de menu
- Mostrar o texto da dica dos itens de menu
- Não avisar sobre o LayoutDirection no SearchField
- Verificação adequada do estado activo da Action nos botões da ActionToolBar
- Uso da propriedade 'action' do MenuItem directamente no ActionMenuItem
- Possibilidade de a área global se tornar num menu, se o desejar
- Ser mais explícitos sobre os tipos de propriedades das acções

### KItemViews

- [RFC] Unificação do estilo do novo Kirigami.ListSectionHeader e CategoryDrawer

### KJS

- Melhor mensagem para os erros de intervalos do String.prototype.repeat(quantidade)
- Simplificação do processamento dos literais numéricos
- Processamento dos literais binários de JS
- Detecção de literais hexadecimais e octais truncados
- Suporte da nova forma-padrão de definiçãod e literais octais
- Colecção dos testes de regressão retirados do repositório 'khtmltests'

### KNewStuff

- Garantia de que a propriedade 'changedEntries' é devidamente propagada
- Correcção da obtenção do KNSCore::Cache ao inicializar o Engine (erro 408716)

### KNotification

- [KStatusNotifierItem] Permitir o 'click' com o botão esquerdo quando o menu é nulo (erro 365105)
- Remoção do suporte para o Growl
- Adição e activação do suporte para o Centro de Notificações no macOS

### KPeople

- Resolução da compilação: limitar o DISABLE_DEPRECATED para o KService para &lt; 5.0

### KService

- Possibilidade de compilação com o Qt 5.15 sem métodos obsoletos

### KTextEditor

- KateModeMenuList: melhoria na mudança de linha
- correcção de estoiro (erro 413474)
- chegaram mais 'ok's, mais v2+
- chegaram mais 'ok's, mais v2+
- adição de sugestão ao cabeçalho do 'copyright'
- as linhas de código não-triviais não permanecem mais aqui para as pessoas que concordaram com o v2+ =&gt; v2+
- as linhas de código não-triviais não permanecem mais aqui para os autores que não respondem mais, v2+
- mais ficheiros em v2+, dado o OK pelos autores - consulte o kwrite-devel@kde.org
- mais ficheiros em v2+, dado o OK pelos autores - consulte o kwrite-devel@kde.org
- mais ficheiros em v2+, dado o OK pelos autores - consulte o kwrite-devel@kde.org
- mais ficheiros em v2+, dado o OK pelos autores - consulte o kwrite-devel@kde.org
- mais ficheiros em v2+, dado o OK pelos autores - consulte o kwrite-devel@kde.org
- mais ficheiros em v2+, dado o OK pelos autores - consulte o kwrite-devel@kde.org
- O katedocument.h já é v2+
- OK por parte do loh.tar, sars, lgplv2+
- mudança da licença para lgplv2+ com o OK do sven + michal
- mudança da licença para lgplv2+ com o OK do sven + michal
- todos os ficheiros com o SPDX-License-Identifier são lgplv2+
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- actualização da licença - o 'dh' está na secção lgplv2+ do relicensecheck.pl
- actualização da licença - o 'dh' está na secção lgplv2+ do relicensecheck.pl
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- lgplv2.1+ =&gt; lgplv2+
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- esta inclusão não contém lógica apenas para GPL v2 há muito tempo
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, adição do SPDX-License-Identifier
- clarificação da licença, 'michalhumpula' =&gt; ['gplv23', 'lgplv23', 'gplv2+', 'lgplv2+', '+eV' ]
- adição de 's' em falta (erro 413158)
- KateModeMenuList: forçar a posição vertical acima do botão
- melhores cabeçalhos auto-contidos
- inclusões no grupo por questões semânticas
- inclusões da ordenação

### KTextWidgets

- Remoção da chamada ao KIconTheme::assignIconsToContextMenu que não é mais necessária

### KWayland

- FakeInput: adição do suporte para a pressão e libertação das teclas do teclado
- Correcção da cópia à criação com uma escala não inteira do OutputChangeSet

### KXMLGUI

- correcção da detecção do atalho predefinido

### NetworkManagerQt

- Adição do suporte para a autenticação SAE, usada pelo WPA3

### Plataforma do Plasma

- associação do 'disabledTextColor' ao ColorScope
- adição do DisabledTextColor ao Theme
- [PC3/botão] Reticências no texto sempre
- Melhoria dos itens do menu de opções do painel
- [icons/media.svg] Adição dos ícones de 16 &amp; 32px, actualização do estilo
- [Componentes do Plasma 3] Correcção do fundo do botão de ferramentas assinalável

### Prisão

- Correcção do tratamento da memória no 'datamatrix'

### Purpose

- i18n: Adição de reticências nos itens de acção (X-Purpose-ActionDisplay)

### QQC2StyleBridge

- Não atribuir o 'currentIndex' da lista suspensa, dado que quebra a associação
- Monitorização da mudança de estilo da aplicação

### Solid

- Não compilar uma biblioteca estática quando o BUILD_TESTING=OFF

### Realce de Sintaxe

- VHDL: todas as palavras-chave não fazem distinção entre maiúsculas e minúsculas (erro 413409)
- Adição de caracteres de escape do texto à sintaxe do PowerShell
- Linhas de modo: correcção do fim do comentário
- Meson: mais funções incoroporadas e adição das funções-membro incorporadas
- debchangelog: adição do Focal Fossa
- Actualizações para o CMake 3.16
- Meson: Adição de uma secção de comentário para a opção comentar/descomentar com o Kate
- TypeScript: actualização da gramática e correcções

### ThreadWeaver

- Possibilidade de compilação contra o Qt 5.15

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
