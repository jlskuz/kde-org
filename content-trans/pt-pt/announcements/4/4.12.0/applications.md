---
date: '2013-12-18'
hidden: true
title: As Aplicações do KDE 4.12 Trazem um Grande Avanço na Gestão de Informações
  Pessoais e Outras Melhorias Globais
---
A comunidade do KDE orgulha-se em anunciar as últimas grandes actualizações nas Aplicações do KDE, trazendo novas funcionalidades e correcções. Esta versão marca grandes melhorias na plataforma do KDE PIM, ganhando uma performance muito melhor e muitas funcionalidades novas. O Kate agilizou a integração dos 'plugins' de Python e adicionou o suporte inicial de macros do Vim; finalmente, os jogos e as aplicações educativas tiveram muitas novas funcionalidades.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/kate.png" width="600px">}}

O editor de texto gráfico mais avançado no Linux, o Kate, recebeu mais alguns desenvolvimentos na completação de código, introduzindo desta vez a <a href='http://scummos.blogspot.com/2013/10/advanced-code-completion-filtering-in.html'>correspondência avançada de código, o tratamento de abreviaturas e a correspondência parcial nas classes</a>. Por exemplo, o novo código iria corresponder um 'QualIdent' tipificado com 'QualifiedIdentifier'. O Kate também recebeu o <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-18th-august-2013'>suporte inicial para as macros do Vim</a>. O melhor de tudo é que estas melhorias também afectam o KDevelop e as outras aplicações que usam a tecnologia do Kate.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/okular.png" width="600px">}}

O visualizador de documentos  Okular <a href='http://tsdgeos.blogspot.com/2013/10/changes-in-okular-printing-for-412.html'>agora tem em conta as margens do 'hardware' da impressora</a>, tem o suporte de áudio e vídeo para o ePub, assim como uma melhor pesquisa e o suporte para mais transformações, incluindo as dos meta-dados de imagens EXIF. Na ferramenta de diagramas UML Umbrello, as associações agora podem ser <a href='http://dot.kde.org/2013/09/20/kde-commit-digest-1st-september-2013'>desenhadas com diferentes disposições</a> e o Umbrello <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-25th-august-2013'>adiciona algumas reacções visuais, caso um elemento esteja documento</a>.

O gestor de privacidade KGpg mostra mais informações aos utilizadores e o KWalletManager, a ferramenta para guardar as suas senhas, pode agora <a href='http://www.rusu.info/wp/?p=248'>guardá-las no formato do GPG</a>. O Konsole introduz uma nova funcionalidade: carregue na tecla Ctrl e com o botão do rato para abrir directamente URL's no resultado da consola. Também poderá agora <a href='http://martinsandsmark.wordpress.com/2013/11/02/mangonel-1-1-and-more/'>listar os processos ao avisar sobre a saída</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

O KWebKit adicionou a capacidade de <a href='http://dot.kde.org/2013/08/09/kde-commit-digest-7th-july-2013'>dimensionar automaticamente o conteúdo para corresponder melhor à resolução do ecrã</a>. O gestor de ficheiros Dolphin introduziu algumas melhorias de performance na ordenação e apresentação de ficheiros, reduzindo a utilização da memória e acelerando mais as coisas. O KRDC introduziu a repetição da ligação no VNC e o KDialog agora dá acesso às janelas de mensagens 'detailedsorry' e 'detailederror' para os programas de consola que tenham mais informações para dar. O Kopete actualizou o seu 'plugin' de OTR e o protocolo Jabber tem agora suporte para o XEP-0264: Miniaturas das Transferências de Ficheiros. Para além destas funcionalidades, o foco principal foi na limpeza de código e na correcções de avisos de compilação.

### Jogos e aplicações educativas

Os Jogos do KDE viram algum trabalho em diversas áreas. O KReversi é <a href='http://tsdgeos.blogspot.ch/2013/10/kreversi-master-is-now-qt-quick-based.html'>agora baseado em QML e no Qt Quick</a>, tornando a experiência de jogo mais bonita e fluída. O KNetwalk também <a href='http://tsdgeos.blogspot.ch/2013/08/knetwalk-portedx-to-qtquick.html'>foi migrado</a> com os mesmos benefícios, para além da capacidade de definir uma grelha com largura e altura personalizadas. O Konquest agora tem um novo jogador por IA desafiante chamado 'Becai'.

Nas aplicações Educativas, foram feitas algumas alterações importantes. O KTouch <a href='http://blog.sebasgo.net/blog/2013/11/12/what-is-new-for-ktouch-in-kde-sc-4-dot-12/'>introduz o suporte para lições personalizadas, assim como novos exercícios novos</a>; o KStars tem agora um novo <a href='http://knro.blogspot.ch/2013/10/demo-of-ekos-alignment-module.html'>módulo de alinhamento mais preciso para os telescópios</a>; pode ver <a href='http://www.youtube.com/watch?v=7Dcn5aFI-vA'>um vídeo do Youtube aqui</a> com as novas funcionalidades. O Cantor, que oferece uma interface simples e poderosa para uma variedade de plataformas matemáticas, agora tem infra-estruturas <a href='http://blog.filipesaraiva.info/?p=1171'>para o Python2 e o Scilab</a>. Leia mais sobre a poderosa plataforma do Scilab <a href='http://blog.filipesaraiva.info/?p=1159'>aqui</a>. O Marble adicionou a integração com o ownCloud (as definições estão disponíveis nas Preferências) e adiciona o suporte para o desenho de camadas sobrepostas. O KAlgebra possibilita a exportação de gráficos 3D para PDF, dando uma forma excelente de partilhar o seu trabalho. Por último, muitos erros foram corrigidos nas diversas aplicações educativas do KDE.

### Correio, calendário e informações pessoais

O KDE PIM, o conjunto de aplicações do KDE para lidar com o correio electrónico, a gestão de calendários e outras informações pessoais, viu bastantes melhorias.

A começar no cliente de e-mail KMail, existe agora o <a href='http://dot.kde.org/2013/10/11/kde-commit-digest-29th-september-2013'>suporte para o AdBlock</a> (quando o HTML está activo) e um suporte melhorado para a detecção de fraudes, ao resolver os URL's curtos. Um novo agente do Akonadi, chamado FolderArchiveAgent, permite aos utilizadores arquivarem as mensagens lidas em pastas específicas; do mesmo modo, a interface da funcionalidade 'Enviar Mais Tarde' foi melhorada. O KMail também beneficia de um suporte melhorado para os filtros do Sieve. O Sieve permite a filtragem das mensagens de e-mail do lado do servidor, pelo que poderá agora <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-script-parsing-22/'>criar e modificar os filtros nos servidores</a>, assim como permite <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-12/'>converter os filtros de KMail existentes para filtros no servidor</a>. O suporte de 'mbox' do KMail <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-mboximporter/'>foi também melhorado</a>.

Noutras aplicações, diversas alterações tornaram o trabalho ainda mais simples e divertido. Foi introduzida uma nova ferramenta, o <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>ContactThemeEditor</a>, que permite criar temas para o KAddressbook, baseados no Grantlee, para mostrar os contactos. O livro de endereços também pode agora mostrar antevisões antes de imprimir os dados. O KNotes viu algum <a href='http://www.aegiap.eu/kdeblog/2013/11/what-news-in-kdepim-4-12-knotes/'>trabalho aprofundado na resolução de erros</a>. A ferramenta de 'blogs' Blogilo pode agora lidar com traduções e ocorreu também uma grande quantidade de correcções e melhorias em todas as aplicações do KDE PIM.

Beneficiando todas as aplicações, a 'cache' de dados subjacente ao KDE PIM foi <a href='http://ltinkl.blogspot.ch/2013/11/this-month-october-in-red-hat-kde.html'>sujeita a trabalhos activos para melhorar a performance, estabilidade e escalabilidade</a>, corrigindo o <a href='http://www.progdan.cz/2013/10/akonadi-1-10-3-with-postgresql-fix/'>suporte para o PostgreSQL com o último Qt 4.8.5</a>. Existe agora também uma nova ferramenta de linha de comandos, o 'calendarjanitor', que pode percorrer todos os dados do calendário à procura de incidências problemáticas, adicionando uma janela de depuração para a pesquisa. Vai um abraço muito especial para o Laurent Montel, pelo trabalho que tem feito para as funcionalidades do KDE PIM!

#### Instalar Aplicações do KDE

O KDE, incluindo todas as suas bibliotecas e aplicações, está disponível gratuitamente segundo licenças de código aberto. As aplicações do KDE correm sobre várias configurações de 'hardware' e arquitecturas de CPU, como a ARM e a x86, bem como em vários sistemas operativos e gestores de janelas ou ambientes de trabalho. Para além do Linux e de outros sistemas operativos baseados em UNIX, poderá descobrir versões para o Microsoft Windows da maioria das aplicações do KDE nas <a href='http://windows.kde.org'>aplicações do KDE em Windows</a>, assim como versões para o Mac OS X da Apple nas <a href='http://mac.kde.org/'>aplicações do KDE no Mac</a>. As versões experimentais das aplicações do KDE para várias plataformas móveis, como o MeeGo, o MS Windows Mobile e o Symbian poderão ser encontradas na Web, mas não são suportadas de momento. O <a href='http://plasma-active.org'>Plasma Active</a> é uma experiência de utilizador para um espectro mais amplo de dispositivos, como tabletes ou outros dispositivos móveis.

As aplicações do KDE podem ser obtidas nos formatos de código-fonte e em vários formatos binários a partir de <a href='http://download.kde.org/stable/4.12.0'>download.kde.org</a> e também podem ser obtidos via <a href='/download'>CD-ROM</a> ou com qualquer um dos <a href='/distributions'>principais sistemas GNU/Linux e UNIX</a> dos dias de hoje.

##### Pacotes

Alguns distribuidores de SO's Linux/UNIX forneceram simpaticamente alguns pacotes binários do %1 para algumas versões das suas distribuições e, em alguns casos, outros voluntários da comunidade também o fizeram.

##### Localizações dos Pacotes

Para uma lista actualizada dos pacotes binários disponíveis, dos quais a Equipa de Versões do KDE foi informada, visite por favor o <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.12.0'>Wiki da Comunidade</a>.

Poderá <a href='/info/4/4.12.0'>transferir à vontade</a> o código-fonte completo de 4.12.0. As instruções de compilação e instalação da aplicação do KDE 4.12.0 está disponível na <a href='/info/4/4.12.0#binary'>Página de Informações do 4.12.0</a>.

#### Requisitos do Sistema

Para tirar o máximo partido destas versões, recomendamos a utilização de uma versão recente do Qt, como a 4.8.4. Isto é necessário para garantir uma experiência estável e rápida, assim como algumas melhorias feitas no KDE poderão ter sido feitas de facto na plataforma Qt subjacente.

Para tirar um partido completo das capacidades das aplicações do KDE, recomendamos também que use os últimos controladores gráficos para o seu sistema, dado que isso poderá melhorar substancialmente a experiência do utilizador, tanto nas funcionalidades opcionais como numa performance e estabilidade globais.
