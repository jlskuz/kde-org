---
aliases:
- ../announce-applications-17.12.0
date: 2017-12-14
description: O KDE Lança as Aplicações do KDE 17.12.0
layout: application
title: O KDE Lança as Aplicações do KDE 17.12.0
version: 17.12.0
---
14 de Dezembro de 2017. O KDE anuncia hoje o lançamento das Aplicações do KDE 17.12.0.

Trabalhamos de forma contínua na melhoria do 'software' incluído na nossa série de Aplicações do KDE, esperando que ache as novas melhorias e correcções de erros úteis!

### O que há de novo nas Aplicações do KDE 17.12

#### Sistema

{{<figure src="/announcements/applications/17.12.0/dolphin1712.gif" width="600px" >}}

O <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, o nosso gestor de ficheiros, consegue agora gravar s alterações e limitar a pesquisa apenas às pastas. A mudança dos nomes dos ficheiros agora é mais simples; basta fazer duplo-click no nome do ficheiro. Estão disponíveis mais informações dos ficheiros ao seu dispor, dado que a data de modificação e o URL de origem dos ficheiros transferidos agora aparecem no painel de informações. Para além disso, foram introduzidas novas colunas de Género, Taxa de Dados e Ano de Lançamento.

#### Gráficos

O nosso visualizador de documentos poderoso <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a> ganhou o suporte para os ecrãs HiDPI e para a linguagem Markdown, sendo o desenho dos documentos lentos a carregar mostrado de forma progressiva. Fica agora uma opção disponível para partilhar um documento por e-mail.

O visualizador de imagens <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a> consegue agora abrir e realçar as imagens no gestor de ficheiros, a ampliação é mais suave, a navegação com o teclado foi melhorada e agora suporta os formatos FITS e TGA da Truevision. As imagens agora são protegidas contra remoções acidentais na tecla Delete quando não estiverem seleccionadas.

#### Multimédia

O <a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> agora usa menos memória ao lidar com projectos de vídeo que incluam muitas imagens, os perfis de 'clips' indirectos foram agora refinados e foi corrigido um erro incómodo relacionado com o salto de um segundo à frente quando se reproduzia para trás.

#### Utilitários

{{<figure src="/announcements/applications/17.12.0/kate1712.png" width="600px" >}}

O suporte para ZIP do <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> na infra-estrutura da 'libzip' foi melhorado. O <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a> tem um novo <a href='https://frinring.wordpress.com/2017/09/25/ktexteditorpreviewplugin-0-1-0/'>'plugin' de antevisão</a> que lhe permite ver uma antevisão em directo do documento de texto no formato final, aplicando-se os 'plugins' de KParts disponíveis (p.ex. para o Markdown, SVG, grafos do Dot, UI do Qt ou modificações). Este 'plugin' também funciona no <a href='https://www.kde.org/applications/development/kdevelop/'>KDevelop.</a>

#### Desenvolvimento

{{<figure src="/announcements/applications/17.12.0/kuiviewer1712.png" width="600px" >}}

O <a href='https://www.kde.org/applications/development/kompare/'>Kompare</a> agora oferece um menu de contexto na área das diferenças, permitindo um acesso mais rápido às acções de navegação ou modificação. Se for um programador, poderá descobrir a nova antevisão no local do KUIViewer do objecto UI descrito pelos ficheiros UI do Qt (janelas, elementos gráficos, etc.) bastante útil. Agora também suporta a API de transmissão sequencial do KParts.

#### Escritório

A equipa do <a href='https://www.kde.org/applications/office/kontact'>Kontact</a> esteve a trabalhar arduamente na melhoria e afinações do seu trabalho. Muito do esforço foi aplicado na modernização do código, mas os utilizadores irão reparar que a apresentação das mensagens encriptadas foi melhorada e foi adicionado o suporte para o 'text/pgp' e para o <a href='https://phabricator.kde.org/D8395'>Apple® Wallet Pass</a>. Agora existe uma opção para seleccionar a pasta de IMAP durante a configuração das férias, um novo aviso no KMail quando for reaberta uma mensagem de e-mail e a identidade/transporte de correio não forem os mesmos; foi também adicionado um novo <a href='https://micreabog.wordpress.com/2017/10/05/akonadi-ews-resource-now-part-of-kde-pim/'>suporte para o Microsoft® Exchange™</a>, o suporte para o Nylas Mail e foi melhorada a importação do Geary no 'akonadi-import-wizard', em conjunto com diversas correcções de erros e melhorias gerais.

#### Jogos

{{<figure src="/announcements/applications/17.12.0/ktuberling1712.jpg" width="600px" >}}

O <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a> consegue agora chegar a uma audiência mais vasta, já que foi <a href='https://tsdgeos.blogspot.nl/2017/10/kde-edu-sprint-in-berlin.html'>migrado para o Android</a>. O <a href='https://www.kde.org/applications/games/kolf/'>Kolf</a>, <a href='https://www.kde.org/applications/games/ksirk/'>KsirK</a> e o <a href='https://www.kde.org/applications/games/palapeli/'>Palapeli</a> terminaram a migração dos jogos do KDE para as Plataformas do KDE 5.

### Mais Migrações para as Plataformas do KDE 5

Ainda mais aplicações que eram baseadas no 'kdelibs4' foram agora migradas para as Plataformas do KDE 5. Estas incluem o leitor multimédia <a href='https://www.kde.org/applications/multimedia/juk/'>JuK</a>, o gestor de transferências <a href='https://www.kde.org/applications/internet/kget/'>KGet</a>, o <a href='https://www.kde.org/applications/multimedia/kmix/'>KMix</a>, utilitários como o <a href='https://www.kde.org/applications/utilities/sweeper/'>Sweeper</a> e o <a href='https://www.kde.org/applications/utilities/kmouth/'>KMouth</a>, assim como o <a href='https://www.kde.org/applications/development/kimagemapeditor/'>KImageMapEditor</a> e o 'IO slave' do Zeroconf. Muito obrigado aos programadores esforçados que se voluntariaram com o seu tempo e esforço para que tudo acontecesse!

### Aplicações que passaram para o seu calendário próprio de lançamento

O <a href='https://www.kde.org/applications/education/kstars/'>KStars</a> agora tem o seu próprio calendário de lançamento; veja este anúncio no <a href='https://knro.blogspot.de'>'blog' de desenvolvimento</a>. Vale a pena notar que diversas aplicações, como o Kopete e o Blogilo, já <a href='https://community.kde.org/Applications/17.12_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>não são mais lançadas</a> com a série das Aplicações, dado não terem sido ainda migrados para as Plataformas do KDE 5, ou não são mantidas neste momento.

### Eliminação de Erros

Foram resolvidos mais de 110 erros nas aplicações, incluindo o pacote Kontact, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello e muito mais!

### Registo de Alterações Completo

Se quiser saber mais sobre as alterações nesta versão, <a href='/announcements/changelogs/applications/17.12.0'>veja o registo completo das alterações</a>. Ainda que seja um pouco assustadora devido à sua abrangência, o registo de alterações poderá ser uma forma excelente de aprender sobre o funcionamento interno do KDE e para descobrir aplicações e funcionalidades que não sabia que tinha.
