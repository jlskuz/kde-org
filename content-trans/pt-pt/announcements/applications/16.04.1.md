---
aliases:
- ../announce-applications-16.04.1
changelog: true
date: 2016-05-10
description: O KDE Lança as Aplicações do KDE 16.04.1
layout: application
title: O KDE Lança as Aplicações do KDE 16.04.1
version: 16.04.1
---
10 de Maio de 2016. Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../16.04.0'>Aplicações do KDE 16.04</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 25 correcções de erros registadas incluem as melhorias nos módulos 'kdepim' e nas aplicações 'ark', 'kate', 'dolphin', 'kdenlive', 'lokalize' e 'spectacle', entre outros.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.20.
