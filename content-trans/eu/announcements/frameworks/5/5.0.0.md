---
aliases:
- ../../kde-frameworks-5.0
- ./5.0
customIntro: true
date: '2014-07-07'
description: KDEk Frameworks 5en aurreneko argitalpena kaleratu du.
layout: framework
qtversion: 5.2
title: KDE Frameworks 5-en aurreneko argitalpena
---
July 7th, 2014. The KDE Community is proud to announce KDE Frameworks 5.0. Frameworks 5 is the next generation of KDE libraries, modularized and optimized for easy integration in Qt applications. The Frameworks offer a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. There are over 50 different Frameworks as part of this release providing solutions including hardware integration, file format support, additional widgets, plotting functions, spell checking and more. Many of the Frameworks are cross platform and have minimal or no extra dependencies making them easy to build and add to any Qt application.

The KDE Frameworks represent an effort to rework the powerful KDE Platform 4 libraries into a set of independent, cross platform modules that will be readily available to all Qt developers to simplify, accelerate and reduce the cost of Qt development. The individual Frameworks are cross-platform and well documented and tested and their usage will be familiar to Qt developers, following the style and standards set by the Qt Project. Frameworks are developed under the proven KDE governance model with a predictable release schedule, a clear and vendor neutral contributor process, open governance and flexible licensing (LGPL).

The Frameworks have a clear dependency structure, divided into Categories and Tiers. The Categories refer to runtime dependencies:

- <strong>Functional</strong> elements have no runtime dependencies.
- <strong>Integration</strong> designates code that may require runtime dependencies for integration depending on what the OS or platform offers.
- <strong>Solutions</strong> have mandatory runtime dependencies.

The <strong>Tiers</strong> refer to compile-time dependencies on other Frameworks. Tier 1 Frameworks have no dependencies within Frameworks and only need Qt and other relevant libraries. Tier 2 Frameworks can depend only on Tier 1. Tier 3 Frameworks can depend on other Tier 3 Frameworks as well as Tier 2 and Tier 1.

The transition from Platform to Frameworks has been in progress for over 3 years, guided by top KDE technical contributors. Learn more about Frameworks 5 <a href='http://dot.kde.org/2013/09/25/frameworks-5'>in this article from last year</a>.

## nabarmentzekoak

There are over 50 Frameworks currently available. Browse the complete set <a href='http://api.kde.org/frameworks-api/frameworks5-apidocs/'>in the online API documentation</a>. Below an impression of some of the functionality Frameworks offers to Qt application developers.

<strong>KArchive</strong> offers support for many popular compression codecs in a self-contained, featureful and easy-to-use file archiving and extracting library. Just feed it files; there's no need to reinvent an archiving function in your Qt-based application!

<strong>ThreadWeaver</strong> offers a high-level API to manage threads using job- and queue-based interfaces. It allows easy scheduling of thread execution by specifying dependencies between the threads and executing them satisfying these dependencies, greatly simplifying the use of multiple threads.

<strong>KConfig</strong> is a Framework to deal with storing and retrieving configuration settings. It features a group-oriented API. It works with INI files and XDG-compliant cascading directories. It generates code based on XML files.

<strong>Solid</strong> offers hardware detection and can inform an application about storage devices and volumes, CPU, battery status, power management, network status and interfaces, and Bluetooth. For encrypted partitions, power and networking, running daemons are required.

<strong>KI18n</strong> adds Gettext support to applications, making it easier to integrate the translation workflow of Qt applications in the general translation infrastructure of many projects.
