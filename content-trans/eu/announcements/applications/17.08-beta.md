---
aliases:
- ../announce-applications-17.08-beta
date: 2017-07-21
description: KDEk, Aplikazioak 17.08 Beta kaleratzen du.
layout: application
release: applications-17.07.80
title: KDEk, KDE Aplikazioak 17.08 Beta kaleratu du
---
July 21, 2017. Today KDE released the beta of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

Check the <a href='https://community.kde.org/Applications/17.08_Release_Notes'>community release notes</a> for information on tarballs that are now KF5 based and known issues. A more complete announcement will be available for the final release

The KDE Applications 17.08 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.
