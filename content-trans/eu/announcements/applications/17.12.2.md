---
aliases:
- ../announce-applications-17.12.2
changelog: true
date: 2018-02-08
description: KDEk, KDE Aplikazioak 17.12.2 kaleratzen du
layout: application
title: KDEk, KDE Aplikazioak 17.12.2 kaleratzen du
version: 17.12.2
---
February 8, 2018. Today KDE released the second stability update for <a href='../17.12.0'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

About 20 recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, KGet, Okular, among others.
