---
aliases:
- ../announce-applications-14.12.2
changelog: true
date: '2015-02-03'
description: KDEk, Aplikazioak 14.12.2 kaleratzen du.
layout: application
title: KDEk, KDE Aplikazioak 14.12.2 kaleratzen du
version: 14.12.2
---
February 3, 2015. Today KDE released the second stability update for <a href='../14.12.0'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 20 recorded bugfixes include improvements to the anagram game Kanagram, Umbrello UML Modeller, the document viewer Okular and the virtual globe Marble.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.16, KDE Development Platform 4.14.5 and the Kontact Suite 4.14.5.
