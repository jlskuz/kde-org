---
aliases:
- ../announce-applications-18.08.3
changelog: true
date: 2018-11-08
description: KDEk, KDE Aplikazioak 18.08.3 kaleratu du
layout: application
title: KDEk, KDE Aplikazioak 18.08.3 kaleratu du
version: 18.08.3
---
November 8, 2018. Today KDE released the third stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

About 20 recorded bugfixes include improvements to Kontact, Ark, Dolphin, KDE Games, Kate, Okular, Umbrello, among others.

Improvements include:

- HTML viewing mode in KMail is remembered, and again loads external images if allowed
- Kate now remembers meta information (including bookmarks) between editing sessions
- Automatic scrolling in the Telepathy text UI was fixed with newer QtWebEngine versions
