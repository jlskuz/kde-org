---
date: 2013-08-14
hidden: true
title: Platforma KDE 4.11 zagotavlja boljše delovanje
---
Platforma KDE 4 je od izdaje 4.9 zamrznjena. Ta različica zato vključuje le številne popravke napak in izboljšave zmogljivosti.

Semantični pomnilnik in iskalnik Nepomuk sta prejela velike izboljšave zmogljivosti, kot je nabor optimizacij branja, ki omogočajo do šestkrat hitrejše branje podatkov. Indeksiranje je postalo pametnejše in je razdeljeno na dve stopnji. Prva faza takoj pridobi splošne informacije (kot sta vrsta in ime datoteke); dodatne informacije, kot so medijske oznake, podatki o avtorju itd., se izvlečejo v drugi, nekoliko počasnejši fazi. Prikaz metapodatkov na novo ustvarjeni ali sveže preneseni vsebini je zdaj veliko hitrejši. Poleg tega so razvijalci Nepomuka izboljšali sistem za varnostno kopiranje in obnovitev. Nenazadnje lahko Nepomuk zdaj tudi indeksira različne formate dokumentov, vključno z ODF in docx.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/dolphin.png" caption=`Semantične značilnosti v akciji v Dolphinu` width="600px">}}

Nepomukov optimiziran format za shranjevanje in na novo napisan indeksirnik e-pošte zahtevata ponovno indeksiranje nekatere vsebine trdega diska. Posledično bo zagon ponovnega indeksiranja porabil nenavadno količino računalniške zmogljivosti za določeno obdobje – odvisno od količine vsebine, ki jo je treba ponovno indeksirati. Samodejna pretvorba baze podatkov Nepomuk se bo zagnala ob prvi prijavi.

Bilo je več manjših popravkov, ki <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'>jih lahko najdete v dnevnikih git</a>.

#### Nameščanje razvojne platforme KDE

Programska oprema KDE, vključno z vsemi knjižnicami in aplikacijami, je prosto na voljo pod odprto-kodnimi licencami. Programska oprema KDE deluje na različnih konfiguracijah strojne opreme in arhitekturah CPE, kot sta ARM in x86, operacijskih sistemih in deluje s kakršnim koli upravljalnikom oken ali namiznim okoljem. Poleg Linuxa in drugih operacijskih sistemov, ki temeljijo na sistemu UNIX, lahko najdete različice večine aplikacij KDE za Microsoft Windows na spletnem mestu <a href='http://windows.kde.org'>Programska oprema KDE za Windows</a> in Apple Mac OS X različice na <a href='http://mac.kde.org/'>Programska oprema KDE na spletnem mestu Mac</a>. Eksperimentalne različice aplikacij KDE za različne mobilne platforme, kot so MeeGo, MS Windows Mobile in Symbian, je mogoče najti na spletu, vendar trenutno niso podprte. <a href='http://plasma-active.org'>Plasma Active</a> je uporabniška izkušnja za širši spekter naprav, kot so tablični računalniki in druga mobilna strojna oprema.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Paketi

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11.0 for some versions of their distribution, and in other cases community volunteers have done so. <br />

##### Mesta paketov

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community Wiki</a>.

The complete source code for 4.11.0 may be <a href='/info/4/4.11.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.11.0 are available from the <a href='/info/4/4.11.0#binary'>4.11.0 Info Page</a>.

#### Sistemske zahteve

Da bi kar najbolje izkoristili te izdaje, priporočamo uporabo novejše različice Qt, kot je 4.8.4. To je potrebno za zagotovitev stabilne in učinkovite izkušnje, saj so bile nekatere izboljšave programske opreme KDE dejansko narejene v osnovnem okviru Qt.<br /> Da bi v celoti izkoristili zmogljivosti programske opreme KDE, priporočamo tudi za uporabo najnovejših grafičnih gonilnikov za vaš sistem, saj lahko s tem bistveno izboljšate uporabniško izkušnjo, tako v izbirni funkcionalnosti kot pri splošni zmogljivosti in stabilnosti.

## Danes objavljeno tudi:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" />Plasma Workspaces 4.11 še naprej izboljšuje uporabniško izkušnjo</a>

Kot priprava za dolgoročno vzdrževanje Plasma Workspaces prinaša nadaljnje izboljšave osnovne funkcionalnosti z bolj gladko opravilno vrstico, pametnejšim pripomočkom za baterijo in izboljšanim mešalnikom zvoka. Uvedba KScreen prinaša inteligentno upravljanje z več monitorji v delovne prostore, obsežne izboljšave zmogljivosti v kombinaciji z majhnimi popravki uporabnosti pa zagotavljajo na splošno lepšo izkušnjo.

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> Aplikacije KDE 4.11 prinašajo ogromen korak naprej pri upravljanju osebnih podatkov in vsepovsod izboljšave</a>

Ta izdaja zaznamuje velike izboljšave sklada KDE PIM, kar daje veliko boljšo zmogljivost in številne nove zmožnosti. Kate izboljšuje produktivnost razvijalcev Python in Javascript z novimi vtičniki, Dolphin je postal hitrejši in izobraževalne aplikacije prinašajo različne nove zmožnosti.
