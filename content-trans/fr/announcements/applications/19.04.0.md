---
aliases:
- ../announce-applications-19.04.0
changelog: true
date: 2019-04-18
description: KDE publie la version 19.04 des applications.
layout: application
release: applications-19.04.0
title: KDE publie les applications de KDE en version 19.04.0
version: 19.04.0
version_number: 19.04.0
version_text: '19.04'
---
{{%youtube id="1keUASEvIvE"%}}

{{% i18n_date %}}

La communauté KDE est heureuse d'annoncer la publication d'applications de KDE en version 19.04.

Notre communauté travaille continuellement à l'amélioration des logiciels inclus dans notre série d'applications KDE. En plus des nouvelles fonctionnalités, nous améliorons la conception, l'ergonomie et la stabilité de tous nos utilitaires, jeux et outils de développement. Notre objectif est de vous faciliter la vie en rendant les logiciels KDE plus agréables à utiliser. Nous espérons que vous apprécierez toutes les nouvelles améliorations et corrections de bogues que vous trouverez dans les 19.04 !

## Quoi de neuf dans les applications de KDE 19.04

Plus de 150 bogues ont été corrigés. Ces corrections implémentent à nouveau des fonctionnalités désactivées, homogénéisent les raccourcis et éliminent des plantages. Elles rendent les applications de KDE plus conviviales et vous permettent d'être plus productif.

### Gestion de fichiers

{{<figure src="/announcements/applications/19.04.0/app1904_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a> est le gestionnaire de fichiers de KDE. Il se connecte aussi aux services de réseau, tels que les serveurs « SSH », « FTP » et « Samba ». Il est fourni avec des outils avancés pour trouver et organiser vos données.

Nouvelles fonctionnalités :

+ Nous avons étendu la prise en charge des vignettes, de sorte que Dolphin peut maintenant afficher des vignettes pour plusieurs nouveaux types de fichiers : <a href='https://phabricator.kde.org/D18768'>Fichiers Microsoft Office</a>, <a href='https://phabricator.kde.org/D18738'>Fichiers eBook « .epub » et « .fb2 » </a>, <a href='https://bugs.kde.org/show_bug.cgi?id=246050'>Fichiers Blender</a>, et <a href='https://phabricator.kde.org/D19679'>Fichiers PCX</a>. De plus, les vignettes des fichiers texte affichent désormais <a href='https://phabricator.kde.org/D19432'>la coloration syntaxique</a> du texte à l'intérieur de la vignette. </li>
+ Vous pouvez maintenant choisir <a href='https://bugs.kde.org/show_bug.cgi?id=312834'> quelle vue partielle est à fermer </a> en cliquant sur le bouton « Fermer la vue partielle ». </li>
+ Cette version de Dolphin introduit <a href='https://bugs.kde.org/show_bug.cgi?id=403690'>le placement intelligent d'onglet</a>. Lorsque vous ouvrez un dossier dans un nouvel onglet, celui-ci sera désormais placé immédiatement à droite de l'onglet actuel, au lieu d'être toujours à la fin de la barre d'onglets. </li>
+ <a href='https://phabricator.kde.org/D16872'>L'étiquetage des éléments</a> est maintenant bien plus pratique puisque les étiquettes peuvent être ajoutées et supprimées par utilisation du menu contextuel. </li>
+ Nous avons <a href='https://phabricator.kde.org/D18697'>amélioré les paramètres de tri par défaut</a> pour certains dossiers couramment utilisés. Par défaut, le dossier Téléchargements est désormais trié par date avec le regroupement activé, et la vue Documents récents (accessible en naviguant vers recentdocuments:/) est triée par date avec un affichage par liste. </li>

Les corrections de bogues incluent :

+ Lors de l'utilisation d'une version moderne du protocole « SMB », vous pouvez maintenant <a href='https://phabricator.kde.org/D16299'>découvrir les partages « Samba »</a> pour ordinateurs Mac et Linux. </li>
+ <a href='https://bugs.kde.org/show_bug.cgi?id=399430'>Le ré-arrangement des éléments dans le panneau « Emplacements » </a> fonctionne de nouveau correctement, même quand certains éléments sont masqués. </li>
+ Après l'ouverture d'un nouvel onglet dans Dolphin, l'affichage de celui-ci se fait automatiquement avec le <a href='https://bugs.kde.org/show_bug.cgi?id=401899'>focus du clavier</a>. </li>
+ Dolphin vous prévient maintenant lorsque vous tentez de le fermer quand <a href='https://bugs.kde.org/show_bug.cgi?id=304816'> le panneau de terminal est ouvert</a> et qu'un programme s'y exécute. </li>
+ De nombreuses fuites de mémoire ont été corrigées, améliorant les performances générales de Dolphin.</li>

Le module <a href='https://cgit.kde.org/audiocd-kio.git/'>AudioCD-KIO</a> permet à d'autres applications de KDE de lire de l'audio de CD et de le convertir automatiquement dans d'autres formats.

+ Le module audio « CD-KIO » prend maintenant en charge la copie vers <a href='https://bugs.kde.org/show_bug.cgi?id=313768'>Opus</a>. </li>
+ Le <a href='https://bugs.kde.org/show_bug.cgi?id=400849'>texte d'informations de CD</a> est devenu réellement transparent pour l'affichage. </li>

### Éditeur vidéo

{{<figure src="/announcements/applications/19.04.0/app1904_kdenlive.png" width="600px" >}}

Il s'agit d'une version majeure pour l'éditeur vidéo de KDE. <a href='https://kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> a fait l'objet d'une réécriture approfondie de son code de base puisque plus de 60% de son code interne a été modifié, améliorant ainsi son architecture globale.

Les améliorations incluses sont :

+ La frise temporelle a été re-développée pour utiliser « QML ».
+ Quand vous placez une vidéo sur la frise chronologique, l'audio et la vidéo se placeront toujours sur des pistes séparées.
+ La frise chronologique prend désormais en charge la navigation au clavier : les vidéos, les compositions et les images clés peuvent être déplacées à l'aide du clavier. De plus, la hauteur des pistes elles-mêmes est réglable.
+ Dans cette version de Kdenlive, l'enregistrement audio sur piste est livré avec une nouvelle fonctionnalité <a href='https://bugs.kde.org/show_bug.cgi?id=367676'>voix hors champ</a>.
+ Nous avons amélioré le copier / coller : il fonctionne entre différentes fenêtres de projet. La gestion des vidéos en cache a également été améliorée, puisque les vidéos peuvent désormais être supprimés individuellement.
+ La version 19.04 offre le retour de prise en charge des affichages d'écrans externes « BlackMagic ». Il y a aussi de nouveaux guides de pré réglages sur écrans.
+ Nous avons amélioré la gestion des images clés, en lui donnant un aspect et un flux de travail plus cohérents. Le titreur a également été amélioré en faisant en sorte que les boutons d'alignement s'enclenchent dans des zones sûres, en ajoutant des guides et des couleurs d'arrière-plan configurables, et en affichant les éléments manquants.
+ Le bogue concernant la corruption de la frise chronologique a été corrigé. Ce bogue provoquait un mauvais placement ou des pertes de vidéos, déclenché sur le déplacement d'un groupe de vidéos.
+ Le bogue concernant l'image « jpg » a été corrigé, affichant les images en écran blanc sous Windows. Les bogues affectant la copie d'écran sous Windows ont été aussi corrigés.
+ En plus de tout ce qui est mentionné ci-dessus, de nombreuses petites améliorations d'ergonomie ont été ajoutées, rendant Kdenlive plus agréable et fluide.

### Bureau

{{<figure src="/announcements/applications/19.04.0/app1904_okular.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/okular/'>Okular</a> est l'afficheur polyvalent de documents de KDE. Idéal pour lire et annoter des fichiers « PDF », il peut également ouvrir des fichiers « ODF » (tels qu'utilisés par LibreOffice et OpenOffice), des fichiers « ebooks » publiés sous forme de fichiers « ePub », les fichiers de bandes dessinées les plus courants, des fichiers « PostScript » et beaucoup d'autres encore.

Les améliorations incluses sont :

+ Afin que vos documents soient toujours bien en forme, des <a href='https://bugs.kde.org/show_bug.cgi?id=348172'>options de dimensionnement</a> ont été ajoutées à la boîte de dialogue d'impression de Okular.
+ Okular prend maintenant en charge l'affichage et la vérification des <a href='https://cgit.kde.org/okular.git/commit/?id=a234a902dcfff249d8f1d41dfbc257496c81d84e'>signatures numériques</a> dans les fichiers « PDF ».
+ Grâce à une intégration améliorée avec les applications, Okular prend maintenant en charge la modification des documents LaTex dans <a href='https://bugs.kde.org/show_bug.cgi?id=404120'>TexStudio</a>.
+ La prise en charge améliorée de la <a href='https://phabricator.kde.org/D18118'>navigation par écran tactile</a> signifie que vous pourrez vous déplacer en arrière et en avant à l'aide d'un écran tactile en mode Présentation.
+ Les utilisateurs qui préfèrent manipuler les documents à partir de la ligne de commandes pourront effectuer une <a href='https://bugs.kde.org/show_bug.cgi?id=362038'>recherche de texte intelligente</a> grâce au nouvel indicateur de ligne de commandes permettant d'ouvrir un document et de mettre en valeur toutes les occurrences d'un élément de texte donné.
+ Okular affiche maintenant correctement les liens dans les <a href='https://bugs.kde.org/show_bug.cgi?id=403247'>documents « Markdown »</a>, s'étendant sur plus d'une ligne.
+ L'<a href='https://bugs.kde.org/show_bug.cgi?id=397768'>outils de réglage</a> ont reçu de nouvelles joies icônes.

{{<figure src="/announcements/applications/19.04.0/app1904_kmail.png" width="600px" >}}

<a href='https://kde.org/applications/internet/kmail/'>KMail</a> le client de messagerie de KDE protégé votre vie privée. Faisant partie de la <a href='https://kde.org/applications/office/kontact/'>suite du logiciel de groupe de travail Kontact</a>, KMail prend en charge tous les systèmes de messagerie et vous permet d'organiser vos messages dans une boîte de réception virtuelle partagée ou dans des comptes séparés (au choix). Il prend en charge tous les types de chiffrement et de signature des messages et vous permet de partager des données telles que des contacts, des dates de réunion et des informations de voyage avec d'autres applications de Kontact.

Les améliorations incluses sont :

+ Encore plus, Grammarly ! Cette version de KMail arrive avec la prise en charge de « languagetools » (vérificateur de grammaire) et « grammalecte » (vérificateur de grammaire, unique pour le Français).
+ Les numéros de téléphone dans les courriels sont maintenant détectés et peuvent être utilisés pour une numérotation direct grâce à <a href='https://community.kde.org/KDEConnect'>KDE Connect</a>.
+ KMail possède maintenant une option pour <a href='https://phabricator.kde.org/D19189'>démarrer directement dans la boîte à miniatures</a> sans ouvrir la fenêtre principale.
+ La prise en charge du module externe « Markdown » a été améliorée.
+ Le chargement des courriels grâce à « IMAP » n'est plus stoppé quand la connexion échoue.
+ De nombreuses corrections ont été réalisées dans le moteur Akonadi de KMail, pour améliorer la fiabilité et la performance.

<a href='https://kde.org/applications/office/korganizer/'>KOrganizer</a> est le composant d'agenda de Kontact, gérant tous vos évènements.

+ Les évènements périodiques provenant de <a href='https://bugs.kde.org/show_bug.cgi?id=334569'>Google Calendar</a> sont de nouveau, correctement synchronisés.
+ La fenêtre de rappel d'évènements prévient maintenant <a href='https://phabricator.kde.org/D16247'> pour un affichage sur tous les bureaux</a>.
+ L'apparence des <a href='https://phabricator.kde.org/T9420'>affichages d'évènements</a> a été modernisée.

<a href='https://cgit.kde.org/kitinerary.git/'>Kitinerary</a> est un nouvel assistant de voyage sous la marque Kontact, qui vous aidera à arriver à votre destination et vous conseillera sur votre chemin.

- Il y a un nouveau extracteur générique pour les tickets « RTC2 » (Par exemple, utilisé par les companies de train comme DSB, ÖBB, SBB, NS).
- La détection des noms d'aéroports et la levée d'ambiguïté ont été largement améliorées.
- Nous avons ajouté de nouveaux extracteurs personnalisés pour des fournisseurs qui n'étaient pas pris en charge auparavant (par exemple BCD Travel, NH Group), et amélioré les variations de format/langue des fournisseurs déjà pris en charge (par exemple SNCF, Easyjet, Booking.com, Hertz).

### Développement

{{<figure src="/announcements/applications/19.04.0/app1904_kate.png" width="600px" >}}

<a href='https://kde.org/applications/utilities/kate/'>Kate</a> est l'éditeur de texte de KDE, idéal pour la programmation grâce à des fonctionnalités telles que les onglets, le mode en deux parties de l'écran, la coloration syntaxique, un panneau terminal intégré, le complètement de mots, la recherche et la substitution d'expressions rationnelles et bien d'autres via l'infrastructure flexible reposant sur des modules externes.

Les améliorations incluses sont :

- Kate peut maintenant afficher tous les caractères invisibles non éditables et pas seulement quelques un.
- Vous pouvez facilement activer et désactiver le retour à la ligne statique des mots, en utilisant la propre entrée de menu par document, sans avoir à modifier les paramètres globaux par défaut.
- Les menus contextuels des fichiers et des onglets comprennent désormais un grand nombre de nouvelles actions, telles que « Renommer », « Supprimer », « Ouvrir le dossier contenant », « Copier l'emplacement du fichier », « Comparer [avec un autre fichier ouvert] » et « Propriétés ».
- Cette version de Kate est fournie avec plus de modules externes activés par défaut, y compris la fonctionnalité intégrée et très utile du terminal.
- Lors de la fermeture, Kate ne demande plus de confirmer que les fichiers ont été modifiés sur le disque par un autre processus, tel qu'une modification par la gestion des sources.
- L'affichage en arborescence des modules externes affiche maintenant correctement tous les éléments de menu pour les entrées git, ayant un tréma dans leurs noms.
- Lors de l'ouverture de multiples fichiers en utilisant la ligne de commandes, les fichiers sont ouverts dans de nouveaux onglets, dans le même ordre que celui spécifié dans la ligne de commandes.

{{<figure src="/announcements/applications/19.04.0/app1904_konsole.png" width="600px" >}}

<a href='https://kde.org/applications/system/konsole/'>Konsole</a> est l'émulateur de terminal de KDE. Il prend en charge les onglets, les arrière-plans translucides, le mode de vues fractionnées, les thèmes de couleurs et les raccourcis clavier personnalisables, les signets de dossiers et les signets « SSH » et beaucoup d'autres fonctionnalités.

Les améliorations incluses sont :

+ La gestion des onglets a fait l'objet d'un certain nombre d'améliorations qui vous aideront à être plus productif. De nouveaux onglets peuvent être créés en cliquant avec le bouton central sur les parties vides de la barre d'onglets, et une option vous permet de fermer les onglets en cliquant avec le bouton central. Les boutons de fermeture sont affichés par défaut sur les onglets, et les icônes ne s'affichent que si vous utilisez un profil avec une icône personnalisée. Enfin, le raccourci Ctrl+Tab vous permet de passer rapidement de l'onglet actuel à l'onglet précédent.
+ La boîte de dialogue « Modifier le profil » a reçu un important <a href='https://phabricator.kde.org/D17244'> remaniement de l'interface utilisateur</a>.
+ Le thème de couleurs Breeze est maintenant utilisé comme thème de couleurs par défaut de Konsole. Son contraste et sa cohérence ont été amélioré avec le thème Breeze, applicable à l'ensemble du système.
+ Les problèmes lors de l'affichage de texte en gras ont été résolus.
+ Konsole affiche maintenant correctement le curseur en style sous-ligné.
+ L'affichage des <a href='https://bugs.kde.org/show_bug.cgi?id=402415'>caractères de boîtes et de lignes</a>, comme, par exemple, les <a href='https://bugs.kde.org/show_bug.cgi?id=401298'>émoticônes</a>.
+ Les raccourcis pour basculer de profils permet maintenant de changer le profil de l'onglet courant, plutôt que d'ouvrir un nouvel onglet avec un autre profil.
+ Les onglets inactifs qui reçoivent une notification et dont la couleur du texte du titre a été modifiée reprennent désormais la couleur normale du texte du titre de l'onglet lorsque celui-ci est activé.
+ La fonctionnalité « Faire évoluer l'arrière-plan pour chaque onglet » fonctionne maintenant quand la couleur de base de l'arrière-plan est très sombre ou noire.

<a href='https://kde.org/applications/development/lokalize/'>Lokalize</a> est un système de traduction assistée par ordinateur axé sur la productivité et l'assurance qualité. Il est destiné à la traduction de logiciels, mais intègre également des outils de conversion externes pour la traduction de documents bureautiques.

Les améliorations incluses sont :

- Lokalize prend maintenant en charge l'affichage de la source de traduction avec un éditeur personnalisé.
- Le placement des composants graphiques de panneaux a été amélioré, ainsi que la façon dont les paramètres sont enregistrés et restaurés.
- La position dans les fichiers « .po » est maintenant préservée lors du filtrage des messages.
- Un certain nombre de bogues concernant l'interface utilisateur sont corrigés (commentaires des développeurs, Basculement vers RegExp bascule avec un remplacement de masse, compteur de messages vide, etc.)

### Utilitaires

{{<figure src="/announcements/applications/19.04.0/app1904_gwenview.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/gwenview/'>Gwenview</a> est un afficheur et un organiseur avancé d'images, avec des outils d'édition intuitifs et faciles à utiliser.

Les améliorations incluses sont :

+ La version de Gwenview livrée avec Applications 19.04 comprend une prise en charge complète de l'écran tactile, avec les gestes pour glisser, zoomer, faire un panoramique, etc.
+ Une autre amélioration ajoutée à Gwenview est la <a href='https://bugs.kde.org/show_bug.cgi?id=373178'>prise en charge de la haute résolution</a> totale, ce qui donne aux images une meilleure apparence sur les écrans à haute résolution.
+ La meilleure prise en charge des <a href='https://phabricator.kde.org/D14583'>boutons en avant et en arrière de la souris</a> vous permet de naviguer parmi les images en les utilisant.
+ Vous pouvez maintenant utiliser Gwenview pour ouvrir des fichiers d'images créés par<a href='https://krita.org/'>Krita</a> – l'outil favori de peinture numérique de tout le monde.
+ Gwenview prend maintenant en charge les <a href='https://phabricator.kde.org/D6083'>vignettes de 512 pixels</a>, vous permettant d'afficher un aperçu de vos images plus facilement.
+ Gwenview utilise maintenant le <a href='https://bugs.kde.org/show_bug.cgi?id=395184'>raccourci de clavier standard « Ctrl » + « L »</a> pour déplacer le focus vers le champ de l'URL.
+ Vous pouvez utiliser maintenant la <a href='https://bugs.kde.org/show_bug.cgi?id=386531'>fonctionnalité « Filtrer-par-nom » </a> avec le raccourci « Ctrl » + « I », exactement comme avec Dolphin.

{{<figure src="/announcements/applications/19.04.0/app1904_spectacle.jpg" width="600px" >}}

<a href='https://kde.org/applications/graphics/spectacle/'>Spectacle</a> est l'application de copie d'écran de Plasma. Vous pouvez copier des bureaux complets couvrant plusieurs écrans, des écrans individuels, des fenêtres, des sections de fenêtres ou des régions personnalisées à l'aide de la fonction de sélection de rectangle.

Les améliorations incluses sont :

+ Nous avons amélioré le mode Région rectangulaire en rajoutant quelques options. Il peut être configuré pour <a href='https://bugs.kde.org/show_bug.cgi?id=404829'>accepter automatiquement</a> la boîte glissée au lieu de vous demander de l'ajuster d'abord. Il existe également une nouvelle option par défaut qui permet de mémoriser la zone de sélection actuelle de la <a href='https://phabricator.kde.org/D19117'>région rectangulaire</a>, mais uniquement jusqu'à la fermeture du programme.
+ Vous pouvez configurer ce qui arrive lors d'un appui sur le raccourci de copie d'écran <a href='https://phabricator.kde.org/T9855'>alors que Spectacle est déjà en cours d'exécution</a>.
+ Spectacle vous permet de modifier le <a href='https://bugs.kde.org/show_bug.cgi?id=63151'>niveau de compression</a> pour les formats d'images avec perte.
+ La fonctionnalité « Enregistrer les paramètres » vous montre à quoi ressemblera le <a href='https://bugs.kde.org/show_bug.cgi?id=381175'>nom de fichier d'une copie d'écran</a>. Vous pouvez aussi facilement adapter le modèle de <a href='https://bugs.kde.org/show_bug.cgi?id=390856'>nom de fichier</a> à vos préférences en cliquant simplement sur les espaces réservés.
+ Spectacle n'affiche plus maintenant à la fois les options « Plein écran (Tous les écrans) » et « Écran courant » quand votre ordinateur n'a qu'un seul écran.
+ Le texte d'aide dans le mode « Zone rectangulaire » s'affiche maintenant dans le milieu de l'écran principal, plutôt que réparti entre les écrans.
+ Lors de l'exécution sous Wayland, Spectacle ne prend en compte que les fonctionnalités qui sont opérationnelles.

### Jeux et éducation

{{<figure src="/announcements/applications/19.04.0/app1904_kmplot.png" width="600px" >}}

Notre suite d'applications intègre de nombreux <a href='https://games.kde.org/'>jeux</a> et de nombreuses <a href='https://edu.kde.org/'>applications pédagogiques</a>.

<a href='https://kde.org/applications/education/kmplot'>KmPlot</a> est un traceur de fonctions mathématiques. Il dispose d'un puissant analyseur syntaxique intégré. Les graphiques peuvent être colorés et la vue est évolutive, ce qui vous permet de zoomer au niveau dont vous avez besoin. Les utilisateurs peuvent tracer différentes fonctions simultanément et les combiner pour construire de nouvelles fonctions.

+ Vous pouvez maintenant faire un zoom avant en appuyant sur « CTRL » et en utilisant la <a href='https://bugs.kde.org/show_bug.cgi?id=159772'>molette de souris</a>.
+ Cette version de Kmplot propose une option d'<a href='https://phabricator.kde.org/D17626'>aperçu d'impression</a>
+ La valeur de racine carré ou la parie (x, y) peuvent être maintenant copiée dans le <a href='https://bugs.kde.org/show_bug.cgi?id=308168%'>presse-papier</a>.

<a href='https://kde.org/applications/games/kolf/'>Kolf</a> est un jeu de golf miniature.

+ La <a href='https://phabricator.kde.org/D16978'>prise en charge du son</a> a été remise en opération.
+ Kolf a été porté avec succès à partir de « kdelibs4 ».
