---
aliases:
- ../announce-applications-17.08.0
changelog: true
date: 2017-08-17
description: KDE publie les applications de KDE 17.08.0
layout: application
title: KDE publie les applications de KDE 17.08.0
version: 17.08.0
---
17 août 2017 . La version 17.08 de KDE Applications est arrivée. Nous avons travaillé pour rendre à la fois les applications et les bibliothèques sous-jacentes plus stables et plus faciles à utiliser. En prenant compte de vos commentaires, nous avons rendu la suite KDE Applications moins sujette aux petits pépins et beaucoup plus conviviale. Profitez de vos nouvelles applications !

### Plus de portage vers l'environnement de développement version 5

Nous sommes heureux que les applications suivantes, qui étaient basées sur kdelibs4, soient maintenant basées sur KDE Frameworks 5 : kmag, kmousetool, kgoldrunner, kigo, konquest, kreversi, ksnakeduel, kspaceduel, ksudoku, kubrick, lskat, et umbrello. Merci aux développeurs qui ont donné de leur temps et de leur travail pour que cela soit possible.

### Quoi de neuf dans les applications de KDE 17.08

#### Dolphin

Les développeurs de Dolphin indiquent que Dolphin affiche maintenant « l'heure de suppression » dans la corbeille et affiche « la date de création » si le système d'exploitation le prend en charge comme sous BSD.

#### KIO-Extras

« kio-extras » fournit maintenant une meilleure prise en charge des partages « Samba ».

#### KAlgebra

Les développeurs de KAlgebra ont travaillé aux améliorations dans leur interface avec Kirigami pour le bureau et implémenté l'auto-complètement du code.

#### Kontact

- Dans KMailtransport, les développeurs ont ré-activé la prise en charge du transport avec Akonadi, ont créé la prise en charge des modules externes et re-créé la prise en charge du transport de courriels par sendmail.
- Dans SieveEditor, de nombreux bogues dans les scripts de création automatique ont été corrigés et fermés. En plus de corrections générales, l'éditeur de lignes d'expressions régulières a été ajouté.
- La possibilité d'utiliser un éditeur externe dans KMail a été restaurée sous la forme d'un module externe.
- L'assistant d'importation de Akonadi possède maintenant un « convertisseur pour toute conversion » sous forme de module externe. Ainsi, les développeurs peuvent créer facilement de nouveaux convertisseurs.
- Les applications dépendent maintenant de Qt 5.7. Les développeurs ont corrigé beaucoup d'erreurs de compilation sous Windows. L'ensemble de kdepim ne compile pas encore sous Windows mais les développeurs ont fait de gros progrès. Pour commencer, les développeurs ont créé un outil dédié. Beaucoup de corrections de bogues ont été faites pour moderniser le code (C++11). Le support de Wayland sur Qt 5.9. Kdepim-runtime ajoute une ressource facebook.

#### Kdenlive

Pour Kdenlive, l'équipe a corrigé l'« Effet de gel » cassé. Dans les versions récentes, il était impossible de modifier le cadre gelé pour l'effet de gel. Un raccourci de clavier pour la fonctionnalité « Extraction de trames » est maintenant disponible. L'utilisateur peut maintenant enregistrer des copies d'écran de leur frise chronologique avec un raccourci clavier et un nom est aussi suggéré, selon le numéro de trame <a href='https://bugs.kde.org/show_bug.cgi?id=381325'>https://bugs.kde.org/show_bug.cgi?id=381325</a>. La correction des scintillements lumineux sur transitions n'apparaît pas dans l'interface : <a href='https://bugs.kde.org/show_bug.cgi?id=382451'>https://bugs.kde.org/show_bug.cgi?id=382451</a>. Une correction d'un problème sur des clics dans l'audio (A ce jour, ceci nécessite la compilation des « MLT » de dépendances à partir du dépôt « git », jusqu'à la publication d'une « MLT ») : <a href='https://bugs.kde.org/show_bug.cgi?id=371849'>https://bugs.kde.org/show_bug.cgi?id=371849</a>.

#### Krfb

Les développeurs ont terminé le portage du plugin X11 vers Qt5, et krfb fonctionne à nouveau en utilisant un backend X11 beaucoup plus rapide que le plugin Qt. Il y a une nouvelle page de paramètres, permettant à l'utilisateur de choisir son plugin framebuffer préféré.

#### Konsole

Konsole permet maintenant un scrollback illimité pour dépasser la limite de 2GB (32bit). Konsole permet maintenant aux utilisateurs d'entrer n'importe quel emplacement pour stocker les fichiers scrollback. De plus, une régression a été corrigée, Konsole peut à nouveau permettre à KonsolePart d'appeler la boîte de dialogue Manage Profile.

#### KAppTemplate

Dans KAppTemplate il y a maintenant une option pour installer de nouveaux modèles à partir du système de fichiers. D'autres modèles ont été retirés de KAppTemplate et intégrés dans des produits connexes ; le modèle de plugin ktexteditor et le modèle kpartsapp (porté vers Qt5/KF5 maintenant) font partie des cadres KDE KTextEditor et KParts depuis 5.37.0. Ces changements on pour but de simplifier la création de modèles dans les applications KDE.

### Éradication des bogues

Plus de 80 corrections de bogues ont été apportées dans les applications dont la suite Kontact, Ark, Dolphin, K3b, Kdenlive, KGpg, Konsole et bien d'autres !

### Journal complet des changements
