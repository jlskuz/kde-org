---
aliases:
- ../announce-applications-17.04.3
changelog: true
date: 2017-07-13
description: KDE에서 KDE 프로그램 17.04.3 출시
layout: application
title: KDE에서 KDE 프로그램 17.04.3 출시
version: 17.04.3
---
July 13, 2017. Today KDE released the third stability update for <a href='../17.04.0'>KDE Applications 17.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

kdepim, dolphin, dragonplayer, kdenlive, umbrello 등에 25개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support version of KDE Development Platform 4.14.34.
