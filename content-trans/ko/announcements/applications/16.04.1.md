---
aliases:
- ../announce-applications-16.04.1
changelog: true
date: 2016-05-10
description: KDE에서 KDE 프로그램 16.04.1 출시
layout: application
title: KDE에서 KDE 프로그램 16.04.1 출시
version: 16.04.1
---
May 10, 2016. Today KDE released the first stability update for <a href='../16.04.0'>KDE Applications 16.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

kdepim, Ark, Kate, Dolphin, Kdenlive, Lokalize, Spectacle 등에 25개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support version of KDE Development Platform 4.14.20.
