---
aliases:
- ../announce-applications-19.04.2
changelog: true
date: 2019-06-06
description: KDE Ships Applications 19.04.2.
layout: application
major_version: '19.04'
release: applications-19.04.2
title: KDE에서 KDE 프로그램 19.04.2 출시
version: 19.04.2
---
{{% i18n_date %}}

Today KDE released the second stability update for <a href='../19.04.0'>KDE Applications 19.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Ark, Dolphin, JuK, Kdenlive, KmPlot, Okular, Spectacle 등에 50개 이상의 버그 수정 및 기능 개선이 있었습니다.

개선 사항:

- Okular에서 특정 EPUB 문서를 보는 중 충돌이 수정됨
- Kleopatra 암호화 관리자에서 비밀 키를 다시 내보낼 수 있음
- KAlarm 이벤트 알림이를 최신 PIM 라이브러리와 함께 사용할 때 더 이상 시작이 실패하지 않음
