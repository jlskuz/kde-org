---
aliases:
- ../announce-applications-15.12.3
changelog: true
date: 2016-03-16
description: KDE에서 KDE 프로그램 15.12.3 출시
layout: application
title: KDE에서 KDE 프로그램 15.12.3 출시
version: 15.12.3
---
March 15, 2016. Today KDE released the third stability update for <a href='../15.12.0'>KDE Applications 15.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

kdepim, akonadi, Ark, KBlocks, KCalc, KTouch 및 Umbrello 등에 15개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support version of KDE Development Platform 4.14.18.
