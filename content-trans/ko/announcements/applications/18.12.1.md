---
aliases:
- ../announce-applications-18.12.1
changelog: true
date: 2019-01-10
description: KDE Ships Applications 18.12.1.
layout: application
major_version: '18.12'
title: KDE Ships KDE Applications 18.12.1
version: 18.12.1
---
{{% i18n_date %}}

Today KDE released the first stability update for <a href='../18.12.0'>KDE Applications 18.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Cantor, Dolphin, JuK, Kdenlive, Konsole, Okular 등에 20개 이상의 버그 수정 및 기능 개선이 있었습니다.

개선 사항:

- Akregator는 Qt 5.11 이상의 WebEngine과 호환됨
- JuK 음악 재생기의 열 정렬 수정
- Konsole에서 상자 그리기 문자가 다시 올바르게 표시됨
