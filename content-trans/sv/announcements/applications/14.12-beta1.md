---
aliases:
- ../announce-applications-14.12-beta1
custom_spread_install: true
date: '2014-11-06'
description: KDE levererar Program 14.12 Beta 1.
layout: application
title: KDE levererar första betaversion av KDE-program 14.12
---
6:e november, 2014. Idag ger KDE ut betautgåvan av de nya versionerna av KDE-program. Med beroenden och funktioner frysta, fokuserar KDE-grupperna nu på att rätta fel och ytterligare finputsning.

Med de olika programmen baserade på KDE Ramverk 5, behöver KDE-program utgåva 14.12 omfattande utprovning för att behålla och förbättra kvaliteten och användarupplevelsen. Verkliga användare är väsentliga för att upprätthålla hög kvalitet i KDE, eftersom utvecklare helt enkelt inte kan prova varje möjlig konfiguration. Vi räknar med dig för att hjälpa oss hitta fel tidigt, så att de kan krossas innan den slutliga utgåvan. Överväg gärna att gå med i gruppen genom att installera betaversionen och <a href='https://bugs.kde.org/'>rapportera eventuella fel</a>.

#### Installera KDE-program 14.12 Beta1 binärpaket

<em>Paket</em>. Vissa Linux- och UNIX-operativsystemleverantörer har vänligen tillhandahållit binärpaket av KDE-program 14.12 Beta1 (internt 14.11.80) för vissa versioner av sina distributioner, och i andra fall har volontärer i gemenskapen gjort det. Ytterligare binärpaket, samt uppdateringar av paketen som nu är tillgängliga, kan bli tillgängliga under kommande veckor.

<em>Paketplatser</em>. Besök <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Gemenskapens Wiki</a> för en aktuell lista med tillgängliga binärpaket som har kommit till KDE-projektets kännedom.

#### Kompilera KDE-program 14.12 Beta1

Fullständig källkod för KDE-program 14.12 Beta 1 kan <a href='http://download.kde.org/unstable/applications/14.11.80/src/'>laddas ner fritt</a>. Instruktioner om hur man kompilerar och installerar är tillgängliga på <a href='/info/applications/applications-14.11.80'>informationssidan om KDE-program Beta 1</a>.
