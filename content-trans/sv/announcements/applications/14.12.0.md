---
aliases:
- ../announce-applications-14.12.0
changelog: true
date: '2014-12-17'
description: KDE levererar Program 14.12.
layout: application
title: KDE levererar KDE-program 14.12
version: 14.12.0
---
17:e december, 2014. Idag ger KDE ut KDE-program 14.12. Utgåvan innehåller nya funktioner och felrättningar för mer än hundra program. De flesta av dessa program är baserade på KDE:s utvecklingsplattform 4, men vissa har konverterats till de nya <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Ramverk 5</a>, en uppsättning modulära bibliotek som är baserade på Qt5, den senaste versionen av det populära programramverket för flera plattformar.

<a href='http://api.kde.org/4.x-api/kdegraphics-apidocs/libs/libkface/libkface/html/index.html'>Libkface</a> är nytt i den här utgåvan. Det är ett bibliotek som möjliggör ansiktsdetektering och ansiktsigenkänning i fotografier.

Utgåvan innehåller de första KDE Ramverk 5-baserade versionerna av <a href='http://www.kate-editor.org'>Kate</a> och <a href='https://www.kde.org/applications/utilities/kwrite/'>Kwrite</a>, <a href='https://www.kde.org/applications/system/konsole/s'>Konsole</a>, <a href='https://www.kde.org/applications/graphics/gwenview/s'>Gwenview</a>, <a href='http://edu.kde.org/kalgebras'>Kalgebra</a>, <a href='http://edu.kde.org/kanagram'>Kanagram</a>, <a href='http://edu.kde.org/khangman'>Hänga gubben</a>, <a href='http://edu.kde.org/kig'>Kig</a>, <a href='http://edu.kde.org/parley'>Parley</a>, <a href='https://www.kde.org/applications/development/kapptemplate/'>Kapptemplate</a> och <a href='https://www.kde.org/applications/utilities/okteta/'>Okteta</a>. Vissa bibliotek är också redo för användning i KDE Ramverk 5: analitza och libkeduvocdocument.

<a href='http://kontact.kde.org'>Kontact-sviten</a> version 4.14 är nu i långtidsunderhåll, medan utvecklarna använder sin nya energi för att konvertera den till KDE Ramverk 5.

Vissa av de nya funktionerna i den här utgåvan omfattar:

+ <a href='http://edu.kde.org/kalgebra'>Kalgebra</a> har en ny Android-version tack vare KDE Ramverk 5, och klarar nu av att <a href='http://www.proli.net/2014/09/18/touching-mathematics/'>skriva ut sina diagram i tre dimensioner</a>
+ <a href='http://edu.kde.org/kgeography'>Kgeografi</a> har en ny karta över Bihar.
+ Dokumentvisaren <a href='http://okular.kde.org'>Okular</a> har nu stöd för omvänd sökning för latex-synctex i DVI, och några mindre förbättringar av stödet för ePub.
+ <a href='http://umbrello.kde.org'>Umbrello</a>, UML-modelleringsverktyget, har många nya funktioner för talrika att lista här.

April-utgåvan av KDE-program 15.04 kommer att innehålla många nya funktioner samt fler program baserade på det modulära KDE Ramverk 5.
