---
aliases:
- ../announce-applications-15.12.2
changelog: true
date: 2016-02-16
description: KDE levererar KDE-program 15.12.2
layout: application
title: KDE levererar KDE-program 15.12.2
version: 15.12.2
---
16:e februari, 2016. Idag ger KDE ut den andra stabilitetsuppdateringen av <a href='../15.12.0'>KDE-program 15.12</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 30 registrerade felrättningar omfattar förbättringar av bland annat kdelibs, kdepim, kdenlive, marble, konsole, spectacle, akonadi, ark och umbrello.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.17.
