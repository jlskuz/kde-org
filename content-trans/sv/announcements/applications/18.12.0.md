---
aliases:
- ../announce-applications-18.12.0
changelog: true
date: 2018-12-13
description: KDE levererar Program 18.12.
layout: application
release: applications-18.12.0
title: KDE levererar KDE-program 18.12.0
version: 18.12.0
---
{{% i18n_date %}}

KDE-program 18.12 är nu utgivna.

{{%youtube id="ALNRQiQnjpo"%}}

Vi arbetar kontinuerligt på att förbättra programvaran som ingår i vår KDE-programserie, och vi hoppas att du finner alla nya förbättringar och felrättningar användbara.

## Vad är nytt i KDE-program 18.12

Mer än 140 fel har rättats i program, inklusive Kontact-sviten, Ark, Cantor, Dolphin, Gwenview, Kate, KmPlot, Konsole, Lokalize, Okular, Spectacle, Umbrello med flera.

### Filhantering

{{<figure src="/announcements/applications/18.12.0/app1812_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE:s kraftfulla filhanterare:

- Ny MTP-implementering som gör den fullständigt användbar för produktion
- Enorma prestandaförbättringar vid läsning av filer via SFTP-protokollet
- För förhandsgranskning av miniatyrbilder ritas nu ramar och skuggor bara för bildfiler utan transparens, vilket förbättrar visning av ikoner.
- Ny förhandsgranskning av miniatyrbilder för Libreoffice-dokument och Appimage-program.
- Videofiler mes större storlek än 5 MiB visas nu i katalogminiatyrbilder när miniatyrbildvisning av kataloger är aktiverad.
- När en ljud-cd läses, kan Dolphin ny ändra variabel bithastighet för MP3-avkodaren och rätta tidsstämplar för FLAC
- Dolpins kontrollmeny visar nu alternativen 'Skapa ny…' och har ett nytt menyalternativ 'Visa dolda platser'
- Dolphin avslutas nu när det bara finns en flik öppen och den vanliga snabbtangenten 'stäng flik' (Ctrl+W) används
- Efter att en volym avmonterats från panelen Platser är det nu möjligt att montera den igen
- Vyn Senaste dokument (tillgänglig genom att bläddra till recentdocuments:/ i Dolphin) visar nu bara verkliga dokument, och filtrerar automatiskt bort webbadresser
- Dolphin visar nu en varning innan man får tillåtelse att byta namn på filer eller kataloger på ett sådant sätt att de omedelbart skulle döljas.
- Det är inte längre möjligt att försöka avmontera det aktiva operativsystemets eller hemkatalogens disk från panelen Platser

<a href='https://www.kde.org/applications/utilities/kfind'>Kfind</a>, KDE:s traditionella filsökningsverktyg, har nu en sökmetod för metadata baserad på KFileMetaData.

### Kontor

<a href='https://www.kde.org/applications/internet/kmail/'>Kmail</a>, KDE:s kraftfulla e-postprogram:

- Kmail kan nu visa en förenad inkorg
- Nytt insticksprogram: Skapa HTML-brev från språket Markdown
- Använd Purpose för att dela text (som e-post)
- HTML-brev går nu att läsa oberoende av vilket färgschema som används

{{<figure src="/announcements/applications/18.12.0/app1812_okular01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, KDE:s mångsidiga dokumentvisare:

- Nytt kommentarverktyg 'Skrivmaskinstext' som kan användas för att skriva text var som helst
- Den hierarkiska visningen av innehållsförteckning har nu möjligt att expandera och dra ihop allting, inte bara ett specifikt avsnitt
- Förbättrat beteende vid radbrytning i kommentarer på plats
- När musen hålls över en länk visas nu webbadressen alltid när det går att klicka på den, istället för bara när bläddringsläge används
- ePub-filer som innehåller resurser med mellanslag i sina webbadresser visas nu på ett riktigt sätt

### Utveckling

{{<figure src="/announcements/applications/18.12.0/app1812_kate01.png" width="600px" >}}

<a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, KDE:s avancerade texteditor:

- När den inbäddade terminalen används, synkroniserar den automatiskt aktuell katalog med det aktiva dokumentets plats på disken
- Kates inbäddade terminal kan nu ges fokus eller inte genom att använda snabbtangenten F4
- Kates inbyggda flikbyte visar nu hela sökvägar för filer med liknande namn
- Radnummer är nu normalt på
- Det oerhört användbara och kraftfulla insticksprogrammet Textfilter är nu normal aktiverat och lättare att upptäcka
- Att öppna ett dokument som redan är öppet med snabböppningsfunktionen går nu tillbaka till detta dokument
- Snabböppningsfunktionen visar nu inte längre dubbletter
- När flera aktiviteter används, öppnas filer nu i den riktiga aktiviteten
- Kate visar nu alla rätta ikoner när det körs på Gnome med användning av Gnomes ikontema

{{<figure src="/announcements/applications/18.12.0/app1812_konsole.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/konsole/'>Terminal</a>, KDE:s terminalemulator:

- Terminal stöder nu emoji-tecken helt och hållet
- Ikoner för inaktiva flikar markeras nu när de tar emot en ljudsignal
- Efterföljande kolon anses inte längre vara en del av ett ord vid markering med dubbelklick, vilket gör det enklare att välja sökvägar och utdata från 'grep'
- När en mus med bakåt- och framåtknappar kopplas in kan Terminal nu använda knapparna för att byta mellan flikar
- Terminal har nu ett menyalternativ för att återställa teckenstorlek till profilens normalvärde om storleken har ökats eller reducerats
- Det är nu svårare att koppla loss flikar av misstag, och det går snabbare att ordna om dem noggrant
- Förbättrat beteende vid skift-klick
- Rättade dubbelklick på en textrad som överskrider fönsterstorleken
- Sökraden stängs återigen när Esc tangenten trycks ned

<a href='https://www.kde.org/applications/development/lokalize/'>Lokalize</a>, KDE:s översättningsverktyg:

- Dölj översatta filer under projektfliken
- Tillägg av grundstöd för pology, systemet för kontroll av syntax och ordanvändning
- Förenklad navigering med flikordning och öppnande av flera flikar
- Rättade segmenteringsfel beroende på samtidig åtkomst av databasobjekt
- Rättade inkonsekvent drag och släpp
- Rättade fel för genvägar som var olika mellan editorer
- Förbättrat sökbeteende (sökning hittar och visar pluralformer)
- Återställ kompatibilitet med Windows tack vara byggsystemet craft

### Verktyg

<a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE:s bildvisare:

- Verktyget 'Reducera röda ögon' fick ett antal olika trevliga användbarhetsförbättringar
- Gwenview visar nu en varningsdialogruta när menyraden döljes som talar om hur man får tillbaka den

{{<figure src="/announcements/applications/18.12.0/app1812_spectacle01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE:s skärmbildsverktyg:

- Spectacle har nu möjlighet att numrera skärmbildsfiler i sekvens, och använder normalt denna namngivningsmetod om textfältet för filnamn rensas
- Rättade att spara bilder med ett annat format än .png när Spara som… används
- När Spectacle används för att visa en skärmbild i ett annat program är det nu möjligt att ändra och spara bilden efter man är klar med den
- Spectacle öppnar nu rätt katalog när man klickar på Verktyg > Öppna skärmbildskatalog
- Skärmbildernas tidsstämplar gäller nu när bilden skapades, inte när den sparades
- Alla alternativ för att spara finns nu på sidan "Spara"

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, KDE:s arkivhanterare:

- Tillägg av stöd för formatet Zstandard (tar.zst-arkiv)
- Rättade förhandsgranskning av vissa filer i Ark (t.ex. Open Document) som arkiv istället för att öppna dem i lämpligt program

### Matematik

<a href='https://www.kde.org/applications/utilities/kcalc/'>Kcalc</a>, KDE:s enkla miniräknare, har nu en inställning för att upprepa den senaste beräkningen flera gånger.

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, KDE:s matematiska gränssnitt:

- Tillägg av till Markdown-posttyp
- Animerad färgläggning av kommandoinmatningen som för närvarande beräknas
- Visualisering av väntande kommandoinmatning (köad, men som ännu inte beräknas)
- Tillåt att kommandoinmatning formateras (bakgrundsfärg, förgrundsfärg, teckensnittsegenskaper)
- Tillåt att ny kommandoinmatning infogas på godtyckliga platser i arbetsbladet genom att placera markören på önskad position och börja skriva
- Visa resultat som oberoende resultatobjekt i arbetsbladet för uttryck som har flera kommandon
- Tillägg av stöd för att öppna arbetsblad med relativa sökvägar från terminalen
- Tillägg av stöd för att öppna flera filer i ett Cantor-skal
- Ändra färg och teckensnitt när ytterligare information efterfrågas för att bättre skilja det från vanlig kommandoinmatning
- Tillägg av genvägar för navigering mellan arbetsbladen (Ctrl+Page Up, Ctrl+Page Down)
- Tillägg av åtgärd för återställning av zoom i undermenyn 'Visa'
- Aktivera nerladdning av Cantor-projekt från store.kde.org (för närvarande fungerar bara uppladdning till webbplatsen)
- Öppna arbetsbladet skrivskyddat om bakgrundsprogrammet inte är tillgängligt på systemet

<a href='https://www.kde.org/applications/education/kmplot/'>Kmplot</a>, KDE:s funktionsritverktyg, rättade många problem:

- Rättade felaktiga namn på diagrammen för derivator och integraler med konventionell notation
- SVG-export i Kmplot fungerar nu riktigt
- Funktionen för förstaderivator har ingen prim-notation
- Avmarkering av funktioner utan fokus döljer nu deras grafer:
- Löste krasch av Kmplot när 'Redigera begränsning' öppnas rekursivt från funktionseditorn
- Löste krasch av Kmplot när en funktion togs bort och muspekaren följde den i diagrammet
- Det går nu att exportera uppritad data med vilket bildformat som helst
