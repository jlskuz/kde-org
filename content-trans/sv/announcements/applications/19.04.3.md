---
aliases:
- ../announce-applications-19.04.3
changelog: true
date: 2019-07-11
description: KDE levererar Program 19.04.3.
layout: applications
major_version: '19.04'
release: applications-19.04.3
title: KDE levererar KDE-program 19.04.3
version: 19.04.3
---
{{% i18n_date %}}

Idag ger KDE ut den tredje stabilitetsuppdateringen av <a href='../19.04.0'>KDE-program 19.04</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Omkring sextio registrerade felrättningar omfattar förbättringar av bland annat Kontact, Ark, Cantor, JuK, K3b, Kdenlive, Ktouch, Okular och Umbrello.

Förbättringar omfattar:

- Konqueror och Kontact kraschar inte längre vid avslutning med QtWebEngine 5.13
- Att klippa ut grupper med kompositioner kraschar inte längre i videoeditorn Kdenlive
- Python-import i Umbrello UML-designprogrammet hanterar nu parametrar med förvalsargument
