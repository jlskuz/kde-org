---
aliases:
- ../../kde-frameworks-5.44.0
date: 2018-03-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- balooctl: Ta bort alternativet checkDb (fel 380465)
- indexerconfig: Beskriv några funktioner
- indexerconfig: Exponera funktionen canBeSearched (fel 388656)
- balooctl monitor: Vänta på D-bus gränssnitt
- fileindexerconfig: Introducera canBeSearched() (fel 388656)

### Breeze-ikoner

- Ta bort view-media-playlist från inställningsikoner
- Lägg till 24-bildpunkters ikon media-album-cover
- Lägga till stöd för Babe QML (22 bildpunkter)
- Uppdatera handle- ikoner för kirigami
- Lägg till 64-bildpunkters mediaikoner för elisa

### Extra CMake-moduler

- Definiera **__ANDROID_API__**
- Rätta readelf kommandonamn på x86
- Android-verktygskedja: Lägg till variabeln ANDROID_COMPILER_PREFIX, rätta inkluderingssökväg för x86-mål, utöka sökvägen för NDK-beroenden

### KDE Doxygen-verktyg

- Avsluta med fel om utdatakatalogen inte är tom (fel 390904)

### KConfig

- Spara en del minnesanvändning genom att använda rätt programmeringsgränssnitt
- Exportera kconf_update med verktyg

### KConfigWidgets

- Förbättra KLanguageButton::insertLanguage när inget namn skickas med
- Lägg till ikoner för KStandardActions Deselect och Replace

### KCoreAddons

- Städa m_inotify_wd_to_entry innan invalidering av Entry-pekare (fel 390214)
- kcoreaddons_add_plugin: Ta bort OBJECT_DEPENDS utan effekt för json-fil
- Hjälp automoc att hitta metadata JSON-filer refererade till i koden
- kcoreaddons_desktop_to_json: Notera genererade filen i byggloggen
- Öka shared-mime-info till 1.3
- Introducera K_PLUGIN_CLASS_WITH_JSON

### KDeclarative

- Rätta byggfel på armhf/aarch64
- Döda QmlObjectIncubationController
- Koppla bort render() vid fönsterändring (fel 343576)

### KHolidays

- Allen Winter är nu officiellt underhållsansvarig för KHolidays

### KI18n

- Dokumentation av programmeringsgränssnitt: Lägg till not om att anropa setApplicationDomain efter QApp skapas

### KIconThemes

- [KIconLoader] Ta hänsyn till devicePixelRatio för överlagringar

### KIO

- Anta inte layout av msghdr- och iovec-struktur (fel 391367)
- Rätta protokollval i KUrlNavigator
- Ändra qSort till std::sort
- [KUrlNavigatorPlacesSelector] Använd KFilePlacesModel::convertedUrl
- [Drop Job] Skapa riktig skräpfil vid länkning
- Rätta oavsiktlig aktivering av menyalternativ i länkstig (fel 380287)
- [KFileWidget] Dölj platsram och rubrik
- [KUrlNavigatorPlacesSelector] Placera kategorier i undermenyer (fel 389635)
- Utnyttja KIO test standardhjälpdeklaration
- Lägg till Ctrl+H i listan över genvägar för "visa/dölj dolda filer" (fel 390527)
- Lägg till stöd för flyttningssemantik i KIO::UDSEntry
- Rätta problem med "tvetydig genväg" introducerad med D10314
- Stoppa in meddelanderutan "Kunde inte hitta körbar fil" i en köad lambda (fel 385942)
- Förbättra användbarhet för dialogrutan "Öppna med" genom att lägga till ett alternativ för att filtrera programträdet
- [KNewFileMenu] KDirNotify::emitFilesAdded efter storedPut (fel 388887)
- Rätta assert när dialogrutan rebuild-ksycoca avbryts (fel 389595)
- Rätta fel nr. 382437 "Regression i kdialog orsakar felaktig filändelse" (fel 382437)
- Snabbare start av simplejob
- Reparera kopiering av fil till VFAT utan varningar
- kio_file: Hoppa över felhantering för ursprungliga rättigheter vid filkopiering
- Tillåt att flyttningssemantik genereras för KFileItem. Den befintliga kopieringskonstruktorn, destruktorn och kopieringstilldelningsoperatorn genereras nu också av kompilatorn.
- Gör inte stat(/etc/localtime) mellan read() och write() vid kopiering av filer (fel 384561)
- remote: Skapa inte poster med tomma namn
- Lägg till funktionalitet supportedSchemes
- Använd F11 som genväg för att visa eller dölja sidoförhandsgranskningen
- [KFilePlacesModel] Gruppera delade nätverksresurser under "fjärrkategorin"

### Kirigami

- Visa verktygsknappen som markerad medan menyn visas
- Icke interaktiva rullningsindikatorer på mobiler
- Rätta undermenyer för åtgärder
- Gör det möjligt att använda QQC2.Action
- Gör det möjligt att stödja exklusiva åtgärdsgrupper (fel 391144)
- Visa texten vid verktygsknapparna för sidåtgärder
- Gör det möjligt för åtgärder att visa undermenyer
- Ha inte specifika komponentpositioner i överliggande objekt
- Utlös inte åtgärder i SwipeListItem om de inte är exponerade
- Lägg till kontrollen isNull() innan inställning om QIcon är en mask
- Lägg till FormLayout.qml i kirigami.qrc
- Rätta färger i swipelistitem
- Bättre beteende för sidhuvud och sidfot
- Förbättra vänstervadderings- och avkortningsbeteende för ToolBarApplicationHeader
- Säkerställ att navigeringsknapparna inte hamnar under åtgärden
- Stöd för sidhuvud- och sidfot-egenskaper i overlaysheet
- Eliminera onödig underkantsvaddering för OverlaySheets (fel 390032)
- Polskt utseende för ToolBarApplicationHeader
- Visa en stängknapp på skrivbord (fel 387815)
- Inte möjligt att stänga arket med mushjul
- Multiplicera bara ikonstorleken om Qt inte redan gör det (fel 390076)
- Ta hänsyn till global sidfot för grepposition
- Komprimera händelser för att skapa och förstöra rullningslister
- ScrollView: Gör scrollbar policy öppen och rätta den

### KNewStuff

- Lägg till vokoscreen i KMoreTools och lägg till den i "screenrecorder" gruppering

### KNotification

- Använd QWidget för att se om fönstret är synlig

### Ramverket KPackage

- Hjälp automoc att hitta metadata JSON-filer refererade till i koden

### KParts

- Rensa bort gammal onåbar kod

### Kör program

- Uppdatera krunner insticksprogrammall

### KTextEditor

- Lägg till ikoner för KTextEditor dokumentexport, bokmärkesborttagning och textformatering stora bokstäver, små bokstäver och kapitalisering

### Kwayland

- Implementera frisättande av klientfrigjord utmatning
- [server] Hantera situationen när DataSource för en dragning förstörs på ett riktigt sätt (fel 389221)
- [server] Krascha inte när en delyta tilldelas vars överliggande yta har förstörts (fel 389231)

### KXMLGUI

- Nollställ interna objekt i QLocale när ett anpassat programspråk används
- Tillåt inte att åtgärder för avskiljare anpassas via sammanhangsberoende meny
- Visa inte sammanhangsberoende meny vid högerklick utanför (fel 373653)
- Förbättra KSwitchLanguageDialogPrivate::fillApplicationLanguages

### Oxygen-ikoner

- Lägg till ikon för Artikulate (fel 317527)
- Lägg till ikon för folder-games (fel 318993)
- Rätta felaktig 48 bildpunkters ikon för calc.template (fel 299504)
- Lägg till ikon för media-playlist-repeat och shuffle (fel 339666)
- Oxygen: Lägg till etikettikoner som i Breeze (fel 332210)
- Länka emblem-mount till media-mount (fel 373654)
- Lägg till nätverksikoner som är tillgängliga i breeze-icons (fel 374673)
- Synkronisera Oxygen med breeze-icons lägg till ikoner för ljudplasmoid
- Lägg till edit-select-none i Oxygen för Krusader (fel 388691)
- Lägg till ikon rating-unrated (fel 339863)

### Plasma ramverk

- Använd det nya värdet på largeSpacing i Kirigami
- Reducera synligheten för PC3 TextField platsmarkeringstext
- Gör inte heller titlar 20 % genomskinliga
- [PackageUrlInterceptor] Skriv inte om "inline"
- Gör inte rubriker 20 % genomskinliga, för att stämma med Kirigami
- Placera inte fullrep i popup om ej ihopdragen
- Hjälp automoc att hitta metadata JSON-filer refererade till i koden
- [AppletQuickItem] Förladda bara miniprogramexpanderare om inte redan expanderad
- Andra mikrooptimeringar av förladdning
- Ställ in förval i IconItem till smooth=true
- Förladda också expanderaren (dialogrutan)
- [AppletQuickItem] Rätta inställning av förvald förladdningspolicy om ingen miljövariabel är inställd
- Rätta höger-till-vänster utseende för kombinationsruta (fel https://bugreports.qt.io/browse/QTBUG-66446)
- Försök att förladda vissa miniprogram på ett smart sätt
- [Icon Item] Ställ in filtrering på FadingNode struktur
- Initiera m_actualGroup till NormalColorGroup
- Säkerställ att FrameSvg- och Svg-instanser har rätt devicePixelRatio

### Prison

- Uppdatera länkar till beroenden, och markera att Android officiellt stöds
- Gör beroende av DMTX valfritt
- Lägg till QML-stöd för Prison
- Ställ också in minimal storlek för endimensionella streckkoder

### Syfte

- Rätta lager, tillgodose KIO

### QQC2StyleBridge

- Rätta syntaxfel i föregående incheckning, detekterat genom att starta ruqola
- Visa en alternativknapp när en exklusive kontroll visas (fel 391144)
- Implementera MenuBarItem
- Implementera DelayButton
- Ny komponent: rund knapp
- Ta hänsyn till verktygsradsposition
- Stöd färger för ikoner i knappar
- Stöd --reverse
- Ikoner i meny fullt fungerande
- Konsekventa skuggor med den nya Breeze-stilen
- Några QStyles verkar inte returnera rimliga bildpunktsmått här
- Första grova stöd för ikoner
- Gå inte runt med mushjulet

### Solid

- Rätta en läcka och felaktig kontroll av null-pekare i DADictionary
- [UDisks] Rätta regression för automatisk montering (fel 389479)
- [UDisksDeviceBackend] Undvik flera uppslagningar
- Mac/IOKit-gränssnitt: Stöd för drivrutiner, diskar och volymer

### Sonnet

- Använd Locale::name() istället för Locale::bcp47Name()
- Hitta libhunspell byggd av msvc

### Syntaxfärgläggning

- Grundstöd för PHP och Python omgärdade kodblock i Markdown
- Stöd skiftlägesokänslig orddetektering
- Scheme-färgläggning: Ta bort hårdkodade färger
- Lägg till syntaxfärgläggning för SELinux CIL-policy och filsammanhang
- Lägg till ctp-filändelse till PHP syntaxfärgläggning
- Yacc/Bison: Rätta symbolen $ och uppdatera syntax för Bison
- awk.xml: Lägg till nyckelord för gawk-utökningen (fel 389590)
- Lägg till APKBUILD för att färgläggas som en Bash-fil
- Återställ "Lägg till APKBUILD för att färgläggas som en Bash-fil"
- Lägg till APKBUILD för att färgläggas som en Bash-fil

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
