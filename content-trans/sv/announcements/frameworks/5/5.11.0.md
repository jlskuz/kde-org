---
aliases:
- ../../kde-frameworks-5.11.0
date: 2015-06-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Extra CMake-moduler

- Nya argument för ecm_add_tests(). (fel 345797)

### Integrering med ramverk

- Använd korrekt initialDirectory för KDirSelectDialog
- Säkerställ schema är angivet när webbadressens startvärde överskrids
- Acceptera bara befintliga kataloger för FileMode::Directory mode

### KActivities

(ingen ändringslogg tillhandahållen)

### KAuth

- Gör KAUTH_HELPER_INSTALL_ABSOLUTE_DIR tillgängligt för alla användare av KAuth

### KCodecs

- KEmailAddress: Lägg till överlagring av extractEmailAddress och firstEmailAddress som returnerar ett felmeddelande.

### KCompletion

- Rätta oönskad markering vid redigering av filnamnet i fildialogrutan (fel 344525)

### KConfig

- Förhindra krasch om QWindow::screen() är null
- Lägg till KConfigGui::setSessionConfig() (fel 346768)

### KCoreAddons

- Nytt programmeringsgränssnitt för bekvämlighet KPluginLoader::findPluginById()

### KDeclarative

- stöd skapa ConfigModule från KPluginMetdata
- rätta händelser för tryck och håll (pressAndhold)

### Stöd för KDELibs 4

- Använd QTemporaryFile istället för att hårdkoda en tillfällig fil.

### KDocTools

- Uppdatera översättningar
- Uppdatera customization/ru
- Rätta entiteter med felaktiga länkar

### KEmoticons

- Lagra temat i integreringsinsticksprogrammet i cache

### KGlobalAccel

- [vid körning] Flytta plattformsspecifik kod till insticksprogram

### KIconThemes

- Optimera KIconEngine::availableSizes()

### KIO

- Försök inte att komplettera användare och använd assert när prepend inte är tom (fel 346920).
- Använd KPluginLoader::factory() när KIO::DndPopupMenuPlugin laddas
- Rättade låsning när nätverks-proxy används (fel 346214)
- Rättade KIO::suggestName för att bevara filändelser
- Kör igång kbuildsycoca4 när sycoca5 uppdateras.
- KFileWidget: Acceptera inte filer i läget för enbart kataloger
- KIO::AccessManager: Gör det möjligt att behandla en sekventiell QIODevice asynkront

### KNewStuff

- Lägg till ny metod fillMenuFromGroupingNames
- KMoreTools: lägg till många nya grupperingar
- KMoreToolsMenuFactory: hantering för "git-clients-and-actions"
- createMenuFromGroupingNames: gör webbadressparametern valfri

### KNotification

- Rätta krasch i NotifyByExecute när ingen grafisk komponent har angivits (fel 348510)
- Förbättra hantering av underrättelser som stängs (fel 342752)
- Ersätt användning av QDesktopWidget med QScreen
- Säkerställ att KNotification kan användas av en icke-grafisk tråd

### Paketet Framework

- Skydda åtkomst av strukturen qpointer (fel 347231)

### KPeople

- Använd QTemporaryFile istället för att hårdkoda /tmp.

### KPty

- Använd tcgetattr och tcsetattr om tillgängliga

### Kross

- Rätta laddning av Kross-modulerna  "forms" och "kdetranslation"

### KService

- Bevara filägarskap för befintliga cachefiler vid körning som systemadministratör (fel 342438)
- Skydda mot att inte kunna öppna ström (fel 342438)
- Rätta kontroll av felaktiga rättigheter vid skrivning till fil (fel 342438)
- Rätta förfrågan till ksycoca för x-scheme-handler/* pseudo Mime-typer (fel 347353).

### KTextEditor

- Tillåt att tredjepartsprogram eller insticksmoduler att installera egna XML-färgläggningsfiler i katepart5/syntax, liksom i tiden med KDE 4.x
- Lägg till KTextEditor::Document::searchText()
- För tillbaka användning av KEncodingFileDialog (fel 343255)

### KTextWidgets

- Lägg till en metod att rensa decorator
- Tillåt användning av egen sonnet decorator
- Implementera "sök föregående" i KTextEdit.
- Lägg tillbaka stöd för tal-till-text

### KWidgetsAddons

- KAssistantDialog: Lägg tillbaka hjälpknappen som fanns i KDELibs4-versionen

### KXMLGUI

- Lägg till sessionsstöd för KMainWindow (fel 346768)

### NetworkManagerQt

- Sluta stödja WiMAX för NM 1.2.0+

### Plasma ramverk

- Kalenderkomponenter kan nu visa veckonummer (fel 338195)
- Använd QtRendering för teckensnitt i lösenordsfält
- Rätta uppslagning av AssociatedApplicationManager när en Mime-typ har (fel 340326)
- Rätta färgläggning av panelbakgrund (fel 347143)
- Bli av med meddelandet "Kunde inte ladda miniprogram"
- Möjlighet att ladda QML-inställningsmoduler i Plasmoid-inställningsfönster
- Använd inte DataEngineStructure för miniprogram
- Konvertera libplasma så att sycoca inte används i så stor utsträckning som möjligt
- [plasmacomponents] Gör så att SectionScroller följer ListView.section.criteria
- Rullningslister döljes inte längre automatiskt när en pekskärm finns (fel 347254)

### Sonnet

- Använd en central cache för SpellerPlugins.
- Reducera tillfällig minnestilldelning
- Optimera: Töm inte dict-cache när stavningsobjekt kopieras.
- Optimera bort anrop till save() genom att anropa en gång i slutet vid behov.

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
