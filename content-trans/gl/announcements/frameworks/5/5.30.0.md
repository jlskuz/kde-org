---
aliases:
- ../../kde-frameworks-5.30.0
date: 2017-01-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Iconas de Breeze

- engadir iconas de Haguichi de Stephen Brandt (grazas)
- completar a icona de Calligra
- engadir unha icona de CUPS, grazas a colin (fallo 373126)
- actualizar a icona de KAlarm (fallo 362631)
- engadir unha icona de aplicación para krfb (fallo 373362)
- engadir compatibilidade co tipo MIME de R (fallo 371811)
- e máis

### Módulos adicionais de CMake

- appstreamtest: xestionar programas non instalados
- Activar os avisos colorados na saída de ninja
- Corrixir un :: que falta na documentación da API para causar a aplicación de estilo ao código
- Ignorar os ficheiros de libs/includes/cmakeconfig da máquina na cadea de ferramentas de Android
- Documentar o uso de gnustl_shared coa cadea de ferramentas de Android
- Non usar nunca -Wl e --no-undefined en Mac (APPLE)

### Integración de infraestruturas

- Mellorar o KNSHandler de KPackage 
- Buscar a versión necesaria máis precisa de AppstreamQt
- Engadir compatibilidade con KNewStuff a KPackage

### KActivitiesStats

- Facer que compile con -fno-operator-names
- Non obter máis recursos ligados cando un deles marcha
- Engadiuse un rol de modelo para obter a lista de actividades ligadas cun recurso

### Ferramentas de Doxygen de KDE

- engadir Android á lista de plataformas dispoñíbeis
- incluír ficheiros fonte de ObjC++

### KConfig

- Xerar unha instancia con KSharedConfig::Ptr para o singleton e o argumento
- kconfig_compiler: Usar nullptr no código xerado

### KConfigWidgets

- Agochar a acción de «Mostrar a barra de menú» se todas as barras de menú son nativas
- KConfigDialogManager: retirar as clases de kdelibs3

### KCoreAddons

- Devolver tipo de lista de cadeas ou booleanos en KPluginMetaData::value
- DesktopFileParser: Respectar o campo ServiceTypes

### KDBusAddons

- Engadir unha API para Python a KDBusAddons

### KDeclarative

- Introducir a importación de QML org.kde.kconfig con KAuthorized

### KDocTools

- kdoctools_install: capturar a ruta completa do programa (fallo 374435)

### KGlobalAccel

- [runtime] Introducir a variábel de contorno KGLOBALACCEL_TEST_MODE

### Complementos de interface gráfica de usuario de KDE

- Engadir unha API para Python a KGuiAddons

### KHTML

- Definir os datos de complemento de xeito que o visualizador de imaxes integrábel funcione

### KIconThemes

- Tamén informar a QIconLoader cando se cambia o tema de iconas de escritorio (fallo 365363)

### KInit

- Ter en conta a propiedade X-KDE-RunOnDiscreteGpu ao iniciar unha aplicación usando KLauncher

### KIO

- Agora KIO::iconNameForUrl devolverá iconas especiais para lugares de xdg como o cartafol de imaxes do usuario
- ForwardingSlaveBase: corrixir o paso da marca Overwrite a kio_desktop (fallo 360487)
- Mellorar e exportar a clase KPasswdServerClient, a API de cliente para kpasswdserver
- [KPropertiesDialog] Retirar a opción «Colocar na área de notificacións»
- [KPropertiesDialog] Non cambiar o «Name» dos  ficheiro .desktop de «Link» se o nome de ficheiro é de só lectura
- [KFileWidget] Usar urlFromString en vez de QUrl(QString) en setSelection (fallo 369216)
- Engadir unha opción para executar unha aplicación nunha tarxeta gráfica aparte en KPropertiesDialog
- terminar DropJob de maneira axeitada tras pasar a un executábel
- Corrixir o uso de categorías de rexistro en Windows
- DropJob: emitir o traballo de copia iniciado tras a súa creación
- Engadir a posibilidade de chamar a suspend() en CopyJob antes de que comece
- Engadir a posibilidade de suspender traballos inmediatamente, polo menos nos casos de SimpleJob e FileCopyJob

### KItemModels

- Actualizar os proxys a raíz dunha clase de fallos dos que nos decatamos recentemente (xestión de layoutChanged)
- Permitir que KConcatenateRowsProxyModel funcione en QML
- KExtraColumnsProxyModel: persistir os índices de modelo tras emitir layoutChange, non antes
- Corrixir a aserción (en beginRemoveRows) ao retirar da selección un elemento subordinado doutro elemento subordinado seleccionado en korganizer

### KJobWidgets

- Non enfocar xanelas de progreso (fallo 333934)

### KNewStuff

- [Botón de GHNS] Agochar cando corresponda unha restrición de KIOSK
- Corrixir a preparación do ::Engine ao crealo cunha ruta absoluta de ficheiro de configuración

### KNotification

- Engadir unha proba manual para iniciadores de Unity
- [KNotificationRestrictions] Permitir ao usuario indicar unha cadea de motivo da restrición

### Infraestrutura KPackage

- [PackageLoader] Non acceder a un KPluginMetadata incorrecto (fallo 374541)
- corrixir a descrición da opción -t na páxina de manual
- Mellorar a mensaxe de erro
- Corrixir a mensaxe de axuda de --type
- Mellorar o proceso de instalación de paquetes de KPackage
- Instalar un ficheiro kpackage-generic.desktop
- Excluír os ficheiros qmlc da instalación
- Non listar de maneira separada plasmoides de metadata.desktop e .json
- Corrección adicional para paquetes con distintos tipos pero o mesmo identificador
- Corrixir un fallo de CMake cando dous paquetes con distinto tipo teñen o mesmo identificador

### KParts

- Chamar o novo checkAmbiguousShortcuts() desde MainWindow::createShellGUI

### KService

- KSycoca: non seguir as ligazóns simbólicas a directorios, aumenta o risco de recursividade

### KTextEditor

- Corrección: Arrastrar texto cara adiante causa unha selección incorrecta (fallo 374163)

### Infraestrutura de KWallet

- Indicar a versión 1.7.0 de GpgME++ como a mínima necesaria
- Reverter «Se non se atopa Gpgmepp, intentar usar KF5Gpgmepp»

### KWidgetsAddons

- Engadir API para Python
- Engadir KToolTipWidget, un consello que contén outro trebello
- Corrixir as comprobacións de KDateComboBox de datas escritas correctamente
- KMessageWidget: usar unha cor vermella máis escura cando o tipo é Error (fallo 357210)

### KXMLGUI

- Avisar no inicio de atallos ambiguos (coa excepción de Maiús+Supr)
- MSWin e Mac teñen un comportamento similar de garda automática de tamaño de xanela
- Mostrar a versión da aplicación na cabeceira do diálogo sobre ela (fallo 372367)

### Iconas de Oxygen

- sincronizar coas iconas de Breeze

### Infraestrutura de Plasma

- Corrixir as propiedades renderType de varios compoñentes
- [ToolTipDialog] Usar KWindowSystem::isPlatformX11(), que se garda en caché
- [Elemento de icona] Corrixir a actualización do tamaño implícito cando os tamaños das iconas cambian
- [Dialog] Usar setPosition e setSize en vez de definir todo por separado
- [Unidades] Facer constante a propiedade iconSizes
- Agora hai unha instancia de «Units» global, reducindo o consumo de memoria e o tempo de creación dos elementos SVG
- [Elemento de icona] Permitir iconas non cadradas (fallo 355592)
- buscar e substituír tipos vellos definidos a man de plasmapkg2 (fallo 374463)
- Corrixir os tipos X-Plasma-Drop* (fallo 374418)
- [ScrollViewStyle de Plasma] Só mostrar o fondo da barra de desprazamento ao cubrila
- Corrixir #374127 a colocación incorrecta de xanelas emerxentes de doca gaña
- Marcar a API de Plasma::Package en PluginLoader como obsoleta
- Comprobar de novo que representación deberíamos usar en setPreferredRepresentation
- [declarativeimports] Usar QtRendering en dispositivos móbiles
- considerar que nun panel baleiro os miniaplicativos sempre están cargados (fallo 373836)
- Emitir toolTipMainTextChanged se cambia en resposta a un cambio de título
- [TextField] Permitir desactivar o botón de mostrar o contrasinal mediante unha restrición de KIOSK
- [AppletQuickItem] Engadir unha mensaxe de erro de inicio
- Corrixir a lóxica de xestión de frechas nas configuracións rexionais RTL (fallo 373749)
- TextFieldStyle: Corrixir o valor de implicitHeight de xeito que o cursor de texto se centre

### Sonnet

- cmake: buscar hunspell-1.6 tamén

### Realce da sintaxe

- Corrixir o realce de Makefile.inc engadindo prioridade a makefile.xml
- Salientar os ficheiros SCXML como XML
- makefile.xml: moitas melloras, demasiadas para listalas aquí
- sintaxe de Python: engadíronse os literais f e mellorouse a xestión de cadeas

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
