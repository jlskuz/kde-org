---
aliases:
- ../announce-applications-17.08.0
changelog: true
date: 2017-08-17
description: KDE publica a versión 17.08.0 das aplicacións de KDE
layout: application
title: KDE publica a versión 17.08.0 das aplicacións de KDE
version: 17.08.0
---
17 de agosto de 2017. Chegou a versión 17.08 das aplicacións de KDE. Traballamos en estabilizar e facilitar o uso tanto das aplicacións como das bibliotecas que usan. Solucionando pequenos problemas e escoitando as vosas experiencias fixemos que a colección de aplicacións de KDE falle menos e sexa máis fácil de usar. Goce das novas aplicacións!

### Máis adaptación á versión 5 da infraestrutura de KDE

Alégranos anunciar que as seguintes aplicacións, que estaban baseadas en KDE Libs 4, están agora baseadas na versión 5 das infraestruturas de KDE: KMag, KMouseTool, KGoldRunner, Kigo, Konquest, KReversi, KSnakeDuel, KSpaceDuel, KSudoku, Kubrick, LSkat e Umbrello. Grazas aos desenvolvedores que traballaron voluntariamente para conseguilo.

### What's new in KDE Applications 17.08

#### Dolphin

Os desenvolvedores de Dolphin informan de que agora Dolphin mostra a «Data de eliminación» no lixo, e mostra a «Data de creación» se o sistema operativo o permite, como no caso de BSD.

#### KIO-Extras

Agora Kio-Extras funciona mellor con comparticións de Samba.

#### KAlgebra

Os desenvolvedores de KAlgebra traballaron en mellorar a súa interface de Kirigami no escritorio e incluíron a funcionalidade de completado de código.

#### Kontact

- Os desenvolvedores activaron de novo en KMailtransport a compatibilidade co transporte de Akonadi, os complementos creados e o transporte de correo de sendmail recreado.
- Corrixíronse e pecháronse moitos fallos dos scripts de creación automática de SieveEditor. Xunto coas correccións xerais de fallos engadiuse un editor de liña de expresións regulares.
- A funcionalidade de usar un editor externo en KMail creouse de novo en forma de complemento.
- Agora o asistente de importación de Akonadi ten o «convertedor de convertelo todo» como complemento, para que os desenvolvedores poidan crear novos convertedores facilmente.
- Agora as aplicacións dependen de Qt 5.7. Os desenvolvedores corrixiron unha morea de erros de compilación en Windows. Non todo KDE PIM compila en Windows aínda pero os desenvolvedores progresaron moito. Para empezar, os desenvolvedores crearon unha receita de Craft para KDE PIM. Fixéronse moitas correccións de fallos para modernizar código (C++11). Compatibilidade con Wayland en Qt 5.9. Engadiuse un recurso de Facebook a kdepim-runtime.

#### Kdenlive

En Kdenlive o equipo corrixiu o «efecto de conxelación», que estaba roto. En versións recentes non foi posíbel cambiar o fotograma conxelado para o efecto de conxelación. Agora permítese o atallo de teclado da funcionalidade de extraer un fotograma. Agora o usuario pode gardar capturas de pantalla da súa liña de tempo cun atallo de teclado, e agora suxírese un nome segundo o número de fotogramas <a href='https://bugs.kde.org/show_bug.cgi?id=381325'>https://bugs.kde.org/show_bug.cgi?id=381325</a>. Corrixir que a transición descargada non apareza na interface: <a href='https://bugs.kde.org/show_bug.cgi?id=382451'>https://bugs.kde.org/show_bug.cgi?id=382451</a>. Corrixir o problema dos clic de son (de momento, require construír a dependencia MLT a partir de Git ata que se publique unha nova versión de MLT): <a href='https://bugs.kde.org/show_bug.cgi?id=371849'>https://bugs.kde.org/show_bug.cgi?id=371849</a>.

#### Krfb

Os desenvolvedores remataron de migrar os complementos de X11 a Qt 5, e krfb volve funcionar usando unha infraestrutura de X11 que é moito máis rápida que o complemento de Qt. Hai unha nova páxina de configuración, permitindo ao usuario cambiar o complemento de búfer de fotogramas preferido.

#### Konsole

Agora Konsole permite un desprazamento ilimitado para estender alén do límite de 2 GB (32 bits). Agora Konsole permite aos usuarios escribir calquera lugar para almacenar os ficheiros de desprazamento. Ademais corrixiuse unha regresión, Konsole volve permitir a KonsolePart chamar ao diálogo de xestión de perfís.

#### KAppTemplate

In KAppTemplate there is now an option to install new templates from the filesystem. More templates have been removed from KAppTemplate and instead integrated into related products; ktexteditor plugin template and kpartsapp template (ported to Qt5/KF5 now) have become part of KDE Frameworks KTextEditor and KParts since 5.37.0. These changes should simplify creation of templates in KDE applications.

### Destrución de fallos

Solucionáronse máis de 80 fallos en aplicacións como, entre outros, a colección Kontact, Ark, Dolphin, K3b, Kdenlive, KGpg e Konsole!

### Historial completo de cambios
