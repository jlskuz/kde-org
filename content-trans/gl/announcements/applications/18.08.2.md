---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: KDE publica a versión 18.08.2 das aplicacións de KDE
layout: application
title: KDE publica a versión 18.08.2 das aplicacións de KDE
version: 18.08.2
---
October 11, 2018. Today KDE released the second stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis dunha ducia de correccións de erros inclúen melloras en, entre outros, Kontact, Dolphin, Gwenview, KCalc e Umbrello.

Entre as melloras están:

- Arrastrar un ficheiro en Dolphin xa non pode disparar accidentalmente un cambio de nome
- KCalc volve permitir tanto punto como coma ao escribir decimais
- Corrixiuse un erro visual na baralla de París para os xogos de cartas de KDE
