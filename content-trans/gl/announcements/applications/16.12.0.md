---
aliases:
- ../announce-applications-16.12.0
changelog: true
date: 2016-12-15
description: KDE publica a versión 16.12.0 das aplicacións de KDE
layout: application
title: KDE publica a versión 16.12.0 das aplicacións de KDE
version: 16.12.0
---
15 de decembro de 2016. KDE introduce a versión 16.12 das aplicacións de KDE con innumerábeis anovacións no que respecta a unha maior facilidade de acceso, a introdución de funcionalidades moi útiles e solucionar algúns pequenos problemas que achegan ás aplicacións de KDE un pouco máis a ofrecer a configuración perfecta para o seu dispositivo.

<a href='https://okular.kde.org/'>Okular</a>, <a href='https://konqueror.org/'>Konqueror</a>, <a href='https://www.kde.org/applications/utilities/kgpg/'>KGpg</a>, <a href='https://www.kde.org/applications/education/ktouch/'>KTouch</a>, <a href='https://www.kde.org/applications/education/kalzium/'>Kalzium</a> and more (<a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Release Notes</a>) have now been ported to KDE Frameworks 5. We look forward to your feedback and insight into the newest features introduced with this release.

In the continued effort to make applications easier to build standalone, we have split the kde-baseapps, kdepim and kdewebdev tarballs. You can find the newly created tarballs at <a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_we_have_split'>the Release Notes document</a>

Detívose o desenvolvemento dos seguintes paquetes: kdgantt2, gpgmepp e kuser. Isto axudaranos a centrarnos no resto do código.

### O editor de sons Kwave únese ás aplicacións de KDE!

{{<figure src="https://www.kde.org/images/screenshots/kwave.png" width="600px" >}}

<a href='http://kwave.sourceforge.net/'>Kwave</a> is a sound editor, it can record, play back, import and edit many sorts of audio files including multi channel files. Kwave includes some plugins to transform audio files in several ways and presents a graphical view with a complete zoom and scroll capability.

### O mundo como fondo de escritorio

{{<figure src="https://frinring.files.wordpress.com/2016/08/screenshot_20160804_171642.png" width="600px" >}}

Agora Marble inclúe tanto un fondo de escritorio e un trebello para Plasma que mostran a hora na parte superior dunha vista de satélite da Terra, con visualización en tempo real do ciclo de día e noite. Outrora estaban dispoñíbeis en Plasma 4; agora actualizáronse para funcionar con Plasma 5.

You can find more information on <a href='https://frinring.wordpress.com/2016/08/04/wip-plasma-world-map-wallpaper-world-clock-applet-powered-by-marble/'>Friedrich W. H. Kossebau's blog</a>.

### Emoticonas por todas partes!

{{<figure src="/announcements/applications/16.12.0/kcharselect1612.png" width="600px" >}}

Agora KCharSelect pode mostrar o bloque de emoticonas de Unicode (e outros bloques de símbolos de SMP).

Tamén recibiu un menú de marcadores para que poida marcar como favoritos os seus caracteres preferidos.

### As matemáticas son mellores con Julia

{{<figure src="https://2.bp.blogspot.com/-BzJNpF5SXZQ/V7skrKcQttI/AAAAAAAAAA8/7KD8g356FfAd9-ipPcWYi6QX5_nCQJFKgCLcB/s640/promo.png" width="600px" >}}

Cantor ten unha nova infraestrutura para Julia, o que permite aos seus usuarios usar as últimas innovacións en computación científica.

You can find more information on <a href='https://juliacantor.blogspot.com/2016/08/cantor-gets-support-of-julia-language.html'>Ivan Lakhtanov's blog</a>.

### Arquivado avanzado

{{<figure src="https://rthomsen6.files.wordpress.com/2016/11/blog-1612-comp-method.png" width="600px" >}}

Ark ten varias funcionalidades novas:

- Agora pódense cambiar de nome, copiar e mover ficheiros e cartafoles dentro de arquivos
- Agora pódense seleccionar algoritmos de compresión e cifrado ao crear arquivos
- Ark can now open AR files (e.g. Linux \*.a static libraries)

You can find more information on <a href='https://rthomsen6.wordpress.com/2016/11/26/new-features-in-ark-16-12/'>Ragnar Thomsen's blog</a>.

### E máis!

Agora Kopete é compatíbel coa autenticación SASL de X-OAUTH2 do protocolo jabber e corrixíronse algúns problemas co complemento de cifrado de OTR.

Kdenlive has a new Rotoscoping effect, support for downloadable content and an updated Motion Tracker. It also provides <a href='https://kdenlive.org/download/'>Snap and AppImage</a> files for easier installation.

KMail e Akregator poden usar a navegación segura de Google para comprobar se unha ligazón que se preme é maliciosa. Ambos os dous recuperaron ademais a funcionalidade de impresión (require Qt 5.8).

### Exterminación de erros

Solucionáronse máis de 130 fallos en aplicacións como, entre outras, Dolphin, Akonadi, KAddressBook, KNotes, Akregator, Cantor, Ark e Kdenlive!

### Historial completo de cambios
