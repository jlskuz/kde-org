---
aliases:
- ../announce-applications-16.08.2
changelog: true
date: 2016-10-13
description: KDE publica a versión 16.08.2 das aplicacións de KDE
layout: application
title: KDE publica a versión 16.08.2 das aplicacións de KDE
version: 16.08.2
---
October 13, 2016. Today KDE released the second stability update for <a href='../16.08.0'>KDE Applications 16.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 30 correccións de erros inclúen melloras en, entre outros, KDE PIM, Ark, Dolphin, KGPG, KolourPaint e Okular.

This release also includes Long Term Support version of KDE Development Platform 4.14.25.
