---
aliases:
- ../announce-applications-15.08-rc
date: 2015-08-06
description: KDE Ships Applications 15.08 Release Candidate.
layout: application
release: applications-15.07.90
title: KDE toob välja rakenduste 15.08 väljalaskekandidaadi
---
6. august 2015. KDE laskis täna välja rakenduste uute versioonide väljalaskekandidaadi. Kuna sõltuvused ja omadused on külmutatud, on KDE meeskonnad keskendunud vigade parandamisele ja tarkvara viimistlemisele.

Et paljud rakendused on juba viidud KDE Frameworks 5 peale, vajavad KDE rakendused 15.08 põhjalikku testimist kvaliteedi ja kasutajakogemuse tagamiseks ja parandamiseks. Kasutajatel on tihtipeale õigus suhtuda kriitiliselt KDE taotlusse hoida kõrget kvaliteeti, sest arendajad pole lihtsalt võimelised järele proovima kõiki võimalikke kombinatsioone. Me loodame oma kasutajate peale, kes oleksid suutelised varakult vigu üles leidma, et me võiksime need enne lõplikku väljalaset ära parandada. Niisiis - palun kaaluge mõtet ühineda meeskonnaga väljalaskekandidaati paigaldades <a href='https://bugs.kde.org/'>ja kõigist ette tulevatest vigadest teada andes</a>.
