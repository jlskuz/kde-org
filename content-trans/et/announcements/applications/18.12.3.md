---
aliases:
- ../announce-applications-18.12.3
changelog: true
date: 2019-03-07
description: KDE Ships Applications 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.3
title: KDE Ships KDE Applications 18.12.3
version: 18.12.3
---
{{% i18n_date %}}

Today KDE released the third stability update for <a href='../18.12.0'>KDE Applications 18.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Üle 20 teadaoleva veaparanduse hulka kuuluvad muu hulgas Kontacti, Arki, Cantori, Dolphini, Filelighti, JuKi, Lokalize ja Umbrello täiustused.

Täiustused sisaldavad muu hulgas:

- .tar.zstd arhiivide laadimine Arkis parandati ära
- Dolphinit ei taba enam krahh, kui Plasma tegevus peatatakse
- Filelighti ei taba enam krahh teisele partitsioonile lülitudes
